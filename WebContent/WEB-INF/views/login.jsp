<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:actionmessage />
<s:form action="/login" method="post">
    <s:textfield name="login.username" value="" label="Username" />
    <s:password name="login.password" value="" label="Password" />
    <s:textfield name="login.db" value="" label="Esquema" />
    
  	<s:submit value="Submit"/>
</s:form>