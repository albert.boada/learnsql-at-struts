package catalogo.auth;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import catalogo.gestiondatos.WrapperConexion;

public class LoginService 
{
	private Map<String,String> validation=new HashMap<String,String>();
	
	public void setValidation(Map<String,String> validation) {
		this.validation = validation;
	}
	public Map<String,String> getValidation() {
		return validation;
	}
	
	public void validate(Login login)
	{
		this.validation=new HashMap<String,String>();
		if(login==null)
		{
			this.validation.put("entity","null");
			return; 
		}
		
		//username
		if(login.getUsername()==null || login.getUsername().trim().isEmpty())
		{
			//System.out.println("login buit");
			this.validation.put("login.username", "empty");
			//System.out.println(this.validation);
		}
		
		//password
		if(login.getPassword()==null || login.getPassword().trim().isEmpty())
		{
			this.validation.put("login.password", "empty");
		}
		
		//db
		String db=login.getDb();
		if(db==null || db.isEmpty())
		{
			this.validation.put("login.db", "empty");
		}
		/*else if(!db.equals("bd") && !db.equals("dabd"))
		{
			this.validation.put("login.db", "invalid");			
		}*/
		
		return;
	}
	
	public boolean doLogin(Login login)
	{
		//validate input
		this.validate(login);
		if(!this.validation.isEmpty())
		{
			return false;
		}
			
		ResourceBundle dbconfig=ResourceBundle.getBundle("conexion");
		//System.out.println(dbconfig);
		if(dbconfig==null)
		{
			return false;
		}
		
		boolean ssl=false;
		if (dbconfig.getString("ssl").trim().equalsIgnoreCase("true")) ssl = true;
		
		//Nova connexió a la BD amb les dades que hem llegit del .properties, i les dades de l'usuari que són a la Session
		WrapperConexion wrapperConexion=new WrapperConexion(
			dbconfig.getString("sgbd").trim() /*"postgresql"*/, 
			dbconfig.getString("host").trim() /*"localhost"*/,
			dbconfig.getString("port").trim() /*"5432"*/, 
			dbconfig.getString("server").trim() /*"nothing"*/,
			login.getDb().trim() /*"learnsql9"*/, 
			login.getUsername() /*"cquer"*/,
			login.getPassword() /*"1234"*/, 
			ssl /*false*/
		);
		
		try
		{
			wrapperConexion.conectar();
		}
		catch(Exception e)
		{
			
		}
		
		Connection con=wrapperConexion.getConexion();
		System.out.println(con);
		
		if(con==null) return false;
		else return true;
	}
}
