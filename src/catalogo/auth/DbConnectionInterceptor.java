package catalogo.auth;

import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import catalogo.actions.ModelAware;
import catalogo.gestiondatos.WrapperConexion;
import catalogo.modelo.DataModel;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class DbConnectionInterceptor implements Interceptor
{
	private static final long serialVersionUID = 8496267219147430670L;

	@Override
	public void destroy() {}

	@Override
	public void init() {}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		boolean allow = false;

		ActionContext context = invocation.getInvocationContext();
		HttpServletRequest request = ServletActionContext.getRequest();

		Login login = null;

		if (!allow) {
			// 1) Read Session
			Map<String,Object> session = context.getSession();

			Object loginobject = session.get("login");
			if (loginobject == null || !(loginobject instanceof Login)) {
				session.put("loginRedirect", request.getRequestURL().toString());
				return "unauthorized";
			}
			login = (Login)loginobject;
		}
		else {
			login = new Login();
			login.setDb("BDdesenvolupament");
			//login.setUsername("lonmnuaotpxvkw");
			//login.setPassword("Cu9ajKfjiXmCcQ55OKP939myK2");
			login.setUsername("albert");
			login.setPassword("ab78563421");
			//login.setUsername("usuari.desenvolupador");
			//login.setPassword("f4M1JWGt");
		}
		
		ResourceBundle dbconfig = ResourceBundle.getBundle("conexion");
		if (dbconfig == null) {
			return "connerror";
		}

		boolean ssl = false;
		if (dbconfig.getString("ssl").trim().equalsIgnoreCase("true")) { ssl = true; }

		// 3) Nova connexió a la BD amb les dades que hem llegit del .properties, i les dades de l'usuari que són a la Session
		WrapperConexion wrapperConexion = new WrapperConexion(
			dbconfig.getString("sgbd").trim() /*"postgresql"*/,
			dbconfig.getString("host").trim() /*"localhost"*/,
			dbconfig.getString("port").trim() /*"5432"*/,
			dbconfig.getString("server").trim() /*"nothing"*/,
			login.getDb().trim() /*"learnsql9"*/,
			login.getUsername() /*"cquer"*/,
			login.getPassword() /*"1234"*/,
			ssl /*false*/
		);

		try {
			wrapperConexion.conectar();
		}
		catch(Exception e) {
			return "unauthorized";
		}

		//Connection con = wrapperConexion.getConexion();

	    Action action = (Action)invocation.getAction();

		// 4) Creem i injecte el Datalayer a l'Acció
	    if (action instanceof ModelAware) {
	    	this.__initAndSetDataModelToAction(wrapperConexion, action);
	    }

	    // 5) Executem l'Acció
	    String result = invocation.invoke();

	    // 6) Tanquem la connexió (tota la lògica de l'Acció ja s'ha executat i som l'últim interceptor en post-processat)
	    wrapperConexion.getConexion().close();

	    return result;
	}

	/**
	 *
	 */
	private void __initAndSetDataModelToAction(WrapperConexion wrapperConexion, Action action) throws Exception {
		((ModelAware)action).initModel(new DataModel(wrapperConexion));
	}

}
