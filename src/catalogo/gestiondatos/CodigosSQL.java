//////////////////////////////////////////////////////
//  CLASSE: CodigosSQL.java                         //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

public class CodigosSQL {
    public final static String PRIMARY_KEY          = "23505";
    public final static String FOREIGN_KEY          = "23503";
    public final static String UNTRANSLATABLE_CHAR  = "22P05";
}
