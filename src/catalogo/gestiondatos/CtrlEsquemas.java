//////////////////////////////////////////////////////
//  CLASSE: EsquemaDao.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import catalogo.modelo.Esquema;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CtrlEsquemas {
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private PreparedStatement psSelectOne, psInsert, psDelete, psUpdate1, psUpdate2, psDeferredFK, psImmediateFK;
    private Connection con;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public CtrlEsquemas(Connection c) throws Exception {
        con = c;
        try {
            psSelectOne = con.prepareStatement("SELECT * FROM t_esquemes WHERE nom = ? LIMIT 1");
            psInsert = con.prepareStatement("INSERT INTO t_esquemes(nom, descripcio) VALUES(?, ?)");
            psDelete = con.prepareStatement("DELETE FROM t_esquemes WHERE nom = ?");
            psUpdate1 = con.prepareStatement("UPDATE t_esquemes SET nom = ?, descripcio = ? WHERE nom = ?");
            psUpdate2 = con.prepareStatement("UPDATE t_questions SET esquema = ? WHERE esquema = ?");
            psDeferredFK = con.prepareStatement("SET CONSTRAINTS t_questions_esquema_fkey DEFERRED");
            psImmediateFK = con.prepareStatement("SET CONSTRAINTS t_questions_esquema_fkey IMMEDIATE");
        } catch (SQLException e) {
            throw e;
        }
    }

    public Esquema get(String nom) throws Exception {
        ResultSet rs = null;
        Esquema sch = null;

        try {
            this.psSelectOne.setString(1, nom);
            rs = psSelectOne.executeQuery();
            while (rs.next()) {
                sch = new Esquema(
                    rs.getString("nom"),
                    rs.getString("descripcio")
                );
            }
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
        finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }

        return sch;
    }

    public List<Esquema> select() throws Exception {
        Statement s = null;
        ResultSet rs = null;
        List<Esquema> esquemas = null;

        try {
            s = con.createStatement();
            rs = s.executeQuery("SELECT nom, descripcio FROM t_esquemes");
            esquemas = new ArrayList<Esquema>();

            while (rs.next()) {
                esquemas.add(new Esquema(rs.getString("nom"), rs.getString("descripcio")));
            }
            Collections.sort(esquemas);
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
            try { if (s != null) s.close(); } catch (SQLException e) {}
        }
        return esquemas;
    }

    // Métodos para gestionar esquemas ****************************************

    public void insert(String nombre, String descripcion) throws Exception {
        try {
            psInsert.setString(1, nombre);
            psInsert.setString(2, descripcion);
            psInsert.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY)) throw new Exception("Ya existe un esquema con ese nombre");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR)) throw new Exception("Existen caracteres no admitidos por la base de datos");
            else throw e;
            */
        }
    }

    public void update(String nombreOld, String nombreNew, String descripcion) throws Exception {
        try {
            psDeferredFK.executeUpdate();
            psUpdate1.setString(1, nombreNew);
            psUpdate1.setString(2, descripcion);
            psUpdate1.setString(3, nombreOld);
            psUpdate1.executeUpdate();
            psUpdate2.setString(1, nombreNew);
            psUpdate2.setString(2, nombreOld);
            psUpdate2.executeUpdate();
            psImmediateFK.executeUpdate();
            con.commit();
        } catch (SQLException e) {
        	try { con.rollback(); } catch (SQLException e2) {}
        	throw e;
            /*
        	if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY)) throw new Exception("Ya existe un esquema con ese nombre");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR)) throw new Exception("Existen caracteres no admitidos por la base de datos");
            else throw e;
            */
        }
    }

    public void delete(String nombre) throws Exception {
        try {
            psDelete.setString(1, nombre);
            psDelete.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*
            if (e.getSQLState().equals(CodigosSQL.FOREIGN_KEY)) throw new Exception("Existen cuestiones asociadas al esquema");
            else throw e;
            */
        }
    }

} // Fin clase EsquemaDao
