//////////////////////////////////////////////////////
//  CLASSE: TematicaDao.java                      //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import catalogo.modelo.Categoria;
import catalogo.modelo.Tematica;

public class CtrlTematicas {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private PreparedStatement psSelect, psSelect_, psSelect2, psSelect3;
    private PreparedStatement psInsert, psInsert2;
    private PreparedStatement psDelete, psDelete2;
    private PreparedStatement psUpdate, psUpdate2;
    private PreparedStatement psDeferredFK, psDeferredFK2, psImmediateFK, psImmediateFK2;
    private Connection con;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public CtrlTematicas(Connection c) throws Exception {
        con = c;
        try {
            psSelect = con.prepareStatement("SELECT * FROM t_tematiques ORDER BY nom");
            psSelect_ = con.prepareStatement("SELECT * FROM t_tematiques WHERE nom = ?");
            psSelect2 = con.prepareStatement("SELECT id FROM t_categoriestematiques WHERE nom = ?");
            psSelect3 = con.prepareStatement("SELECT id, descripcio, inits, postinits, neteja, estat," +
                    "solucio, initsjp, entradajp, netejajp, comprovacions FROM t_categories WHERE id = ? ORDER BY id");
            psInsert = con.prepareStatement("INSERT INTO t_tematiques(nom, descripcio) VALUES(?, ?)");
            psInsert2 = con.prepareStatement("INSERT INTO t_categoriestematiques(id, nom) VALUES(?, ?)");
            psDelete = con.prepareStatement("DELETE FROM t_tematiques WHERE nom = ?");
            psDelete2 = con.prepareStatement("DELETE FROM t_categoriestematiques WHERE nom = ?");
            psUpdate = con.prepareStatement("UPDATE t_tematiques SET nom = ?, descripcio = ? WHERE nom = ?");
            psUpdate2 = con.prepareStatement("UPDATE t_tematiquesquestions SET nom = ? WHERE nom = ?");
            psDeferredFK = con.prepareStatement("SET CONSTRAINTS t_tematiquesquestions_nom_fkey DEFERRED");
            psDeferredFK2 = con.prepareStatement("SET CONSTRAINTS t_categoriestematiques_nom_fkey DEFERRED");
            psImmediateFK = con.prepareStatement("SET CONSTRAINTS t_tematiquesquestions_nom_fkey IMMEDIATE");
            psImmediateFK2 = con.prepareStatement("SET CONSTRAINTS t_categoriestematiques_nom_fkey IMMEDIATE");
        } catch (SQLException e) {
            throw e;
        }
    }

    public List<Tematica> select() throws Exception {
        ResultSet rs = null;
        List<Tematica> tematicas = null;
        List<Categoria> categorias = null;

        try {
			tematicas = new ArrayList<Tematica>();
            categorias = new ArrayList<Categoria>();

			// select * from tematiques
            rs=psSelect.executeQuery();
            while (rs.next())
			{
                String nom=rs.getString("nom");

				// seleccionem categories relacionades amb la temàtica
                categorias=selectCategorias(nom); //pfc2

                tematicas.add(new Tematica(nom, rs.getString("descripcio"), categorias));
            }
            //Collections.sort(tematicas);
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) { throw e2;}
            throw e;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }
        return tematicas;
    }

    /*
    public List<Tematica> getByCuestion(cuestion_id) {

    }
    */

    public Tematica get(String nombre) throws Exception {
        List<String> nombres = new ArrayList<String>();
        nombres.add(nombre);
        List<Tematica> tematicas = this.getByNombres(nombres);
        if (tematicas.isEmpty()) {
            return null;
        } else {
            return tematicas.get(0);
        }
    }

    public List<Tematica> getByNombres(List<String> nombres) throws Exception {
    	List<Tematica> tematicas = new ArrayList<Tematica>();
        
    	if (nombres.isEmpty()) {
        	return tematicas;
        }
    	
    	ResultSet rs = null;
        Statement s = null;
        List<Categoria> categorias = new ArrayList<Categoria>();

        String sql = "SELECT * FROM t_tematiques";
        String where = "";
        for (String nombre : nombres) {
            where += " OR nom = '"+nombre+"'";
        }
        sql += " WHERE "+where.substring(4)+" ORDER BY nom";

        try {
            s = con.createStatement();
            rs = s.executeQuery(sql);
			while (rs.next()) {
 				// seleccionem categories relacionades amb la temàtica
                categorias=selectCategorias(rs.getString("nom")); //pfc2

                tematicas.add(new Tematica(rs.getString("nom"), rs.getString("descripcio"), categorias));
            }
            //Collections.sort(tematicas);
            con.commit();

            return tematicas;
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) { throw e2;}
            throw e;
        }
        finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }

    }


    public List<Categoria> selectCategorias(String nombre) throws Exception {
        ResultSet rs = null;
        ResultSet rs2 = null;
        List<Categoria> categorias = null;

        try {

            categorias = new ArrayList<Categoria>();

			// select totes les categories de la temàtica "nombre"
			psSelect2.setString(1, nombre);
            rs=psSelect2.executeQuery();
            while (rs.next())
			{
                psSelect3.setInt(1, rs.getInt("id"));
                rs2 = psSelect3.executeQuery();
                while (rs2.next()){
                    categorias.add(new Categoria(Integer.parseInt(rs2.getString("id")), rs2.getString("descripcio"),
                        rs2.getBoolean("inits"),rs2.getBoolean("postinits"),rs2.getBoolean("neteja"),
                        rs2.getBoolean("estat"),rs2.getBoolean("solucio"),rs2.getBoolean("initsjp"),
                        rs2.getBoolean("entradajp"),rs2.getBoolean("netejajp"),rs2.getBoolean("comprovacions")));
                }
            }
            //Collections.sort(categorias);
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) { throw e2;}
            throw e;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }
        return categorias;
    }

    /*********************************************************/
    /*      Métodos para gestionar temáticas                 */
    /*********************************************************/

    public void insert(String nombre, String descripcion, List<Categoria> categorias) throws Exception {
        try {
            // Inserir en la tabla t_tematiques
            psInsert.setString(1, nombre);
            psInsert.setString(2, descripcion);
            psInsert.executeUpdate();
            // Inserir en la tabla t_categoriestematiques
            insertCategorias(categorias,nombre);

            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY))
                throw new Exception("Ya existe una temática con ese nombre");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR))
                throw new Exception("Existen caracteres no admitidos por la base de datos");
            else throw e;
            */
        }
    }

    public void update(String nombreOld, String nombreNew, String descripcion, List<Categoria> categorias) throws Exception {
        try {
            psDeferredFK.executeUpdate();
            psDeferredFK2.executeUpdate();

            // Modificar en la tabla t_tematiques
            psUpdate.setString(1, nombreNew);
            psUpdate.setString(2, descripcion);
            psUpdate.setString(3, nombreOld);
            psUpdate.executeUpdate();
            // Modificar en la tabla t_tematiquesquestions
            psUpdate2.setString(1, nombreNew);
            psUpdate2.setString(2, nombreOld);
            psUpdate2.executeUpdate();
            // Modificar en la tabla t_categoriestematiques
            psDelete2.setString(1, nombreOld);
            psDelete2.executeUpdate();
            insertCategorias(categorias,nombreNew);

            psImmediateFK.executeUpdate();
            psImmediateFK2.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY))
                throw new Exception("Ya existe una temática con ese nombre");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR))
                throw new Exception("Existen caracteres no admitidos por la base de datos");
            else throw e;
            */
        }
    }

    public void delete(String nombre) throws Exception {
        try {
            // Eliminar en la tabla t_categoriestematiques
            psDelete2.setString(1, nombre);
            psDelete2.executeUpdate();
            // Eliminar en la tabla t_tematiques
            psDelete.setString(1, nombre);
            psDelete.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*
            if (e.getSQLState().equals(CodigosSQL.FOREIGN_KEY))
                throw new Exception("Existen cuestiones asociadas a la tem\u00e1tica");
            else throw e;
            */
        }
    }

    /*********************************************************/
    /*      Métodos auxiliares privados                      */
    /*********************************************************/

    private void insertCategorias(List<Categoria> lista, String nombre) throws SQLException {
        for(Categoria cat : lista){
            psInsert2.setInt(1, cat.getId());
            psInsert2.setString(2, nombre);
            psInsert2.executeUpdate();
        }
    }

} // Fin clase TematicaDao
