//////////////////////////////////////////////////////
//  CLASSE: WrapperConexion.java                    //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import java.sql.*;
import java.util.Properties;

public class WrapperConexion {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/
    
    public final static String SGBD_ORACLE      = "oracle";
    public final static String SGBD_INFORMIX    = "informix";
    public final static String SGBD_POSTGRESQL  = "postgresql";
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    
    private String sgbd;
    private String host;
    private String servidor;
    private String puerto;
    private String nombre;
    private String usuario;
    private String clave;
    private boolean ssl;
    private Connection conexion;
    
    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/
    
    public WrapperConexion(String sgbd, String host, String puerto, String servidor,
            String nombre, String usuario, String clave, boolean ssl) {
        this.sgbd = sgbd.toLowerCase().trim();
        this.host = host.trim();
        this.servidor = servidor.trim();
        this.puerto = puerto.trim();
        this.nombre = nombre.trim();
        this.usuario = usuario.trim();
        this.clave = clave;
        this.ssl = ssl;
        this.conexion = null;
    }

    /*********************************************************/
    /*      Método para conectar a la BD                     */
    /*********************************************************/
    
    public void conectar() throws Exception {
        try {
            if (sgbd.equalsIgnoreCase(SGBD_ORACLE)) {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                conexion = DriverManager.getConnection("jdbc:oracle:thin:@" + host + ":" + puerto + ":" + nombre, usuario, clave);
            }
            else if (sgbd.equalsIgnoreCase(SGBD_INFORMIX)) {
                Class.forName("com.informix.jdbc.IfxDriver");
                conexion = DriverManager.getConnection("jdbc:informix-sqli://" + host + ":" + puerto + "/" + nombre + ":"+"INFORMIXSERVER=" + servidor + ";" + "user=" + usuario + ";password=" + clave);
            }
            else if (sgbd.equalsIgnoreCase(SGBD_POSTGRESQL)) {
                Class.forName("org.postgresql.Driver");
                String url = "jdbc:postgresql://" + host + ":" + puerto + "/" + nombre;
                Properties props = new Properties();
                props.setProperty("user", usuario);
                props.setProperty("password", clave);
                if (ssl) {
                    props.setProperty("ssl", "true");
                    props.setProperty("sslfactory", "org.postgresql.ssl.NonValidatingFactory");
                }
                conexion = DriverManager.getConnection(url, props);
            }
            else throw new Exception("No se ha contemplado el SGDB '" + sgbd + "'");
            
            conexion.setAutoCommit(false);
        }
        catch (ClassNotFoundException e)
        {
            throw new Exception("No se ha encontrado el driver: " + e.getMessage());
        }
        catch (SQLException e)
        {
            String s;
            if (sgbd.equalsIgnoreCase(SGBD_ORACLE))  s = usuario;
            else s = nombre;
            throw new Exception("No se ha podido conectar con '" + s + "': " + e.getMessage());
        }
        catch (Exception e)
        {
            throw new Exception("Error de conexión: " + e.getMessage());
        }
    }

    /*********************************************************/
    /*      Métodos Get/Set                                  */
    /*********************************************************/
    
    public Connection getConexion() {
        return conexion;
    }
    
    public String getHost() {
        return host;
    }
    
    public String getNombre() {
        return nombre;
    }

    public String getUsuario() {
        return usuario;
    }
    

    /*********************************************************/
    /*      Otros metodos                                    */
    /*********************************************************/
    
    public void setTxReadCommitted() throws Exception {
        try {
            conexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        } catch (SQLException e) {
            throw new Exception ("Error estableciendo nivel de aislamiento a READ COMMITED: " + e.getMessage());
        }
    }
    
} // Fin clase WrapperConexion

