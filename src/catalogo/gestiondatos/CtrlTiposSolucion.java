//////////////////////////////////////////////////////
//  CLASSE: TiposolucionDao.java                  //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import catalogo.modelo.TipoSolucion;
import catalogo.modelo.TipossolucionService;

import java.util.ArrayList;

public class CtrlTiposSolucion {

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    private ArrayList<TipoSolucion> lista;

    public CtrlTiposSolucion()
    {
        lista = new ArrayList<TipoSolucion>();
        lista.add(new TipoSolucion(TipossolucionService.ID_TEXTO, "Texto")); // @revisit WHY the fuck are the CONSTANTS in the SERVICE?
        lista.add(new TipoSolucion(TipossolucionService.ID_FICHERO, "Fichero adjunto"));
        lista.add(new TipoSolucion(TipossolucionService.ID_URL, "URL + usuario + clave"));
    }

    public ArrayList<TipoSolucion> select() {
        return lista;
    }

    public TipoSolucion get(int id) {
        for (TipoSolucion sol : this.lista) if (sol.getId() == id) return sol;
        return null;
    }

} // Fin clase TiposolucionDao
