//////////////////////////////////////////////////////
//  CLASSE: ComprobacionDao.java                   //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import catalogo.modelo.Comprobacion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CtrlComprobacion {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private Connection con;
    private PreparedStatement psInsert, psUpdate, psDelete;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public CtrlComprobacion(Connection con) throws Exception {
        this.con = con;

        try {
            psInsert = con.prepareStatement(
                    "INSERT INTO t_comprovacions(id, nomjp, nomcompr, ordre, descripcio, msgerror, incompr, binaria," +
                    "consumeixintent, msgerroresp, msgerroring) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            psUpdate = con.prepareStatement(
                    "UPDATE t_comprovacions SET nomcompr = ?, descripcio = ?, msgerror = ?, incompr = ?, binaria = ?," +
                    "consumeixintent = ?, msgerroresp = ?, msgerroring = ? WHERE id = ? AND nomjp = ? AND nomcompr = ?");
            psDelete = con.prepareStatement("DELETE FROM t_comprovacions WHERE id = ? AND nomjp = ? AND nomcompr = ?");
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void insert(Comprobacion compr, int id) throws Exception {
        try {
            psInsert.setInt(1, id);
            psInsert.setString(2, compr.getJuegoPruebas().getNombre());
            psInsert.setString(3, compr.getNombre());
            psInsert.setInt(4, compr.getOrden());
            psInsert.setString(5, compr.getDescripcion());
            psInsert.setString(6, compr.getMsgError());
            if (compr.getEntrada() == null || compr.getEntrada().isEmpty()) psInsert.setNull(7, java.sql.Types.BINARY);
            else psInsert.setBytes(7, compr.getEntrada().getBytes());
            
            if (compr.getBinaria() == null) psInsert.setNull(8, java.sql.Types.BOOLEAN);
            else psInsert.setBoolean(8, compr.getBinaria());

            if (compr.getConsumeix() == null) psInsert.setNull(9, java.sql.Types.BOOLEAN);
            else psInsert.setBoolean(9, compr.getConsumeix());

            if (compr.getMsgErrorIng() == null || compr.getMsgErrorIng().isEmpty()) psInsert.setNull(10, java.sql.Types.CHAR);
            else psInsert.setString(10, compr.getMsgErrorIng());

            if (compr.getMsgErrorIng() == null || compr.getMsgErrorIng().isEmpty()) psInsert.setNull(11, java.sql.Types.CHAR);
            else psInsert.setString(11, compr.getMsgErrorIng());

            psInsert.executeUpdate();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw new SQLException(compr.getNombre()+":"+e.getMessage());
            /*
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY))
                throw new Exception("No se puede guardar la comprobación \"" + compr.getNombre() + "\" del juego de pruebas \"" +
                        compr.getJuegoPruebas().getNombre() + "\":\nEl juego de pruebas ya dispone de una comprobación con el mismo nombre");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR))
                throw new Exception("No se puede guardar la comprobación \"" + compr.getNombre() + "\" del juego de pruebas \"" +
                        compr.getJuegoPruebas().getNombre() + "\":\nExisten datos de la comprobación con caracteres no admitidos por la base de datos");
            else throw new Exception("No se puede guardar la comprobación \"" + compr.getNombre() + "\" del juego de pruebas \"" +
                        compr.getJuegoPruebas().getNombre() + "\":\n" + e.getMessage());
            */
        }
    }

    public void update(Comprobacion compr, String nombre) throws Exception {
        try {
            // atributos a actualizar
            psUpdate.setString(1, compr.getNombre());
            psUpdate.setString(2, compr.getDescripcion());
            psUpdate.setString(3, compr.getMsgError());
            psUpdate.setBytes(4, compr.getEntrada().getBytes());
            
            if (compr.getBinaria() == null) psUpdate.setNull(5, java.sql.Types.BOOLEAN);
            else psUpdate.setBoolean(5, compr.getBinaria());

            if (compr.getConsumeix() == null) psUpdate.setNull(6, java.sql.Types.BOOLEAN);
            else psUpdate.setBoolean(6, compr.getConsumeix());

            if (compr.getMsgErrorEsp() == null || compr.getMsgErrorEsp().isEmpty()) psUpdate.setNull(7, java.sql.Types.CHAR);
            else psUpdate.setString(7, compr.getMsgErrorEsp());

            if (compr.getMsgErrorIng() == null || compr.getMsgErrorIng().isEmpty()) psUpdate.setNull(8, java.sql.Types.CHAR);
            else psUpdate.setString(8, compr.getMsgErrorIng());

            // condiciones clausula WHERE
            psUpdate.setInt(9, compr.getJuegoPruebas().getCuestion().getId());
            psUpdate.setString(10, compr.getJuegoPruebas().getNombre());
            psUpdate.setString(11, nombre);

            psUpdate.executeUpdate();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw new SQLException(nombre+":"+e.getMessage());
            /*
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY))
                throw new Exception("No se puede guardar la comprobación \"" + compr.getNombre() + "\" del juego de pruebas \"" +
                        compr.getJuegoPruebas().getNombre() + "\":\nEl juego de pruebas ya dispone de una comprobación con el mismo nombre");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR))
                throw new Exception("No se puede guardar la comprobación \"" + compr.getNombre() + "\" del juego de pruebas \"" +
                        compr.getJuegoPruebas().getNombre() + "\":\nExisten datos de la comprobación con caracteres no admitidos por la base de datos");
            else throw new Exception("No se puede guardar la comprobación \"" + compr.getNombre() + "\" del juego de pruebas \"" +
                        compr.getJuegoPruebas().getNombre() + "\":\n" + e.getMessage());
            */
        }
    }

    public void delete(Comprobacion compr, String nombre) throws Exception {
        try {
            psDelete.setInt(1, compr.getJuegoPruebas().getCuestion().getId());
            psDelete.setString(2, compr.getJuegoPruebas().getNombre());
            psDelete.setString(3, nombre);
            psDelete.executeUpdate();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

} // fin clase ComprobacionDao
