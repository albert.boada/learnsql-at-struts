//////////////////////////////////////////////////////
//  CLASSE: JuegopruebasDao.java                   //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import catalogo.modelo.Comprobacion;
import catalogo.modelo.JuegoPruebas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CtrlJuegoPruebas {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private Connection con;
    private PreparedStatement psInsert, psUpdate, psDelete;
    private PreparedStatement psDeferredFK, psImmediateFK;
    private PreparedStatement psDeleteComprs, psUpdateNombre;
    private PreparedStatement psUpdateOrden, psUpdateOrdenesProv;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public CtrlJuegoPruebas(Connection con) throws Exception    {
        this.con = con;

        try {
            psDeferredFK = con.prepareStatement("SET CONSTRAINTS t_comprovacions_id_nomjp_fkey DEFERRED");
            psImmediateFK = con.prepareStatement("SET CONSTRAINTS t_comprovacions_id_nomjp_fkey IMMEDIATE");
            psInsert = con.prepareStatement("INSERT INTO t_jocsproves(id, ordre, nomjp, descripcio, msgerror, " +
                    "injp, inits, neteja, pes, binaria, missatge, consumeixintent, msgerroresp, msgerroring) " +
                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            psUpdate = con.prepareStatement("UPDATE t_jocsproves SET nomjp = ?, descripcio = ?, msgerror = ?, " +
                    "injp = ?, inits = ?, neteja = ?, pes = ?, binaria = ?, missatge = ?, consumeixintent = ?," +
                    "msgerroresp = ?, msgerroring = ? WHERE id = ? AND nomjp = ?");
            psDelete = con.prepareStatement("DELETE FROM t_jocsproves WHERE id = ? AND nomjp = ?");
            psDeleteComprs = con.prepareStatement("DELETE FROM t_comprovacions WHERE id = ? AND nomjp = ?");
            psUpdateNombre = con.prepareStatement("UPDATE t_comprovacions set nomjp = ? WHERE id = ? AND nomjp = ?");
            psUpdateOrden = con.prepareStatement("UPDATE t_comprovacions SET ordre = ? WHERE id = ? AND nomjp = ? AND nomcompr = ?");
            psUpdateOrdenesProv = con.prepareStatement("UPDATE t_comprovacions SET ordre = (-1 * ordre) WHERE id = ? AND nomjp = ?");
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void insert(JuegoPruebas jp, int id) throws Exception {
        try {
            psInsert.setInt(1, id);
            psInsert.setInt(2, jp.getOrden());
            psInsert.setString(3, jp.getNombre());
            psInsert.setString(4, jp.getDescripcion());
            psInsert.setString(5, jp.getMsgError());

            if (jp.getEntrada() == null || jp.getEntrada().isEmpty()) psInsert.setNull(6, java.sql.Types.BINARY);
            else psInsert.setBytes(6, jp.getEntrada().getBytes());
            if (jp.getInits() == null || jp.getInits().isEmpty()) psInsert.setNull(7, java.sql.Types.BINARY);
            else psInsert.setBytes(7, jp.getInits().getBytes());
            if (jp.getLimpieza() == null || jp.getLimpieza().isEmpty()) psInsert.setNull(8, java.sql.Types.BINARY);
            else psInsert.setBytes(8, jp.getLimpieza().getBytes());
            
            psInsert.setFloat(9, jp.getPeso());

            if (jp.getBinaria() == null) psInsert.setNull(10, java.sql.Types.BOOLEAN);
            else psInsert.setBoolean(10, jp.getBinaria());

            if (jp.getMensaje() == null) psInsert.setNull(11, java.sql.Types.BOOLEAN);
            else psInsert.setBoolean(11, jp.getMensaje());

            if (jp.getConsumeix() == null) psInsert.setNull(12, java.sql.Types.BOOLEAN);
            else psInsert.setBoolean(12, jp.getConsumeix());

            if (jp.getMsgErrorEsp() == null || jp.getMsgErrorEsp().isEmpty()) psInsert.setNull(13, java.sql.Types.CHAR);
            else psInsert.setString(13, jp.getMsgErrorEsp());

            if (jp.getMsgErrorIng() == null || jp.getMsgErrorIng().isEmpty()) psInsert.setNull(14, java.sql.Types.CHAR);
            else psInsert.setString(14, jp.getMsgErrorIng());

            psInsert.executeUpdate();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw new SQLException(jp.getNombre()+":"+e.getMessage());
            /*if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY))
                throw new Exception("No se puede guardar el juego de pruebas \"" + jp.getNombre() + "\":\n" +
                        "La cuestión ya dispone de un juego de pruebas con el mismo nombre");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR))
                throw new Exception("No se puede guardar el juego de pruebas \"" + jp.getNombre() + "\":\n" +
                        "Existen datos del juego de pruebas con caracteres no admitidos por la base de datos");
            else throw new Exception("No se puede guardar el juego de pruebas \"" + jp.getNombre() + "\":\n" + e.getMessage());
            */
        }
    }

    public void update(JuegoPruebas jp, String nombre) throws Exception {
        try {
            psDeferredFK.executeUpdate();
            psUpdateNombre.setString(1, jp.getNombre());
            psUpdateNombre.setInt(2, jp.getCuestion().getId());
            psUpdateNombre.setString(3, nombre);
            psUpdateNombre.executeUpdate();

            // atributos a actualizar
            psUpdate.setString(1, jp.getNombre());
            psUpdate.setString(2, jp.getDescripcion());
            psUpdate.setString(3, jp.getMsgError());

            if (jp.getEntrada() == null || jp.getEntrada().isEmpty()) psUpdate.setNull(4, java.sql.Types.BINARY);
            else psUpdate.setBytes(4, jp.getEntrada().getBytes());
            if (jp.getInits() == null || jp.getInits().isEmpty()) psUpdate.setNull(5, java.sql.Types.BINARY);
            else psUpdate.setBytes(5, jp.getInits().getBytes());
            if (jp.getLimpieza() == null || jp.getLimpieza().isEmpty()) psUpdate.setNull(6, java.sql.Types.BINARY);
            else psUpdate.setBytes(6, jp.getLimpieza().getBytes());
            
            psUpdate.setFloat(7, jp.getPeso());

            if (jp.getBinaria() == null) psUpdate.setNull(8, java.sql.Types.BOOLEAN);
            else psUpdate.setBoolean(8, jp.getBinaria());

            if (jp.getMensaje() == null) psUpdate.setNull(9, java.sql.Types.BOOLEAN);
            else psUpdate.setBoolean(9, jp.getMensaje());

            if (jp.getConsumeix() == null) psUpdate.setNull(10, java.sql.Types.BOOLEAN);
            else psUpdate.setBoolean(10, jp.getConsumeix());

            if (jp.getMsgErrorEsp() == null || jp.getMsgErrorEsp().isEmpty()) psUpdate.setNull(11, java.sql.Types.CHAR);
            else psUpdate.setString(11, jp.getMsgErrorEsp());
            
            if (jp.getMsgErrorIng() == null || jp.getMsgErrorIng().isEmpty()) psUpdate.setNull(12, java.sql.Types.CHAR);
            else psUpdate.setString(12, jp.getMsgErrorIng());

            // condiciones clausula WHERE
            psUpdate.setInt(13, jp.getCuestion().getId());
            psUpdate.setString(14, nombre);

            psUpdate.executeUpdate();
            psImmediateFK.executeUpdate();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw new SQLException(nombre+":"+e.getMessage());
            /*
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY))
                throw new Exception("No se puede guardar el juego de pruebas \"" + jp.getNombre() + "\":\n" +
                        "La cuestión ya dispone de un juego de pruebas con el mismo nombre");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR))
                throw new Exception("No se puede guardar el juego de pruebas \"" + jp.getNombre() + "\":\n" +
                        "Existen datos del juego de pruebas con caracteres no admitidos por la base de datos");
            else throw new Exception("No se puede guardar el juego de pruebas \"" + jp.getNombre() + "\":\n" + e.getMessage());
            */
        }
    }

    public void delete(JuegoPruebas jp, String nombre) throws Exception {
        try {
            psDeleteComprs.setInt(1, jp.getCuestion().getId());
            psDeleteComprs.setString(2, nombre);
            psDeleteComprs.executeUpdate();
            psDelete.setInt(1, jp.getCuestion().getId());
            psDelete.setString(2, nombre);
            psDelete.executeUpdate();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void updateOrdenes(JuegoPruebas jp) throws Exception {
        int orden = 1;
        try {
            psUpdateOrdenesProv.setInt(1, jp.getCuestion().getId());
            psUpdateOrdenesProv.setString(2, jp.getNombre());
            psUpdateOrdenesProv.executeUpdate();
            psUpdateOrden.setInt(2, jp.getCuestion().getId());
            psUpdateOrden.setString(3, jp.getNombre());
            for (Comprobacion compr : jp.getComprobaciones()) {
                psUpdateOrden.setString(4, compr.getNombre());
                psUpdateOrden.setInt(1, orden);
                psUpdateOrden.executeUpdate();
                orden++;
            }
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void updateOrdenesSwap(Comprobacion compr1, Comprobacion compr2) throws Exception {
        try {
            psUpdateOrden.setInt(2, compr1.getJuegoPruebas().getCuestion().getId());
            psUpdateOrden.setString(3, compr1.getJuegoPruebas().getNombre());

            psUpdateOrden.setString(4, compr2.getNombre());
            psUpdateOrden.setInt(1, -1);
            psUpdateOrden.executeUpdate();

            psUpdateOrden.setString(4, compr1.getNombre());
            psUpdateOrden.setInt(1, compr2.getOrden());
            psUpdateOrden.executeUpdate();

            psUpdateOrden.setString(4, compr2.getNombre());
            psUpdateOrden.setInt(1, compr1.getOrden());
            psUpdateOrden.executeUpdate();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

} // fin clase JuegopruebasDao
