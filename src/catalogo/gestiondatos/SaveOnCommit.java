//////////////////////////////////////////////////////
//  CLASSE: SaveOnCommit.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import catalogo.modelo.*;
import catalogo.soporte.modelos.ArrayListModel;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class SaveOnCommit {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    public final static int ESTADO_NUEVO           = 1;
    public final static int ESTADO_MODIFICADO      = 2;
    public final static int ESTADO_ELIMINADO       = 3;

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private CtrlCuestion ctrlCuestion;
    private CtrlJuegoPruebas ctrlJuegoPruebas;
    private CtrlComprobacion ctrlComprobacion;

    private ArrayList<JuegoPruebas> jpsNuevos;
    private ArrayList<JuegoPruebas> jpsModif;
    private ArrayList<JuegoPruebas> jpsEliminados;
    private ArrayList<Comprobacion> comprsNuevas;
    private ArrayList<Comprobacion> comprsModif;
    private ArrayList<Comprobacion> comprsEliminadas;
    private Hashtable nombresJPs;
    private Hashtable nombresComprs;

    private Cuestion cuestion, copia;
    private boolean cuestionModif;
    private boolean salidasModif;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public SaveOnCommit(CtrlCuestion ctrlCuestion, CtrlJuegoPruebas ctrlJuegoPruebas, CtrlComprobacion ctrlComprobacion) {
        this.ctrlCuestion = ctrlCuestion;
        this.ctrlJuegoPruebas = ctrlJuegoPruebas;
        this.ctrlComprobacion = ctrlComprobacion;
        jpsNuevos = new ArrayList<JuegoPruebas>();
        jpsModif = new ArrayList<JuegoPruebas>();;
        jpsEliminados = new ArrayList<JuegoPruebas>();
        comprsNuevas = new ArrayList<Comprobacion>();
        comprsModif = new ArrayList<Comprobacion>();
        comprsEliminadas = new ArrayList<Comprobacion>();
        nombresJPs = new Hashtable();
        nombresComprs = new Hashtable();
    }

    public void inicializa(Cuestion c) {
        reset();
        cuestion = c;
    }

    public void setCuestionModif() {
        copiaCuestion();
        cuestionModif = true;
    }

    public void setBlock(Boolean block){
        try{
            ctrlCuestion.updateBlock(cuestion, block);
            ctrlCuestion.commit();
        }catch(Exception e){

        }
    }

    // PRE: se ha invocado setCuestionModif(), set(JuegoPruebas, int) o set(Comprobacion, int)
    public void setSalidasModif() {
    	salidasModif = true;
    }
    
    public boolean isSalidasModif() {
        return this.salidasModif;
    }

    public void set(JuegoPruebas jp, int estado) {
        copiaCuestion();
        switch (estado) {
            case ESTADO_NUEVO:
                jpsNuevos.add(jp);
                for (Comprobacion compr : jp.getComprobaciones()) comprsNuevas.add(compr);
                break;

            case ESTADO_MODIFICADO:
                if (!jpsNuevos.contains(jp) && !jpsModif.contains(jp)) {
                    nombresJPs.put(jp, jp.getNombre());
                    jpsModif.add(jp);
                }
                break;

            case ESTADO_ELIMINADO:
                if (!jpsNuevos.remove(jp))                {
                    if (!jpsModif.remove(jp)) nombresJPs.put(jp, jp.getNombre());
                    jpsEliminados.add(jp);
                }
                for (Comprobacion compr : jp.getComprobaciones()) {
                    comprsNuevas.remove(compr);
                    comprsModif.remove(compr);
                    comprsEliminadas.remove(compr);
                    nombresComprs.remove(compr);
                }
        }
        if (!existenCambios()) reset();
    }

    public void set(Comprobacion compr, int estado) {
        copiaCuestion();
        switch (estado)
        {
            case ESTADO_NUEVO:
                comprsNuevas.add(compr);
                break;

            case ESTADO_MODIFICADO:
                if (!comprsNuevas.contains(compr) && !comprsModif.contains(compr))
                {
                    nombresComprs.put(compr, compr.getNombre());
                    comprsModif.add(compr);
                }
                break;

            case ESTADO_ELIMINADO:
                if (!comprsNuevas.remove(compr))
                {
                    if (!comprsModif.remove(compr)) nombresComprs.put(compr, compr.getNombre());
                    comprsEliminadas.add(compr);
                }
        }
        if (!existenCambios()) reset();
    }

    public void commit() throws Exception {
        int id = cuestion.getId();
        ArrayList<JuegoPruebas> jpsOrdenar = new ArrayList<JuegoPruebas>();

        try {
            if (cuestionModif && cuestion.getId() == 0) {
                id = ctrlCuestion.insert(cuestion);
            }
            else if (cuestionModif) {
                ctrlCuestion.update(cuestion);
            }

            for (JuegoPruebas jp : jpsEliminados) { ctrlJuegoPruebas.delete(jp, jp.getId()); }
            for (JuegoPruebas jp : jpsModif) { ctrlJuegoPruebas.update(jp, jp.getId()); }
            if (!jpsEliminados.isEmpty()) ctrlCuestion.updateOrdenes(cuestion); //@revisit
            for (JuegoPruebas jp : jpsNuevos) { ctrlJuegoPruebas.insert(jp, id); }
            for (Comprobacion compr : comprsEliminadas) {
                ctrlComprobacion.delete(compr, compr.getId());
                if (!jpsOrdenar.contains(compr.getJuegoPruebas())) jpsOrdenar.add(compr.getJuegoPruebas()); //@revisit
            }
            for (Comprobacion compr : comprsModif) { ctrlComprobacion.update(compr, compr.getId()); }
            for (JuegoPruebas jp : jpsOrdenar) { ctrlJuegoPruebas.updateOrdenes(jp); } //@revisit
            for (Comprobacion compr : comprsNuevas) { ctrlComprobacion.insert(compr, id); }
            // cuestionDao.updateSalidas neteja les sortides dels JPs i Comps i marca la Q com a NO RESOLTA (BD-wise)
            if (salidasModif) ctrlCuestion.updateSalidas(cuestion);
            ctrlCuestion.commit();

            cuestion.setId(id);
            if (salidasModif) cuestion.clearSalidas();
            reset();
            copia = new Cuestion(cuestion, cuestion.getId());
        }
        catch (Exception e) {
            throw e;
        }
    }

    public Cuestion rollback() {
        cuestion = copia;
        reset();
        return cuestion;
    }
    /*
    public void deleteAndCommit() throws Exception {
        cuestionDao.delete(cuestion);
        cuestionDao.commit();
        reset();
    }
    */
    public void clearSalidasAndCommit() throws Exception {
        ctrlCuestion.updateSalidas(cuestion);
        ctrlCuestion.commit();
        reset();
    }

    public void setCorrectoresAndCommit(List<Corrector> lista) throws Exception {
        ctrlCuestion.insertCorrectores(lista, cuestion.getId());
        ctrlCuestion.updateSalidas(cuestion);
        ctrlCuestion.commit();
        reset();
    }

    public void swapAndCommit(JuegoPruebas jp1, JuegoPruebas jp2) throws Exception {
        ctrlCuestion.updateOrdenesSwap(jp1, jp2);
        ctrlCuestion.updateSalidas(cuestion);
        ctrlCuestion.commit();
        reset();
    }

    public void swapAndCommit(Comprobacion compr1, Comprobacion compr2) throws Exception {
        ctrlJuegoPruebas.updateOrdenesSwap(compr1, compr2);
        ctrlCuestion.updateSalidas(cuestion);
        ctrlCuestion.commit();
        reset();
    }

    public void setCuestionActual(Cuestion c){
        cuestion = c;
    }

    public void fillSalidas() throws Exception {
        ctrlCuestion.selectSalidas(cuestion);
        ctrlCuestion.commit();
        reset();
    }

    public boolean existenCambios() {
        return (cuestionModif || !jpsNuevos.isEmpty() || !jpsModif.isEmpty() || !jpsEliminados.isEmpty() ||
                !comprsNuevas.isEmpty() || !comprsModif.isEmpty() || !comprsEliminadas.isEmpty());
    }

    public boolean existe(JuegoPruebas jp) {
        return (jpsNuevos.contains(jp) || jpsModif.contains(jp));
    }

    public boolean existe(Comprobacion compr) {
        return (comprsNuevas.contains(compr) || comprsModif.contains(compr));
    }

    public String getMsgCambios(String msgInicial) {
        String msg = "";

        if (cuestionModif) msg += "\n  - Los datos de la cuestión han cambiado";

        if (!jpsNuevos.isEmpty() || !jpsModif.isEmpty() || !jpsEliminados.isEmpty()) msg += "\n  - Los juegos de pruebas han cambiado (";
        if (jpsNuevos.size() == 1) msg += "1 nuevo, ";
        else if (jpsNuevos.size() > 1) msg += jpsNuevos.size() + " nuevos, ";
        if (jpsModif.size() == 1) msg += "1 modificado, ";
        else if (jpsModif.size() > 1) msg += jpsModif.size() + " modificados, ";
        if (jpsEliminados.size() == 1) msg += "1 eliminado, ";
        else if (jpsEliminados.size() > 1) msg += jpsEliminados.size() + " eliminados, ";
        if (!jpsNuevos.isEmpty() || !jpsModif.isEmpty() || !jpsEliminados.isEmpty()) msg = msg.substring(0, msg.length() - 2) + ")";

        if (!comprsNuevas.isEmpty() || !comprsModif.isEmpty() || !comprsEliminadas.isEmpty()) msg += "\n  - Las comprobaciones han cambiado (";
        if (comprsNuevas.size() == 1) msg += "1 nueva, ";
        else if (comprsNuevas.size() > 1) msg += comprsNuevas.size() + " nuevas, ";
        if (comprsModif.size() == 1) msg += "1 modificada, ";
        else if (comprsModif.size() > 1) msg += comprsModif.size() + " modificadas, ";
        if (comprsEliminadas.size() == 1) msg += "1 eliminada, ";
        else if (comprsEliminadas.size() > 1) msg += comprsEliminadas.size() + " eliminadas, ";
        if (!comprsNuevas.isEmpty() || !comprsModif.isEmpty() || !comprsEliminadas.isEmpty()) msg = msg.substring(0, msg.length() - 2) + ")";

        return msgInicial + "\nDetalles:" + msg + "\n";
    }

    public ArrayListModel<Tematica> getTematicas(Categoria cat) throws Exception{
        ArrayListModel<Tematica> tem;
        tem = new ArrayListModel<Tematica>();
        tem.addAll(ctrlCuestion.selectTematicas(cat));
        return tem;
    }

    // Métodos auxiliares *****************************************************

    private void reset() {
        copia = null;
        jpsNuevos.clear();
        jpsModif.clear();
        jpsEliminados.clear();
        comprsNuevas.clear();
        comprsModif.clear();
        comprsEliminadas.clear();
        nombresJPs.clear();
        nombresComprs.clear();
        cuestionModif = false;
        salidasModif = false;
    }

    private void copiaCuestion() {
        if (copia == null) copia = new Cuestion(cuestion, cuestion.getId());
    }
    
    public void printJpChanges(){
    	System.out.println(this.jpsNuevos);
    	System.out.println(this.jpsModif);
    	System.out.println(this.jpsEliminados);
    	System.out.println(this.comprsNuevas);
    	System.out.println(this.comprsModif);
    	System.out.println(this.comprsEliminadas);
    }

} // Fin clase SaveOnCommit
