//////////////////////////////////////////////////////
//  CLASSE: CategoriaDao.java                     //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import catalogo.modelo.Tematica;
import catalogo.modelo.Categoria;

import java.sql.*;
import java.util.*;


public class CtrlCategorias
{
    private static final String __dbtable = "t_categories";

    private Connection con;

    private PreparedStatement psSelect, psSelectOne, psSelect2, psSelect3,
                              psInsert, psDelete,
                              psUpdate1, psDeferredFK, psImmediateFK;

    // private ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala"); // NEEDED?? Albert.

    /**
     *
     * @throws Exception
     */
    public CtrlCategorias(Connection con) throws Exception {
        this.con = con;

        /**
         * @throws SQLException
         */
        this.__initPreparedQueries();
    }
    
    /*
    public Categoria get(int id) throws Exception {
        ResultSet rs = null;
        Categoria cat = null;

        try {
            this.psSelectOne.setInt(1, id);
            rs = psSelectOne.executeQuery();
            while (rs.next()) {
                cat = new Categoria(
                    Integer.parseInt(rs.getString("id")),
                    rs.getString("descripcio"),
                    rs.getBoolean("inits"),
                    rs.getBoolean("postinits"),
                    rs.getBoolean("neteja"),
                    rs.getBoolean("estat"),
                    rs.getBoolean("solucio"),
                    rs.getBoolean("initsjp"),
                    rs.getBoolean("entradajp"),
                    rs.getBoolean("netejajp"),
                    rs.getBoolean("comprovacions")
                );
            }
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
        finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }

        return cat;
    }*/
    
    public Categoria get(int id) throws Exception {
        List<Integer> ids = new ArrayList<Integer>();
        ids.add(id);
        List<Categoria> categorias = this.getByIds(ids);
        if (categorias.isEmpty()) {
            return null;
        } else {
            return categorias.get(0);
        }
    }
    
    public List<Categoria> getByIds(List<Integer> ids) throws Exception {
		List<Categoria> categorias = new ArrayList<Categoria>();
        
    	if (ids.isEmpty()) {
        	return categorias;
        }
    	
    	ResultSet rs = null;
        Statement s = null;

        String sql = "SELECT * FROM t_categories";
        String where = "";
        for (int id : ids) {
            where += " OR id = '"+id+"'";
        }
        sql += " WHERE "+where.substring(4)+" ORDER BY id";

        try {
            s = con.createStatement();
            rs = s.executeQuery(sql);
			while (rs.next()) {
                categorias.add(new Categoria(
            		Integer.parseInt(rs.getString("id")),
                    rs.getString("descripcio"),
                    rs.getBoolean("inits"),
                    rs.getBoolean("postinits"),
                    rs.getBoolean("neteja"),
                    rs.getBoolean("estat"),
                    rs.getBoolean("solucio"),
                    rs.getBoolean("initsjp"),
                    rs.getBoolean("entradajp"),
                    rs.getBoolean("netejajp"),
                    rs.getBoolean("comprovacions")
                ));
            }
            //Collections.sort(tematicas);
            con.commit();
            
            return categorias;
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) { throw e2;}
            throw e;
        }
    	finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }
    }

    public List<Categoria> select() throws Exception {
        ResultSet rs = null;
        List<Categoria> categorias = null;

        try {
            rs = psSelect.executeQuery();
            categorias = new ArrayList<Categoria>();

            while (rs.next()) {
                categorias.add(new Categoria(Integer.parseInt(rs.getString("id")), rs.getString("descripcio"),
                        rs.getBoolean("inits"),rs.getBoolean("postinits"),rs.getBoolean("neteja"),
                        rs.getBoolean("estat"),rs.getBoolean("solucio"),rs.getBoolean("initsjp"),
                        rs.getBoolean("entradajp"),rs.getBoolean("netejajp"),rs.getBoolean("comprovacions")));
            }
            //Collections.sort(categorias);
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }
        return categorias;
    }

    public List<Tematica> selectTematicas(int id) throws Exception {
        ResultSet rs = null;
        ResultSet rs2 = null;
        List<Tematica> tematicas = null;

        try {
            psSelect2.setInt(1, id);
            rs = psSelect2.executeQuery();
            tematicas = new ArrayList<Tematica>();

            while (rs.next()) {
                psSelect3.setString(1, rs.getString("nom"));
                rs2 = psSelect3.executeQuery();
                while (rs2.next()){
//                    Categoria c = new Categoria();
//                    tematicas.add(new Tematica(Integer.parseInt(rs2.getString("nom")), rs2.getString("descripcio")));
                }
            }
            //Collections.sort(tematicas);
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) { throw e2;}
            throw e;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }
        return tematicas;
    }

    /*public void setRecursos(ResourceBundle recursos){
        //this.recursos = recursos;
    }*/

    /*********************************************************/
    /*      Métodos para gestionar categorias                */
    /*********************************************************/
    public void insert(int id,
                       String descripcion,
                       Boolean inits,
                       Boolean postinits,
                       Boolean limpieza, Boolean estado,
                       Boolean solucion,
                       Boolean initsJP,
                       Boolean entradaJP,
                       Boolean limpiezaJP,
                       Boolean comprobaciones
    ) throws Exception {
        try {
            psInsert.setInt(1, id);
            psInsert.setString(2, descripcion);
            psInsert.setBoolean(3, inits);
            psInsert.setBoolean(4, postinits);
            psInsert.setBoolean(5, limpieza);
            psInsert.setBoolean(6, estado);
            psInsert.setBoolean(7, solucion);
            psInsert.setBoolean(8, initsJP);
            psInsert.setBoolean(9, entradaJP);
            psInsert.setBoolean(10, limpiezaJP);
            psInsert.setBoolean(11, comprobaciones);
            psInsert.executeUpdate();
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY)) throw new Exception("Ya existe una categoria con ese identificador");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR)) throw new Exception("Existen caracteres no admitidos por la base de datos");
            else throw e;
            */
        }
    }

    public void update(int idOld,
                       int idNew,
                       String descripcion,
                       Boolean inits,
                       Boolean postinits,
                       Boolean limpieza,
                       Boolean estado,
                       Boolean solucion,
                       Boolean initsJP,
                       Boolean entradaJP,
                       Boolean limpiezaJP,
                       Boolean comprobaciones
    ) throws Exception {
        try {
            psDeferredFK.executeUpdate();
            psUpdate1.setInt(1, idNew);
            psUpdate1.setString(2, descripcion);
            psUpdate1.setBoolean(3, inits);
            psUpdate1.setBoolean(4, postinits);
            psUpdate1.setBoolean(5, limpieza);
            psUpdate1.setBoolean(6, estado);
            psUpdate1.setBoolean(7, solucion);
            psUpdate1.setBoolean(8, initsJP);
            psUpdate1.setBoolean(9, entradaJP);
            psUpdate1.setBoolean(10, limpiezaJP);
            psUpdate1.setBoolean(11, comprobaciones);
            psUpdate1.setInt(12, idOld);
            psUpdate1.executeUpdate();
            psImmediateFK.executeUpdate();
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY)) throw new Exception("Ya existe una categoria con ese identificador");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR)) throw new Exception("Existen caracteres no admitidos por la base de datos");
            else throw e;
            */
        }
    }

    public void delete(int id) throws Exception {
        try {
            psDelete.setInt(1, id);
            psDelete.executeUpdate();
            con.commit();
        } catch (SQLException e) {
        	try { con.rollback(); } catch (SQLException e2) {}
        	throw e;
            // if (e.getSQLState().equals(CodigosSQL.FOREIGN_KEY)) throw new Exception("Existen cuestiones asociadas a la categoria");
            // else throw e;
        }
    }

    /**
     *
     * @throws SQLException
     */
    private void __initPreparedQueries() throws SQLException
    {
        try {
            this.psSelect = this.con.prepareStatement(
                "SELECT id, descripcio, inits, postinits, neteja, estat,solucio, initsjp, entradajp, netejajp, comprovacions"
              +" FROM "+CtrlCategorias.__dbtable
              +" ORDER BY id"
            );

            this.psSelectOne = this.con.prepareStatement(
                "SELECT * FROM "+CtrlCategorias.__dbtable+" WHERE id = ? LIMIT 1"
            );

            this.psSelect2 = this.con.prepareStatement(
                "SELECT nom"
              +" FROM t_categoriestematiques"
              +" WHERE id = ?"
            );

            this.psSelect3 = this.con.prepareStatement(
                "SELECT nom, descripcio"
              +" FROM t_tematiques"
              +" WHERE nom = ?"
              +" ORDER BY id"
            );

            this.psInsert = this.con.prepareStatement(
                "INSERT INTO "+CtrlCategorias.__dbtable+"("
              +     "id, descripcio, inits, postinits, neteja, estat, solucio, initsjp"
              +   ", entradajp, netejajp, comprovacions"
              + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            );

            this.psDelete = this.con.prepareStatement(
                "DELETE FROM "+CtrlCategorias.__dbtable
              +" WHERE id = ?"
            );

            this.psUpdate1 = this.con.prepareStatement(
                "UPDATE "+CtrlCategorias.__dbtable
              +" SET id = ?, descripcio = ?, inits = ?, postinits = ?, neteja = ?"
              +   ", estat = ?, solucio = ?, initsjp = ?, entradajp = ?, netejajp = ?"
              +   ", comprovacions = ?"
              +" WHERE id = ?"
            );

            this.psDeferredFK = this.con.prepareStatement(
                "SET CONSTRAINTS t_questions_categories_fkey DEFERRED"
            );

            this.psImmediateFK = this.con.prepareStatement(
                "SET CONSTRAINTS t_questions_categories_fkey IMMEDIATE"
            );
        }
        catch (SQLException e) { throw e; }
    }

} // Fin clase CategoriaDao
