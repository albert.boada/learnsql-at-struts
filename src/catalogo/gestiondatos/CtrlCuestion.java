//////////////////////////////////////////////////////
//  CLASSE: CuestionDao.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//          versio 3: Albert Boada                  //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import catalogo.modelo.Categoria;
import catalogo.modelo.Comprobacion;
import catalogo.modelo.Corrector;
import catalogo.modelo.Cuestion;
import catalogo.modelo.Esquema;
import catalogo.modelo.JuegoPruebas;
import catalogo.modelo.Tematica;
import catalogo.modelo.TipossolucionService;

import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;

public class CtrlCuestion
{
    private static final String __dbtable = "t_questions";

    private Connection con;

    private PreparedStatement psInsert, psUpdate, psDelete,
                              psDeleteJPs, psDeleteComprs,
                              psInsertTematica, psInsertCorrector, psDeleteTematicas, psDeleteCorrectores,
                              psSelectMaxId, psSelectTematicas, psSelectTematicas2,
                              psUpdateResuelta, psUpdateBlock, psUpdateSalidasJPs, psUpdateSalidasComprs,
                              psUpdateOrden, psUpdateOrdenesProv,
                              psSelectSalidasJPs, psSelectSalidasComprs
    ;

    private CtrlCategorias    ctrlCategorias;
    private CtrlTematicas     ctrlTematicas;
    private CtrlEsquemas      ctrlEsquemas;
    private CtrlTiposSolucion ctrlTiposSolucion;
    private CtrlCuestion     ctrlCuestion;
    private CtrlCorrectores    ctrlCorrectores;

    /**
     *
     * @throws Exception
     */
    public CtrlCuestion(Connection con,
                       CtrlCategorias categoria_dao,
                       CtrlTematicas tematica_dao,
                       CtrlEsquemas esquema_dao,
                       CtrlTiposSolucion tiposolucion_dao,
                       CtrlCorrectores corrector_dao
    ) throws Exception
    {
        this.con = con;

        this.ctrlCategorias    = categoria_dao;
        this.ctrlTematicas     = tematica_dao;
        this.ctrlEsquemas      = esquema_dao;
        this.ctrlTiposSolucion = tiposolucion_dao;
        this.ctrlCorrectores    = corrector_dao;

        /**
         * @throws SQLException
         */
        this.__initPreparedQueries();
    }

    /*********************************************************/
    /*      Métodos para gestionar cuestiones                */
    /*********************************************************/

    public List<Cuestion> select(int identificador,
                                 int estado,
                                 String titulo,
                                 String autor,
                                 float mindificultad,
                                 float maxdificultad,
                                 List<Categoria> categorias,
                                 List<Esquema> esquemas,
                                 List<Tematica> tematicas
    ) throws Exception
    {
        String select =
            "SELECT DISTINCT"
          +    " q.id, q.autor, q.titol, q.enunciat, q.tipussolucio, q.tipusquestio"
          +   ", q.fitxeradjunt, q.inits, q.urlurl, q.usuariurl, q.clauurl, q.sslurl"
          +   ", q.solucio, q.neteja, q.resolta, q.esquema, q.dificultat, q.extensiofitxer"
          +   ", q.extensiosolucio, q.postinits, q.postinitjp, q.snapshot, q.binaria, q.gabia"
          +   ", q.enunciatesp, q.enunciating, q.block"
          +" FROM "+CtrlCuestion.__dbtable+" q"
        ;

        String where = "";

        if (titulo != null && titulo.length() > 0) {
            where = "LOWER(q.titol) LIKE LOWER('%" + titulo.replace("'", "''") + "%')";
        }

        if (autor != null && autor.length() > 0) {
            if (where.length() > 0) {
                where += " AND ";
            }
            where += "LOWER(q.autor) LIKE LOWER('%" + autor.replace("'", "''") + "%')";
        }

        if (identificador > 0) {
            if (where.length() > 0) {
                where += " AND ";
            }
            where += "q.id = " + identificador + "";
        }

        if (estado != 0) {
            if (where.length() > 0) {
                where += " AND ";
            }
            if (estado == 1) {
                where += "q.resolta = true";
            } else {
                where += "q.resolta = false";
            }
        }

        if (mindificultad != -1 && maxdificultad != -1) {
            if (where.length() > 0) {
                where += " AND ";
            }
            where += "q.dificultat >= " + (mindificultad - 0.00001) + " AND ";
            where += "q.dificultat <= " + (maxdificultad + 0.00001) + "";
        }

        if (categorias != null && categorias.size() > 0) {
            if (where.length() > 0) {
                where += " AND ";
            }
            where += "(";

            for (Categoria cat : categorias) {
                where += "q.tipusquestio = " + cat.getId() + " OR ";
            }
            where = where.substring(0, where.length() - 4);
            where += ")";
        }

        if (esquemas != null && esquemas.size() > 0) {
            if (where.length() > 0) {
                where += " AND ";
            }
            where += "(";
            for (Esquema esq : esquemas) {
                where += "q.esquema = '" + esq.getNombre().replace("'", "''") + "' OR ";
            }
            where = where.substring(0, where.length() - 4);
            where += ")";
        }

        if (tematicas != null && tematicas.size() > 0) {
            if (where.length() > 0) {
                where += " AND ";
            }
            where += "q.id IN (SELECT t.id FROM t_tematiquesquestions t WHERE ";
            for (Tematica tem : tematicas) {
                where += "t.nom = '" + tem.getNombre().replace("'", "''") + "' OR ";
            }
            where = where.substring(0, where.length() - 4);
            where += " GROUP BY t.id HAVING COUNT(*) = " + tematicas.size() + ")";
        }

        if (where.length() > 0) {
            where = " WHERE " + where + " ORDER BY q.id ASC";
        } else {
        	where = " ORDER BY q.id ASC";
        }

        Statement s = null;
        ResultSet rs = null;
        List<Cuestion> cuestiones = null;

        try {
            s = con.createStatement();
            rs = s.executeQuery(select + where);

            cuestiones = new ArrayList<Cuestion>();
            this.__mapRsToList(rs, cuestiones);

            con.commit();
        }
        catch (SQLException e) {
            try {
                con.rollback();
            }
            catch (SQLException e2) {}
            throw e;
        }
        finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            }
            catch (SQLException e) {}
            try {
                if (s != null) { s.close(); }
            }
            catch (SQLException e) {}
        }
        return cuestiones;
    }

    public List<Tematica> selectTematicas(Categoria c) throws Exception
    {
        /**
         * @todo I això per què coi no està a `CategoriaDao`?????
         */

        ResultSet rs = null;
        ResultSet rs2 = null;
        List<Tematica> tematicas = null;
        List<Categoria> categoria = null;
        categoria = new ArrayList<Categoria>();
        categoria.add(c);

        try {
            psSelectTematicas.setInt(1, c.getId());
            rs = psSelectTematicas.executeQuery();
            tematicas = new ArrayList<Tematica>();
            while (rs.next()) {
            	psSelectTematicas2.setString(1, rs.getString("nom"));
                rs2 = psSelectTematicas2.executeQuery();
                while (rs2.next()){
                    tematicas.add(new Tematica(rs2.getString("nom"), rs2.getString("descripcio"), categoria));
                }
            }
            //Collections.sort(tematicas);
            con.commit();
        } catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) { throw e2;}
            throw e;
        } finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }
        return tematicas;
    }

    public int insert(Cuestion c) throws Exception {
        int id = 0;
        ResultSet rs = null;
        try {
            psInsert.setString(1, c.getAutor());
            psInsert.setString(2, c.getTitulo());
            psInsert.setString(3, c.getEnunciado());
            psInsert.setInt(4, c.getTipoSolucion().getId());

            if (c.getUrl().isEmpty()) psInsert.setNull(5, java.sql.Types.BINARY);
            else psInsert.setBytes(5, c.getUrl().getBytes());

            if (c.getUsuario().isEmpty()) psInsert.setNull(6, java.sql.Types.BINARY);
            else psInsert.setBytes(6, c.getUsuario().getBytes());

            if (c.getClave().isEmpty()) psInsert.setNull(7, java.sql.Types.BINARY);
            else psInsert.setBytes(7, c.getClave().getBytes());

            if (c.getSsl() == null) psInsert.setNull(8, java.sql.Types.BOOLEAN);
            else psInsert.setBoolean(8, c.getSsl());
            
            if (c.getCategoria() == null) psInsert.setNull(9, java.sql.Types.INTEGER);
            else psInsert.setInt(9, c.getCategoria().getId());
            
            if (c.existeFicheroAdjunto()) {
                psInsert.setBytes(10, c.getBytesFichero());
                psInsert.setString(11, c.getExtensionFichero());
            } else {
                psInsert.setNull(10, java.sql.Types.BINARY);
                psInsert.setNull(11, java.sql.Types.VARCHAR);
            }

            if (c.getTipoSolucion().getId() == TipossolucionService.ID_FICHERO) {
                psInsert.setBytes(12, c.getBytesSolucion());
                psInsert.setString(13, c.getExtensionSolucion());
            } else {
                psInsert.setBytes(12, c.getSolucion().getBytes());
                psInsert.setNull(13, java.sql.Types.VARCHAR);
            }

            if (c.getInits().isEmpty()) psInsert.setNull(14, java.sql.Types.BINARY);
            else psInsert.setBytes(14, c.getInits().getBytes());

            if (c.getLimpieza().isEmpty()) psInsert.setNull(15, java.sql.Types.BINARY);
            else psInsert.setBytes(15, c.getLimpieza().getBytes());

            if (c.getPostInits() == null || c.getPostInits().isEmpty()) psInsert.setNull(16, java.sql.Types.CHAR);
            else psInsert.setString(16, c.getPostInits());

            psInsert.setBoolean(17, false);
            
            if (c.getEsquema() == null) psInsert.setNull(18, java.sql.Types.CHAR);
            else psInsert.setString(18, c.getEsquema().getNombre());

            if(c.getDificultad() == -1) psInsert.setNull(19, java.sql.Types.FLOAT);
            else psInsert.setFloat(19, c.getDificultad());

            if (c.getEstado() == null || c.getEstado().isEmpty()) psInsert.setNull(20, java.sql.Types.CHAR);
            else psInsert.setString(20, c.getEstado());

            if (c.getBinaria() == null) psInsert.setNull(21, java.sql.Types.BOOLEAN);
            else psInsert.setBoolean(21, c.getBinaria());

            if (c.getGabia() == null) psInsert.setNull(22, java.sql.Types.BOOLEAN);
            else psInsert.setBoolean(22, c.getGabia());

            if (c.getEnunciadoEsp() == null || c.getEnunciadoEsp().isEmpty()) psInsert.setNull(23, java.sql.Types.CHAR);
            else psInsert.setString(23, c.getEnunciadoEsp());

            if (c.getEnunciadoIng() == null || c.getEnunciadoIng().isEmpty()) psInsert.setNull(24, java.sql.Types.CHAR);
            else psInsert.setString(24, c.getEnunciadoIng());

            psInsert.setInt(25, c.getPostInitsJP());
            psInsert.setBoolean(26, c.getBloqueada());
            psInsert.executeUpdate();

            rs = psSelectMaxId.executeQuery();
            rs.next();
            id = rs.getInt("id");

            insertTematicas(c.getTematicas(), id);

            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR)) {
                throw new Exception("No se puede guardar la cuestión:\nExisten datos de la cuestión con caracteres no admitidos por la base de datos");
            } else {
                throw e; //new Exception("No se puede guardar la cuestión:\n"+e.getMessage());
            }*/
        }
        finally
        {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
        }
        return id;
    }

    public void update(Cuestion c) throws Exception {
        try {
            psUpdate.setString(1, c.getAutor());
            psUpdate.setString(2, c.getTitulo());
            psUpdate.setString(3, c.getEnunciado());
            psUpdate.setInt(4, c.getTipoSolucion().getId());

            if (c.getUrl().isEmpty()) psUpdate.setNull(5, java.sql.Types.BINARY);
            else psUpdate.setBytes(5, c.getUrl().getBytes());

            if (c.getUsuario().isEmpty()) psUpdate.setNull(6, java.sql.Types.BINARY);
            else psUpdate.setBytes(6, c.getUsuario().getBytes());

            if (c.getClave().isEmpty()) psUpdate.setNull(7, java.sql.Types.BINARY);
            else psUpdate.setBytes(7, c.getClave().getBytes());

            if (c.getSsl() == null) psUpdate.setNull(8, java.sql.Types.BOOLEAN);
            else psUpdate.setBoolean(8, c.getSsl());

            psUpdate.setInt(9, c.getCategoria().getId());

            if (c.existeFicheroAdjunto()) {
                psUpdate.setBytes(10, c.getBytesFichero());
                psUpdate.setString(11, c.getExtensionFichero());
            } else {
                psUpdate.setNull(10, java.sql.Types.BINARY);
                psUpdate.setNull(11, java.sql.Types.VARCHAR);
            }

            if (c.getTipoSolucion().getId() == TipossolucionService.ID_FICHERO) {
                psUpdate.setBytes(12, c.getBytesSolucion());
                psUpdate.setString(13, c.getExtensionSolucion());
            } else {
                psUpdate.setBytes(12, c.getSolucion().getBytes());
                psUpdate.setNull(13, java.sql.Types.VARCHAR);
            }

            if (c.getInits().isEmpty()) psUpdate.setNull(14, java.sql.Types.BINARY);
            else psUpdate.setBytes(14, c.getInits().getBytes());

            if (c.getLimpieza().isEmpty()) psUpdate.setNull(15, java.sql.Types.BINARY);
            else  psUpdate.setBytes(15, c.getLimpieza().getBytes());

            psUpdate.setString(16, c.getPostInits());

            psUpdate.setString(17, c.getEsquema().getNombre());

            if(c.getDificultad() == -1) psUpdate.setNull(18, java.sql.Types.FLOAT);
            else psUpdate.setFloat(18, c.getDificultad());

            if (c.getEstado() == null || c.getEstado().isEmpty()) psUpdate.setNull(19, java.sql.Types.CHAR);
            else psUpdate.setString(19, c.getEstado());

            if (c.getBinaria() == null) psUpdate.setNull(20, java.sql.Types.BOOLEAN);
            else psUpdate.setBoolean(20, c.getBinaria());

            if (c.getGabia() == null) psUpdate.setNull(21, java.sql.Types.BOOLEAN);
            else psUpdate.setBoolean(21, c.getGabia());

            if (c.getEnunciadoEsp() == null || c.getEnunciadoEsp().isEmpty()) psUpdate.setNull(22, java.sql.Types.CHAR);
            else psUpdate.setString(22, c.getEnunciadoEsp());

            if (c.getEnunciadoIng() == null || c.getEnunciadoIng().isEmpty()) psUpdate.setNull(23, java.sql.Types.CHAR);
            else psUpdate.setString(23, c.getEnunciadoIng());

            psUpdate.setInt(24, c.getPostInitsJP());
            psUpdate.setInt(25, c.getId());

            psUpdate.executeUpdate();

            insertTematicas(c.getTematicas(), c.getId());

            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
            /*
            if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR)) {
                throw new Exception("No se puede guardar la cuestión:\nExisten datos de la cuestión con caracteres no admitidos por la base de datos");
            } else {
                throw new Exception("No se puede guardar la cuestión:\n" + e.getMessage());
            }
            */
        }
    }

    public void delete(Cuestion c) throws Exception {
        try {
            psDeleteTematicas.setInt(1, c.getId());
            psDeleteTematicas.executeUpdate();
            psDeleteCorrectores.setInt(1, c.getId());
            psDeleteCorrectores.executeUpdate();
            psDeleteComprs.setInt(1, c.getId());
            psDeleteComprs.executeUpdate();
            psDeleteJPs.setInt(1, c.getId());
            psDeleteJPs.executeUpdate();
            psDelete.setInt(1, c.getId());
            psDelete.executeUpdate();
        }
        catch (SQLException e)
        {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void updateBlock(Cuestion c, boolean block) throws SQLException {
        psUpdateBlock.setBoolean(1, block);
        psUpdateBlock.setInt(2, c.getId());
        psUpdateBlock.executeUpdate();
    }

    public void insertCorrectores(List<Corrector> lista, int id) throws Exception {
        try {
            psDeleteCorrectores.setInt(1, id);
            psDeleteCorrectores.executeUpdate();
            psInsertCorrector.setInt(1, id);
            for (Corrector cor : lista){
                psInsertCorrector.setString(2, cor.getURL());
                psInsertCorrector.executeUpdate();
            }
        }
        catch (SQLException e){
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void updateSalidas(Cuestion c) throws Exception {
        try {
            psUpdateSalidasJPs.setInt(1, c.getId());
            psUpdateSalidasJPs.executeUpdate();
            psUpdateSalidasComprs.setInt(1, c.getId());
            psUpdateSalidasComprs.executeUpdate();
            updateResuelta(c, false);
        }
        catch (SQLException e){
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void updateOrdenes(Cuestion c) throws Exception {
        int orden = 1;
        try {
            psUpdateOrdenesProv.setInt(1, c.getId());
            psUpdateOrdenesProv.executeUpdate();
            psUpdateOrden.setInt(2, c.getId());
            for (JuegoPruebas jp : c.getJuegosPruebas()) {
                psUpdateOrden.setString(3, jp.getNombre());
                psUpdateOrden.setInt(1, orden);
                psUpdateOrden.executeUpdate();
                orden++;
            }
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void updateOrdenesSwap(JuegoPruebas jp1, JuegoPruebas jp2) throws Exception {
        try {
            psUpdateOrden.setInt(2, jp1.getCuestion().getId());

            psUpdateOrden.setString(3, jp2.getNombre());
            psUpdateOrden.setInt(1, -1);
            psUpdateOrden.executeUpdate();

            psUpdateOrden.setString(3, jp1.getNombre());
            psUpdateOrden.setInt(1, jp2.getOrden());
            psUpdateOrden.executeUpdate();

            psUpdateOrden.setString(3, jp2.getNombre());
            psUpdateOrden.setInt(1, jp1.getOrden());
            psUpdateOrden.executeUpdate();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    public void selectSalidas(Cuestion c) throws Exception {
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        try {
            psSelectSalidasJPs.setInt(1, c.getId());
            rs1 = psSelectSalidasJPs.executeQuery();
            for (JuegoPruebas jp : c.getJuegosPruebas()) {
                rs1.next();
                jp.setSalida(new String(rs1.getBytes("outjp")));
                if (!jp.getComprobaciones().isEmpty()){
                    psSelectSalidasComprs.setInt(1, c.getId());
                    psSelectSalidasComprs.setString(2, jp.getNombre());
                    rs2 = psSelectSalidasComprs.executeQuery();
                    for (Comprobacion compr : jp.getComprobaciones()){
                        rs2.next();
                        compr.setSalida(new String(rs2.getBytes("outcompr")));
                    }
                    rs2.close();
                }
            }
            updateResuelta(c, true);
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
        finally {
            try { if (rs1 != null) rs1.close(); } catch (SQLException e) {}
            try { if (rs2 != null) rs2.close(); } catch (SQLException e) {}
        }
    }

    public void commit() throws Exception {
        try {
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
    }

    /*********************************************************/
    /*      Métodos auxiliares privados                      */
    /*********************************************************/

    /**
     *
     * @throws SQLException
     */
    private void __initPreparedQueries() throws SQLException
    {
        try {
            this.psSelectMaxId = this.con.prepareStatement(
                "SELECT MAX(id) AS id"
              +" FROM "+CtrlCuestion.__dbtable
            );

            this.psSelectTematicas = this.con.prepareStatement(
                "SELECT nom"
              +" FROM t_categoriestematiques"
              +" WHERE id = ?"
            );

            this.psSelectTematicas2 = this.con.prepareStatement(
                "SELECT nom, descripcio"
              +" FROM t_tematiques"
              +" WHERE nom = ?"
            );

            this.psInsert = this.con.prepareStatement(
                "INSERT INTO t_questions("
              +     "autor, titol, enunciat, tipussolucio, urlurl, usuariurl"
              +   ", clauurl, sslurl, tipusquestio, fitxeradjunt, extensiofitxer"
              +   ", solucio, extensiosolucio, inits, neteja, postinits, resolta"
              +   ", esquema, dificultat, snapshot, binaria, gabia, enunciatesp"
              +   ", enunciating, postinitjp, block"
              + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            );

            this.psUpdate = this.con.prepareStatement(
                "UPDATE t_questions"
              +" SET autor = ?, titol = ?, enunciat = ?, tipussolucio = ?"
              +   ", urlurl = ?, usuariurl = ?, clauurl = ?, sslurl = ?, tipusquestio = ?"
              +   ", fitxeradjunt = ?, extensiofitxer = ?, solucio = ?, extensiosolucio = ?"
              +   ", inits = ?, neteja = ?, postinits = ?, esquema = ?, dificultat = ?"
              +   ", snapshot = ?, binaria = ?, gabia = ?, enunciatesp = ?, enunciating = ?"
              +   ", postinitjp = ?"
              +" WHERE id = ?"
            );

            this.psDelete = this.con.prepareStatement(
                "DELETE FROM "+CtrlCuestion.__dbtable
              +" WHERE id = ?"
            );

            this.psDeleteTematicas = this.con.prepareStatement(
                "DELETE FROM t_tematiquesquestions"
              +" WHERE id = ?"
            );

            this.psDeleteCorrectores = this.con.prepareStatement(
                "DELETE FROM t_urlswquestions"
              +" WHERE id = ?"
            );

            this.psDeleteJPs = this.con.prepareStatement(
                "DELETE FROM t_jocsproves"
              +" WHERE id = ?"
            );

            this.psDeleteComprs = this.con.prepareStatement(
                "DELETE FROM t_comprovacions"
              +" WHERE id = ?"
            );

            this.psInsertTematica = this.con.prepareStatement(
                "INSERT INTO t_tematiquesquestions("
              +     "id, nom"
              + ") VALUES (?, ?)"
            );

            this.psInsertCorrector = this.con.prepareStatement(
                "INSERT INTO t_urlswquestions("
              +     "id, urlsw"
              + ") VALUES(?, ?)"
            );

            this.psUpdateResuelta = this.con.prepareStatement(
                "UPDATE "+CtrlCuestion.__dbtable+""
              +" SET resolta = ?"
              +" WHERE id = ?"
            );

            this.psUpdateBlock = this.con.prepareStatement(
                "UPDATE "+CtrlCuestion.__dbtable+""
              +" SET block = ?"
              +" WHERE id = ?"
            );

            this.psUpdateSalidasJPs = this.con.prepareStatement(
                "UPDATE t_jocsproves"
              +" SET outjp = NULL"
              +" WHERE id = ?"
            );

            this.psUpdateSalidasComprs = this.con.prepareStatement(
                "UPDATE t_comprovacions"
              +" SET outcompr = NULL"
              +" WHERE id = ?"
            );

            this.psUpdateOrdenesProv = this.con.prepareStatement(
                "UPDATE t_jocsproves"
              +" SET ordre = (-1 * ordre)"
              +" WHERE id = ?"
            );

            this.psUpdateOrden = this.con.prepareStatement(
                "UPDATE t_jocsproves"
              +" SET ordre = ?"
              +" WHERE id = ? AND nomjp = ?"
            );

            this.psSelectSalidasJPs = this.con.prepareStatement(
                "SELECT ordre, outjp"
              +" FROM t_jocsproves"
              +" WHERE id = ?"
              +" ORDER BY ordre"
            );

            this.psSelectSalidasComprs = this.con.prepareStatement(
                "SELECT ordre, outcompr"
              +" FROM t_comprovacions"
              +" WHERE id = ? AND nomjp = ?"
              +" ORDER BY ordre"
            );
        }
        catch (SQLException e) {
            try { this.con.rollback(); /*????????*/ } catch (SQLException e2) {}
            throw e;
        }
    }

    /**
     *
     */
    private void __mapRsToList(ResultSet rs, List<Cuestion> cuestiones) throws Exception
    {
        Cuestion c;

        while (rs.next()) {
            c = new Cuestion();
            c.setId(          Integer.parseInt(rs.getString("id")));
            c.setAutor(       rs.getString( "autor"));
            c.setTitulo(      rs.getString( "titol"));
            c.setBloqueada(   rs.getBoolean("block"));
            c.setEnunciado(   rs.getString( "enunciat"));
            c.setEnunciadoEsp(rs.getString( "enunciatesp"));
            c.setEnunciadoIng(rs.getString( "enunciating"));
            c.setEstado(      rs.getString( "snapshot"));

            rs.getBoolean("binaria");
            if (!rs.wasNull()) c.setBinaria(rs.getBoolean("binaria"));
            else c.setBinaria(null);

            rs.getBoolean("gabia");
            if (!rs.wasNull()) c.setGabia(rs.getBoolean("gabia"));
            else c.setGabia(null);

            rs.getFloat("dificultat");
            if (!rs.wasNull()) c.setDificultad(rs.getFloat("dificultat"));
            else c.setDificultad(new Float(-1.0f));

            rs.getBytes("urlurl");
            if (!rs.wasNull()) {
                c.setUrl(new String(rs.getBytes("urlurl")));
            }

            rs.getBytes("usuariurl");
            if (!rs.wasNull()) {
                c.setUsuario(new String(rs.getBytes("usuariurl")));
            }

            rs.getBytes("clauurl");
            if (!rs.wasNull()) {
                c.setClave(new String(rs.getBytes("clauurl")));
            }

            rs.getBoolean("sslurl");
            if (!rs.wasNull()) c.setSsl(rs.getBoolean("sslurl"));
            else c.setSsl(null);

            c.setCategoria(this.ctrlCategorias.get(rs.getInt("tipusquestio")));

            rs.getBytes("inits");
            if (!rs.wasNull()) {
                c.setInits(new String(rs.getBytes("inits")));
            }

            rs.getBytes("neteja");
            if (!rs.wasNull()) {
                c.setLimpieza(new String(rs.getBytes("neteja")));
            }

            c.setTipoSolucion(this.ctrlTiposSolucion.get(rs.getInt("tipussolucio")));

            if (c.getTipoSolucion().getId() == TipossolucionService.ID_FICHERO) {
                c.setFicheroSolucion(rs.getBytes("solucio"), rs.getString("extensiosolucio"));
            	//c.setFicheroSolucion(rs.getString("solucio"), rs.getString("extensiosolucio"));
            } else {
            	c.setSolucion(new String(rs.getBytes("solucio")));
            }

            c.setPostInits(rs.getString("postinits"));
            c.setPostInitsJP(rs.getInt("postinitjp"));

            rs.getBytes("fitxeradjunt");
            if (!rs.wasNull()) {
                c.setFicheroAdjunto(rs.getBytes("fitxeradjunt"), rs.getString("extensiofitxer"));
            }

            c.setDisponible(rs.getBoolean("resolta"));

            c.setEsquema(this.ctrlEsquemas.get(rs.getString("esquema")));

            cuestiones.add(c);
        }

        __selectJuegosPruebasAndTematicasAndCorrectores(cuestiones);
        __selectComprobaciones(cuestiones);

        // Collections.sort(cuestiones); //
    }

    private void __selectJuegosPruebasAndTematicasAndCorrectores(List<Cuestion> cuestiones) throws Exception
    {

        /**
         * @todo I això per què coi no està a `JuegopruebasDao`?????
         */

        if (cuestiones.size() == 0) {
            return;
        }

        ResultSet rs = null;
        Statement s = null;
        Tematica tem;
        Corrector cor;
        JuegoPruebas jp;
        Cuestion c = null;
        int id;

        Hashtable hashCuestiones = new Hashtable();
        Hashtable hashTematicas = new Hashtable();
        Hashtable hashCorrectores = new Hashtable();

        String selectJPs = "SELECT id, ordre, nomjp, descripcio, msgerror, injp, outjp, inits, neteja, pes, " +
                "binaria, missatge, consumeixintent, msgerroresp, msgerroring FROM t_jocsproves";
        String selectTematicas = "SELECT id, nom FROM t_tematiquesquestions";
        String selectCorrectores = "SELECT id, urlsw FROM t_urlswquestions";
        String where = "";

        for (Cuestion q : cuestiones) {
            where += " OR id = " + q.getId();
            hashCuestiones.put(q.getId(), q);
        }

        where = " WHERE " + where.substring(4) + " ORDER BY id ASC";

        try {

            s = con.createStatement();
            rs = s.executeQuery(selectJPs + where + ", ordre ASC");
            id = 0;

            while (rs.next()) {
                if (rs.getInt("id") != id) {
                    id = rs.getInt("id");
                    c = (Cuestion) hashCuestiones.get(id);
                }

                jp = new JuegoPruebas(rs.getString("nomjp"));
                c.addJuegoPruebas(jp);
                jp.setDescripcion(rs.getString("descripcio"));
                jp.setMsgError(rs.getString("msgerror"));
                jp.setMsgErrorEsp(rs.getString("msgerroresp"));
                jp.setMsgErrorIng(rs.getString("msgerroring"));
                jp.setPeso(rs.getFloat("pes"));

                rs.getBoolean("binaria");
                if (!rs.wasNull()) jp.setBinaria(rs.getBoolean("binaria"));
                else jp.setBinaria(null);

                rs.getBoolean("missatge");
                if (!rs.wasNull()) jp.setMensaje(rs.getBoolean("missatge"));
                else jp.setMensaje(null);

                rs.getBoolean("consumeixintent");
                if (!rs.wasNull()) jp.setConsumeix(rs.getBoolean("consumeixintent"));
                else jp.setConsumeix(null);

                rs.getBytes("injp");
                if (!rs.wasNull()) {
                    jp.setEntrada(new String(rs.getBytes("injp")));
                }
                rs.getBytes("outjp");
                if (!rs.wasNull()) {
                    jp.setSalida(new String(rs.getBytes("outjp")));
                }
                rs.getBytes("inits");
                if (!rs.wasNull()) {
                    jp.setInits(new String(rs.getBytes("inits")));
                }
                rs.getBytes("neteja");
                if (!rs.wasNull()) {
                    jp.setLimpieza(new String(rs.getBytes("neteja")));
                }
            }

            rs.close();

            List<Tematica> tematicas = this.ctrlTematicas.select();
            for (Tematica t : tematicas) {
                hashTematicas.put(t.getNombre(), t);
            }

            rs = s.executeQuery(selectTematicas+where+", nom");
            id = 0;

            while (rs.next()) {
            	if (rs.getInt("id") != id) {
                	id = rs.getInt("id");
                    c = (Cuestion) hashCuestiones.get(id);
                }

                tem = (Tematica) hashTematicas.get(rs.getString("nom"));
                c.addTematica(tem);
            }

            rs.close();

            List<Corrector> correctores = this.ctrlCorrectores.select();
            for (Corrector corr : correctores) {
                hashCorrectores.put(corr.getURL(), corr);
            }

            rs = s.executeQuery(selectCorrectores + where);
            id = 0;

            while (rs.next()) {
                if (rs.getInt("id") != id) {
                    id = rs.getInt("id");
                    c = (Cuestion) hashCuestiones.get(id);
                }


                cor = (Corrector) hashCorrectores.get(rs.getString("urlsw"));

                c.addCorrector(cor);
            }

            rs.close();

        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (s != null) {
                    s.close();
                }
            } catch (SQLException e) {
            }
        }
    }

    private void __selectComprobaciones(List<Cuestion> cuestiones) throws SQLException
    {
        if (cuestiones.size() == 0) {
            return;
        }

        Statement s = null;
        ResultSet rs = null;

        Comprobacion compr;
        boolean existeSiguiente;

        String select = "SELECT c.id, j.ordre AS ordrejp, c.ordre, c.nomcompr, c.descripcio, " +
                "c.msgerror, c.incompr, c.outcompr, c.binaria, c.consumeixintent, c.msgerroresp, c.msgerroring " +
                "FROM t_comprovacions c, t_jocsproves j " +
                "WHERE c.nomjp = j.nomjp AND c.id = j.id AND j.id IN (";
        String whereIn = "";
        for (Cuestion c : cuestiones) {
            whereIn += ", " + c.getId();
        }
        whereIn = whereIn.substring(2) + ")";
        select += whereIn + " ORDER BY c.id ASC, ordrejp ASC, c.ordre ASC";

        try {

            s = con.createStatement();
            rs = s.executeQuery(select);
            existeSiguiente = rs.next();

            for (Cuestion c : cuestiones) {
                for (JuegoPruebas jp : c.getJuegosPruebas()) {
                    while (existeSiguiente && rs.getInt("id") == c.getId() && rs.getInt("ordrejp") == jp.getOrden()) {
                        compr = new Comprobacion(rs.getString("nomcompr"));
                        jp.addComprobacion(compr);
                        compr.setDescripcion(rs.getString("descripcio"));
                        compr.setMsgError(rs.getString("msgerror"));
                        compr.setMsgErrorEsp(rs.getString("msgerroresp"));
                        compr.setMsgErrorIng(rs.getString("msgerroring"));

                        rs.getBoolean("binaria");
                        if (!rs.wasNull()) compr.setBinaria(rs.getBoolean("binaria"));
                        else compr.setBinaria(null);

                        rs.getBoolean("consumeixintent");
                        if (!rs.wasNull()) compr.setConsumeix(rs.getBoolean("consumeixintent"));
                        else compr.setConsumeix(null);

                        compr.setEntrada(new String(rs.getBytes("incompr")));
                        rs.getBytes("outcompr");
                        if (!rs.wasNull()) {
                            compr.setSalida(new String(rs.getBytes("outcompr")));
                        }
                        existeSiguiente = rs.next();
                    }
                }
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (s != null) {
                    s.close();
                }
            } catch (SQLException e) {
            }
        }
    }

    private void insertTematicas(List<Tematica> lista, int id) throws SQLException {
        psDeleteTematicas.setInt(1, id);
        psDeleteTematicas.executeUpdate();
        psInsertTematica.setInt(1, id);
        for (Tematica tem : lista)
        {
            psInsertTematica.setString(2, tem.getNombre());
            psInsertTematica.executeUpdate();
        }
    }

    private void updateResuelta(Cuestion c, boolean resuelta) throws SQLException {
        psUpdateResuelta.setBoolean(1, resuelta);
        psUpdateResuelta.setInt(2, c.getId());
        psUpdateResuelta.executeUpdate();
    }

} // Fin clase CuestionDao
