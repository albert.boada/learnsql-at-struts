//////////////////////////////////////////////////////
//  CLASSE: GestionDatosException.java              //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import catalogo.modelo.Comprobacion;
import catalogo.modelo.Cuestion;
import catalogo.modelo.JuegoPruebas;

public class GestionDatosException extends Exception {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/
    
    public final static int ERROR_CUESTION        = 1;
    public final static int ERROR_JUEGO_PRUEBAS   = 2;
    public final static int ERROR_COMPROBACION    = 3;
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    
    private int codigo;
    private Object elemento;
    
    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/
    
    public GestionDatosException(Cuestion c, String msg) {
        super(msg);
        this.codigo = ERROR_CUESTION;
        this.elemento = c;
    }
    
    public GestionDatosException(JuegoPruebas jp, String msg) {
        super(msg);
        this.codigo = ERROR_JUEGO_PRUEBAS;
        this.elemento = jp;
    }
    
    public GestionDatosException(Comprobacion compr, String msg) {
        super(msg);
        this.codigo = ERROR_COMPROBACION;
        this.elemento = compr;
    }

    public int getCodigo() {
        return codigo;
    }
    
    public Cuestion getCuestion() {
        if (elemento instanceof Cuestion) return (Cuestion) elemento;
        else return null;
    }
    
    public JuegoPruebas getJuegoPruebas() {
        if (elemento instanceof JuegoPruebas) return (JuegoPruebas) elemento;
        else return null;
    }
    
    public Comprobacion getComprobacion() {
        if (elemento instanceof Comprobacion) return (Comprobacion) elemento;
        else return null;
    }
    
} // Fin clase GestionDatosException
