//////////////////////////////////////////////////////
//  CLASSE: CorrectorDao.java                    //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.gestiondatos;

import catalogo.modelo.Corrector;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CtrlCorrectores {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private PreparedStatement psInsert, psDelete, psUpdate1, psUpdate2, psDeferredFK, psImmediateFK;
    private PreparedStatement psUpdCuestiones, psUpdJuegosPruebas, psUpdComprobaciones;
    private Connection con;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public CtrlCorrectores(Connection c) throws Exception {
        Statement s;
        ResultSet rs;
        con = c;

        try {
            psInsert = con.prepareStatement("INSERT INTO t_urlsw(urlsw, descripcio) VALUES(?, ?)");
            psDelete = con.prepareStatement("DELETE FROM t_urlsw WHERE urlsw = ?");
            psUpdate1 = con.prepareStatement("UPDATE t_urlsw SET urlsw = ?, descripcio = ? WHERE urlsw = ?");
            psUpdate2 = con.prepareStatement("UPDATE t_urlswquestions SET urlsw = ? WHERE urlsw = ?");
            psDeferredFK = con.prepareStatement("SET CONSTRAINTS t_urlswquestions_urlsw_fkey DEFERRED");
            psImmediateFK = con.prepareStatement("SET CONSTRAINTS t_urlswquestions_urlsw_fkey IMMEDIATE");
            psUpdComprobaciones = con.prepareStatement("UPDATE t_comprovacions SET outcompr = NULL WHERE id IN (SELECT u.id FROM t_urlswquestions u WHERE u.urlsw = ?)");
            psUpdJuegosPruebas = con.prepareStatement("UPDATE t_jocsproves SET outjp = NULL WHERE id IN (SELECT u.id FROM t_urlswquestions u WHERE u.urlsw = ?)");
            psUpdCuestiones = con.prepareStatement("UPDATE t_questions SET resolta = false WHERE id IN (SELECT u.id FROM t_urlswquestions u WHERE u.urlsw = ?)");
        }
        catch (SQLException e) {
            throw e;
        }
    }

    public List<Corrector> select() throws Exception {
        Statement s = null;
        ResultSet rs = null;
        List<Corrector> correctores = null;

        try {
            s = con.createStatement();
            rs = s.executeQuery("SELECT urlsw, descripcio FROM t_urlsw");
            correctores = new ArrayList<Corrector>();

            while (rs.next()) {
                correctores.add(new Corrector(rs.getString("urlsw"), rs.getString("descripcio")));
            }
            Collections.sort(correctores);
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
        finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
            try { if (s != null) s.close(); } catch (SQLException e) {}
        }
        return correctores;
    }

    public Corrector get(String url) throws SQLException {
        Statement s = null;
        ResultSet rs = null;
        Corrector corrector = null;

        try {
            s = con.createStatement();
            rs = s.executeQuery("SELECT urlsw, descripcio FROM t_urlsw WHERE urlsw='"+url+"'");
            //corrector = new Corrector();

            while (rs.next()) {
                corrector=new Corrector(rs.getString("urlsw"), rs.getString("descripcio"));
            }
            //Collections.sort(correctores);
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            throw e;
        }
        finally {
            try { if (rs != null) rs.close(); } catch (SQLException e) {}
            try { if (s != null) s.close(); } catch (SQLException e) {}
        }
        return corrector;
    }



    // Métodos para gestionar correctores *************************************

    public void insert(String url, String descripcion) throws Exception  {
        try {
            psInsert.setString(1, url);
            psInsert.setString(2, descripcion);
            psInsert.executeUpdate();
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY)) throw new Exception("Ya existe un corrector con esa URL");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR)) throw new Exception("Existen caracteres no admitidos por la base de datos");
            else throw e;
        }
    }

    public void update(String urlOld, String urlNew, String descripcion) throws Exception {
        try {
            if (!urlOld.equals(urlNew)) {
                psUpdComprobaciones.setString(1, urlOld);
                psUpdComprobaciones.executeUpdate();
                psUpdJuegosPruebas.setString(1, urlOld);
                psUpdJuegosPruebas.executeUpdate();
                psUpdCuestiones.setString(1, urlOld);
                psUpdCuestiones.executeUpdate();
                psDeferredFK.executeUpdate();
                psUpdate2.setString(1, urlNew);
                psUpdate2.setString(2, urlOld);
                psUpdate2.executeUpdate();
            }

            psUpdate1.setString(1, urlNew);
            psUpdate1.setString(2, descripcion);
            psUpdate1.setString(3, urlOld);
            psUpdate1.executeUpdate();

            if (!urlOld.equals(urlNew)) psImmediateFK.executeUpdate();
            con.commit();
        }
        catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            if (e.getSQLState().equals(CodigosSQL.PRIMARY_KEY)) throw new Exception("Ya existe un corrector con esa URL");
            else if (e.getSQLState().equals(CodigosSQL.UNTRANSLATABLE_CHAR)) throw new Exception("Existen caracteres no admitidos por la base de datos");
            else throw e;
        }
    }

    public void delete(String url) throws Exception {
        try {
            psDelete.setString(1, url);
            psDelete.executeUpdate();
            con.commit();
        }
         catch (SQLException e) {
            try { con.rollback(); } catch (SQLException e2) {}
            if (e.getSQLState().equals(CodigosSQL.FOREIGN_KEY)) throw new Exception("Existen cuestiones asociadas al corrector");
            else throw e;
        }
    }

} // Fin clase CorrectorDao

