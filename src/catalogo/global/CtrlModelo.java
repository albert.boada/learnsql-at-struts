//////////////////////////////////////////////////////
//  CLASSE: CtrlModelo.java                         //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.global;

import java.io.*;

import catalogo.modelo.*;
import catalogo.gestiondatos.*;
import catalogo.comunicacion.CorrectorCliente;
import catalogo.comunicacion.CorrectorStub;

import java.sql.Connection;
import java.util.Properties;

public class CtrlModelo {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private final static String PATH_CONFIG       = "config/conexion.txt";
    private final static int TIMEOUT_DEFAULT      = 10000;

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    public static WrapperConexion wrapperConexion;
    public static TipossolucionService tiposSolucion;
    public static DiccIdiomas idiomas;
    public static CategoriasService categorias;
    public static EsquemasService esquemas;
    public static TematicasService tematicas;
    public static CorrectoresService correctores;
    public static CuestionesService catalogo;
    public static SaveOnCommit saveOnCommit;

    public static Properties getCfgConexion() throws Exception {
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        Properties p = null;
        String sgbd, host, puerto, servidor, nombre;
        boolean ssl;

        try {
            //fis = new FileInputStream(PATH_CONFIG);
            //bis = new BufferedInputStream(fis);
            InputStream is = CtrlModelo.class.getResourceAsStream("/config/conexion.txt");
    		bis = new BufferedInputStream(is);

            p = new Properties();
            p.load(bis);

            if (p.containsKey("timeout")) CorrectorStub.TIMEOUT = Integer.parseInt(p.getProperty("timeout"));
            else CorrectorStub.TIMEOUT = TIMEOUT_DEFAULT;

            if (!p.containsKey("sgbd")) throw new Exception("Error en el fichero de config.: No se ha hallado la entrada \"sgbd\"");
            else if (!p.containsKey("host")) throw new Exception("Error en el fichero de config.: No se ha hallado la entrada \"host\"");
            else if (!p.containsKey("puerto")) throw new Exception("Error en el fichero de config.: No se ha hallado la entrada \"puerto\"");
            else if (!p.containsKey("servidor")) throw new Exception("Error en el fichero de config.: No se ha hallado la entrada \"servidor\"");
            else if (!p.containsKey("nombre")) throw new Exception("Error en el fichero de config.: No se ha hallado la entrada \"nombre\"");

        } catch (FileNotFoundException e) {
            throw new Exception("No se ha podido abrir el fichero \"" + PATH_CONFIG + "\" con los datos de conexión: " +e.getMessage());
        } catch (IOException e) {
            throw new Exception("No se ha podido leer los datos de conexión: " + e.getMessage());
        }
        finally {
            try { if (bis != null) bis.close(); } catch (IOException e) {}
            try { if (fis != null) fis.close(); } catch (IOException e) {}
        }
        return p;
    }

    public static void initConexion(Properties p, String usuario, String clave) throws Exception {
        String sgbd, host, puerto, servidor, nombre;
        boolean ssl;

        sgbd = p.getProperty("sgbd").trim();
        host = p.getProperty("host").trim();
        puerto = p.getProperty("puerto").trim();
        nombre = p.getProperty("nombre").trim();
        servidor = p.getProperty("servidor").trim();
        if (p.containsKey("ssl") && p.getProperty("ssl").trim().equalsIgnoreCase("true")) ssl = true;
        else ssl = false;

        wrapperConexion = new WrapperConexion(sgbd, host, puerto, servidor, nombre, usuario, clave, ssl);
        wrapperConexion.conectar();
    }

    public static void initCtrl() throws Exception
    {
        CorrectorCliente cliente = new CorrectorCliente(wrapperConexion.getHost(), wrapperConexion.getNombre());

        Connection con = wrapperConexion.getConexion();

        CtrlCategorias categoria_dao        = new CtrlCategorias(con);
        CtrlEsquemas esquema_dao            = new CtrlEsquemas(con);
        CtrlTematicas tematica_dao          = new CtrlTematicas(con);
        CtrlCorrectores corrector_dao        = new CtrlCorrectores(con);
        CtrlJuegoPruebas jp_dao            = new CtrlJuegoPruebas(con);
        CtrlComprobacion comprobacion_dao  = new CtrlComprobacion(con);
        CtrlTiposSolucion tipossolucion_dao = new CtrlTiposSolucion();
        CtrlCuestion cuestion_dao          = new CtrlCuestion(con, categoria_dao, tematica_dao, esquema_dao, tipossolucion_dao, corrector_dao);

        CtrlModelo.categorias    = new CategoriasService(categoria_dao);
        CtrlModelo.esquemas      = new EsquemasService(esquema_dao);
        CtrlModelo.tematicas     = new TematicasService(tematica_dao);
        CtrlModelo.correctores   = new CorrectoresService(corrector_dao, CtrlModelo.categorias, cliente);
        CtrlModelo.tiposSolucion = new TipossolucionService(tipossolucion_dao);
        CtrlModelo.catalogo      = new CuestionesService(cuestion_dao, jp_dao, comprobacion_dao, cliente, wrapperConexion.getUsuario());

        CtrlModelo.idiomas       = new DiccIdiomas();

        CtrlModelo.saveOnCommit  = new SaveOnCommit(cuestion_dao, jp_dao, comprobacion_dao);
    }

} // fin clase CtrlModelo
