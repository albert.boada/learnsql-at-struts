//////////////////////////////////////////////////////
//  CLASSE: CtrlAyuda.java                          //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.global;

import java.io.File;
import org.jdesktop.jdic.desktop.Desktop;
import org.jdesktop.jdic.desktop.DesktopException;

public class CtrlAyuda {
    
    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private final static String PATH_MANUAL     = "ayuda/manual.pdf";
    private final static String PATH_CATEGORIAS = "ayuda/categorias.pdf";

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private static File ficheroAyuda = null;
    private static File ficheroCategorias = null;
    
    public static void showHelp() throws Exception {
        if (ficheroAyuda == null) ficheroAyuda = new File(PATH_MANUAL);
        try {
            Desktop.open(ficheroAyuda);
        } catch (DesktopException e) {
            throw new Exception("No se ha podido abrir el fichero de ayuda \"" + PATH_MANUAL + "\":\n" + e.getMessage());
        }
    }
    
    public static void showCategorias() throws Exception {
        if (ficheroCategorias == null) ficheroCategorias = new File(PATH_CATEGORIAS);
        try {
            Desktop.open(ficheroCategorias);
        } catch (DesktopException e) {
            throw new Exception("No se ha podido abrir el fichero de categorías \"" + PATH_CATEGORIAS + "\":\n" + e.getMessage());
        }
    }

} // Fin clase CtrlAyuda
