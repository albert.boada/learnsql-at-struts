//////////////////////////////////////////////////////
//  CLASSE: CtrlVista.java                          //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.global;

import catalogo.presentacion.FramePrincipal;
import catalogo.presentacion.PanelCatalogo;
import catalogo.presentacion.PanelCuestion;
import catalogo.modelo.Corrector;
import catalogo.modelo.Cuestion;
import catalogo.modelo.Esquema;
import catalogo.modelo.Tematica;
import catalogo.modelo.Categoria;
import catalogo.soporte.copypaste.ContextMouseAdapter;

public class CtrlVista {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private static FramePrincipal framePrincipal;
    private static PanelCatalogo panelCatalogo;
    private static PanelCuestion panelCuestion;
    public static ContextMouseAdapter contextMouseAdapter;
    
    public static void initVista(FramePrincipal frame, PanelCatalogo pCatalogo, PanelCuestion pCuestion) {
        framePrincipal = frame;
        panelCatalogo = pCatalogo;
        panelCuestion = pCuestion;
    }
    
    public static void initCtrl() {
        contextMouseAdapter = new ContextMouseAdapter();
    }
    
    public static void setControlesModif(boolean enabled) {
        framePrincipal.setEnabledMenu(!enabled);
        panelCatalogo.setEnabledControlesBusqueda(!enabled);
    }
    
    public static void clearSalidas(Corrector cor) {
        panelCuestion.clearSalidas(cor);
    }
    
    public static void setSelectedCuestion(Cuestion c) {
        panelCatalogo.setSelected(c);
    }
    
    public static void creaCuestion() {
        panelCatalogo.setSelected(null);
        panelCuestion.muestraCuestion();
    }

    public static void setTituloModif(Cuestion c){
        panelCuestion.ModificaCorrector(c);
    }

    public static void ejecuta(Cuestion c){
        panelCuestion.ejecuta(c);
    }
    
    public static boolean guarda() {
        return panelCuestion.guarda();
    }
    
    public static boolean esquemaEnUso(Esquema esq) {
        return (esq == panelCuestion.getSelectedEquema());
    }
    
    public static boolean tematicaEnUso(Tematica tem) {
        return panelCuestion.getSelectedTematicas().contains(tem);
    }

    public static boolean categoriaEnUso(Categoria cat) {
        return (cat == panelCuestion.getSelectedCategoria());
    }

} // fin clase CtrlVista
