//////////////////////////////////////////////////////
//  CLASSE: CorrectorCliente.java                   //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.comunicacion;

import catalogo.comunicacion.CorrectorStub.*;
import catalogo.soporte.utils.TextParser;
import catalogo.soporte.modelos.ArrayListModel;
import catalogo.modelo.Resultado;
import java.rmi.RemoteException;
import org.apache.axis2.AxisFault;

public class CorrectorCliente {
    
    public final static int SALIDAS_DIFERENTES  = -120;
    public final static int SALIDA_NULA         = -121;
    public final static int SIN_BD_CORRECCIONES = -122;
    
    private final static int ERROR_CODIGO = -1;
    private final static String ERROR_MENSAJE = "No se puede acceder al servicio web indicado, verifique la URL";
    
    private CorrectorStub stub;
    private String host, nombre, url, msg, salidaExistente, salidaGenerada;
    private int ordenJP, ordenCompr;
    
    public CorrectorCliente(String hostBD, String nombreBD) {
        host = hostBD;
        nombre = nombreBD;
        url = null;
        msg = null;
        salidaExistente = null;
        salidaGenerada = null;
        ordenJP = 0;
        ordenCompr = 0;
    }
    
    public int checkRefereeStatus(String urlReferee) {   
        int codigo;
        CheckRefereeStatusRequest request = new CheckRefereeStatusRequest();
        CheckRefereeStatusRequestParameters params = new CheckRefereeStatusRequestParameters();
        request.setCheckRefereeStatusRequest(params);
        try {
            if (url == null  || (url.compareTo(urlReferee) != 0)) setURL(urlReferee);
            CheckRefereeStatusResponse response = stub.checkRefereeStatus(request);
            codigo = response.getCheckRefereeStatusResponse().getCode();
            msg = response.getCheckRefereeStatusResponse().getMessage();
        }
        catch (RemoteException e) {
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println("ERROR AL CONSULTAR ESTADO");
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println(e.getMessage());
            System.err.println("--------------------------------------------------------------------------------");
            codigo = ERROR_CODIGO;
            msg = ERROR_MENSAJE;
        }
        return codigo;
    }
    
    public int beginRefereeingProcess(String urlCorrector, int idCuestion, int idCategoria, int idTipoSol, String[] s) {
        int codigo;
        BeginRefereeingProcessRequest request = new BeginRefereeingProcessRequest();
        BeginRefereeingProcessRequestParameters params = new BeginRefereeingProcessRequestParameters();
        Solutions sol = new Solutions();
        sol.setSolution(s);
        params.setMoodle("Authoring tool");
        params.setPolicy(2);
        params.setRemoteQuestionId(idCuestion);
        params.setRemoteQuestionType(idCategoria);
        params.setSolutionType(idTipoSol);
        params.setSolutions(sol);
        params.setDbHost(host);
        params.setDbName(nombre);
        params.setUserId(1);
        request.setBeginRefereeingProcessRequest(params);
        try {
            if (url == null || (url.compareTo(urlCorrector) != 0)) setURL(urlCorrector);
            BeginRefereeingProcessResponse response = stub.beginRefereeingProcess(request);
            BeginRefereeingProcessResponseParameters responseParams = response.getBeginRefereeingProcessResponse();
            codigo = responseParams.getCode();
            msg = responseParams.getMessage();
        }
        catch(RemoteException e) {
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println("ERROR AL INICIAR CORRECCIÓN");
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println(e.getMessage());
            System.err.println("--------------------------------------------------------------------------------");
            codigo = ERROR_CODIGO;
            msg = ERROR_MENSAJE;
        }
        return codigo;
    }
    
    public ArrayListModel<Resultado> checkRefereeingProcessStatus(String urlCorrector, int idCuestion, int idCategoria, int idTipoSol) {
        ArrayListModel<Resultado> resultado = new ArrayListModel();
        int codigo;
        int contlinea = 0;
        int comprobacion = 0;
        String salida ="";
        CheckRefereeingProcessStatusRequest request = new CheckRefereeingProcessStatusRequest();
        CheckRefereeingProcessStatusRequestParameters params = new CheckRefereeingProcessStatusRequestParameters();
        params.setMoodle("Authoring tool");
        params.setRemoteQuestionId(idCuestion);
        params.setDbHost(this.host);
        params.setDbName(this.nombre);
        params.setUserId(1);
        request.setCheckRefereeingProcessStatusRequest(params);
        try {
            if (url == null || (url.compareTo(urlCorrector) != 0)) setURL(urlCorrector);
            CheckRefereeingProcessStatusResponse response = stub.checkRefereeingProcessStatus(request);
            RefereeResponseParameters[] responseParams = response.getCheckRefereeingProcessStatusResponse().getParameters();
            for(int f=0;f<responseParams.length;f++){
                if(responseParams[f].getNumComp()>0)comprobacion = responseParams[f].getNumComp();
                else comprobacion = 0;
                if(responseParams[f].getCode()<=0 || responseParams[f].getCode()==100) {
                    contlinea = 1;
                    salida=responseParams[f].getMessage();
                }
                else {
                    contlinea = TextParser.getContador(responseParams[f].getOutput());
                    salida = TextParser.parseSalida(responseParams[f].getOutput());
                }
                Resultado r = new Resultado(responseParams[f].getCode(), responseParams[f].getMessage(), 
                        responseParams[f].getTestOrder(),comprobacion,salida,contlinea);
                resultado.add(r);
            }
        }
        catch(RemoteException e) {
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println("ERROR AL CONSULTAR ESTADO DE LA CORRECCIÓN");
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println(e.getMessage());
            System.err.println("--------------------------------------------------------------------------------");
            codigo = ERROR_CODIGO;
            msg = ERROR_MENSAJE;
        }
        catch(Exception ev){
            System.out.println(ev.getMessage());
        }
        return resultado;
    }
    
    public int generateOutputs(String urlCorrector, int idCuestion, int idCategoria, int idTipoSol) {
        int codigo;
        SolverRequest request = new SolverRequest();
        SolverRequestParameters params = new SolverRequestParameters();
        params.setRemoteQuestionId(idCuestion);
        params.setRemoteQuestionType(idCategoria);
        params.setSolutionType(idTipoSol);
        params.setDbHost(this.host);
        params.setDbName(this.nombre);
        request.setSolverRequest(params);
        try {
            if (url == null || (url.compareTo(urlCorrector) != 0)) setURL(urlCorrector);
            SolverResponseParameters responseParams = stub.generateOutputs(request).getSolverResponse();
            codigo = responseParams.getCode();
            msg = responseParams.getMessage();
            ordenJP = responseParams.getTestOrder();
            ordenCompr = responseParams.getVerificationOrder();
        }
        catch(RemoteException e) {
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println("ERROR AL GENERAR SALIDAS");
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println(e.getMessage());
            System.err.println("--------------------------------------------------------------------------------");
            codigo = ERROR_CODIGO;
            msg = ERROR_MENSAJE;
        }
        return codigo;
    }
    
    public int compareOutputs(String urlReferee, int idCuestion, int idCategoria, int idTipoSol) {
        salidaExistente = null;
        salidaGenerada = null;
        int codigo = 0;
        SolverRequest request = new SolverRequest();
        SolverRequestParameters params = new SolverRequestParameters();
        params.setRemoteQuestionId(idCuestion);
        params.setRemoteQuestionType(idCategoria);
        params.setSolutionType(idTipoSol);
        params.setDbHost(this.host);
        params.setDbName(this.nombre);
        request.setSolverRequest(params);
        try {
            if (url == null || (url.compareTo(urlReferee) != 0)) setURL(urlReferee);
            SolverResponseParameters responseParams = stub.compareOutputs(request).getSolverResponse();
            codigo = responseParams.getCode();
            msg = responseParams.getMessage();
            ordenJP = responseParams.getTestOrder();
            ordenCompr = responseParams.getVerificationOrder();
            if (responseParams.getExistingOutput() != null) {
                salidaExistente = TextParser.parseSalida(responseParams.getExistingOutput());
                salidaGenerada = TextParser.parseSalida(responseParams.getGeneratedOutput());
            }
        }
        catch(RemoteException e) {
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println("ERROR AL COMPARAR SALIDAS");
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println(e.getMessage());
            System.err.println("--------------------------------------------------------------------------------");
            codigo = ERROR_CODIGO;
            msg = ERROR_MENSAJE;
        }
        return codigo;
    }
    
    public String getDBMS(String urlReferee) {
        String sgbd = null;
        GetDBMSRequest request = new GetDBMSRequest();
        GetDBMSRequestParameters params = new GetDBMSRequestParameters();
        request.setGetDBMSRequest(params);
        try {
            if (url == null || (url.compareTo(urlReferee) != 0)) setURL(urlReferee);
            GetDBMSResponseParameters responseParams = stub.getDBMS(request).getGetDBMSResponse();
            sgbd = responseParams.getDbms();
        }
        catch(RemoteException e) {
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println("ERROR AL CONSULTAR SGBD (" + urlReferee + ")");
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println(e.getMessage());
            System.err.println("--------------------------------------------------------------------------------");
            msg = ERROR_MENSAJE;
        }
        return sgbd;
    }
    
    public int[] getQuestionTypes(String urlReferee) {
        int[] categoriasAceptadas = null;
        GetQuestionTypesRequest request = new GetQuestionTypesRequest();
        GetQuestionTypesRequestParameters params = new GetQuestionTypesRequestParameters();
        request.setGetQuestionTypesRequest(params);
        try {
            if (url == null || (url.compareTo(urlReferee) != 0)) setURL(urlReferee);
            GetQuestionTypesResponseParameters responseParams = stub.getQuestionTypes(request).getGetQuestionTypesResponse();
            categoriasAceptadas = responseParams.getQuestionType();
        }
        catch(RemoteException e) {
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println("ERROR AL CONSULTAR CATEGORIAS (" + urlReferee + ")");
            System.err.println("--------------------------------------------------------------------------------");
            System.err.println(e.getMessage());
            System.err.println("--------------------------------------------------------------------------------");
            msg = ERROR_MENSAJE;
        }
        return categoriasAceptadas;
    }
    
    private String getURL() {
        return url;
    }
    
    private void setURL(String urlReferee) throws AxisFault {
        try {
            url = urlReferee;
            stub = new CorrectorStub(url);
        }
        catch (AxisFault e) {
            throw e;
        }
    }
    
    public String getMensaje() {
        return msg;
    }
    
    public String getSalidaExistente() {
        return salidaExistente;
    }
    
    public String getSalidaGenerada() {
        return salidaGenerada;
    }
    
    public int getOrdenJuegoPruebas() {
        return ordenJP;
    }
    
    public int getOrdenComprobacion() {
        return ordenCompr;
    }

} // Fin clase CorrectorCliente
