//////////////////////////////////////////////////////
//  CLASSE: DialogCategorias.java                   //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import java.beans.*;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import catalogo.modelo.Categoria;
import catalogo.modelo.DiccCategorias;
import catalogo.soporte.modelos.TableModelSeleccionCategorias;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import java.util.*; //pel resourcebundle...
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.AlternateRowHighlighter;
import org.jdesktop.swingx.decorator.HighlighterPipeline;

public class DialogCategorias extends JDialog {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private TableModelSeleccionCategorias tableModel;
    private boolean aceptado;
    ResourceBundle recursos;

    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/

    public DialogCategorias(Frame owner, DiccCategorias categorias, ResourceBundle rec) {
        super(owner, true);
        recursos = rec;
        initComponents();
        inicializa(categorias);
    }

    public DialogCategorias(Dialog owner, DiccCategorias categorias,  ResourceBundle rec) {
        super(owner, true);
        recursos = rec;
        initComponents();
        inicializa(categorias);
    }

    public void setRecursos( ResourceBundle rec) {
        recursos = rec;
    }

    public void setSeleccion(List<Categoria> seleccion) {
        tableModel.setSeleccion(new ArrayList(seleccion));
    }

    public List<Categoria> getSeleccion() {
        return tableModel.getSeleccion();
    }

    public boolean haAceptado() {
        return aceptado;
    }

    private void inicializa(DiccCategorias categorias) {
        setSize(700, 500);
        tableModel = new TableModelSeleccionCategorias(categorias.getAll());
        tableCategorias.setModel(tableModel);
        tableCategorias.getColumnModel().getColumn(0).setResizable(false);
        tableCategorias.getColumnModel().getColumn(0).setPreferredWidth(75);
        tableCategorias.getColumnModel().getColumn(1).setPreferredWidth(150);
        tableCategorias.getColumnModel().getColumn(2).setPreferredWidth(600);
    }

    private void buttonAceptarActionPerformed() {
        aceptado = true;
        this.setVisible(false);
    }

    private void buttonCancelarActionPerformed() {
        aceptado = false;
        this.setVisible(false);
    }

    private void thisPropertyChange() {
        aceptado = false;
    }

    private void initComponents() {
		// Component initialization
		scrollPane1 = new JScrollPane();
		tableCategorias = new JXTable() {
		    public String getToolTipText(MouseEvent e) {
		        String tip = null;
		        java.awt.Point p = e.getPoint();
				int rowIndex = rowAtPoint(p);
		        int colIndex = columnAtPoint(p);
				int realColIndex = convertColumnIndexToModel(colIndex);
				if (realColIndex == 0) tip = ((Boolean) getValueAt(rowIndex, colIndex)) ? recursos.getString("seleccionado") : recursos.getString("noseleccionado");
				else tip = (String) getValueAt(rowIndex, colIndex);
		        return tip;
		    }
		};
		buttonAceptar = new JButton();
		buttonCancelar = new JButton();
		pipeline = new HighlighterPipeline();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle(recursos.getString("dcattitle"));
		addPropertyChangeListener("visible", new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				thisPropertyChange();
			}
		});
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			new ColumnSpec[] {
				FormFactory.UNRELATED_GAP_COLSPEC,
				new ColumnSpec("max(default;150dlu):grow"),
				new ColumnSpec(Sizes.dluX(60)),
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec(Sizes.dluX(60)),
				FormFactory.UNRELATED_GAP_COLSPEC
			},
			new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC,
				new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.UNRELATED_GAP_ROWSPEC
			}));

		//======== scrollPane1 ========
		{
			//---- tableCategorias ----
			tableCategorias.setSortable(false);
			tableCategorias.setHighlighters(pipeline);
			scrollPane1.setViewportView(tableCategorias);
		}
		contentPane.add(scrollPane1, cc.xywh(2, 2, 4, 1));

		//---- buttonAceptar ----
		buttonAceptar.setText(recursos.getString("dcataceptar"));
		buttonAceptar.setIcon(new ImageIcon(getClass().getResource("/imagenes/tick.png")));
		buttonAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonAceptarActionPerformed();
			}
		});
		contentPane.add(buttonAceptar, cc.xy(3, 4));

		//---- buttonCancelar ----
		buttonCancelar.setText(recursos.getString("dcatcancelar"));
		buttonCancelar.setIcon(new ImageIcon(getClass().getResource("/imagenes/cross.png")));
		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCancelarActionPerformed();
			}
		});
		contentPane.add(buttonCancelar, cc.xy(5, 4));
		pack();
		setLocationRelativeTo(getOwner());

		//---- pipeline ----
		pipeline.addHighlighter(new AlternateRowHighlighter());
		// End of component initialization
    }

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private JScrollPane scrollPane1;
	private JXTable tableCategorias;
	private JButton buttonAceptar;
	private JButton buttonCancelar;
	private HighlighterPipeline pipeline;

} // Fin clase DialogCategorias
