//////////////////////////////////////////////////////
//  CLASSE: DialogCorrectores.java                  //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import java.beans.*;
import catalogo.global.CtrlModelo;
import catalogo.modelo.Categoria;
import catalogo.modelo.Corrector;
import catalogo.modelo.DiccCorrectores;
import catalogo.soporte.modelos.TableModelSeleccionCorrectores;
import java.awt.event.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.*;

public class DialogCorrectores extends JDialog {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private TableModelSeleccionCorrectores tableModel;
    private boolean aceptado;
    private DialogEspera dialogEspera;
    private SwingWorker workerProp;
    private SwingWorkerCompletionWaiter waiterProp;
    private DiccCorrectores diccCorrectores;
    ResourceBundle recursos;
    
    public DialogCorrectores(Frame owner, DiccCorrectores dicc, ResourceBundle rec) {
        super(owner, true);
        recursos = rec;
        initComponents();
        inicializa(dicc);
    }
    
    public DialogCorrectores(Dialog owner, DiccCorrectores dicc, ResourceBundle rec) {
        super(owner, true);
        recursos = rec;
        initComponents();
        inicializa(dicc);
    }

    public void setRecursos(ResourceBundle rec){
        recursos = rec;
    }
    
    public void setSeleccion(List<Corrector> seleccion, Categoria cat) {
        labelCategoria.setText(cat.getDescripcio());
        tableModel.setSeleccion(new ArrayList(seleccion), cat);
    }
    
    public List<Corrector> getSeleccion() {
        return tableModel.getSeleccion();
    }
    
    public boolean haAceptado() {
        return aceptado;
    }
    
    private void inicializa(DiccCorrectores dicc) {
        dialogEspera = null;
        diccCorrectores= dicc;
        setSize(800, 500);
        tableModel = new TableModelSeleccionCorrectores(CtrlModelo.correctores.getAll());
        tableCorrectores.setModel(tableModel);
        tableCorrectores.getColumnModel().getColumn(0).setResizable(false);
        tableCorrectores.getColumnModel().getColumn(0).setPreferredWidth(75);
        tableCorrectores.getColumnModel().getColumn(1).setPreferredWidth(525);
        tableCorrectores.getColumnModel().getColumn(2).setPreferredWidth(150);
        tableCorrectores.getColumnModel().getColumn(3).setPreferredWidth(400); // 125 + 275
        tableCorrectores.getColumnExt(4).setVisible(false);
    }
    
    private void creaWorker(boolean getPropsTodos) {
        final boolean todos = getPropsTodos;
        if (dialogEspera == null) {
            dialogEspera = new DialogEspera(this, recursos.getString("corworkerespera2"));
            waiterProp = new SwingWorkerCompletionWaiter(dialogEspera);
        }
        dialogEspera.setLocationRelativeTo(getOwner());
        
        workerProp = new SwingWorker<Boolean, Void>() {
            @Override
            public Boolean doInBackground() {
                diccCorrectores.setPropiedades(todos);
                return new Boolean(todos);
            }
            
            @Override
            public void done() {
                try {
                    get();
                    tableModel.fireTableDataChanged();
                } catch (InterruptedException ignore) {} catch (java.util.concurrent.ExecutionException e) {
                    Throwable cause = e.getCause();
                    if (cause != null) System.err.println(cause.getMessage());
                    else System.err.println(e.getMessage());
                }
            }
        };
        
        workerProp.addPropertyChangeListener(waiterProp);
    }
    
    private void buttonAceptarActionPerformed() {
        aceptado = true;
        this.setVisible(false);
    }
    
    private void buttonCancelarActionPerformed() {
        aceptado = false;
        this.setVisible(false);
    }
    
    private void thisPropertyChange() {
        aceptado = false;
        setPropiedades(false);
    }
    
    private void setPropiedades(boolean todos) {
        boolean faltaInit = false;
        if (!todos) {
            for (Corrector cor : diccCorrectores.getAll()) {
                if (cor.getEstado() == Corrector.DESCONOCIDO) {
                    faltaInit = true;
                    break;
                }
            }
        }
        if (faltaInit || todos) {
            creaWorker(todos);
            workerProp.execute();
            dialogEspera.setVisible(true);
        }
    }
    
    private void buttonActualizarActionPerformed() {
        setPropiedades(true);
    }
    
    private void initComponents() {
		// Component initialization
		label1 = new JLabel();
		labelCategoria = new JLabel();
		scrollPane1 = new JScrollPane();
		tableCorrectores = new JXTable() {
		    public String getToolTipText(MouseEvent e) {
		        String tip = null;
		        java.awt.Point p = e.getPoint();
				int rowIndex = rowAtPoint(p);
		        int colIndex = columnAtPoint(p);
				int realColIndex = convertColumnIndexToModel(colIndex);
				if (realColIndex == 0) tip = ((Boolean) getValueAt(rowIndex, colIndex)) ? recursos.getString("seleccionado") : recursos.getString("noseleccionado");
				else if (realColIndex == 1) tip = (String) getModel().getValueAt(rowIndex, 4);
				else tip = (String) getValueAt(rowIndex, colIndex);
		        return tip;
		    }
		};
		buttonActualizar = new JButton();
		buttonAceptar = new JButton();
		buttonCancelar = new JButton();
		pipeline = new HighlighterPipeline();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle(recursos.getString("dcortitle"));
		addPropertyChangeListener("visible", new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				thisPropertyChange();
			}
		});
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			new ColumnSpec[] {
				FormFactory.UNRELATED_GAP_COLSPEC,
				new ColumnSpec(Sizes.dluX(60)),
				new ColumnSpec(Sizes.dluX(65)),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				new ColumnSpec("max(default;150dlu):grow"),
				new ColumnSpec(Sizes.dluX(60)),
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec(Sizes.dluX(60)),
				FormFactory.UNRELATED_GAP_COLSPEC
			},
			new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.LINE_GAP_ROWSPEC,
				new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.UNRELATED_GAP_ROWSPEC
			}));

		//---- label1 ----
		label1.setText(recursos.getString("dcorlabel1"));
		contentPane.add(label1, cc.xywh(2, 2, 2, 1));

		//---- labelCategoria ----
		labelCategoria.setText(recursos.getString("dcorlabel2"));
		contentPane.add(labelCategoria, cc.xywh(5, 2, 4, 1));

		//======== scrollPane1 ========
		{

			//---- tableCorrectores ----
			tableCorrectores.setSortable(false);
			tableCorrectores.setHighlighters(pipeline);
			scrollPane1.setViewportView(tableCorrectores);
		}
		contentPane.add(scrollPane1, cc.xywh(2, 4, 7, 1));

		//---- buttonActualizar ----
		buttonActualizar.setText(recursos.getString("dcoractualizar"));
		buttonActualizar.setToolTipText(recursos.getString("dcoractualizarToolTip"));
		buttonActualizar.setIcon(new ImageIcon(getClass().getResource("/imagenes/play.png")));
		buttonActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonActualizarActionPerformed();
			}
		});
		contentPane.add(buttonActualizar, cc.xy(2, 6));

		//---- buttonAceptar ----
		buttonAceptar.setText(recursos.getString("dcoraceptar"));
		buttonAceptar.setIcon(new ImageIcon(getClass().getResource("/imagenes/tick.png")));
		buttonAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonAceptarActionPerformed();
			}
		});
		contentPane.add(buttonAceptar, cc.xy(6, 6));

		//---- buttonCancelar ----
		buttonCancelar.setText(recursos.getString("dcorcancelar"));
		buttonCancelar.setIcon(new ImageIcon(getClass().getResource("/imagenes/cross.png")));
		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCancelarActionPerformed();
			}
		});
		contentPane.add(buttonCancelar, cc.xy(8, 6));
		pack();
		setLocationRelativeTo(getOwner());

		//---- pipeline ----
		pipeline.addHighlighter(new AlternateRowHighlighter());

    }
    
	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private JLabel label1;
	private JLabel labelCategoria;
	private JScrollPane scrollPane1;
	private JXTable tableCorrectores;
	private JButton buttonActualizar;
	private JButton buttonAceptar;
	private JButton buttonCancelar;
	private HighlighterPipeline pipeline;

} // Fin clase DialogCorrectores
