//////////////////////////////////////////////////////
//  CLASSE: FramePrincipal.java                     //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import catalogo.global.CtrlAyuda;
import catalogo.global.CtrlModelo;
import catalogo.global.CtrlVista;
import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import org.jdesktop.layout.GroupLayout;
import java.sql.SQLException;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class FramePrincipal extends JFrame {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private boolean inicializado;
    private DialogAcercaDe dialogAcercaDe;
    Mensaje mensaje;
    ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public FramePrincipal() {
        mensaje = new Mensaje();
        initComponents();
        dialogAcercaDe = null;
        this.setLocationRelativeTo(null);
        DialogInit dialogInit = new DialogInit(this, panelPrincipal, this);
        //dialogInit.setVisible(true);
        inicializado = dialogInit.haAceptado();
        dialogInit.dispose();
        if (!inicializado) salir();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }
    
    private void initComponents() {
        panelPrincipal = new PanelPrincipal();
        menuBar = new JMenuBar();
        menuArchivo = new JMenu();
        menuNuevo = new JMenu();

        itemNuevaCuestion = new JMenuItem();
        itemNuevaCategoria = new JMenuItem();
        itemNuevaTematica = new JMenuItem();
        itemNuevoEsquema = new JMenuItem();
        itemNuevoCorrector = new JMenuItem();
        menuIdioma = new JMenu();
        itemIdiomacat = new JMenuItem();
        itemIdiomaesp = new JMenuItem();
        itemIdiomaing = new JMenuItem();
        itemSalir = new JMenuItem();
        menuAyuda = new JMenu();
        itemAyuda = new JMenuItem();
        itemCategorias = new JMenuItem();
        jSeparator1 = new JSeparator();
        itemAcerca = new JMenuItem();

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle(recursos.getString("titleFrame"));
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/database32.png")).getImage());
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        menuArchivo.setText(recursos.getString("menuArchivo"));
        menuNuevo.setText(recursos.getString("menuNuevo"));

        itemNuevaCuestion.setIcon(new ImageIcon(getClass().getResource("/imagenes/panel_cuestion.png")));
        itemNuevaCuestion.setText(recursos.getString("itemNuevaCuestion"));
        itemNuevaCuestion.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemNuevaCuestionActionPerformed(evt);
            }
        });

        menuNuevo.add(itemNuevaCuestion);

        itemNuevaCategoria.setIcon(new ImageIcon(getClass().getResource("/imagenes/panel_categorias.png")));
        itemNuevaCategoria.setText(recursos.getString("itemNuevaCategoria"));
        itemNuevaCategoria.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemNuevaCategoriaActionPerformed(evt);
            }
        });

        menuNuevo.add(itemNuevaCategoria);

        itemNuevaTematica.setIcon(new ImageIcon(getClass().getResource("/imagenes/panel_tematicas.png")));
        itemNuevaTematica.setText(recursos.getString("itemNuevaTematica"));
        itemNuevaTematica.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemNuevaTematicaActionPerformed(evt);
            }
        });

        menuNuevo.add(itemNuevaTematica);

        itemNuevoEsquema.setIcon(new ImageIcon(getClass().getResource("/imagenes/panel_esquemas.png")));
        itemNuevoEsquema.setText(recursos.getString("itemNuevoEsquema"));
        itemNuevoEsquema.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemNuevoEsquemaActionPerformed(evt);
            }
        });

        menuNuevo.add(itemNuevoEsquema);

        itemNuevoCorrector.setIcon(new ImageIcon(getClass().getResource("/imagenes/panel_correctores.png")));
        itemNuevoCorrector.setText(recursos.getString("itemNuevoCorrector"));
        itemNuevoCorrector.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemNuevoCorrectorActionPerformed(evt);
            }
        });

        menuNuevo.add(itemNuevoCorrector);
        menuArchivo.add(menuNuevo);

        menuIdioma.setText(recursos.getString("menuIdioma"));

        itemIdiomacat.setText(recursos.getString("itemIdiomacat"));
        itemIdiomacat.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemIdiomaCatActionPerformed(evt);
            }
        });
        menuIdioma.add(itemIdiomacat);

        itemIdiomaesp.setText(recursos.getString("itemIdiomaesp"));
        itemIdiomaesp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemIdiomaEspActionPerformed(evt);
            }
        });
        menuIdioma.add(itemIdiomaesp);

        itemIdiomaing.setText(recursos.getString("itemIdiomaing"));
        itemIdiomaing.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemIdiomaIngActionPerformed(evt);
            }
        });
        menuIdioma.add(itemIdiomaing);
        menuArchivo.add(menuIdioma);

        itemSalir.setMnemonic('S');
        itemSalir.setText(recursos.getString("itemSalir"));
        itemSalir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemSalirActionPerformed(evt);
            }
        });

        menuArchivo.add(itemSalir);

        menuBar.add(menuArchivo);

        menuAyuda.setText(recursos.getString("menuAyuda"));
        itemAyuda.setIcon(new ImageIcon(getClass().getResource("/imagenes/help.png")));
        itemAyuda.setText(recursos.getString("itemAyuda"));
        itemAyuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemAyudaActionPerformed(evt);
            }
        });

        menuAyuda.add(itemAyuda);

        itemCategorias.setIcon(new ImageIcon(getClass().getResource("/imagenes/help.png")));
        itemCategorias.setText(recursos.getString("itemCategorias"));
        itemCategorias.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemCategoriasActionPerformed(evt);
            }
        });

        menuAyuda.add(itemCategorias);

        menuAyuda.add(jSeparator1);

        itemAcerca.setText(recursos.getString("itemAcerca"));
        itemAcerca.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                itemAcercaActionPerformed(evt);
            }
        });

        menuAyuda.add(itemAcerca);

        menuBar.add(menuAyuda);

        setJMenuBar(menuBar);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
            .addContainerGap()
            .add(panelPrincipal, GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
            .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
            .addContainerGap()
            .add(panelPrincipal, GroupLayout.DEFAULT_SIZE, 627, Short.MAX_VALUE)
            .addContainerGap())
        );

    }

    private void itemCategoriasActionPerformed(ActionEvent evt) {
        try {
            CtrlAyuda.showCategorias();
        } catch (Exception e) {
            mensaje.mensajeError(this, "", e, recursos);
        }
    }

    private void itemAcercaActionPerformed(ActionEvent evt) {
        if (dialogAcercaDe == null) dialogAcercaDe = new DialogAcercaDe(this, recursos);
        dialogAcercaDe.setVisible(true);
    }

    private void itemAyudaActionPerformed(ActionEvent evt) {
        try {
            CtrlAyuda.showHelp();
        } catch (Exception e) {
            mensaje.mensajeError(this, "", e, recursos);
        }
    }

    private void itemNuevoCorrectorActionPerformed(ActionEvent evt) {
        panelPrincipal.setFocoCorrectores();
    }

    private void formWindowClosing(WindowEvent evt) {
        salir();
    }

    private void itemSalirActionPerformed(ActionEvent evt) {
        salir();
    }

    private void itemIdiomaCatActionPerformed(ActionEvent evt) {
        ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");
        cambioIdioma(recursos);
        panelPrincipal.cambioIdioma(recursos);
    }

    private void itemIdiomaEspActionPerformed(ActionEvent evt) {
        ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacastellano");
        cambioIdioma(recursos);
        panelPrincipal.cambioIdioma(recursos);
    }

    private void itemIdiomaIngActionPerformed(ActionEvent evt) {
        ResourceBundle recursos = ResourceBundle.getBundle("config/idiomaenglish");
        cambioIdioma(recursos);
        panelPrincipal.cambioIdioma(recursos);
    }

    private void itemNuevoEsquemaActionPerformed(ActionEvent evt) {
        panelPrincipal.setFocoEsquemas();
    }

    private void itemNuevaCategoriaActionPerformed(ActionEvent evt) {
        panelPrincipal.setFocoCategorias();
    }

    private void itemNuevaTematicaActionPerformed(ActionEvent evt) {
        panelPrincipal.setFocoTematicas();
    }

    private void itemNuevaCuestionActionPerformed(ActionEvent evt) {
        panelPrincipal.setFocoCuestion();
    }
    
    public void setEnabledMenu(boolean enabled) {
        itemNuevaCuestion.setEnabled(enabled);
    }

    private void cambioIdioma(ResourceBundle recursos){

        setTitle(recursos.getString("titleFrame"));

        menuArchivo.setText(recursos.getString("menuArchivo"));
        menuNuevo.setText(recursos.getString("menuNuevo"));
        itemAcerca.setText(recursos.getString("itemAcerca"));
        itemAyuda.setText(recursos.getString("itemAyuda"));
        itemCategorias.setText(recursos.getString("itemCategorias"));
        itemNuevaCuestion.setText(recursos.getString("itemNuevaCuestion"));
        itemNuevaCategoria.setText(recursos.getString("itemNuevaCategoria"));
        itemNuevaTematica.setText(recursos.getString("itemNuevaTematica"));
        itemNuevoCorrector.setText(recursos.getString("itemNuevoCorrector"));
        itemNuevoEsquema.setText(recursos.getString("itemNuevoEsquema"));
        itemIdiomacat.setText(recursos.getString("itemIdiomacat"));
        itemIdiomaesp.setText(recursos.getString("itemIdiomaesp"));
        itemIdiomaing.setText(recursos.getString("itemIdiomaing"));
        itemSalir.setText(recursos.getString("itemSalir"));
        menuIdioma.setText(recursos.getString("menuIdioma"));
        menuAyuda.setText(recursos.getString("menuAyuda"));

        this.recursos = recursos;
    }
    
    private void salir() {
        boolean ok = true;
        
        if (inicializado && CtrlModelo.saveOnCommit.existenCambios()) {
            if (getExtendedState() == java.awt.Frame.ICONIFIED) setExtendedState(java.awt.Frame.NORMAL);
            int res = JOptionPane.showConfirmDialog(this,
                    recursos.getString("frameaviso"),
                    recursos.getString("question"), JOptionPane.YES_NO_CANCEL_OPTION);
            
            switch (res) {
                case JOptionPane.YES_OPTION:
                    ok = CtrlVista.guarda();
                    break;
                    
                case JOptionPane.CANCEL_OPTION:
                    ok = false;
                    
                default:
            }
        }
        
        if (ok) {
            try {
                CtrlModelo.wrapperConexion.getConexion().close();
            } catch (SQLException e) {
                mensaje.mensajeError(this, "", e, recursos);
            }
            dispose();
            System.exit(0);

        }
    }
    
    /*********************************************************/
    /*      MAIN del programa                                */
    /*********************************************************/
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                PlasticLookAndFeel.setPlasticTheme(new com.jgoodies.looks.plastic.theme.ExperienceBlue());
                try {
                    UIManager.setLookAndFeel(new com.jgoodies.looks.plastic.Plastic3DLookAndFeel());
                } catch (Exception e) {}
                new FramePrincipal().setVisible(true);
            }
        });
    }
    
    /*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

    private JMenuItem itemAcerca;
    private JMenuItem itemAyuda;
    private JMenuItem itemCategorias;
    private JMenuItem itemNuevaCuestion;
    private JMenuItem itemNuevaCategoria;
    private JMenuItem itemNuevaTematica;
    private JMenuItem itemNuevoCorrector;
    private JMenuItem itemNuevoEsquema;
    private JMenuItem itemIdiomacat;
    private JMenuItem itemIdiomaesp;
    private JMenuItem itemIdiomaing;
    private JMenuItem itemSalir;
    private JSeparator jSeparator1;
    private JMenu menuArchivo;
    private JMenu menuIdioma;
    private JMenu menuAyuda;
    private JMenuBar menuBar;
    private JMenu menuNuevo;
    private PanelPrincipal panelPrincipal;
    
} // Fin clase FramePrincipal
