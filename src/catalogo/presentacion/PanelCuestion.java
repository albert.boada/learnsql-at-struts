//////////////////////////////////////////////////////
//  CLASSE: PanelCuestion.java                      //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import catalogo.global.CtrlVista;
import catalogo.gestiondatos.SaveOnCommit;
import catalogo.modelo.*;
import catalogo.soporte.modelos.ArrayListModel;
import catalogo.soporte.modelos.ListSelectionModelNone;
import catalogo.soporte.modelos.TableModelPesos;
import catalogo.soporte.modelos.TreeTableModelEjecucion;
import catalogo.soporte.modelos.TreeTableModelSolAlumno;
import catalogo.soporte.renderers.ComboBoxEsquemaRenderer;
import catalogo.soporte.renderers.TreeTableEjecucionCellRenderer;
import catalogo.soporte.renderers.TableSolAlumnoCellRenderer;
import catalogo.soporte.utils.ItemTreeTable;
import catalogo.soporte.utils.TextParser;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.JSpinner.DefaultEditor;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import com.jgoodies.looks.Options;
import com.jgoodies.uif_lite.panel.*;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.jdesktop.jdic.desktop.Desktop;
import org.jdesktop.jdic.desktop.DesktopException;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.AlternateRowHighlighter;
import org.jdesktop.swingx.decorator.HighlighterPipeline;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

public class PanelCuestion extends JPanel {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    public Cuestion cuestion;
    private JuegoPruebas jpSelected;
    private Comprobacion comprSelected;
    private DiccCatalogo catalogo;
    private SaveOnCommit soc;
    private File fileAdjunto, fileSolucion, fileDir;
    private FileFilter filterZip, filterTxt, filterImg;
    private JFileChooser fileChooserabrir;
    private DialogTematicas dialogTematicas;
    private DialogCorrectores dialogCorrectores;
    private DialogCopiaCuestionBD dialogCopiaBD;
    private DialogEspera dialogEspera;
    private DialogAttachement dialogAttach;
    private DialogListado dialogListado;
    private TreeTableModelSolAlumno tableComp;
    private TreeTableModelEjecucion tableExec;
    private SwingWorker workerEjecucion;
    private SwingWorker workerComparar;
    private SwingWorkerCompletionWaiter waiterEjecucion;
    private CellConstraints cc1;
    private Container sifPane;
    private Mensaje mensaje;
    private int lines;
    ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Constantes                                        */
    /*********************************************************/
    // CRLF1: byte no visible a eliminar tras importar fichero con sentencias
    private final static byte CRLF1 = '\015';
    private String STR_FICH_ADJUNTO = recursos.getString("str_fich_adjunto")+" ";
    private String STR_FICH_SOLUCION = recursos.getString("str_fich_solucion")+" ";


    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/
    public PanelCuestion(DiccCatalogo catalogo, DiccCategorias categorias, DiccEsquemas esquemas, DiccTematicas tematicas,
            DiccTiposSolucion tiposSol, DiccIdiomas idiomas, DiccCorrectores correctores, SaveOnCommit soc)
            throws Exception {
        initComponents();
        inicializa(catalogo, categorias, esquemas, tematicas, tiposSol, idiomas, correctores, soc);
    }

    /*********************************************************/
    /*      Métodos públicos                                 */
    /*********************************************************/
    public void muestraCuestion() {
        muestraCuestion(catalogo.create());
        tabbedCuestion.setSelectedIndex(0);
        tabbedCuestion.getComponentAt(0).setVisible(true);
        textTitulo.requestFocusInWindow();
    }

    public void muestraCuestion(Cuestion c) {
        if (cuestion != null && cuestion == c) {
            return;
        }
		
		
        cuestion = c;
        soc.inicializa(c);
		System.out.println(this.cuestion.getId());

        // button guardar enabled, hacerlo en la pulsacion del botón
        buttonGuardar.setEnabled(false);
        buttonCancelar.setEnabled(false);
        toggleEjecucion.setEnabled(false);
        CtrlVista.setControlesModif(false);
		
        if (cuestion.getId() == 0) {
            buttonListado.setEnabled(false);
            buttonDelCuestion.setEnabled(false);
            buttonCopiarCuestion.setEnabled(false);
            buttonCopiarCuestionBD.setEnabled(false);
            buttonHtml.setEnabled(false);

        } else {
            buttonListado.setEnabled(true);
            buttonDelCuestion.setEnabled(true);
            buttonCopiarCuestion.setEnabled(true);
            buttonCopiarCuestionBD.setEnabled(true);
            buttonHtml.setEnabled(true);
            toggleEjecucion.setEnabled(true);
        }

        if(!cuestion.getDisponible()&& toggleSolucion.isSelected()){
            sifPane.remove(panelEjecucion);
            sifPane.remove(panelSolucion);
            sifPane.add(panelInfo, cc1.xy(1, 1));
            sifPane.validate();
            sifPane.repaint();
            toggleInfo.setSelected(true);
        }

        // inicialización de campos *******************************************

        textTitulo.setText(cuestion.getTitulo());
        textId.setText(Integer.toString(cuestion.getId()));
        textPostInits.setText(Integer.toString(cuestion.getPostInitsJP()));
        
        if (cuestion.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
            areaEnunciado.setText(cuestion.getEnunciado());
        } else if (cuestion.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
            areaEnunciado.setText(cuestion.getEnunciadoEsp());
        } else if (cuestion.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
            areaEnunciado.setText(cuestion.getEnunciadoIng());
        }

        textAutor.setText(cuestion.getAutor());
        binaria.setSelected(cuestion.getBinaria());
        gabia.setSelected(cuestion.getGabia());
        spinnerDificultad.setValue(cuestion.getDificultad());
        comboEsquema.setSelectedItem(cuestion.getEsquema());
        comboCategoria.setSelectedItem(cuestion.getCategoria());
        blockPorCategoria();
        comboTipoSol.setSelectedItem(cuestion.getTipoSolucion());
        comboIdiomaEnunciado.setSelectedItem(cuestion.getIdioma());
        textUrl.setText(cuestion.getUrl());
        textUsuario.setText(cuestion.getUsuario());
        textClave.setText(cuestion.getClave());
        ssl.setSelected(cuestion.getSsl());
        areaEstado.setText(cuestion.getEstado());
        areaInitsCuestion.setText(cuestion.getInits());
        areaLimpiezaCuestion.setText(cuestion.getLimpieza());
        areaPostInitsCuestion.setText(cuestion.getPostInits());

        areaEnunciado.setCaretPosition(0);
        areaInitsCuestion.setCaretPosition(0);
        areaLimpiezaCuestion.setCaretPosition(0);
        areaPostInitsCuestion.setCaretPosition(0);
        areaEstado.setCaretPosition(0);

        fileAdjunto = null;
        fileSolucion = null;

        if (cuestion.existeFicheroAdjunto()) {
            buttonVerAdjunto.setEnabled(true);
            buttonDelAdjunto.setEnabled(true);
            textFichero.setText(STR_FICH_ADJUNTO + formateaExtension(cuestion.getExtensionFichero()));
        } else {
            buttonVerAdjunto.setEnabled(false);
            buttonDelAdjunto.setEnabled(false);
            textFichero.setText("");
        }

        if (cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_FICHERO) {
            buttonVerSolucion.setVisible(true);
            areaSolucionCuestion.setEditable(false);
            if (cuestion.existeFicheroSolucion()) {
                areaSolucionCuestion.setText(STR_FICH_SOLUCION + formateaExtension(cuestion.getExtensionSolucion()));
                buttonVerSolucion.setEnabled(true);
            } else {
                areaSolucionCuestion.setText("");
                buttonVerSolucion.setEnabled(false);
            }
        } else {
            buttonVerSolucion.setVisible(false);
            if(cuestion.getCategoria()!=null)
			{
				if(cuestion.getCategoria().getSolucion()) areaSolucionCuestion.setEditable(false);
				else areaSolucionCuestion.setEditable(true);
				areaSolucionCuestion.setText(cuestion.getSolucion());
				areaSolucionCuestion.setCaretPosition(0);
			}
		}

        if (cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_URL) {
            tipusSolucioUrl(true);
        } else {
            tipusSolucioUrl(false);
        }

        listTematicas.setModel(cuestion.getTematicas());
        listCorrectores.setModel(cuestion.getCorrectores());

        // ********************************************************************

        if (cuestion.getCorrectores().isEmpty()) {
            buttonEjecutar.setEnabled(false);
        } else {
            buttonEjecutar.setEnabled(true);
        }

        jpSelected = null;
        comprSelected = null;

        comboJP.setModel(cuestion.getJuegosPruebas());
        if (cuestion.getJuegosPruebas().isEmpty()) {
            comboJP.setSelectedIndex(-1);
        } else {
            comboJP.setSelectedIndex(0);
        }

        ((TableModelPesos) tablePesos.getModel()).setData(cuestion.getJuegosPruebas());

        ((DefaultTreeTableModel) treeTableEjecucion.getTreeTableModel()).setRoot(c);
        treeTableEjecucion.expandAll();

        ((DefaultTreeTableModel) treeTableComparacion.getTreeTableModel()).setRoot(c);
        lines = cuentalinias();
        treeTableComparacion.setRowHeight(18 * lines);
        treeTableComparacion.expandAll();

        setTituloModif(false);

    }

    public void clearSalidas(Corrector cor) {
        if (cuestion != null && cuestion.getCorrectores().contains(cor)) {
            areaSalidaJP.setText("");
            areaSalidaCompr.setText("");
            setTituloModif(false);
        }
    }

    public void ModificaCorrector(Cuestion c){
        try{
            if(c!=null){
    //            Cuestion aux = cuestion;
                cuestion = c;
                cuestion.clearSalidas();
//                cuestion.setDisponible(true);
                setTituloModif(false);
    //            cuestion =aux;
            }
        }catch(Exception error){
            mensaje.mensajeError(getTopLevelAncestor(), "", error, recursos);
        }
    }

    public void ejecuta(Cuestion c){
//        Cuestion aux = cuestion;
        cuestion = c;
        buttonEjecutarActionPerformed();
//        cuestion = aux;
    }

    public boolean guarda() {
        buttonGuardarActionPerformed();
        return (!soc.existenCambios());
    }

    public Esquema getSelectedEquema() {
        return cuestion.getEsquema();
    }

    public List<Tematica> getSelectedTematicas() {
        return cuestion.getTematicas();
    }

    public Categoria getSelectedCategoria() {
        return cuestion.getCategoria();
    }

    public void cambioIdioma(ResourceBundle recursos) {
        this.recursos = recursos;//Sirve para saber en que idioma estoy cuando creo los dialogos.

        //Modificar el titulo
        setTituloModif(false);

        //atributos
        STR_FICH_ADJUNTO = recursos.getString("str_fich_adjunto")+" ";
        STR_FICH_SOLUCION = recursos.getString("str_fich_solucion")+" ";

        //Botones
        toggleInfo.setToolTipText(recursos.getString("toggleInfo"));
        toggleEjecucion.setToolTipText(recursos.getString("toggleEjecucion"));
        toggleSolucion.setToolTipText(recursos.getString("toggleSolucion"));
        
        //PanelCuestionGlobal
        buttonCopiarCuestion.setText(recursos.getString("buttonCopiarCuestion"));
        buttonCopiarCuestion.setToolTipText(recursos.getString("buttonCopiarCuestionTooltip"));
        buttonCopiarCuestionBD.setText(recursos.getString("buttonCopiarCuestionBD"));
        buttonCopiarCuestionBD.setToolTipText(recursos.getString("buttonCopiarCuestionBDTooltip"));
        buttonDelCuestion.setText(recursos.getString("buttonDelCuestion"));
        buttonDelCuestion.setToolTipText(recursos.getString("buttonDelCuestionTooltip"));
        buttonListado.setText(recursos.getString("buttonListado"));
        buttonListado.setToolTipText(recursos.getString("buttonListadoTooltip"));
        buttonGuardar.setText(recursos.getString("buttonGuardar"));
        buttonGuardar.setToolTipText(recursos.getString("buttonGuardarTooltip"));
        buttonCancelar.setText(recursos.getString("buttonCancelar"));
        buttonCancelar.setToolTipText(recursos.getString("buttonCancelarTooltip"));

        //PanelCuestionDatos
        panelCuestionDatos.remove(separator1);
        panelCuestionDatos.remove(separator2);
        separator1 = compFactory.createSeparator(recursos.getString("separator1"));
        separator2 = compFactory.createSeparator(recursos.getString("separator2"));
        panelCuestionDatos.add(separator1, cc.xywh(2, 2, 21, 1));
        panelCuestionDatos.add(separator2, cc.xywh(2, 13, 21, 1));
        tabbedCuestion.setTitleAt(0, recursos.getString("tabbedCuestion1"));
        label1.setText(recursos.getString("label1"));
        label47.setText(recursos.getString("label47"));
        label2.setText(recursos.getString("label2"));
        buttonDelAdjunto.setToolTipText(recursos.getString("buttonDelAdjuntoTooltip"));
        buttonVerAdjunto.setText(recursos.getString("buttonVerAdjunto"));
        buttonVerAdjunto.setToolTipText(recursos.getString("buttonVerAdjuntoTooltip"));
        buttonAdjunto.setText(recursos.getString("buttonAdjunto"));
        buttonAdjunto.setToolTipText(recursos.getString("buttonAdjuntoTooltip"));
        label3.setText(recursos.getString("label3"));
        label31.setText(recursos.getString("label31"));
        label32.setText(recursos.getString("label32"));
        label42.setText(recursos.getString("label42"));
        buttonHtml.setText(recursos.getString("buttonHtml"));
        buttonHtml.setToolTipText(recursos.getString("buttonHtmlTooltip"));
        label14.setText(recursos.getString("label14"));
        spinnerDificultad.setToolTipText(recursos.getString("spinnerDificultad"));
        label8.setText(recursos.getString("label8"));
        label4.setText(recursos.getString("label4"));
        label5.setText(recursos.getString("label5"));
        label7.setText(recursos.getString("label7"));
        label6.setText(recursos.getString("label6"));
        label33.setText(recursos.getString("label33"));
        label34.setText(recursos.getString("label34"));
        label35.setText(recursos.getString("label35"));
        label36.setText(recursos.getString("label36"));
        buttonTematicas.setText(recursos.getString("buttonTematicas"));
        buttonTematicas.setToolTipText(recursos.getString("buttonTematicasTooltip"));

        //PanelCuestionSQL
        tabbedCuestion.setTitleAt(1, recursos.getString("tabbedCuestion2"));
        label15.setText(recursos.getString("label15"));
        label16.setText(recursos.getString("label16"));
        label17.setText(recursos.getString("label17"));
        label30.setText(recursos.getString("label30"));
        buttonInitsCuestion.setText(recursos.getString("buttonInitsCuestion"));
        buttonInitsCuestion.setToolTipText(recursos.getString("buttonInitsCuestionTooltip"));
        buttonVerSolucion.setText(recursos.getString("buttonVerSolucion"));
        buttonVerSolucion.setToolTipText(recursos.getString("buttonVerSolucionTooltip"));
        buttonSolucionCuestion.setText(recursos.getString("buttonSolucionCuestion"));
        buttonSolucionCuestion.setToolTipText(recursos.getString("buttonSolucionCuestionTooltip"));
        buttonLimpiezaCuestion.setText(recursos.getString("buttonLimpiezaCuestion"));
        buttonLimpiezaCuestion.setToolTipText(recursos.getString("buttonLimpiezaCuestionTooltip"));
        buttonPostInitsCuestion.setText(recursos.getString("buttonPostInitsCuestion"));
        buttonPostInitsCuestion.setToolTipText(recursos.getString("buttonPostInitsCuestionTooltip"));

        //PanelCuestionPruebas
        tabbedCuestion.setTitleAt(2, recursos.getString("tabbedCuestion3"));
        label12.setText(recursos.getString("label12"));
        buttonNuevoJP.setText(recursos.getString("buttonNuevoJP"));
        buttonNuevoJP.setToolTipText(recursos.getString("buttonNuevoJPTooltip"));
        buttonCopiarJP.setText(recursos.getString("buttonCopiarJP"));
        buttonCopiarJP.setToolTipText(recursos.getString("buttonCopiarJPTooltip"));
        buttonDelJP.setText(recursos.getString("buttonDelJP"));
        buttonDelJP.setToolTipText(recursos.getString("buttonDelJPTooltip"));

        //PanelPruebasDatos
        tabbedPruebas.setTitleAt(0, recursos.getString("tabbedPruebas1"));
        label18.setText(recursos.getString("label18"));
        label19.setText(recursos.getString("label19"));
        label20.setText(recursos.getString("label20"));
        label37.setText(recursos.getString("label37"));
        label38.setText(recursos.getString("label38"));
        label45.setText(recursos.getString("label45"));

        //PanelPruebasSQL
        tabbedPruebas.setTitleAt(1, recursos.getString("tabbedPruebas2"));
        label21.setText(recursos.getString("label21"));
        label22.setText(recursos.getString("label22"));
        label23.setText(recursos.getString("label23"));
        buttonInitsJP.setText(recursos.getString("buttonInitsJP"));
        buttonInitsJP.setToolTipText(recursos.getString("buttonInitsJPTooltip"));
        buttonEntradaJP.setText(recursos.getString("buttonEntradaJP"));
        buttonEntradaJP.setToolTipText(recursos.getString("buttonEntradaJPTooltip"));
        buttonLimpiezaJP.setText(recursos.getString("buttonLimpiezaJP"));
        buttonLimpiezaJP.setToolTipText(recursos.getString("buttonLimpiezaJPTooltip"));
        label24.setText(recursos.getString("label24"));

        //PanelPruebasCompr
        tabbedPruebas.setTitleAt(2, recursos.getString("tabbedPruebas3"));
        label13.setText(recursos.getString("label13"));
        buttonNuevaCompr.setText(recursos.getString("buttonNuevaCompr"));
        buttonNuevaCompr.setToolTipText(recursos.getString("buttonNuevaComprTooltip"));
        buttonCopiarCompr.setText(recursos.getString("buttonCopiarCompr"));
        buttonCopiarCompr.setToolTipText(recursos.getString("buttonCopiarComprTooltip"));
        buttonDelCompr.setText(recursos.getString("buttonDelCompr"));
        buttonDelCompr.setToolTipText(recursos.getString("buttonDelComprTooltip"));

        //PanelComprDatos
        tabbedCompr.setTitleAt(0, recursos.getString("tabbedCompr1"));
        label9.setText(recursos.getString("label9"));
        label10.setText(recursos.getString("label10"));
        label11.setText(recursos.getString("label11"));
        label39.setText(recursos.getString("label39"));
        label40.setText(recursos.getString("label40"));
        label46.setText(recursos.getString("label46"));

        //PanelComprSQL
        tabbedCompr.setTitleAt(1, recursos.getString("tabbedCompr2"));
        label28.setText(recursos.getString("label28"));
        buttonEntradaCompr.setText(recursos.getString("buttonEntradaCompr"));
        buttonEntradaCompr.setToolTipText(recursos.getString("buttonEntradaComprTooltip"));
        label29.setText(recursos.getString("label29"));

        //PanelCuestionPesos
        tabbedCuestion.setTitleAt(3, recursos.getString("tabbedCuestion4"));
        label26.setText(recursos.getString("label26"));
        label27.setText(recursos.getString("label27"));
        spinnerPeso.setToolTipText(recursos.getString("spinnerPeso"));
        buttonPeso.setText(recursos.getString("buttonPeso"));
        buttonPeso.setToolTipText(recursos.getString("buttonPesoTooltip"));
        ((TableModelPesos) tablePesos.getModel()).setColumnName(recursos);
        tablePesos.getColumnModel().getColumn(0).setPreferredWidth(800);
        tablePesos.getColumnModel().getColumn(1).setPreferredWidth(150);
        tablePesos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        //PanelCuestionEstado
        tabbedCuestion.setTitleAt(4, recursos.getString("tabbedCuestion5"));
        label41.setText(recursos.getString("label41"));

        //PanelEjecucion
        panelEjecucion.remove(separator4);
        panelEjecucion.remove(separator5);
        separator4 = compFactory.createSeparator(recursos.getString("separator4"));
        separator5 = compFactory.createSeparator(recursos.getString("separator5"));
        panelEjecucion.add(separator4, cc.xywh(2, 2, 12, 1));
        panelEjecucion.add(separator5, cc.xywh(2, 9, 12, 1));
        buttonCorrectores.setText(recursos.getString("buttonCorrectores"));
        buttonCorrectores.setToolTipText(recursos.getString("buttonCorrectoresToolTip"));
        buttonSubir.setText(recursos.getString("buttonSubir"));
        buttonSubir.setToolTipText(recursos.getString("buttonSubirToolTip"));
        buttonBajar.setText(recursos.getString("buttonBajar"));
        buttonBajar.setToolTipText(recursos.getString("buttonBajarToolTip"));
        buttonGenerar.setText(recursos.getString("buttonGenerar"));
        buttonGenerar.setToolTipText(recursos.getString("buttonGenerarToolTip"));
        buttonEjecutar.setText(recursos.getString("buttonEjecutar"));
        buttonEjecutar.setToolTipText(recursos.getString("buttonEjecutarToolTip"));
        label25.setText(recursos.getString("label25"));
        buttonLimpiar.setText(recursos.getString("buttonLimpiar"));
        buttonLimpiar.setToolTipText(recursos.getString("buttonLimpiarToolTip"));
        label48.setText(recursos.getString("label48"));
        label49.setText(recursos.getString("label49"));
        label50.setText(recursos.getString("label50"));

        tableExec.setColumnName(recursos);
        treeTableEjecucion.setTreeTableModel(tableExec);
        treeTableEjecucion.getColumnModel().getColumn(0).setPreferredWidth(800);
        treeTableEjecucion.getColumnModel().getColumn(1).setPreferredWidth(150);
        treeTableEjecucion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        treeTableEjecucion.setTreeCellRenderer(new TreeTableEjecucionCellRenderer());
        treeTableEjecucion.setShowsRootHandles(false);

        //PanelSolucion
        label43.setText(recursos.getString("label43"));
        buttonComparar.setText(recursos.getString("buttonComparar"));
        buttonComparar.setToolTipText(recursos.getString("buttonCompararToolTip"));
        buttonLimpiezaSol.setText(recursos.getString("buttonLimpiezaSol"));
        buttonLimpiezaSol.setToolTipText(recursos.getString("buttonLimpiezaSolToolTip"));
        label44.setText(recursos.getString("label44"));

        tableComp.setColumnName(recursos);
        treeTableComparacion.setTreeTableModel(tableComp);
        treeTableComparacion.setRowHeight(18 * lines);
        treeTableComparacion.setTreeCellRenderer(new TreeTableEjecucionCellRenderer());
        treeTableComparacion.getColumnModel().getColumn(0).setPreferredWidth(150);
        treeTableComparacion.getColumnModel().getColumn(1).setPreferredWidth(250);
        treeTableComparacion.getColumnModel().getColumn(1).setCellRenderer(new TableSolAlumnoCellRenderer());
        treeTableComparacion.getColumnModel().getColumn(2).setPreferredWidth(250);
        treeTableComparacion.getColumnModel().getColumn(2).setCellRenderer(new TableSolAlumnoCellRenderer());
        treeTableComparacion.getColumnModel().getColumn(3).setPreferredWidth(100);
        treeTableComparacion.getColumnModel().getColumn(4).setPreferredWidth(50);
//        treeTableComparacion.setTreeCellRenderer(new TableSolAlumnoCellRenderer());
        treeTableComparacion.setShowsRootHandles(false);

        dialogCorrectores.setRecursos(recursos);
        dialogTematicas.setRecursos(recursos);
        //Llamadas a las siguientes pantallas ---> trasladado a donde creo la pantalla... boton que llama a la pantalla
//        dialogAttach.cambioIdioma(recursos); // ya no hace falta por lo que pone arriba
//        this.recursos = recursos; //hecho al principio de la funcion
//        muestraCuestion(cuestion);

    }

    public void bloquear(){

    }

    public void blockPorCategoria()
	{
		//System.out.println(cuestion.getCategoria());
        Categoria cat = cuestion.getCategoria();
        if(cat!=null)
		{
			if(cat.getInits()) {
				areaInitsCuestion.setEditable(false);
			}
			else areaInitsCuestion.setEditable(true);
			if(cat.getSolucion()) areaSolucionCuestion.setEditable(false);
			else areaSolucionCuestion.setEditable(true);
			
			if(cat.getLimpieza()) areaLimpiezaCuestion.setEditable(false);
			else areaLimpiezaCuestion.setEditable(true);
			
			if(cat.getPostinits()) areaPostInitsCuestion.setEditable(false);
			else areaPostInitsCuestion.setEditable(true);
			
			if(cat.getEstado()) areaEstado.setEditable(false);
			else areaEstado.setEditable(true);
			
			if(cat.getInitsJP()) areaInitsJP.setEditable(false);
			else areaInitsJP.setEditable(true);
			
			if(cat.getEntradaJP()) areaEntradaJP.setEditable(false);
			else areaEntradaJP.setEditable(true);
			
			if(cat.getLimpiezaJP()) areaLimpiezaJP.setEditable(false);
			else areaLimpiezaJP.setEditable(true);
			
			if(cat.getComprobaciones()) tabbedPruebas.setEnabledAt(2, false);
			else tabbedPruebas.setEnabledAt(2, true);
		}

    }

    // Métodos auxiliares *****************************************************
    private void inicializa(DiccCatalogo catalogo, DiccCategorias categorias, DiccEsquemas esquemas, DiccTematicas tematicas,
            DiccTiposSolucion tiposSol, DiccIdiomas idiomas, DiccCorrectores correctores, SaveOnCommit soc) {
        this.catalogo = catalogo;
        this.soc = soc;
        cuestion = null;
        jpSelected = null;
        comprSelected = null;
        fileDir = null;
        fileAdjunto = null;
        fileSolucion = null;
        dialogTematicas = null;
        dialogCorrectores = null;
        dialogCopiaBD = null;
        dialogEspera = null;
        dialogAttach = null;
        mensaje = new Mensaje();
        cc1 = new CellConstraints();
        sifPane = sifCuestion.getContentPane();

        textTitulo.addMouseListener(CtrlVista.contextMouseAdapter);
        textId.addMouseListener(CtrlVista.contextMouseAdapter);
        textPostInits.addMouseListener(CtrlVista.contextMouseAdapter);
        textAutor.addMouseListener(CtrlVista.contextMouseAdapter);
        textUrl.addMouseListener(CtrlVista.contextMouseAdapter);
        textUsuario.addMouseListener(CtrlVista.contextMouseAdapter);
        textClave.addMouseListener(CtrlVista.contextMouseAdapter);
//        ssl.setSelected(false);
        textNombreJP.addMouseListener(CtrlVista.contextMouseAdapter);
        textNombreCompr.addMouseListener(CtrlVista.contextMouseAdapter);
        areaEnunciado.addMouseListener(CtrlVista.contextMouseAdapter);
        areaInitsCuestion.addMouseListener(CtrlVista.contextMouseAdapter);
        areaSolucionCuestion.addMouseListener(CtrlVista.contextMouseAdapter);
        areaLimpiezaCuestion.addMouseListener(CtrlVista.contextMouseAdapter);
        areaPostInitsCuestion.addMouseListener(CtrlVista.contextMouseAdapter);
        areaEstado.addMouseListener(CtrlVista.contextMouseAdapter);
        areaDescripJP.addMouseListener(CtrlVista.contextMouseAdapter);
        areaDescripCompr.addMouseListener(CtrlVista.contextMouseAdapter);
        areaMsgErrorJP.addMouseListener(CtrlVista.contextMouseAdapter);
        areaMsgErrorCompr.addMouseListener(CtrlVista.contextMouseAdapter);
        areaInitsJP.addMouseListener(CtrlVista.contextMouseAdapter);
        areaEntradaJP.addMouseListener(CtrlVista.contextMouseAdapter);
        areaLimpiezaJP.addMouseListener(CtrlVista.contextMouseAdapter);
        areaEntradaCompr.addMouseListener(CtrlVista.contextMouseAdapter);
        ((DefaultEditor) spinnerDificultad.getEditor()).getTextField().addMouseListener(CtrlVista.contextMouseAdapter);
        ((DefaultEditor) spinnerPeso.getEditor()).getTextField().addMouseListener(CtrlVista.contextMouseAdapter);

        fileChooserabrir = new JFileChooser();
        filterZip = new FileNameExtensionFilter("Comprimidos (*.zip)", "zip");
        filterTxt = new FileNameExtensionFilter("SQL/Texto (*.sql, *.txt)", "sql", "txt");
        filterImg = new FileNameExtensionFilter("Imágenes (*.bmp, *.gif, *.jpg, *.png)", "bmp", "gif", "jpg", "png");

        dialogTematicas = new DialogTematicas((Frame) getTopLevelAncestor(), tematicas, recursos);
        dialogCorrectores = new DialogCorrectores((Frame) getTopLevelAncestor(), correctores, recursos);

        // ********************************************************************
        // Se define y asigna un KeyListener al editor del JSpinner para que
        // se reconozca la pulsación de teclas.
        final JFormattedTextField spinnerEditorText = ((DefaultEditor) spinnerDificultad.getEditor()).getTextField();
        spinnerEditorText.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                int pos = spinnerEditorText.getCaretPosition();
                float dificultad = ((Float) spinnerDificultad.getValue()).floatValue();
                dificultad *= 1000;
                dificultad = (float) Math.round(dificultad) / 1000;
                spinnerDificultad.setValue(dificultad);

                try {
                    spinnerDificultad.commitEdit();
                } catch (ParseException e1) {
                    spinnerEditorText.setValue(spinnerDificultad.getValue());
                }

                if (pos <= spinnerEditorText.getText().length()) {
                    spinnerEditorText.setCaretPosition(pos);
                } else {
                    spinnerEditorText.setCaretPosition(spinnerEditorText.getText().length());
                }
            }
        });
        // ********************************************************************

        // Inicializaciones de panelInfo

        comboEsquema.setRenderer(new ComboBoxEsquemaRenderer());
        comboEsquema.setModel(esquemas.getAll());
        comboTipoSol.setModel(tiposSol.getAll());
        comboIdiomaEnunciado.setModel(idiomas.getAll());
        comboIdiomaJP.setModel(idiomas.getAll());
        comboIdiomaCompr.setModel(idiomas.getAll());
        comboCategoria.setModel(categorias.getAll());
        ListSelectionModelNone none = new ListSelectionModelNone();
        listTematicas.setSelectionModel(none); // fa que no es pugui seleccionar
        listCorrectores.setSelectionModel(none);
        tablePesos.setModel(new TableModelPesos());
        tablePesos.getColumnModel().getColumn(0).setPreferredWidth(800);
        tablePesos.getColumnModel().getColumn(1).setPreferredWidth(150);
        tablePesos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // Inicializaciones de panelEjecucion
        tableExec = new TreeTableModelEjecucion();
        treeTableEjecucion.setTreeTableModel(tableExec);
        treeTableEjecucion.getColumnModel().getColumn(0).setPreferredWidth(800);
        treeTableEjecucion.getColumnModel().getColumn(1).setPreferredWidth(150);
        treeTableEjecucion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        treeTableEjecucion.setTreeCellRenderer(new TreeTableEjecucionCellRenderer());
        treeTableEjecucion.setShowsRootHandles(false);

        buttonSubir.setEnabled(false);
        buttonBajar.setEnabled(false);
        toggleInfo.setSelected(true);

        // Inicializaciones de panelSolucion
        tableComp = new TreeTableModelSolAlumno();
        treeTableComparacion.setTreeTableModel(tableComp);
        lines = 1;
        treeTableComparacion.setRowHeight(18 * lines);
        treeTableComparacion.setTreeCellRenderer(new TreeTableEjecucionCellRenderer());
        treeTableComparacion.getColumnModel().getColumn(0).setPreferredWidth(150);
        treeTableComparacion.getColumnModel().getColumn(1).setPreferredWidth(250);
        treeTableComparacion.getColumnModel().getColumn(1).setCellRenderer(new TableSolAlumnoCellRenderer());
        treeTableComparacion.getColumnModel().getColumn(2).setPreferredWidth(250);
        treeTableComparacion.getColumnModel().getColumn(2).setCellRenderer(new TableSolAlumnoCellRenderer());
        treeTableComparacion.getColumnModel().getColumn(3).setPreferredWidth(100);
        treeTableComparacion.getColumnModel().getColumn(4).setPreferredWidth(50);
//        treeTableComparacion.setAlignmentY(TOP_ALIGNMENT);
        treeTableComparacion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);;
        treeTableComparacion.setShowsRootHandles(false);

//        textNotaPublic.setText("");
//        textNotaReal.setText("");
//        textNotaBinaria.setText("");
    }

    private int cuentalinias(){
        int contador=1;
        int contadorJP=1;
        int contadorC=1;
        int index;
        int fromIndex=0;
        String salida = "";
        for(JuegoPruebas jpactual : cuestion.getJuegosPruebas()){
            salida = jpactual.getSalida();
            fromIndex = 0;
            while ((index=salida.indexOf("\n", fromIndex))>-1) {
                contadorJP++;
                fromIndex = index+1;
            }
            for(Comprobacion compractual : jpactual.getComprobaciones()){
                salida = compractual.getSalida();
                fromIndex = 0;
                while ((index=salida.indexOf("\n", fromIndex))>-1) {
                    contadorC++;
                    fromIndex = index+1;
                }
                if(contador<contadorC) contador=contadorC;
                contadorC=1;
            }
            if(contador<contadorJP) contador=contadorJP;
            contadorJP=1;
        }
        return contador;
    }


    private void muestraJP(JuegoPruebas jp) {
        if (jpSelected != null && jpSelected == jp) {
            return;
        }
        vuelcaJuegoPruebas();
        vuelcaComprobacion();
        jpSelected = jp;

        if (jp == null) {
            comboJP.setEnabled(false);
            buttonCopiarJP.setEnabled(false);
            buttonDelJP.setEnabled(false);
            tabbedPruebas.setVisible(false);
        } else {
            comboJP.setEnabled(true);
            buttonCopiarJP.setEnabled(true);
            buttonDelJP.setEnabled(true);
            tabbedPruebas.setVisible(true);

            textNombreJP.setText(jp.getNombre());
            areaDescripJP.setText(jp.getDescripcion());

            if (jp.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
                areaMsgErrorJP.setText(jp.getMsgError());
            } else if (jp.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
                areaMsgErrorJP.setText(jp.getMsgErrorEsp());
            } else if (jp.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
                areaMsgErrorJP.setText(jp.getMsgErrorIng());
            }

            areaInitsJP.setText(jp.getInits());
            areaEntradaJP.setText(jp.getEntrada());
            areaLimpiezaJP.setText(jp.getLimpieza());
            areaSalidaJP.setText(jp.getSalida());
            binariaJP.setSelected(jp.getBinaria());
            mensajeJP.setSelected(jp.getMensaje());

            areaDescripJP.setCaretPosition(0);
            areaMsgErrorJP.setCaretPosition(0);
            areaInitsJP.setCaretPosition(0);
            areaEntradaJP.setCaretPosition(0);
            areaLimpiezaJP.setCaretPosition(0);
            areaSalidaJP.setCaretPosition(0);

            comboCompr.setModel(jp.getComprobaciones());
            comboIdiomaJP.setSelectedItem(jp.getIdioma());
           
            if (!jp.getComprobaciones().isEmpty()) {
                comboCompr.setSelectedIndex(0);
            }
            else {
                comboCompr.setSelectedIndex(-1);
            }
        }
    }

    private void muestraComprobacion(Comprobacion compr) {
        if (comprSelected != null && comprSelected == compr) {
            return;
        }
        vuelcaComprobacion();
        comprSelected = compr;

        if (compr == null) {
            comboCompr.setEnabled(false);
            buttonCopiarCompr.setEnabled(false);
            buttonDelCompr.setEnabled(false);
            tabbedCompr.setVisible(false);
        } else {
            comboCompr.setEnabled(true);
            buttonCopiarCompr.setEnabled(true);
            buttonDelCompr.setEnabled(true);
            tabbedCompr.setVisible(true);

            textNombreCompr.setText(compr.getNombre());
            areaDescripCompr.setText(compr.getDescripcion());
            areaMsgErrorCompr.setText(compr.getMsgError());

            if (compr.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
                areaMsgErrorCompr.setText(compr.getMsgError());
            } else if (compr.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
                areaMsgErrorCompr.setText(compr.getMsgErrorEsp());
            } else if (compr.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
                areaMsgErrorCompr.setText(compr.getMsgErrorIng());
            }

            areaEntradaCompr.setText(compr.getEntrada());
            areaSalidaCompr.setText(compr.getSalida());
            binariaCom.setSelected(compr.getBinaria());
            mensajeCom.setSelected(compr.getMensaje());

            areaDescripCompr.setCaretPosition(0);
            areaMsgErrorCompr.setCaretPosition(0);
            areaEntradaCompr.setCaretPosition(0);
            areaSalidaCompr.setCaretPosition(0);

            comboIdiomaCompr.setSelectedItem(compr.getIdioma());
        }
    }

    private void vuelcaCuestion() {
        textTitulo.setText(TextParser.parseUTF8(textTitulo.getText().trim()));
        textId.setText(TextParser.parseUTF8(textId.getText().trim()));
        textPostInits.setText(TextParser.parseUTF8(textPostInits.getText().trim()));
        textAutor.setText(TextParser.parseUTF8(textAutor.getText().trim()));
        areaEnunciado.setText(TextParser.parseUTF8(areaEnunciado.getText()));
        textUrl.setText(TextParser.parseUTF8(textUrl.getText().trim()));
        textUsuario.setText(TextParser.parseUTF8(textUsuario.getText().trim()));
        textClave.setText(TextParser.parseUTF8(new String(textClave.getPassword()).trim()));
        areaEstado.setText(TextParser.parseUTF8(areaEstado.getText()));
        trimTextArea(areaEnunciado);
        trimTextArea(areaInitsCuestion);
        trimTextArea(areaSolucionCuestion);
        trimTextArea(areaLimpiezaCuestion);
        trimTextArea(areaPostInitsCuestion);
        trimTextArea(areaEstado);

        // se vuelca el fichero adjunto cuando se acepta su selección y el
        // esquema, la categoría y el tipo sol. cuando se modifican en los
        // respectivos combos, ademas los checkbox tambien cuando se modifican,
        // se hace en las funciones comboBoxActionPerformed y checkBoxActionPerformed

        cuestion.setTitulo(textTitulo.getText());
        cuestion.setAutor(textAutor.getText());

        if (cuestion.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
            cuestion.setEnunciado(areaEnunciado.getText());
        } else if (cuestion.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
            cuestion.setEnunciadoEsp(areaEnunciado.getText());
        } else if (cuestion.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
            cuestion.setEnunciadoIng(areaEnunciado.getText());
        }

        cuestion.setDificultad(((Float) spinnerDificultad.getValue()).floatValue());

        if (cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_URL) {
            cuestion.setUrl(textUrl.getText());
            cuestion.setUsuario(textUsuario.getText());
            cuestion.setClave(new String(textClave.getPassword()));
            cuestion.setSsl(ssl.isSelected());
        }
        cuestion.setInits(areaInitsCuestion.getText());
        cuestion.setLimpieza(areaLimpiezaCuestion.getText());
        cuestion.setPostInits(areaPostInitsCuestion.getText());
        cuestion.setPostInitsJP(Integer.parseInt(textPostInits.getText()));

        cuestion.setEstado(areaEstado.getText());

        if (cuestion.getTipoSolucion().getId() != DiccTiposSolucion.ID_FICHERO) {
            cuestion.setSolucion(areaSolucionCuestion.getText());
        }
        cuestion.setGabia(gabia.isSelected());
        cuestion.setBinaria(binaria.isSelected());
    }

    private void vuelcaJuegoPruebas() {
        if (jpSelected != null) {
            textNombreJP.setText(TextParser.parseUTF8(textNombreJP.getText().trim()));
            areaDescripJP.setText(TextParser.parseUTF8(areaDescripJP.getText()));
            areaMsgErrorJP.setText(TextParser.parseUTF8(areaMsgErrorJP.getText()));
            trimTextArea(areaDescripJP);
            trimTextArea(areaMsgErrorJP);
            trimTextArea(areaInitsJP);
            trimTextArea(areaEntradaJP);
            trimTextArea(areaLimpiezaJP);
            jpSelected.setNombre(textNombreJP.getText());
            jpSelected.setDescripcion(areaDescripJP.getText());

            if (jpSelected.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
                jpSelected.setMsgError(areaMsgErrorJP.getText());
            } else if (jpSelected.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
                jpSelected.setMsgErrorEsp(areaMsgErrorJP.getText());
            } else if (jpSelected.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
                jpSelected.setMsgErrorIng(areaMsgErrorJP.getText());
            }

            jpSelected.setInits(areaInitsJP.getText());
            jpSelected.setEntrada(areaEntradaJP.getText());
            jpSelected.setLimpieza(areaLimpiezaJP.getText());
            jpSelected.setBinaria(binariaJP.isSelected());
            jpSelected.setMensaje(mensajeJP.isSelected());
        }
    }

    private void vuelcaComprobacion() {
        if (comprSelected != null) {
            textNombreCompr.setText(TextParser.parseUTF8(textNombreCompr.getText().trim()));
            areaDescripCompr.setText(TextParser.parseUTF8(areaDescripCompr.getText()));
            areaMsgErrorCompr.setText(TextParser.parseUTF8(areaMsgErrorCompr.getText()));
            trimTextArea(areaDescripCompr);
            trimTextArea(areaMsgErrorCompr);
            trimTextArea(areaEntradaCompr);
            comprSelected.setNombre(textNombreCompr.getText());
            comprSelected.setDescripcion(areaDescripCompr.getText());

            if (comprSelected.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
                comprSelected.setMsgError(areaMsgErrorCompr.getText());
            } else if (comprSelected.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
                comprSelected.setMsgErrorEsp(areaMsgErrorCompr.getText());
            } else if (comprSelected.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
                comprSelected.setMsgErrorIng(areaMsgErrorCompr.getText());
            }

            comprSelected.setEntrada(areaEntradaCompr.getText());
            comprSelected.setBinaria(binariaCom.isSelected());
            comprSelected.setMensaje(mensajeCom.isSelected());
        }
    }

    private void setControlesModif(boolean enabled) {
        setTituloModif(enabled);
        buttonGuardar.setEnabled(enabled);
        buttonCancelar.setEnabled(enabled);
        buttonDelCuestion.setEnabled(!enabled);
        buttonCopiarCuestion.setEnabled(!enabled);
        buttonCopiarCuestionBD.setEnabled(!enabled);
        buttonListado.setEnabled(!enabled);
        CtrlVista.setControlesModif(enabled);
        toggleEjecucion.setEnabled(!enabled);
//        blockPorCategoria();
    }

    private void setTituloModif(boolean modif) {
        String estado;
        String texto ="";
        if (modif) {
            estado = " [ <B>" + recursos.getString("estadomod") + "</B> ]";
            toggleSolucion.setEnabled(false);
            buttonGenerar.setEnabled(false);
        } else if (!cuestion.getDisponible()) {
            estado = " [ <B><FONT color='#FFBF8B'>" + recursos.getString("estadosinresolver") + "</FONT></B> ]";
            toggleSolucion.setEnabled(false);
            buttonGenerar.setEnabled(false);
        } else {
            int num = cuestion.getNumPesoCero();
            if (num > 1) {
                estado = " [ <B><FONT color='#9FFE66'>" + recursos.getString("estadoresuelta") + "</FONT></B> / <B>" + num + " juegos de pruebas con peso 0</B> ]";
            } else if (num == 1) {
                estado = " [ <B><FONT color='#9FFE66'>" + recursos.getString("estadoresuelta") + "</FONT></B> / <B>1 juego de pruebas con peso 0</B> ]";
            } else {
                estado = " [ <B><FONT color='#9FFE66'>" + recursos.getString("estadoresuelta") + "</FONT></B> ]";
            }
            toggleSolucion.setEnabled(true);
            buttonGenerar.setEnabled(true);
        }
        if(cuestion.getBloqueada()){
            texto=" "+recursos.getString("estadoblock");
        }
        sifCuestion.setTitle("<HTML>" + cuestion.toString() + estado + texto + "</HTML>");
    }

    private void trimTextArea(JTextArea ta) {
        int index = ta.getCaretPosition();
        String s = ta.getText().trim();
        ta.setText(s);
        if (index > s.length()) {
            index = s.length();
        }
        ta.setCaretPosition(index);
    }

    private String getString(File f) throws Exception {
        int numCRLF = 0;
        String s = null;
        FileInputStream in = null;
        byte[] contenidoCRLF, contenido;

        try {
            in = new FileInputStream(f);
            contenidoCRLF = new byte[in.available()];
            in.read(contenidoCRLF);
            for (byte b : contenidoCRLF) {
                if (b == CRLF1) {
                    numCRLF++;
                }
            }

            if (numCRLF == 0) {
                contenido = contenidoCRLF;
            } else {
                contenido = new byte[contenidoCRLF.length - numCRLF];
                int i = 0;
                for (byte b : contenidoCRLF) {
                    if (b != CRLF1) {
                        contenido[i] = b;
                        i++;
                    }
                }
            }
            s = new String(contenido);
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
            }
        }
        return s;
    }

    private File creaFichero(String nombre, String extension, byte[] contenido) {
        File temp = null;
        FileOutputStream out = null;
        try {
            temp = File.createTempFile(nombre, extension);
            temp.deleteOnExit();
            out = new FileOutputStream(temp);
            out.write(contenido);
        } catch (IOException e) {
            mensaje.mensajeError(getTopLevelAncestor(), recursos.getString("cuesaviso1"), e, recursos);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
            }
        }
        return temp;
    }

    private byte[] getBytesFichero(File f) throws Exception {
        FileInputStream fis = null;
        byte[] b = null;
        try {
            fis = new FileInputStream(f);
            b = new byte[(int) f.length()];
            fis.read(b);
        } catch (IOException e) {
            mensaje.mensajeError(getTopLevelAncestor(), recursos.getString("cuesaviso2"), e, recursos);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
            }
        }
        return b;
    }

    private String getExtensionFichero(File f) {
        int dotpos = f.getName().lastIndexOf(".");
        if (dotpos != -1) {
            return f.getName().substring(dotpos).substring(1);
        } else {
            return "";
        }
    }

    private String formateaExtension(String ext) {
        if (ext.isEmpty()) {
            return recursos.getString("cuesextension");
        } else {
            return "\"" + ext + "\"";
        }
    }

    private void creaWorkerEjecucion() {
        if (dialogEspera == null) {
            dialogEspera = new DialogEspera((Frame) getTopLevelAncestor(), recursos.getString("dialogespera1"));
            waiterEjecucion = new SwingWorkerCompletionWaiter(dialogEspera);
        }
        dialogEspera.setLocationRelativeTo(getTopLevelAncestor());

        workerEjecucion = new SwingWorker<String, Void>() {

            @Override
            public String doInBackground() {

                Resultado res = catalogo.generaSalidas(cuestion);
                if (res.codigo == 0) {
                    try {
                        soc.setCuestionActual(cuestion);
                        soc.fillSalidas();
                    } catch (Exception e) {
                        return e.getMessage();
                    }
                    cuestion.setDisponible(true);
                    setTituloModif(false);
                    if (jpSelected != null) {
                        areaSalidaJP.setText(jpSelected.getSalida());
                    }
                    if (comprSelected != null) {
                        areaSalidaCompr.setText(comprSelected.getSalida());
                    }

                    for (JuegoPruebas jp : cuestion.getJuegosPruebas()) {
                        for (Comprobacion compr : jp.getComprobaciones()) {
                            compr.setEstado(ItemTreeTable.ESTADO_RESUELTO);
                        }
                        jp.setEstado(ItemTreeTable.ESTADO_RESUELTO);
                    }
                } else {
                    try {
                        soc.clearSalidasAndCommit();
                        cuestion.clearSalidas();
                        areaSalidaJP.setText("");
                        areaSalidaCompr.setText("");
                        setTituloModif(false);
                    } catch (Exception e) {
//                        mensaje.mensajeError(getTopLevelAncestor(), "", e, recursos);
//                        return "";
                        return e.getMessage();
                    }

                    int ordenJP = res.ordenJP;
                    int ordenCompr = res.ordenCompr;
                    for (JuegoPruebas jp : cuestion.getJuegosPruebas()) {
                        if (jp.getOrden() < ordenJP) {
                            jp.setEstado(ItemTreeTable.ESTADO_RESUELTO);
                        } else if (jp.getOrden() == ordenJP) {
                            if (ordenCompr == 0) {
                                jp.setEstado(ItemTreeTable.ESTADO_ERROR);
                            } else {
                                jp.setEstado(ItemTreeTable.ESTADO_RESUELTO);
                                for (Comprobacion compr : jp.getComprobaciones()) {
                                    if (compr.getOrden() < ordenCompr) {
                                        compr.setEstado(ItemTreeTable.ESTADO_RESUELTO);
                                    } else if (compr.getOrden() == ordenCompr) {
                                        compr.setEstado(ItemTreeTable.ESTADO_ERROR);
                                    } else {
                                        compr.setEstado(ItemTreeTable.ESTADO_SIN_RESOLVER);
                                    }
                                }
                            }
                        } else {
                            jp.setEstado(ItemTreeTable.ESTADO_SIN_RESOLVER);
                        }
                    }
                }

                ((DefaultTreeTableModel) treeTableEjecucion.getTreeTableModel()).reload(cuestion);
                treeTableEjecucion.expandAll();

                return res.texto;
            }

            @Override
            public void done() {
                try {
                    editorResultados.append(get());
                } catch (InterruptedException ignore) {
                } catch (java.util.concurrent.ExecutionException e) {
                    Throwable cause = e.getCause();
                    if (cause != null) {
                        System.out.println(cause.getMessage());
                    } else {
                        System.out.println(e.getMessage());
                    }
                }
            }
        };

        workerEjecucion.addPropertyChangeListener(waiterEjecucion);
    }

    private void creaWorkerComparar() {

        if (dialogEspera == null) {
            dialogEspera = new DialogEspera((Frame) getTopLevelAncestor(), recursos.getString("dialogespera2"));
            waiterEjecucion = new SwingWorkerCompletionWaiter(dialogEspera);
        }
        dialogEspera.setLocationRelativeTo(getTopLevelAncestor());

        workerComparar = new SwingWorker<String, Void>() {

            @Override
            public String doInBackground() {

                ArrayListModel<Resultado> res = new ArrayListModel();
                String mostrar = "";
                float bien = 10.0f;
                Boolean acabado;
                try{
                    res = catalogo.generaComparacion(cuestion);
                }catch(Exception error){
                    mensaje.mensajeError(getTopLevelAncestor(), "", error, recursos);
                }
                cuestion.setDisponible(true);
                setTituloModif(false);
                editorAlumno.setText("");

                if (res.get(0).codigo>0) {
                    
                    for (JuegoPruebas jp : cuestion.getJuegosPruebas()) {

                        acabado =true;
                        for(Resultado actual : res){
                            if(jp.getOrden() < actual.ordenJP){
                                jp.setSalidaAlumno(jp.getSalida());
                                jp.setComparacion(recursos.getString("cuescorrecto"));
                                for (Comprobacion compr : jp.getComprobaciones()) {
                                    compr.setSalidaAlumno(compr.getSalida());
                                    compr.setComparacion(recursos.getString("cuescorrecto"));
                                }
                                jp.setNota(Float.toString(bien*jp.getPeso()));
                                acabado =false;
                                break;
                            }
                            else if(jp.getOrden() == actual.ordenJP){
                                if(actual.ordenCompr == 0){
                                    jp.setSalidaAlumno(actual.solucion);
                                    jp.setComparacion(recursos.getString("cueserror"));
                                    for (Comprobacion compr : jp.getComprobaciones()) {
//                                        compr.setSalidaAlumno("--");
                                        compr.setComparacion(recursos.getString("cueserror"));
                                    }
                                }
                                else{
                                    jp.setSalidaAlumno(jp.getSalida());
                                    jp.setComparacion(recursos.getString("cuescorrecto"));
                                    for (Comprobacion compr : jp.getComprobaciones()) {
                                        if(compr.getOrden() < actual.ordenCompr){
                                            compr.setSalidaAlumno(compr.getSalida());
                                            compr.setComparacion(recursos.getString("cuescorrecto"));
                                        }
                                        else if(compr.getOrden() == actual.ordenCompr){
                                            compr.setSalidaAlumno(actual.solucion);
                                            compr.setComparacion(recursos.getString("cueserror"));
                                        }
                                    }
                                }
                                jp.setNota("0");
                                acabado =false;
                                break;
                            }
                        }
                        if(acabado){
                            jp.setSalidaAlumno(jp.getSalida());
                            jp.setComparacion(recursos.getString("cuescorrecto"));
                            for (Comprobacion compr : jp.getComprobaciones()) {
                                compr.setSalidaAlumno(compr.getSalida());
                                compr.setComparacion(recursos.getString("cuescorrecto"));
                            }
                            jp.setNota(Float.toString(bien*jp.getPeso()));
                        }
                    }
                    mostrar = recursos.getString("salidascorrectas");
                } else if(res.get(0).codigo == 0){
                    for (JuegoPruebas jp : cuestion.getJuegosPruebas()) {
                        jp.setSalidaAlumno(jp.getSalida());
                        jp.setComparacion(recursos.getString("cuescorrecto"));
                        for (Comprobacion compr : jp.getComprobaciones()) {
                            compr.setSalidaAlumno(compr.getSalida());
                            compr.setComparacion(recursos.getString("cuescorrecto"));
                        }
                        jp.setNota(Float.toString(bien*jp.getPeso()));
                    }
                    mostrar=res.get(0).texto+"\n";
                } else {
                    mostrar=res.get(0).codigo+" "+res.get(0).texto+"\n";
      
                }

                int contador=1;
                for(Resultado resultado : res){

                    if(resultado.contador>contador) contador=resultado.contador;
                }
                if(contador>lines) lines=contador;

                ((DefaultTreeTableModel) treeTableComparacion.getTreeTableModel()).reload(cuestion);
                treeTableComparacion.setRowHeight(18 * lines);
                treeTableComparacion.expandAll();

                return mostrar;
            }

            @Override
            public void done() {
                try {
                    editorAlumno.append(get());
                } catch (InterruptedException ignore) {
                } catch (java.util.concurrent.ExecutionException e) {
                    Throwable cause = e.getCause();
                    if (cause != null) {
                        System.out.println(cause.getMessage());
                    } else {
                        System.out.println(e.getMessage());
                    }
                }
            }
        };

        workerComparar.addPropertyChangeListener(waiterEjecucion);
    }

    // Métodos que realizan comprobaciones ************************************
    private boolean checkCuestion() {
        boolean correcto = false;
        int postjp = cuestion.getMaxOrden();
        if (textTitulo.getText().isEmpty()) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror1"), recursos);
            tabbedCuestion.setSelectedIndex(0);
            tabbedCuestion.getComponentAt(0).setVisible(true);
            textTitulo.requestFocusInWindow();
        } else if (areaEnunciado.getText().isEmpty()) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror2"), recursos);
            tabbedCuestion.setSelectedIndex(0);
            tabbedCuestion.getComponentAt(0).setVisible(true);
            areaEnunciado.requestFocusInWindow();
        } else if (textAutor.getText().isEmpty()) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror3"), recursos);
            tabbedCuestion.setSelectedIndex(0);
            tabbedCuestion.getComponentAt(0).setVisible(true);
            textAutor.requestFocusInWindow();
        } else if (Integer.parseInt(textPostInits.getText())> postjp) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror4")+postjp, recursos);
            tabbedCuestion.setSelectedIndex(1);
            tabbedCuestion.getComponentAt(1).setVisible(true);
            textPostInits.requestFocusInWindow();
        } else if (cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_FICHERO && cuestion.getBytesSolucion() == null) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror5"), recursos);
            tabbedCuestion.setSelectedIndex(1);
            tabbedCuestion.getComponentAt(1).setVisible(true);
            buttonSolucionCuestion.requestFocusInWindow();
        } else if (cuestion.getTipoSolucion().getId() != DiccTiposSolucion.ID_FICHERO && areaSolucionCuestion.getText().isEmpty()) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror6"), recursos);
            tabbedCuestion.setSelectedIndex(1);
            tabbedCuestion.getComponentAt(1).setVisible(true);
            areaSolucionCuestion.requestFocusInWindow();
        } else if (cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_URL && textUrl.getText().isEmpty()) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror7"), recursos);
            tabbedCuestion.setSelectedIndex(0);
            tabbedCuestion.getComponentAt(0).setVisible(true);
            textUrl.requestFocusInWindow();
        } else if (cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_URL && textUsuario.getText().isEmpty()) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror8"), recursos);
            tabbedCuestion.setSelectedIndex(0);
            tabbedCuestion.getComponentAt(0).setVisible(true);
            textUsuario.requestFocusInWindow();
        } else if (cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_URL && new String(textClave.getPassword()).isEmpty()) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror9"), recursos);
            tabbedCuestion.setSelectedIndex(0);
            tabbedCuestion.getComponentAt(0).setVisible(true);
            textClave.requestFocusInWindow();
        } else {
            correcto = true;
        }
        return correcto;
    }

    private boolean checkPesos() {
        if (!cuestion.getJuegosPruebas().isEmpty()) {
            float total = .0f;
            for (JuegoPruebas jp : cuestion.getJuegosPruebas()) {
                total += jp.getPeso();
            }
            total *= 100;
            total = (float) Math.round(total) / 100;
            if (total != 1.0f) {
                mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror10"), recursos);
                tabbedCuestion.setSelectedIndex(3);
                tabbedCuestion.getComponentAt(3).setVisible(true);
                return false;
            }
        }
        return true;
    }

    private boolean checkAtributos() {
        for (JuegoPruebas jp : cuestion.getJuegosPruebas()) {
            if (soc.existe(jp) && !checkJuegoPruebas(jp)) {
                return false;
            }
            for (Comprobacion compr : jp.getComprobaciones()) {
                if (soc.existe(compr) && !checkComprobacion(compr)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkJuegoPruebas(JuegoPruebas jp) {
        boolean correcto = false;
        if (jp.getNombre().length() == 0) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror11"), recursos);
            tabbedCuestion.setSelectedIndex(2);
            tabbedCuestion.getComponentAt(2).setVisible(true);
            comboJP.setSelectedItem(jp);
            tabbedPruebas.setSelectedIndex(0);
            tabbedPruebas.getComponentAt(0).setVisible(true);
            textNombreJP.requestFocusInWindow();
        } else if (jp.getDescripcion().length() == 0) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror12"), recursos);
            tabbedCuestion.setSelectedIndex(2);
            tabbedCuestion.getComponentAt(2).setVisible(true);
            comboJP.setSelectedItem(jp);
            tabbedPruebas.setSelectedIndex(0);
            tabbedPruebas.getComponentAt(0).setVisible(true);
            areaDescripJP.requestFocusInWindow();
        } else if (jp.getMsgError().length() == 0) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror13"), recursos);
            tabbedCuestion.setSelectedIndex(2);
            tabbedCuestion.getComponentAt(2).setVisible(true);
            comboJP.setSelectedItem(jp);
            tabbedPruebas.setSelectedIndex(0);
            tabbedPruebas.getComponentAt(0).setVisible(true);
            areaMsgErrorJP.requestFocusInWindow();
        } else {
            correcto = true;
        }

        return correcto;
    }

    private boolean checkComprobacion(Comprobacion c) {
        boolean correcto = false;
        if (c.getNombre().length() == 0) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror14"), recursos);
            tabbedCuestion.setSelectedIndex(2);
            tabbedCuestion.getComponentAt(2).setVisible(true);
            comboJP.setSelectedItem(c.getJuegoPruebas());
            tabbedPruebas.setSelectedIndex(2);
            tabbedPruebas.getComponentAt(2).setVisible(true);
            comboCompr.setSelectedItem(c);
            tabbedCompr.setSelectedIndex(0);
            tabbedCompr.getComponentAt(0).setVisible(true);
            textNombreJP.requestFocusInWindow();
        } else if (c.getDescripcion().length() == 0) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror15"), recursos);
            tabbedCuestion.setSelectedIndex(2);
            tabbedCuestion.getComponentAt(2).setVisible(true);
            comboJP.setSelectedItem(c.getJuegoPruebas());
            tabbedPruebas.setSelectedIndex(2);
            tabbedPruebas.getComponentAt(2).setVisible(true);
            comboCompr.setSelectedItem(c);
            tabbedCompr.setSelectedIndex(0);
            tabbedCompr.getComponentAt(0).setVisible(true);
            areaDescripCompr.requestFocusInWindow();
        } else if (c.getMsgError().length() == 0) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror16"), recursos);
            tabbedCuestion.setSelectedIndex(2);
            tabbedCuestion.getComponentAt(2).setVisible(true);
            comboJP.setSelectedItem(c.getJuegoPruebas());
            tabbedPruebas.setSelectedIndex(2);
            tabbedPruebas.getComponentAt(2).setVisible(true);
            comboCompr.setSelectedItem(c);
            tabbedCompr.setSelectedIndex(0);
            tabbedCompr.getComponentAt(0).setVisible(true);
            areaMsgErrorCompr.requestFocusInWindow();
        } else if (c.getEntrada().length() == 0) {
            mensaje.mensajeIncorrecto(getTopLevelAncestor(), recursos.getString("cueserror17"), recursos);
            tabbedCuestion.setSelectedIndex(2);
            tabbedCuestion.getComponentAt(2).setVisible(true);
            comboJP.setSelectedItem(c.getJuegoPruebas());
            tabbedPruebas.setSelectedIndex(2);
            tabbedPruebas.getComponentAt(2).setVisible(true);
            comboCompr.setSelectedItem(c);
            tabbedCompr.setSelectedIndex(1);
            tabbedCompr.getComponentAt(1).setVisible(true);
            areaEntradaCompr.requestFocusInWindow();
        } else {
            correcto = true;
        }

        return correcto;
    }

    private void buttonBloquearActionPerformed() {
        Boolean actual = cuestion.getBloqueada();
        cuestion.setBloqueada(!actual);
        soc.setBlock(!actual);
        setTituloModif(false);
    }

    // Métodos que atienden a Listeners ***************************************
    private void buttonGuardarActionPerformed() {
        if(cuestion.getBloqueada()){
            mensaje.mensajeIncorrecto(this, recursos.getString("cueserror20"), recursos);
            return;
        }

        vuelcaJuegoPruebas();
        vuelcaComprobacion();
        if (checkCuestion() && checkPesos() && checkAtributos()) {
            if (mensaje.mensajeConfirmacion(this, soc.getMsgCambios(recursos.getString("cuesaviso3")), recursos) == 0) {
                try {
                    vuelcaCuestion();
                    soc.commit();
                    if (!cuestion.getDisponible()) {
                        areaSalidaJP.setText("");
                        areaSalidaCompr.setText("");
                        ((DefaultTreeTableModel) treeTableEjecucion.getTreeTableModel()).reload(cuestion);
                    }
                    setControlesModif(false);
                    TableModelPesos tmp = (TableModelPesos) tablePesos.getModel();
                    tmp.setModificado(false);
                } catch (Exception e) {
                    mensaje.mensajeError(this, "", e, recursos);
                }
            }
        }
    }

    private void buttonCancelarActionPerformed() {

        if (mensaje.mensajeConfirmacion(this, soc.getMsgCambios(recursos.getString("cuesaviso4")), recursos) == 0) {
            Cuestion original = soc.rollback();
            catalogo.replace(cuestion, original);
            muestraCuestion(original);
        }
    }

    private void textCuestionKeyReleased(KeyEvent e) {
        String s;
        JTextComponent tc = (JTextComponent) e.getSource();
        if (tc == textTitulo) {
            s = cuestion.getTitulo();
        } else if (tc == areaEnunciado) {
            s = cuestion.getEnunciado();
            buttonHtml.setEnabled(true);
        } else if (tc == textAutor) {
            s = cuestion.getAutor();
        } else if (tc == textPostInits) {
            s = Integer.toString(cuestion.getPostInitsJP());
        } else if (tc == textUrl) {
            s = cuestion.getUrl();
        } else if (tc == textUsuario) {
            s = cuestion.getUsuario();
        } else if (tc == textClave) {
            s = cuestion.getClave();
        } else if (tc == areaInitsCuestion) {
            s = cuestion.getInits();
        } else if (tc == areaSolucionCuestion) {
            s = cuestion.getSolucion();
        } else if (tc == areaLimpiezaCuestion) {
            s = cuestion.getLimpieza();
        } else if (tc == areaPostInitsCuestion) {
            s = cuestion.getPostInits();
        } else if (tc == areaEstado) {
            s = cuestion.getEstado();
        } else {
            return;
        }
        if (!tc.getText().trim().equals(s)) {
            soc.setCuestionModif();
            if (tc == areaInitsCuestion || tc == areaSolucionCuestion || tc == areaLimpiezaCuestion || tc == areaPostInitsCuestion) {
                soc.setSalidasModif();
            }
            setControlesModif(true);
        }
    }

    private void textJuegoPruebasKeyReleased(KeyEvent e) {
        String s;
        JTextComponent tc = (JTextComponent) e.getSource();
        if (tc == textNombreJP) {
            s = jpSelected.getNombre();
        } else if (tc == areaDescripJP) {
            s = jpSelected.getDescripcion();
        } else if (tc == areaMsgErrorJP) {
            s = jpSelected.getMsgError();
        } else if (tc == areaInitsJP) {
            s = jpSelected.getInits();
        } else if (tc == areaEntradaJP) {
            s = jpSelected.getEntrada();
        } else if (tc == areaLimpiezaJP) {
            s = jpSelected.getLimpieza();
        } else {
            return;
        }
        if (!tc.getText().trim().equals(s)) {
            soc.set(jpSelected, SaveOnCommit.ESTADO_MODIFICADO);
            if (tc == areaInitsJP || tc == areaEntradaJP || tc == areaLimpiezaJP) {
                soc.setSalidasModif();
            }
            setControlesModif(true);
        }

    }

    private void textComprobacionKeyReleased(KeyEvent e) {
        String s;
        JTextComponent tc = (JTextComponent) e.getSource();
        if (tc == textNombreCompr) {
            s = comprSelected.getNombre();
        } else if (tc == areaDescripCompr) {
            s = comprSelected.getDescripcion();
        } else if (tc == areaMsgErrorCompr) {
            s = comprSelected.getMsgError();
        } else if (tc == areaEntradaCompr) {
            s = comprSelected.getEntrada();
        } else {
            return;
        }
        if (!tc.getText().trim().equals(s)) {
            soc.set(comprSelected, SaveOnCommit.ESTADO_MODIFICADO);
            if (tc == areaEntradaCompr) {
                soc.setSalidasModif();
            }
            setControlesModif(true);
        }

    }

    private void checkBoxCuestionActionPerformed(ActionEvent e) {
        JCheckBox cb = (JCheckBox) e.getSource();
        boolean b;
        if (cb == binaria) {
            b=cuestion.getBinaria();
        } else if (cb == gabia) {
            b=cuestion.getGabia();
        } else if (cb == ssl) {
            b=cuestion.getSsl();
        } else {
            return;
        }
        if (!cb.isSelected() == b) {
            soc.setCuestionModif();
            soc.setSalidasModif();
            setControlesModif(true);
        }

    }

    private void checkBoxJPActionPerformed(ActionEvent e) {
        JCheckBox cb = (JCheckBox) e.getSource();
        boolean b;
        if (cb == binariaJP) {
            b=jpSelected.getBinaria();
        } else if (cb == mensajeJP) {
            b=jpSelected.getMensaje();
        } else {
            return;
        }
        if (!cb.isSelected() == b) {
            soc.set(jpSelected, SaveOnCommit.ESTADO_MODIFICADO);
            soc.setSalidasModif();
            setControlesModif(true);
        }
    }

    private void checkBoxComActionPerformed(ActionEvent e) {
        JCheckBox cb = (JCheckBox) e.getSource();
        boolean b;
        if (cb == binariaCom) {
            b=comprSelected.getBinaria();
        } else if (cb == mensajeCom) {
            b=comprSelected.getMensaje();
        } else {
            return;
        }
        if (!cb.isSelected() == b) {
            soc.set(comprSelected, SaveOnCommit.ESTADO_MODIFICADO);
            soc.setSalidasModif();
            setControlesModif(true);
        }
    }

    private void tipusSolucioUrl(boolean b) {
        label33.setVisible(b);
        textUrl.setVisible(b);
        label34.setVisible(b);
        textUsuario.setVisible(b);
        label35.setVisible(b);
        textClave.setVisible(b);
        label36.setVisible(b);
        ssl.setVisible(b);
    }

    private void comboBoxActionPerformed(ActionEvent e) {
        JComboBox combo = (JComboBox) e.getSource();
        if (combo == comboEsquema && combo.getSelectedItem() != cuestion.getEsquema()) {
            soc.setCuestionModif();
            cuestion.setEsquema((Esquema) combo.getSelectedItem());
            setControlesModif(true);
        } else if (combo == comboCategoria && combo.getSelectedItem() != cuestion.getCategoria()) {
            soc.setCuestionModif();
            soc.setSalidasModif();
            cuestion.setCategoria((Categoria) combo.getSelectedItem());
            cuestion.removeTematicas();
            blockPorCategoria();
            setControlesModif(true);
        } else if (combo == comboIdiomaEnunciado && combo.getSelectedItem() != cuestion.getIdioma()) {
            soc.setCuestionModif();
            soc.setSalidasModif();
            if (cuestion.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
                cuestion.setEnunciado(areaEnunciado.getText().trim());
            } else if (cuestion.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
                cuestion.setEnunciadoEsp(areaEnunciado.getText().trim());
            } else if (cuestion.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
                cuestion.setEnunciadoIng(areaEnunciado.getText().trim());
            }


            if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_CATALA) {
                areaEnunciado.setText(cuestion.getEnunciado());
            } else if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_ESPAÑOL) {
                areaEnunciado.setText(cuestion.getEnunciadoEsp());
            } else if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_ENGLISH) {
                areaEnunciado.setText(cuestion.getEnunciadoIng());
            }

            cuestion.setIdioma((Idioma) combo.getSelectedItem());
//            setControlesModif(true);
        } else if (combo == comboTipoSol && combo.getSelectedItem() != cuestion.getTipoSolucion()) {
            soc.setCuestionModif();
            soc.setSalidasModif();
            tipusSolucioUrl(false);
            if (((TipoSolucion) combo.getSelectedItem()).getId() == DiccTiposSolucion.ID_FICHERO) {
                cuestion.setSolucion(areaSolucionCuestion.getText().trim());
                buttonVerSolucion.setVisible(true);
                areaSolucionCuestion.setEditable(false);
                if (cuestion.existeFicheroSolucion()) {
                    buttonVerSolucion.setEnabled(true);
                    areaSolucionCuestion.setText(STR_FICH_SOLUCION + formateaExtension(cuestion.getExtensionSolucion()));
                } else {
                    fileSolucion = null;
                    buttonVerSolucion.setEnabled(false);
                    areaSolucionCuestion.setText("");
                }
            } else if (((TipoSolucion) combo.getSelectedItem()).getId() == DiccTiposSolucion.ID_URL) {
                tipusSolucioUrl(true);

            }
            if (cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_FICHERO) {
                buttonVerSolucion.setVisible(false);
                areaSolucionCuestion.setEditable(true);
                areaSolucionCuestion.setText(cuestion.getSolucion());
                areaSolucionCuestion.setCaretPosition(0);
            }
            cuestion.setTipoSolucion((TipoSolucion) combo.getSelectedItem());
            setControlesModif(true);
        }
    }

    private void comboIdiomaActionPerformed(ActionEvent e) {
        JComboBox combo = (JComboBox) e.getSource();
        if (jpSelected != null) {
            if (combo == comboIdiomaJP && combo.getSelectedItem() != jpSelected.getIdioma()) {
                soc.setCuestionModif();
                soc.setSalidasModif();
                if (jpSelected.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
                    jpSelected.setMsgError(areaMsgErrorJP.getText().trim());
                } else if (jpSelected.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
                    jpSelected.setMsgErrorEsp(areaMsgErrorJP.getText().trim());
                } else if (jpSelected.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
                    jpSelected.setMsgErrorIng(areaMsgErrorJP.getText().trim());
                }

                if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_CATALA) {
                    areaMsgErrorJP.setText(jpSelected.getMsgError());
                } else if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_ESPAÑOL) {
                    areaMsgErrorJP.setText(jpSelected.getMsgErrorEsp());
                } else if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_ENGLISH) {
                    areaMsgErrorJP.setText(jpSelected.getMsgErrorIng());
                }

                jpSelected.setIdioma((Idioma) combo.getSelectedItem());
//                setControlesModif(true);
            }
        }
        if (comprSelected != null) {
            if (combo == comboIdiomaCompr && combo.getSelectedItem() != comprSelected.getIdioma()) {
                soc.setCuestionModif();
                soc.setSalidasModif();
                if (comprSelected.getIdioma().getId() == DiccIdiomas.ID_CATALA) {
                    comprSelected.setMsgError(areaMsgErrorCompr.getText().trim());
                } else if (comprSelected.getIdioma().getId() == DiccIdiomas.ID_ESPAÑOL) {
                    comprSelected.setMsgErrorEsp(areaMsgErrorCompr.getText().trim());
                } else if (comprSelected.getIdioma().getId() == DiccIdiomas.ID_ENGLISH) {
                    comprSelected.setMsgErrorIng(areaMsgErrorCompr.getText().trim());
                }

                if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_CATALA) {
                    areaMsgErrorCompr.setText(comprSelected.getMsgError());
                } else if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_ESPAÑOL) {
                    areaMsgErrorCompr.setText(comprSelected.getMsgErrorEsp());
                } else if (((Idioma) combo.getSelectedItem()).getId() == DiccIdiomas.ID_ENGLISH) {
                    areaMsgErrorCompr.setText(comprSelected.getMsgErrorIng());
                }

                comprSelected.setIdioma((Idioma) combo.getSelectedItem());
//                setControlesModif(true);
            }
        }
    }

    private void comboJPActionPerformed() {
        if (comboJP.getSelectedIndex() == -1) {
            muestraJP(null);
        } else {
            muestraJP((JuegoPruebas) comboJP.getSelectedItem());
        }
    }

    private void comboComprActionPerformed() {
        if (comboCompr.getSelectedIndex() == -1) {
            muestraComprobacion(null);
        } else {
            muestraComprobacion((Comprobacion) comboCompr.getSelectedItem());
        }
    }

    private void buttonTematicasActionPerformed() {
        try {
            Categoria actualcat = cuestion.getCategoria(); //afegit meu
            dialogTematicas.setLocationRelativeTo(getTopLevelAncestor());
            dialogTematicas.setTableModel(soc.getTematicas(actualcat)); //afegit meu
            dialogTematicas.setSeleccion(cuestion.getTematicas());
            dialogTematicas.firePropertyChange("visible", 0, 1);
            dialogTematicas.setVisible(true);
            List<Tematica> seleccion = dialogTematicas.getSeleccion();
            //Collections.sort(seleccion);

            if (dialogTematicas.haAceptado() && !seleccion.equals(cuestion.getTematicas())) {
                soc.setCuestionModif();
                cuestion.setTematicas(seleccion);
                setControlesModif(true);
            }
        } catch (Exception evento) {
            mensaje.mensajeError(this, "", evento, recursos);
        }
    }

    private void buttonCorrectoresActionPerformed() {
        dialogCorrectores.setLocationRelativeTo(getTopLevelAncestor());
        dialogCorrectores.firePropertyChange("visible", 0, 1);
        dialogCorrectores.setSeleccion(cuestion.getCorrectores(), cuestion.getCategoria());
        dialogCorrectores.setVisible(true);
        List<Corrector> seleccion = dialogCorrectores.getSeleccion();
        Collections.sort(seleccion);
        if (dialogCorrectores.haAceptado() && !seleccion.equals(cuestion.getCorrectores())) {
            try {
                soc.setCorrectoresAndCommit(seleccion);
                cuestion.setCorrectores(seleccion);
                cuestion.clearSalidas();
                areaSalidaJP.setText("");
                areaSalidaCompr.setText("");
                setTituloModif(false);
                if (seleccion.isEmpty()) {
                    buttonEjecutar.setEnabled(false);
                } else {
                    buttonEjecutar.setEnabled(true);
                }
            } catch (Exception e) {
                mensaje.mensajeError(this, "", e, recursos);
            }
        }
    }

    private void buttonPesoActionPerformed() {
        int rowIndex = tablePesos.getSelectedRow();
        if (rowIndex != -1) {
            TableModelPesos tmp = (TableModelPesos) tablePesos.getModel();
            JuegoPruebas jp = (JuegoPruebas) tmp.getValueAt(tablePesos.convertRowIndexToModel(rowIndex));
            float peso = ((Float) spinnerPeso.getValue()).floatValue();
            System.out.println("peso2 "+peso);
            peso *= 100;
            peso = (float) Math.round(peso) / 100;
            spinnerPeso.setValue(peso);
//            if (jp.getPeso() != peso) {
                soc.set(jp, SaveOnCommit.ESTADO_MODIFICADO);
                System.out.println("peso3 "+peso);
                jp.setPeso(peso);
                tmp.fireTableDataChanged();
                selectionModelPesos.setSelectionInterval(rowIndex, rowIndex);
                setControlesModif(true);
//            }
        }
    }

    private void toggleInfoItemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            sifPane.remove(panelEjecucion);
            sifPane.remove(panelSolucion);
            sifPane.add(panelInfo, cc1.xy(1, 1));
            sifPane.validate();
            sifPane.repaint();
        }
    }

    private void toggleEjecucionItemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            sifPane.remove(panelInfo);
            sifPane.remove(panelSolucion);
            sifPane.add(panelEjecucion, cc1.xy(1, 1));
            treeTableEjecucion.expandAll();
            sifPane.validate();
            sifPane.repaint();
        }
    }

    private void toggleSolucionItemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            sifPane.remove(panelInfo);
            sifPane.remove(panelEjecucion);
            sifPane.add(panelSolucion, cc1.xy(1, 1));
            treeTableComparacion.expandAll();
            sifPane.validate();
            sifPane.repaint();
        }
    }

    private void spinnerDificultadStateChanged() {
        float dificultad = ((Float) spinnerDificultad.getValue()).floatValue();
        dificultad *= 1000;
        dificultad = (float) Math.round(dificultad) / 1000;
        spinnerDificultad.setValue(dificultad);
        if (dificultad != cuestion.getDificultad()) {
            soc.setCuestionModif();
            setControlesModif(true);
        }
    }

    private void selectionModelValueChanged(ListSelectionEvent e) {

        if (!e.getValueIsAdjusting()) {
            int rowIndex = tablePesos.getSelectedRow();
            int colIndex = tablePesos.getSelectedColumn();
            if (rowIndex == -1) {
                textNombrePeso.setText("");
                spinnerPeso.setValue(0);
            } else {
                TableModelPesos tmp = (TableModelPesos) tablePesos.getModel();
                JuegoPruebas jp = (JuegoPruebas) tmp.getValueAt(tablePesos.convertRowIndexToModel(rowIndex));
                textNombrePeso.setText(jp.getNombre());
                spinnerPeso.setValue(jp.getPeso());
                if(tmp.getModificado() && colIndex==1){
                    soc.set(jp, SaveOnCommit.ESTADO_MODIFICADO);
                    float p = tmp.getPeso();
                    jp.setPeso(p);
                    tmp.setModificado(false);
                    tmp.fireTableDataChanged();
                    selectionModelPesos.setSelectionInterval(rowIndex, rowIndex);
                    setControlesModif(true);
                }
            }
        }
    }

    private void buttonDelCuestionActionPerformed() {
        String s;
        if(cuestion.getBloqueada()){
            mensaje.mensajeIncorrecto(this, recursos.getString("cueserror20"), recursos);
            return;
        }

        if (cuestion.getId() == 0) {
            s = recursos.getString("cuesaviso5");
        } else {
            s = recursos.getString("cuesaviso6") + cuestion.toString() + "\"?";
        }

        if (mensaje.mensajeConfirmacion(this, s, recursos) == 0) {
            try {
                CtrlVista.setSelectedCuestion(catalogo.remove(cuestion));
            } catch (Exception e) {
                mensaje.mensajeError(this, "", e, recursos);
            }
        }
    }

    private void buttonDelJPActionPerformed() {
        if (mensaje.mensajeConfirmacion(this, recursos.getString("cuesaviso7") + jpSelected.toString() + "\"?", recursos) == 0) {
            soc.set(jpSelected, SaveOnCommit.ESTADO_ELIMINADO);
            soc.setSalidasModif();
            int index = comboJP.getSelectedIndex();
            cuestion.removeJuegoPruebas(jpSelected);
            int total = cuestion.getJuegosPruebas().size();
            if (total == 0) {
                comboJP.setSelectedIndex(-1);
            } else if (index < total) {
                comboJP.setSelectedIndex(index);
            } else {
                comboJP.setSelectedIndex(total - 1);
            }
            if (soc.existenCambios()) {
                setControlesModif(true);
            } else {
                setControlesModif(false);
            }
        }
    }

    private void buttonDelComprActionPerformed() {
        if (mensaje.mensajeConfirmacion(this, recursos.getString("cuesaviso8") + comprSelected.toString() + "\"?", recursos) == 0) {

            soc.set(comprSelected, SaveOnCommit.ESTADO_ELIMINADO);
            soc.setSalidasModif();
            int index = comboCompr.getSelectedIndex();
            jpSelected.removeComprobacion(comprSelected);
            int total = jpSelected.getComprobaciones().size();
            if (total == 0) {
                comboCompr.setSelectedIndex(-1);
            } else if (index < total) {
                comboCompr.setSelectedIndex(index);
            } else {
                comboCompr.setSelectedIndex(total - 1);
            }
            if (soc.existenCambios()) {
                setControlesModif(true);
            } else {
                setControlesModif(false);
            }
        }
    }

    private void buttonNuevoJPActionPerformed() {
        JuegoPruebas jp = new JuegoPruebas();
        soc.set(jp, SaveOnCommit.ESTADO_NUEVO);
        cuestion.addJuegoPruebas(jp);

        comboJP.setSelectedItem(jp);
        tabbedPruebas.setSelectedIndex(0);
        tabbedPruebas.getComponentAt(0).setVisible(true);
        textNombreJP.requestFocusInWindow();
        ((AbstractTableModel) tablePesos.getModel()).fireTableDataChanged();
        setControlesModif(true);
    }

    private void buttonNuevaComprActionPerformed() {
        Comprobacion compr = new Comprobacion();
        soc.set(compr, SaveOnCommit.ESTADO_NUEVO);
        jpSelected.addComprobacion(compr);

        comboCompr.setSelectedItem(compr);
        tabbedCompr.setSelectedIndex(0);
        tabbedCompr.getComponentAt(0).setVisible(true);
        textNombreCompr.requestFocusInWindow();
        setControlesModif(true);

    }

    private void buttonCopiarJPActionPerformed() {
        JuegoPruebas jp = new JuegoPruebas(jpSelected);
        jp.setNombre(recursos.getString("nombrecopia")+ " " + jpSelected.toString());
        jp.setPeso(.0f);
        soc.set(jp, SaveOnCommit.ESTADO_NUEVO);
        cuestion.addJuegoPruebas(jp);

        soc.setSalidasModif();
        comboJP.setSelectedItem(jp);
        tabbedPruebas.setSelectedIndex(0);
        tabbedPruebas.getComponentAt(0).setVisible(true);
        textNombreJP.requestFocusInWindow();
        ((AbstractTableModel) tablePesos.getModel()).fireTableDataChanged();
        setControlesModif(true);
    }

    private void buttonCopiarComprActionPerformed() {
        Comprobacion compr = new Comprobacion(comprSelected);
        compr.setNombre(recursos.getString("nombrecopia")+ " " + comprSelected.toString());
        soc.set(compr, SaveOnCommit.ESTADO_NUEVO);
        jpSelected.addComprobacion(compr);

        soc.setSalidasModif();
        comboCompr.setSelectedItem(compr);
        tabbedCompr.setSelectedIndex(0);
        tabbedCompr.getComponentAt(0).setVisible(true);
        textNombreCompr.requestFocusInWindow();
        setControlesModif(true);
    }

    private void buttonCopiarCuestionActionPerformed() {
        if (mensaje.mensajeConfirmacion(this, recursos.getString("cuesaviso9") + cuestion.getTitulo() + "\"?", recursos) == 0) {
            try {
                Cuestion c = catalogo.copy(cuestion);
                CtrlVista.setSelectedCuestion(null);
                muestraCuestion(c);
                tabbedCuestion.setSelectedIndex(0);
                tabbedCuestion.getComponentAt(0).setVisible(true);
                textTitulo.requestFocusInWindow();
            } catch (Exception e) {
                mensaje.mensajeError(getTopLevelAncestor(), "", e, recursos);
            }
        }
    }

    private void buttonCopiarCuestionBDActionPerformed() {
        if (dialogCopiaBD == null) {
            dialogCopiaBD = new DialogCopiaCuestionBD((JFrame) getTopLevelAncestor(), recursos);
        }
        dialogCopiaBD.setCuestion(cuestion);
        dialogCopiaBD.setVisible(true);
    }

    private void buttonListadoActionPerformed() {
        try {
            dialogListado = new DialogListado((Frame) getTopLevelAncestor(), cuestion, recursos);
            dialogListado.setLocationRelativeTo(getTopLevelAncestor());
            dialogListado.firePropertyChange("visible", 0, 1);
            dialogListado.setVisible(true);

            if (dialogListado.haAceptado()) {
//                soc.setCuestionModif();
//                File generado = dialogListado.ficheroGenerado();
//                byte[] b = getBytesFichero(generado);
//                String ext = getExtensionFichero(generado);
//                cuestion.setFicheroAdjunto(b, ext);
//                fileAdjunto = generado;
//                textFichero.setText(STR_FICH_ADJUNTO + formateaExtension(ext));
//                buttonVerAdjunto.setEnabled(true);
//                buttonDelAdjunto.setEnabled(true);
//                setControlesModif(true);
            }

        } catch (Exception evento) {
            mensaje.mensajeError(getTopLevelAncestor(), "", evento, recursos);
        }
    }

    private void buttonHtmlActionPerformed() {
        DialogHtml dialoghtml = new DialogHtml((JFrame) getTopLevelAncestor(),recursos);
        dialoghtml.setTextEnunciado(areaEnunciado.getText());
        dialoghtml.setVisible(true);
    }

    private void buttonLimpiarActionPerformed() {
        editorResultados.setText("");
    }

    private void buttonExaminarActionPerformed(ActionEvent e) {
        if (fileDir != null) {
            fileChooserabrir.setCurrentDirectory(fileDir);
        }
        fileChooserabrir.setSelectedFile(null);
        fileChooserabrir.removeChoosableFileFilter(filterZip);
        fileChooserabrir.removeChoosableFileFilter(filterTxt);
        fileChooserabrir.removeChoosableFileFilter(filterImg);

        if (e.getSource() == buttonAdjunto) {
            fileChooserabrir.addChoosableFileFilter(filterImg);
            fileChooserabrir.setFileFilter(filterTxt);
        } else if (e.getSource() == buttonSolucionCuestion
                && cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_FICHERO) {
            fileChooserabrir.setFileFilter(filterZip);
        } else {
            fileChooserabrir.setFileFilter(filterTxt);
        }

        if (fileChooserabrir.showDialog(getTopLevelAncestor(), "Seleccionar") == JFileChooser.APPROVE_OPTION) {
            fileDir = fileChooserabrir.getSelectedFile().getParentFile();

            if (e.getSource() == buttonAdjunto) {
                try {
                    byte[] b = getBytesFichero(fileChooserabrir.getSelectedFile());
                    String ext = getExtensionFichero(fileChooserabrir.getSelectedFile());
                    soc.setCuestionModif();
                    cuestion.setFicheroAdjunto(b, ext);
                    fileAdjunto = fileChooserabrir.getSelectedFile();
                    textFichero.setText(STR_FICH_ADJUNTO + formateaExtension(ext));
                    buttonVerAdjunto.setEnabled(true);
                    buttonDelAdjunto.setEnabled(true);
                    setControlesModif(true);
                } catch (Exception ex) {
                    mensaje.mensajeError(getTopLevelAncestor(), "", ex, recursos);
                }
            } else if (e.getSource() == buttonSolucionCuestion && cuestion.getTipoSolucion().getId() == DiccTiposSolucion.ID_FICHERO) {
                try {
                    byte[] b = getBytesFichero(fileChooserabrir.getSelectedFile());
                    String ext = getExtensionFichero(fileChooserabrir.getSelectedFile());
                    soc.setCuestionModif();
                    cuestion.setFicheroSolucion(b, ext);
                    fileSolucion = fileChooserabrir.getSelectedFile();
                    areaSolucionCuestion.setText(STR_FICH_SOLUCION + formateaExtension(ext));
                    buttonVerSolucion.setEnabled(true);
                    setControlesModif(true);
                } catch (Exception ex) {
                    mensaje.mensajeError(getTopLevelAncestor(), "", ex, recursos);
                }
            } else {
                try {
                    String s = getString(fileChooserabrir.getSelectedFile()).trim();
                    if (e.getSource() == buttonSolucionCuestion && !s.equals(cuestion.getSolucion())) {
                        areaSolucionCuestion.setText(s);
                        areaSolucionCuestion.setCaretPosition(0);
                        soc.setCuestionModif();
                        soc.setSalidasModif();
                        setControlesModif(true);
                    } else if (e.getSource() == buttonInitsCuestion && !s.equals(cuestion.getInits())) {
                        areaInitsCuestion.setText(s);
                        areaInitsCuestion.setCaretPosition(0);
                        soc.setCuestionModif();
                        soc.setSalidasModif();
                        setControlesModif(true);
                    } else if (e.getSource() == buttonLimpiezaCuestion && !s.equals(cuestion.getLimpieza())) {
                        areaLimpiezaCuestion.setText(s);
                        areaLimpiezaCuestion.setCaretPosition(0);
                        soc.setCuestionModif();
                        soc.setSalidasModif();
                        setControlesModif(true);
                    } else if (e.getSource() == buttonPostInitsCuestion && !s.equals(cuestion.getPostInits())) {
                        areaPostInitsCuestion.setText(s);
                        areaPostInitsCuestion.setCaretPosition(0);
                        soc.setCuestionModif();
                        soc.setSalidasModif();
                        setControlesModif(true);
                    } else if (e.getSource() == buttonInitsJP && !s.equals(jpSelected.getInits())) {
                        areaInitsJP.setText(s);
                        areaInitsJP.setCaretPosition(0);
                        soc.set(jpSelected, SaveOnCommit.ESTADO_MODIFICADO);
                        soc.setSalidasModif();
                        setControlesModif(true);
                    } else if (e.getSource() == buttonEntradaJP && !s.equals(jpSelected.getEntrada())) {
                        areaEntradaJP.setText(s);
                        areaEntradaJP.setCaretPosition(0);
                        soc.set(jpSelected, SaveOnCommit.ESTADO_MODIFICADO);
                        soc.setSalidasModif();
                        setControlesModif(true);
                    } else if (e.getSource() == buttonLimpiezaJP && !s.equals(jpSelected.getLimpieza())) {
                        areaLimpiezaJP.setText(s);
                        areaLimpiezaJP.setCaretPosition(0);
                        soc.set(jpSelected, SaveOnCommit.ESTADO_MODIFICADO);
                        soc.setSalidasModif();
                        setControlesModif(true);
                    } else if (e.getSource() == buttonEntradaCompr && !s.equals(comprSelected.getEntrada())) {
                        areaEntradaCompr.setText(s);
                        areaEntradaCompr.setCaretPosition(0);
                        soc.set(comprSelected, SaveOnCommit.ESTADO_MODIFICADO);
                        soc.setSalidasModif();
                        setControlesModif(true);
                    }
                } catch (Exception ex) {
                    mensaje.mensajeError(getTopLevelAncestor(), recursos.getString("cueserror18"), ex, recursos);
                    return;
                }
            }
        }
    }

    private void buttonDelFicheroPerformed(ActionEvent e) {
        if (e.getSource() == buttonDelAdjunto
                && mensaje.mensajeConfirmacion(this, recursos.getString("cuesaviso10"), recursos) == 0) {
            soc.setCuestionModif();
            cuestion.setFicheroAdjunto(null, null);
            fileAdjunto = null;
            textFichero.setText("");
            buttonVerAdjunto.setEnabled(false);
            buttonDelAdjunto.setEnabled(false);
            setControlesModif(true);
        }
    }

    private void buttonVerActionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == buttonVerAdjunto) {
                if (fileAdjunto == null) {
                    fileAdjunto = creaFichero("ficheroadjunto" + cuestion.getId(), "." + cuestion.getExtensionFichero(), cuestion.getBytesFichero());
                }

                Desktop.open(fileAdjunto);
            } else if (e.getSource() == buttonVerSolucion) {
                if (fileSolucion == null) {
                    fileSolucion = creaFichero("ficherosolucion" + cuestion.getId(), "." + cuestion.getExtensionSolucion(), cuestion.getBytesSolucion());
                }
                Desktop.open(fileSolucion);
            }
        } catch (DesktopException ex) {
            mensaje.mensajeError(getTopLevelAncestor(), recursos.getString("cueserror19"), ex, recursos);
        }
    }

    public void buttonEjecutarActionPerformed() {
        creaWorkerEjecucion();
        workerEjecucion.execute();
        dialogEspera.setVisible(true);
    }

    private void buttonCompararActionPerformed() {

        cuestion.setSolucionAlumno(areaSolucionAlumno.getText());
        creaWorkerComparar();
        workerComparar.execute();
        dialogEspera.setVisible(true);
        
        float nota=0;
        for(JuegoPruebas jpactual: cuestion.getJuegosPruebas()){
            if(!jpactual.getNota().equals("--")) nota=nota+Float.parseFloat(jpactual.getNota());
        }
        
        if(cuestion.getJuegosPruebas().get(0).getNota().equals("0")) textNotaPublic.setText("0");
        else textNotaPublic.setText(Float.toString(nota));
        
        textNotaReal.setText(Float.toString(nota));

        if(nota==10) textNotaBinaria.setText("10.0");
        else textNotaBinaria.setText("0");

    }

    private void buttonLimpiarSolActionPerformed() {
        areaSolucionAlumno.setText("");
        editorAlumno.setText("");
        textNotaPublic.setText("");
        textNotaReal.setText("");
        textNotaBinaria.setText("");

        for (JuegoPruebas jp : cuestion.getJuegosPruebas()) {
            jp.setSalidaAlumno("--");
            jp.setComparacion("--");
            for (Comprobacion compr : jp.getComprobaciones()) {
                compr.setSalidaAlumno("--");
                compr.setComparacion("--");
            }
            jp.setNota("--");
        }

        ((DefaultTreeTableModel) treeTableComparacion.getTreeTableModel()).reload(cuestion);
        lines = cuentalinias();
        treeTableComparacion.setRowHeight(18 * lines);
        treeTableComparacion.expandAll();

    }

    private void buttonGenerarActionPerformed() {
        try {
            dialogAttach = new DialogAttachement((Frame) getTopLevelAncestor(), cuestion, recursos);
            dialogAttach.setLocationRelativeTo(getTopLevelAncestor());
            dialogAttach.firePropertyChange("visible", 0, 1);
            dialogAttach.setVisible(true);

            if (dialogAttach.haAceptado()) {

                soc.setCuestionModif();
                File generado = dialogAttach.ficheroGenerado();
                byte[] b = getBytesFichero(generado);
                String ext = getExtensionFichero(generado);
                cuestion.setFicheroAdjunto(b, ext);
                fileAdjunto = generado;
                textFichero.setText(STR_FICH_ADJUNTO + formateaExtension(ext));
                buttonVerAdjunto.setEnabled(true);
                buttonDelAdjunto.setEnabled(true);
                CtrlVista.guarda();
//                setControlesModif(true);
            }

        } catch (Exception evento) {
            mensaje.mensajeError(getTopLevelAncestor(), "", evento, recursos);
        }

    }

    private void buttonMoverActionPerformed(ActionEvent e) {
        TreeNode nodo1 = (TreeNode) treeTableEjecucion.getPathForRow(treeTableEjecucion.getSelectedRow()).getLastPathComponent();
        int pos1 = nodo1.getParent().getIndex(nodo1);
        int pos2 = pos1;
        if (e.getSource() == buttonBajar) {
            pos2++;
        } else {
            pos2--;
        }
        TreeNode nodo2 = nodo1.getParent().getChildAt(pos2);

        try {
            if (nodo1 instanceof JuegoPruebas) {
                JuegoPruebas jp1 = (JuegoPruebas) nodo1;
                JuegoPruebas jp2 = (JuegoPruebas) nodo2;
                soc.swapAndCommit(jp1, jp2);
                cuestion.swapJuegosPruebas(jp1, jp2);
            } else if (nodo1 instanceof Comprobacion) {
                Comprobacion compr1 = (Comprobacion) nodo1;
                Comprobacion compr2 = (Comprobacion) nodo2;
                soc.swapAndCommit(compr1, compr2);
                compr1.getJuegoPruebas().swapComprobaciones(compr1, compr2);
            }

            cuestion.clearSalidas();
            areaSalidaJP.setText("");
            areaSalidaCompr.setText("");
            setTituloModif(false);

            DefaultTreeTableModel dttm = (DefaultTreeTableModel) treeTableEjecucion.getTreeTableModel();
            dttm.nodeStructureChanged(cuestion);
            final TreePath selectedPath = new TreePath(dttm.getPathToRoot(nodo1));

            Runnable setSelectionRunnable = new Runnable() {

                public void run() {
                    treeSelectionModelEjec.setSelectionPath(selectedPath);
                }
            };

            treeTableEjecucion.expandAll();
            SwingUtilities.invokeLater(setSelectionRunnable);
        } catch (Exception ex) {
            ex.printStackTrace();
            mensaje.mensajeError(getTopLevelAncestor(), "", ex, recursos);
        }
    }

    private void treeSelectionModelEjecValueChanged(TreeSelectionEvent e) {
        if (e.getNewLeadSelectionPath() == null) {
            buttonSubir.setEnabled(false);
            buttonBajar.setEnabled(false);
        } else {
            TreeNode nodo = (TreeNode) e.getNewLeadSelectionPath().getLastPathComponent();
            int total = nodo.getParent().getChildCount();
            int pos = nodo.getParent().getIndex(nodo);
            if (total == 1) {
                buttonSubir.setEnabled(false);
                buttonBajar.setEnabled(false);
            } else if (pos == 0) {
                buttonSubir.setEnabled(false);
                buttonBajar.setEnabled(true);
            } else if (pos == total - 1) {
                buttonSubir.setEnabled(true);
                buttonBajar.setEnabled(false);
            } else {
                buttonSubir.setEnabled(true);
                buttonBajar.setEnabled(true);
            }
        }
    }

    private void treeTableEjecucionTreeCollapsed() {
        treeTableEjecucion.expandAll();
        treeTableComparacion.expandAll();
    }

    // initComponents *********************************************************
    private void initComponents() throws Exception {
        // Component initialization
        compFactory = DefaultComponentFactory.getInstance();
        sifCuestion = new SimpleInternalFrame();
        toolBarModo = new JToolBar();
        toggleInfo = new JToggleButton();
        toggleEjecucion = new JToggleButton();
        toggleSolucion = new JToggleButton();
        panelInfo = new JPanel();
        tabbedCuestion = new JTabbedPane();
        panelCuestionDatos = new JPanel();
        separator1 = compFactory.createSeparator(recursos.getString("separator1"));
        label1 = new JLabel();
        textTitulo = new JTextField();
        label47 = new JLabel();
        textId = new JTextField();
        label2 = new JLabel();
        textFichero = new JTextField();
        buttonDelAdjunto = new JButton();
        buttonVerAdjunto = new JButton();
        buttonAdjunto = new JButton();
        label3 = new JLabel();
        scrollPane1 = new JScrollPane();
        areaEnunciado = new JTextArea();
        label31 = new JLabel();
        binaria = new JCheckBox();
        label32 = new JLabel();
        gabia = new JCheckBox();
        label42 = new JLabel();
        comboIdiomaEnunciado = new JComboBox();
        buttonHtml = new JButton();
        separator2 = compFactory.createSeparator(recursos.getString("separator2"));
        label14 = new JLabel();
        spinnerDificultad = new JSpinner();
        label8 = new JLabel();
        scrollPane2 = new JScrollPane();
        listTematicas = new JXList();
        label4 = new JLabel();
        textAutor = new JTextField();
        label6 = new JLabel();
        comboTipoSol = new JComboBox();
        label33 = new JLabel();
        textUrl = new JTextField();
        label34 = new JLabel();
        textUsuario = new JTextField();
        label35 = new JLabel();
        textClave = new JPasswordField();
        label36 = new JLabel();
        ssl = new JCheckBox();
        label5 = new JLabel();
        comboCategoria = new JComboBox();
        label7 = new JLabel();
        comboEsquema = new JComboBox();
        buttonTematicas = new JButton();
        panelCuestionSQL = new JPanel();
        label15 = new JLabel();
        label16 = new JLabel();
        label17 = new JLabel();
        scrollPane5 = new JScrollPane();
        areaInitsCuestion = new JTextArea();
        scrollPane6 = new JScrollPane();
        areaSolucionCuestion = new JTextArea();
        scrollPane7 = new JScrollPane();
        areaLimpiezaCuestion = new JTextArea();
        areaPostInitsCuestion = new JTextArea();
        buttonInitsCuestion = new JButton();
        buttonVerSolucion = new JButton();
        buttonSolucionCuestion = new JButton();
        buttonLimpiezaCuestion = new JButton();
        buttonPostInitsCuestion = new JButton();
        textPostInits = new JTextField();
        panelCuestionPruebas = new JPanel();
        label12 = new JLabel();
        comboJP = new JComboBox();
        buttonNuevoJP = new JButton();
        buttonCopiarJP = new JButton();
        buttonDelJP = new JButton();
        tabbedPruebas = new JTabbedPane();
        panelPruebasDatos = new JPanel();
        label18 = new JLabel();
        textNombreJP = new JTextField();
        label19 = new JLabel();
        scrollPane8 = new JScrollPane();
        areaDescripJP = new JTextArea();
        label20 = new JLabel();
        scrollPane9 = new JScrollPane();
        areaMsgErrorJP = new JTextArea();
        label37 = new JLabel();
        binariaJP = new JCheckBox();
        label38 = new JLabel();
        mensajeJP = new JCheckBox();
        label45 = new JLabel();
        comboIdiomaJP = new JComboBox();
        panelPruebasSQL = new JPanel();
        label21 = new JLabel();
        label22 = new JLabel();
        label23 = new JLabel();
        scrollPane10 = new JScrollPane();
        areaInitsJP = new JTextArea();
        scrollPane11 = new JScrollPane();
        areaEntradaJP = new JTextArea();
        scrollPane12 = new JScrollPane();
        areaLimpiezaJP = new JTextArea();
        buttonInitsJP = new JButton();
        buttonEntradaJP = new JButton();
        buttonLimpiezaJP = new JButton();
        label24 = new JLabel();
        scrollPane13 = new JScrollPane();
        areaSalidaJP = new JTextArea();
        panelPruebasCompr = new JPanel();
        label13 = new JLabel();
        comboCompr = new JComboBox();
        buttonNuevaCompr = new JButton();
        buttonCopiarCompr = new JButton();
        buttonDelCompr = new JButton();
        tabbedCompr = new JTabbedPane();
        panelComprDatos = new JPanel();
        label9 = new JLabel();
        textNombreCompr = new JTextField();
        label10 = new JLabel();
        scrollPane3 = new JScrollPane();
        areaDescripCompr = new JTextArea();
        label11 = new JLabel();
        scrollPane4 = new JScrollPane();
        areaMsgErrorCompr = new JTextArea();
        label39 = new JLabel();
        binariaCom = new JCheckBox();
        label40 = new JLabel();
        mensajeCom = new JCheckBox();
        label46 = new JLabel();
        comboIdiomaCompr = new JComboBox();
        panelComprSQL = new JPanel();
        label28 = new JLabel();
        scrollPane18 = new JScrollPane();
        areaEntradaCompr = new JTextArea();
        buttonEntradaCompr = new JButton();
        label29 = new JLabel();
        label30 = new JLabel();
        scrollPane19 = new JScrollPane();
        scrollPane20 = new JScrollPane();
        areaSalidaCompr = new JTextArea();
        panelCuestionPesos = new JPanel();
        scrollPane17 = new JScrollPane();
        tablePesos = new JXTable();
        label26 = new JLabel();
        textNombrePeso = new JTextField();
        label27 = new JLabel();
        spinnerPeso = new JSpinner();
        buttonPeso = new JButton();
        separator3 = new JSeparator();
        panelCuestionEstado = new JPanel();
        label41 = new JLabel();
        scrollPane21 = new JScrollPane();
        areaEstado = new JTextArea();
        buttonCopiarCuestion = new JButton();
        buttonCopiarCuestionBD = new JButton();
        buttonDelCuestion = new JButton();
        buttonListado = new JButton();
        buttonBloquear = new JButton();
        buttonGuardar = new JButton();
        buttonCancelar = new JButton();
        panelEjecucion = new JPanel();
        separator4 = compFactory.createSeparator(recursos.getString("separator4"));
        scrollPane14 = new JScrollPane();
        listCorrectores = new JXList();
        buttonCorrectores = new JButton();
        separator5 = compFactory.createSeparator(recursos.getString("separator5"));
        scrollPane15 = new JScrollPane();
        treeTableEjecucion = new JXTreeTable();
        buttonSubir = new JButton();
        buttonBajar = new JButton();
        separator6 = new JSeparator();
        buttonGenerar = new JButton();
        buttonEjecutar = new JButton();
        label25 = new JLabel();
        scrollPane16 = new JScrollPane();
        editorResultados = new JTextArea();
        buttonLimpiar = new JButton();
        panelSolucion = new JPanel();
        label43 = new JLabel();
        scrollPane22 = new JScrollPane();
        areaSolucionAlumno = new JTextArea();
        buttonComparar = new JButton();
        buttonLimpiezaSol = new JButton();
        label44 = new JLabel();
        scrollPane23 = new JScrollPane();
        treeTableComparacion = new JXTreeTable();
        scrollPane24 = new JScrollPane();
        editorAlumno = new JTextArea();
        label48 = new JLabel();
        textNotaPublic = new JTextField();
        label49 = new JLabel();
        textNotaReal = new JTextField();
        label50 = new JLabel();
        textNotaBinaria = new JTextField();
        pipeline = new HighlighterPipeline();
        selectionModelPesos = (DefaultListSelectionModel) tablePesos.getSelectionModel();
        treeSelectionModelEjec = (DefaultTreeSelectionModel) treeTableEjecucion.getTreeSelectionModel();
        cc = new CellConstraints();

        //======== this ========
        setLayout(new FormLayout(
                "default:grow",
                "fill:default:grow"));

        //======== sifCuestion ========
        {
            sifCuestion.setToolBar(toolBarModo);
            sifCuestion.setFrameIcon(new ImageIcon(getClass().getResource("/imagenes/panel_cuestion.png")));
            Container sifCuestionContentPane = sifCuestion.getContentPane();
            sifCuestionContentPane.setLayout(new FormLayout(
                    "default:grow",
                    "fill:default:grow"));
        }
        add(sifCuestion, cc.xy(1, 1));

        //======== toolBarModo ========
        {
            //---- toggleInfo ----
            toggleInfo.setToolTipText(recursos.getString("toggleInfo"));
            toggleInfo.setIcon(new ImageIcon(getClass().getResource("/imagenes/application_form.png")));
            toggleInfo.setBorder(UIManager.getBorder("ToggleButton.border"));
            toggleInfo.setMargin(new Insets(0, 2, 0, 2));
            toggleInfo.addItemListener(new ItemListener() {

                public void itemStateChanged(ItemEvent e) {
                    toggleInfoItemStateChanged(e);
                }
            });
            toolBarModo.add(toggleInfo);

            //---- toggleEjecucion ----
            toggleEjecucion.setToolTipText(recursos.getString("toggleEjecucion"));
            toggleEjecucion.setIcon(new ImageIcon(getClass().getResource("/imagenes/cog_go.png")));
            toggleEjecucion.setBorder(UIManager.getBorder("ToggleButton.border"));
            toggleEjecucion.setMargin(new Insets(0, 2, 0, 2));
            toggleEjecucion.addItemListener(new ItemListener() {

                public void itemStateChanged(ItemEvent e) {
                    toggleEjecucionItemStateChanged(e);
                }
            });
            toolBarModo.add(toggleEjecucion);

            //---- toggleSolucion ----
            toggleSolucion.setToolTipText(recursos.getString("toggleSolucion"));
            toggleSolucion.setIcon(new ImageIcon(getClass().getResource("/imagenes/user_go.png")));
            toggleSolucion.setBorder(UIManager.getBorder("ToggleButton.border"));
            toggleSolucion.setMargin(new Insets(0, 2, 0, 2));
            toggleSolucion.addItemListener(new ItemListener() {

                public void itemStateChanged(ItemEvent e) {
                    toggleSolucionItemStateChanged(e);
                }
            });
            toolBarModo.add(toggleSolucion);
        }

        //======== panelInfo ========
        {
            panelInfo.setLayout(new FormLayout(
                    new ColumnSpec[]{
                        FormFactory.UNRELATED_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        new ColumnSpec(ColumnSpec.FILL, Sizes.ZERO, FormSpec.DEFAULT_GROW),
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.UNRELATED_GAP_COLSPEC
                    },
                    new RowSpec[]{
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.UNRELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.UNRELATED_GAP_ROWSPEC
                    }));

            //======== tabbedCuestion ========
            {
                tabbedCuestion.setBorder(null);
                tabbedCuestion.putClientProperty(Options.EMBEDDED_TABS_KEY, Boolean.TRUE);

                //======== panelCuestionDatos ========
                {
                    panelCuestionDatos.setBorder(null);
                    panelCuestionDatos.setLayout(new FormLayout(
                            new ColumnSpec[]{
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.NO_GROW),
                                new ColumnSpec(Sizes.dluX(30)),
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(80), FormSpec.NO_GROW),
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(22), FormSpec.NO_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.DEFAULT, 0.5),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(15)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(23), FormSpec.NO_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(37)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(45), FormSpec.NO_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(15)),
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                FormFactory.UNRELATED_GAP_COLSPEC
                            },
                            new RowSpec[]{
                                FormFactory.UNRELATED_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.PARAGRAPH_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                new RowSpec(Sizes.DLUY8),
                                new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.PARAGRAPH_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.PARAGRAPH_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.RELATED_GAP_ROWSPEC
                            }));
                    panelCuestionDatos.add(separator1, cc.xywh(2, 2, 24, 1));

                    //---- label1 ----
                    label1.setText(recursos.getString("label1"));
                    panelCuestionDatos.add(label1, cc.xy(2, 4));

                    //---- textTitulo ----
                    textTitulo.addKeyListener(new KeyAdapter() {

                        @Override
                        public void keyReleased(KeyEvent e) {
                            textCuestionKeyReleased(e);
                        }
                    });
                    panelCuestionDatos.add(textTitulo, cc.xywh(4, 4, 18, 1));

                    //---- label47 ----
                    label47.setText(recursos.getString("label47"));
                    panelCuestionDatos.add(label47, cc.xy(22, 4));

                    //---- textId ----
                    textId.setEditable(false);
                    panelCuestionDatos.add(textId, cc.xy(24, 4));

                    //---- label2 ----
                    label2.setText(recursos.getString("label2"));
                    panelCuestionDatos.add(label2, cc.xy(2, 6));

                    //---- textFichero ----
                    textFichero.setEditable(false);
                    textFichero.setFocusable(false);
                    panelCuestionDatos.add(textFichero, cc.xywh(4, 6, 11, 1));

                    //---- buttonDelAdjunto ----
                    buttonDelAdjunto.setIcon(new ImageIcon(getClass().getResource("/imagenes/deletefile.png")));
                    buttonDelAdjunto.setToolTipText(recursos.getString("buttonDelAdjuntoTooltip"));
                    buttonDelAdjunto.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonDelFicheroPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(buttonDelAdjunto, cc.xy(16, 6));

                    //---- buttonVerAdjunto ----
                    buttonVerAdjunto.setText(recursos.getString("buttonVerAdjunto"));
                    buttonVerAdjunto.setIcon(new ImageIcon(getClass().getResource("/imagenes/eye.png")));
                    buttonVerAdjunto.setIconTextGap(8);
                    buttonVerAdjunto.setToolTipText(recursos.getString("buttonVerAdjuntoTooltip"));
                    buttonVerAdjunto.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonVerActionPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(buttonVerAdjunto, cc.xywh(18, 6, 3, 1));

                    //---- buttonAdjunto ----
                    buttonAdjunto.setText(recursos.getString("buttonAdjunto"));
                    buttonAdjunto.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                    buttonAdjunto.setIconTextGap(8);
                    buttonAdjunto.setToolTipText(recursos.getString("buttonAdjuntoTooltip"));
                    buttonAdjunto.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonExaminarActionPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(buttonAdjunto, cc.xywh(22, 6, 4, 1));

                    //---- label3 ----
                    label3.setText(recursos.getString("label3"));
                    panelCuestionDatos.add(label3, cc.xy(2, 8));

                    //======== scrollPane1 ========
                    {
                        //---- areaEnunciado ----
                        areaEnunciado.setWrapStyleWord(true);
                        areaEnunciado.setLineWrap(true);
                        areaEnunciado.setTabSize(4);
                        areaEnunciado.addKeyListener(new KeyAdapter() {

                            @Override
                            public void keyReleased(KeyEvent e) {
                                textCuestionKeyReleased(e);
                            }
                        });
                        scrollPane1.setViewportView(areaEnunciado);
                    }
                    panelCuestionDatos.add(scrollPane1, cc.xywh(4, 8, 22, 2));

                    //---- label31 ----
                    label31.setText(recursos.getString("label31"));
                    panelCuestionDatos.add(label31, cc.xy(4, 11));

                    //---- checkboxbinaria ----
                    binaria.setBackground(Color.white);
                    binaria.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            checkBoxCuestionActionPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(binaria, cc.xy(6, 11));

                    //---- label32 ----
                    label32.setText(recursos.getString("label32"));
                    panelCuestionDatos.add(label32, cc.xy(9, 11));

                    //---- checkboxgabia ----
                    gabia.setBackground(Color.white);
                    gabia.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            checkBoxCuestionActionPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(gabia, cc.xy(11, 11));

                    //---- label42 ----
                    label42.setText(recursos.getString("label42"));
                    panelCuestionDatos.add(label42, cc.xy(14, 11));

                    //---- comboIdiomaEnunciado ----
                    comboIdiomaEnunciado.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            comboBoxActionPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(comboIdiomaEnunciado, cc.xywh(16, 11, 5, 1));

                    //---- buttonHtml ----
                    buttonHtml.setText(recursos.getString("buttonHtml"));
                    buttonHtml.setIcon(new ImageIcon(getClass().getResource("/imagenes/eye.png")));
                    buttonHtml.setIconTextGap(8);
                    buttonHtml.setToolTipText(recursos.getString("buttonHtmlTooltip"));
                    buttonHtml.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            buttonHtmlActionPerformed();
                        }
                    });
                    panelCuestionDatos.add(buttonHtml, cc.xywh(22, 11, 4, 1));

                    panelCuestionDatos.add(separator2, cc.xywh(2, 13, 24, 1));

                    //---- label14 ----
                    label14.setText(recursos.getString("label14"));
                    panelCuestionDatos.add(label14, cc.xy(2, 15));

                    //---- spinnerDificultad ----
                    spinnerDificultad.setToolTipText(recursos.getString("spinnerDificultad"));
                    spinnerDificultad.addChangeListener(new ChangeListener() {

                        public void stateChanged(ChangeEvent e) {
                            spinnerDificultadStateChanged();
                        }
                    });
                    spinnerDificultad.setModel(new SpinnerNumberModel(new Float(0.5f), new Float(0.0f), new Float(1.0F), new Float(0.1F)));
                    panelCuestionDatos.add(spinnerDificultad, cc.xy(4, 15));

                    //---- label8 ----
                    label8.setText(recursos.getString("label8"));
                    panelCuestionDatos.add(label8, cc.xy(14, 15));

                    //======== scrollPane2 ========
                    {
                        //---- listTematicas ----
                        listTematicas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                        listTematicas.setHighlighters(pipeline);
                        listTematicas.setFocusable(false);
                        scrollPane2.setViewportView(listTematicas);
                    }
                    panelCuestionDatos.add(scrollPane2, cc.xywh(16, 15, 10, 7));

                    //---- label4 ----
                    label4.setText(recursos.getString("label4"));
                    panelCuestionDatos.add(label4, cc.xy(2, 17));

                    //---- textAutor ----
                    textAutor.addKeyListener(new KeyAdapter() {

                        @Override
                        public void keyReleased(KeyEvent e) {
                            textCuestionKeyReleased(e);
                        }
                    });
                    panelCuestionDatos.add(textAutor, cc.xywh(4, 17, 6, 1));

                    //---- label5 ----
                    label5.setText(recursos.getString("label5"));
                    panelCuestionDatos.add(label5, cc.xy(2, 19));

                    //---- comboCategoria ----
                    comboCategoria.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            comboBoxActionPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(comboCategoria, cc.xywh(4, 19, 6, 1));

                    //---- label7 ----
                    label7.setText(recursos.getString("label7"));
                    panelCuestionDatos.add(label7, cc.xy(2, 21));

                    //---- comboEsquema ----
                    comboEsquema.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            comboBoxActionPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(comboEsquema, cc.xywh(4, 21, 6, 1));

                    //---- label6 ----
                    label6.setText(recursos.getString("label6"));
                    panelCuestionDatos.add(label6, cc.xy(2, 23));

                    //---- comboTipoSol ----
                    comboTipoSol.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            comboBoxActionPerformed(e);
                        }
                    });
                    panelCuestionDatos.add(comboTipoSol, cc.xywh(4, 23, 4, 1));

                    //---- label33 ----
                    label33.setText(recursos.getString("label33"));
                    label33.setVisible(false);
                    panelCuestionDatos.add(label33, cc.xy(2, 25));

                    //---- textUrl ----
                    textUrl.addKeyListener(new KeyAdapter() {

                        @Override
                        public void keyReleased(KeyEvent e) {
                            textCuestionKeyReleased(e);
                        }
                    });
                    textUrl.setVisible(false);
                    panelCuestionDatos.add(textUrl, cc.xywh(4, 25, 6, 1));

                    //---- label34 ----
                    label34.setText(recursos.getString("label34"));
                    label34.setVisible(false);
                    panelCuestionDatos.add(label34, cc.xy(12, 25));

                    //---- textUsuario ----
                    textUsuario.addKeyListener(new KeyAdapter() {

                        @Override
                        public void keyReleased(KeyEvent e) {
                            textCuestionKeyReleased(e);
                        }
                    });
                    textUsuario.setVisible(false);
                    panelCuestionDatos.add(textUsuario, cc.xywh(14, 25, 3, 1));

                    //---- label35 ----
                    label35.setText(recursos.getString("label35"));
                    label35.setVisible(false);
                    panelCuestionDatos.add(label35, cc.xy(18, 25));

                    //---- textClave ----
                    textClave.addKeyListener(new KeyAdapter() {

                        @Override
                        public void keyReleased(KeyEvent e) {
                            textCuestionKeyReleased(e);
                        }
                    });
                    textClave.setVisible(false);
                    panelCuestionDatos.add(textClave, cc.xywh(20, 25, 2, 1));

                    //---- label36 ----
                    label36.setText(recursos.getString("label36"));
                    label36.setVisible(false);
                    panelCuestionDatos.add(label36, cc.xy(22, 25));

                    //---- checkboxssl ----
                    ssl.setBackground(Color.white);
                    ssl.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            checkBoxCuestionActionPerformed(e);
                        }
                    });
                    ssl.setVisible(false);
                    panelCuestionDatos.add(ssl, cc.xy(25, 25));

                    //---- buttonTematicas ----
                    buttonTematicas.setText(recursos.getString("buttonTematicas"));
                    buttonTematicas.setIcon(new ImageIcon(getClass().getResource("/imagenes/editlist.png")));
                    buttonTematicas.setIconTextGap(8);
                    buttonTematicas.setToolTipText(recursos.getString("buttonTematicasTooltip"));
                    buttonTematicas.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonTematicasActionPerformed();
                        }
                    });
                    panelCuestionDatos.add(buttonTematicas, cc.xywh(22, 23, 4, 1));
                }
                tabbedCuestion.addTab(recursos.getString("tabbedCuestion1"), panelCuestionDatos);


                //======== panelCuestionSQL ========
                {
                    panelCuestionSQL.setLayout(new FormLayout(
                            new ColumnSpec[]{
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.30000000000000004),
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.30000000000000004),
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.30000000000000004),
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.UNRELATED_GAP_COLSPEC
                            },
                            new RowSpec[]{
                                FormFactory.UNRELATED_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                new RowSpec(RowSpec.FILL, Sizes.dluY(0), FormSpec.DEFAULT_GROW),
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                new RowSpec(RowSpec.FILL, Sizes.dluY(0), FormSpec.DEFAULT_GROW),
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.RELATED_GAP_ROWSPEC
                            }));
                    ((FormLayout) panelCuestionSQL.getLayout()).setColumnGroups(new int[][]{{2, 7, 12}});

                    //---- label15 ----
                    label15.setText(recursos.getString("label15"));
                    panelCuestionSQL.add(label15, cc.xywh(2, 2, 4, 1));

                    //---- label16 ----
                    label16.setText(recursos.getString("label16"));
                    panelCuestionSQL.add(label16, cc.xywh(7, 2, 4, 1));

                    //---- label17 ----
                    label17.setText(recursos.getString("label17"));
                    panelCuestionSQL.add(label17, cc.xywh(12, 2, 4, 1));

                    //---- label30 ----
                    label30.setText(recursos.getString("label30"));
                    panelCuestionSQL.add(label30, cc.xywh(12, 7, 4, 1));

                    //======== scrollPane5 ========
                    {
                        //---- areaInitsCuestion ----
                        areaInitsCuestion.setWrapStyleWord(true);
                        areaInitsCuestion.setLineWrap(true);
                        areaInitsCuestion.setTabSize(4);
                        areaInitsCuestion.addKeyListener(new KeyAdapter() {

                            @Override
                            public void keyReleased(KeyEvent e) {
                                textCuestionKeyReleased(e);
                            }
                        });
                        scrollPane5.setViewportView(areaInitsCuestion);
                    }
                    panelCuestionSQL.add(scrollPane5, cc.xywh(2, 3, 4, 6));

                    //======== scrollPane6 ========
                    {
                        //---- areaSolucionCuestion ----
                        areaSolucionCuestion.setWrapStyleWord(true);
                        areaSolucionCuestion.setLineWrap(true);
                        areaSolucionCuestion.setTabSize(4);
                        areaSolucionCuestion.addKeyListener(new KeyAdapter() {

                            @Override
                            public void keyReleased(KeyEvent e) {
                                textCuestionKeyReleased(e);
                            }
                        });
                        scrollPane6.setViewportView(areaSolucionCuestion);
                    }
                    panelCuestionSQL.add(scrollPane6, cc.xywh(7, 3, 4, 6));

                    //======== scrollPane7 ========
                    {
                        //---- areaLimpiezaCuestion ----
                        areaLimpiezaCuestion.setWrapStyleWord(true);
                        areaLimpiezaCuestion.setLineWrap(true);
                        areaLimpiezaCuestion.setTabSize(4);
                        areaLimpiezaCuestion.addKeyListener(new KeyAdapter() {

                            @Override
                            public void keyReleased(KeyEvent e) {
                                textCuestionKeyReleased(e);
                            }
                        });
                        scrollPane7.setViewportView(areaLimpiezaCuestion);
                    }
                    panelCuestionSQL.add(scrollPane7, cc.xywh(12, 3, 4, 1));

                    //======== scrollPane20 ========
                    {
                        //---- areaPostInitsCuestion ----
                        areaPostInitsCuestion.setWrapStyleWord(true);
                        areaPostInitsCuestion.setLineWrap(true);
                        areaPostInitsCuestion.setTabSize(4);
                        areaPostInitsCuestion.addKeyListener(new KeyAdapter() {

                            @Override
                            public void keyReleased(KeyEvent e) {
                                textCuestionKeyReleased(e);
                            }
                        });
                        scrollPane20.setViewportView(areaPostInitsCuestion);
                    }
                    panelCuestionSQL.add(scrollPane20, cc.xywh(12, 8, 4, 1));

                    //---- textPostInits ----
                    textPostInits.addKeyListener(new KeyAdapter() {

                        @Override
                        public void keyReleased(KeyEvent e) {
                            textCuestionKeyReleased(e);
                        }
                    });
                    panelCuestionSQL.add(textPostInits, cc.xy(12, 10));

                    //---- buttonInitsCuestion ----
                    buttonInitsCuestion.setText(recursos.getString("buttonInitsCuestion"));
                    buttonInitsCuestion.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                    buttonInitsCuestion.setIconTextGap(8);
                    buttonInitsCuestion.setToolTipText(recursos.getString("buttonInitsCuestionTooltip"));
                    buttonInitsCuestion.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonExaminarActionPerformed(e);
                        }
                    });
                    panelCuestionSQL.add(buttonInitsCuestion, cc.xy(5, 10));

                    //---- buttonVerSolucion ----
                    buttonVerSolucion.setText(recursos.getString("buttonVerSolucion"));
                    buttonVerSolucion.setIcon(new ImageIcon(getClass().getResource("/imagenes/eye.png")));
                    buttonVerSolucion.setToolTipText(recursos.getString("buttonVerSolucionTooltip"));
                    buttonVerSolucion.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonVerActionPerformed(e);
                        }
                    });
                    panelCuestionSQL.add(buttonVerSolucion, cc.xy(8, 10));

                    //---- buttonSolucionCuestion ----
                    buttonSolucionCuestion.setText(recursos.getString("buttonSolucionCuestion"));
                    buttonSolucionCuestion.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                    buttonSolucionCuestion.setIconTextGap(8);
                    buttonSolucionCuestion.setToolTipText(recursos.getString("buttonSolucionCuestionTooltip"));
                    buttonSolucionCuestion.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonExaminarActionPerformed(e);
                        }
                    });
                    panelCuestionSQL.add(buttonSolucionCuestion, cc.xy(10, 10));

                    //---- buttonLimpiezaCuestion ----
                    buttonLimpiezaCuestion.setText(recursos.getString("buttonLimpiezaCuestion"));
                    buttonLimpiezaCuestion.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                    buttonLimpiezaCuestion.setIconTextGap(8);
                    buttonLimpiezaCuestion.setToolTipText(recursos.getString("buttonLimpiezaCuestionTooltip"));
                    buttonLimpiezaCuestion.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonExaminarActionPerformed(e);
                        }
                    });
                    panelCuestionSQL.add(buttonLimpiezaCuestion, cc.xy(15, 5));

                    //---- buttonPostInitsCuestion ----
                    buttonPostInitsCuestion.setText(recursos.getString("buttonPostInitsCuestion"));
                    buttonPostInitsCuestion.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                    buttonPostInitsCuestion.setIconTextGap(8);
                    buttonPostInitsCuestion.setToolTipText(recursos.getString("buttonPostInitsCuestionTooltip"));
                    buttonPostInitsCuestion.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonExaminarActionPerformed(e);
                        }
                    });
                    panelCuestionSQL.add(buttonPostInitsCuestion, cc.xy(15, 10));

                }
                tabbedCuestion.addTab(recursos.getString("tabbedCuestion2"), panelCuestionSQL);


                //======== panelCuestionPruebas ========
                {
                    panelCuestionPruebas.setLayout(new FormLayout(
                            new ColumnSpec[]{
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                                FormFactory.RELATED_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(180), FormSpec.DEFAULT_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.UNRELATED_GAP_COLSPEC
                            },
                            new RowSpec[]{
                                FormFactory.UNRELATED_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.RELATED_GAP_ROWSPEC,
                                new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW)
                            }));

                    //---- label12 ----
                    label12.setText(recursos.getString("label12"));
                    panelCuestionPruebas.add(label12, cc.xy(2, 2));

                    //---- comboJP ----
                    comboJP.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            comboJPActionPerformed();
                        }
                    });
                    panelCuestionPruebas.add(comboJP, cc.xy(4, 2));

                    //---- buttonNuevoJP ----
                    buttonNuevoJP.setText(recursos.getString("buttonNuevoJP"));
                    buttonNuevoJP.setIcon(new ImageIcon(getClass().getResource("/imagenes/add.png")));
                    buttonNuevoJP.setToolTipText(recursos.getString("buttonNuevoJPTooltip"));
                    buttonNuevoJP.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonNuevoJPActionPerformed();
                        }
                    });
                    panelCuestionPruebas.add(buttonNuevoJP, cc.xy(6, 2));

                    //---- buttonCopiarJP ----
                    buttonCopiarJP.setText(recursos.getString("buttonCopiarJP"));
                    buttonCopiarJP.setIcon(new ImageIcon(getClass().getResource("/imagenes/copy.png")));
                    buttonCopiarJP.setToolTipText(recursos.getString("buttonCopiarJPTooltip"));
                    buttonCopiarJP.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonCopiarJPActionPerformed();
                        }
                    });
                    panelCuestionPruebas.add(buttonCopiarJP, cc.xy(8, 2));

                    //---- buttonDelJP ----
                    buttonDelJP.setText(recursos.getString("buttonDelJP"));
                    buttonDelJP.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
                    buttonDelJP.setToolTipText(recursos.getString("buttonDelJPTooltip"));
                    buttonDelJP.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonDelJPActionPerformed();
                        }
                    });
                    panelCuestionPruebas.add(buttonDelJP, cc.xy(10, 2));

                    //======== tabbedPruebas ========
                    {
                        tabbedPruebas.setBorder(BorderFactory.createEmptyBorder());
                        tabbedPruebas.putClientProperty(Options.EMBEDDED_TABS_KEY, Boolean.TRUE);

                        //======== panelPruebasDatos ========
                        {
                            panelPruebasDatos.setLayout(new FormLayout(
                                    new ColumnSpec[]{
                                        FormFactory.UNRELATED_GAP_COLSPEC,
                                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        FormFactory.DEFAULT_COLSPEC,
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        new ColumnSpec(ColumnSpec.LEFT, Sizes.dluX(30), FormSpec.NO_GROW),
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        FormFactory.DEFAULT_COLSPEC,
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        new ColumnSpec(ColumnSpec.LEFT, Sizes.dluX(30), FormSpec.NO_GROW),
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        FormFactory.DEFAULT_COLSPEC,
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(90), FormSpec.NO_GROW),
                                        new ColumnSpec("max(default;110dlu):grow"),
                                        FormFactory.UNRELATED_GAP_COLSPEC
                                    },
                                    new RowSpec[]{
                                        FormFactory.UNRELATED_GAP_ROWSPEC,
                                        FormFactory.DEFAULT_ROWSPEC,
                                        FormFactory.LINE_GAP_ROWSPEC,
                                        new RowSpec(Sizes.DLUY14),
                                        new RowSpec(RowSpec.CENTER, Sizes.DEFAULT, 0.6),
                                        FormFactory.LINE_GAP_ROWSPEC,
                                        new RowSpec(Sizes.DLUY14),
                                        new RowSpec(RowSpec.CENTER, Sizes.DEFAULT, 0.4),
                                        FormFactory.LINE_GAP_ROWSPEC,
                                        FormFactory.DEFAULT_ROWSPEC,
                                        FormFactory.RELATED_GAP_ROWSPEC
                                    }));

                            //---- label18 ----
                            label18.setText(recursos.getString("label18"));
                            panelPruebasDatos.add(label18, cc.xy(2, 2));

                            //---- textNombreJP ----
                            textNombreJP.addKeyListener(new KeyAdapter() {

                                @Override
                                public void keyReleased(KeyEvent e) {
                                    textJuegoPruebasKeyReleased(e);
                                }
                            });
                            panelPruebasDatos.add(textNombreJP, cc.xywh(4, 2, 12, 1));

                            //---- label19 ----
                            label19.setText(recursos.getString("label19"));
                            panelPruebasDatos.add(label19, cc.xy(2, 4));

                            //======== scrollPane8 ========
                            {
                                //---- areaDescripJP ----
                                areaDescripJP.setTabSize(4);
                                areaDescripJP.addKeyListener(new KeyAdapter() {

                                    @Override
                                    public void keyReleased(KeyEvent e) {
                                        textJuegoPruebasKeyReleased(e);
                                    }
                                });
                                scrollPane8.setViewportView(areaDescripJP);
                            }
                            panelPruebasDatos.add(scrollPane8, cc.xywh(4, 4, 12, 2));

                            //---- label20 ----
                            label20.setText(recursos.getString("label20"));
                            panelPruebasDatos.add(label20, cc.xy(2, 7));

                            //======== scrollPane9 ========
                            {
                                //---- areaMsgErrorJP ----
                                areaMsgErrorJP.setTabSize(4);
                                areaMsgErrorJP.addKeyListener(new KeyAdapter() {

                                    @Override
                                    public void keyReleased(KeyEvent e) {
                                        textJuegoPruebasKeyReleased(e);
                                    }
                                });
                                scrollPane9.setViewportView(areaMsgErrorJP);
                            }
                            panelPruebasDatos.add(scrollPane9, cc.xywh(4, 7, 12, 2));

                            //---- label37 ----
                            label37.setText(recursos.getString("label37"));
                            panelPruebasDatos.add(label37, cc.xy(4, 10));

                            //---- checkboxbinariaJP ----
                            binariaJP.setBackground(Color.white);
                            binariaJP.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    checkBoxJPActionPerformed(e);
                                }
                            });
                            panelPruebasDatos.add(binariaJP, cc.xy(6, 10));

                            //---- label38 ----
                            label38.setText(recursos.getString("label38"));
                            panelPruebasDatos.add(label38, cc.xy(8, 10));

                            //---- checkboxmensajeJP ----
                            mensajeJP.setBackground(Color.white);
                            mensajeJP.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    checkBoxJPActionPerformed(e);
                                }
                            });
                            panelPruebasDatos.add(mensajeJP, cc.xy(10, 10));

                            //---- label45 ----
                            label45.setText(recursos.getString("label45"));
                            panelPruebasDatos.add(label45, cc.xy(12, 10));

                            //---- comboIdiomaJP ----
                            comboIdiomaJP.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    comboIdiomaActionPerformed(e);
                                }
                            });
                            panelPruebasDatos.add(comboIdiomaJP, cc.xy(14, 10));

                        }
                        tabbedPruebas.addTab(recursos.getString("tabbedPruebas1"), panelPruebasDatos);


                        //======== panelPruebasSQL ========
                        {
                            panelPruebasSQL.setLayout(new FormLayout(
                                    new ColumnSpec[]{
                                        FormFactory.UNRELATED_GAP_COLSPEC,
                                        new ColumnSpec("max(default;60dlu):grow(0.30000000000000004)"),
                                        new ColumnSpec(Sizes.dluX(60)),
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        new ColumnSpec("max(default;60dlu):grow(0.30000000000000004)"),
                                        new ColumnSpec(Sizes.dluX(60)),
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        new ColumnSpec("max(default;60dlu):grow(0.30000000000000004)"),
                                        new ColumnSpec(Sizes.dluX(60)),
                                        FormFactory.UNRELATED_GAP_COLSPEC
                                    },
                                    new RowSpec[]{
                                        FormFactory.UNRELATED_GAP_ROWSPEC,
                                        FormFactory.DEFAULT_ROWSPEC,
                                        new RowSpec(RowSpec.FILL, Sizes.dluY(0), 0.7),
                                        FormFactory.LINE_GAP_ROWSPEC,
                                        FormFactory.DEFAULT_ROWSPEC,
                                        FormFactory.LINE_GAP_ROWSPEC,
                                        FormFactory.DEFAULT_ROWSPEC,
                                        new RowSpec(RowSpec.FILL, Sizes.dluY(0), 0.30000000000000004),
                                        FormFactory.RELATED_GAP_ROWSPEC
                                    }));
                            ((FormLayout) panelPruebasSQL.getLayout()).setColumnGroups(new int[][]{{2, 5, 8}});

                            //---- label21 ----
                            label21.setText(recursos.getString("label21"));
                            panelPruebasSQL.add(label21, cc.xywh(2, 2, 2, 1));

                            //---- label22 ----
                            label22.setText(recursos.getString("label22"));
                            panelPruebasSQL.add(label22, cc.xywh(5, 2, 2, 1));

                            //---- label23 ----
                            label23.setText(recursos.getString("label23"));
                            panelPruebasSQL.add(label23, cc.xywh(8, 2, 2, 1));

                            //======== scrollPane10 ========
                            {
                                //---- areaInitsJP ----
                                areaInitsJP.setWrapStyleWord(true);
                                areaInitsJP.setLineWrap(true);
                                areaInitsJP.setTabSize(4);
                                areaInitsJP.addKeyListener(new KeyAdapter() {

                                    @Override
                                    public void keyReleased(KeyEvent e) {
                                        textJuegoPruebasKeyReleased(e);
                                    }
                                });
                                scrollPane10.setViewportView(areaInitsJP);
                            }
                            panelPruebasSQL.add(scrollPane10, cc.xywh(2, 3, 2, 1));

                            //======== scrollPane11 ========
                            {
                                scrollPane11.setViewportBorder(null);

                                //---- areaEntradaJP ----
                                areaEntradaJP.setLineWrap(true);
                                areaEntradaJP.setWrapStyleWord(true);
                                areaEntradaJP.setTabSize(4);
                                areaEntradaJP.addKeyListener(new KeyAdapter() {

                                    @Override
                                    public void keyReleased(KeyEvent e) {
                                        textJuegoPruebasKeyReleased(e);
                                    }
                                });
                                scrollPane11.setViewportView(areaEntradaJP);
                            }
                            panelPruebasSQL.add(scrollPane11, cc.xywh(5, 3, 2, 1));

                            //======== scrollPane12 ========
                            {

                                //---- areaLimpiezaJP ----
                                areaLimpiezaJP.setLineWrap(true);
                                areaLimpiezaJP.setWrapStyleWord(true);
                                areaLimpiezaJP.setTabSize(4);
                                areaLimpiezaJP.addKeyListener(new KeyAdapter() {

                                    @Override
                                    public void keyReleased(KeyEvent e) {
                                        textJuegoPruebasKeyReleased(e);
                                    }
                                });
                                scrollPane12.setViewportView(areaLimpiezaJP);
                            }
                            panelPruebasSQL.add(scrollPane12, cc.xywh(8, 3, 2, 1));

                            //---- buttonInitsJP ----
                            buttonInitsJP.setText(recursos.getString("buttonInitsJP"));
                            buttonInitsJP.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                            buttonInitsJP.setIconTextGap(8);
                            buttonInitsJP.setToolTipText(recursos.getString("buttonInitsJPTooltip"));
                            buttonInitsJP.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    buttonExaminarActionPerformed(e);
                                }
                            });
                            panelPruebasSQL.add(buttonInitsJP, cc.xy(3, 5));

                            //---- buttonEntradaJP ----
                            buttonEntradaJP.setText(recursos.getString("buttonEntradaJP"));
                            buttonEntradaJP.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                            buttonEntradaJP.setIconTextGap(8);
                            buttonEntradaJP.setToolTipText(recursos.getString("buttonEntradaJPTooltip"));
                            buttonEntradaJP.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    buttonExaminarActionPerformed(e);
                                }
                            });
                            panelPruebasSQL.add(buttonEntradaJP, cc.xy(6, 5));

                            //---- buttonLimpiezaJP ----
                            buttonLimpiezaJP.setText(recursos.getString("buttonLimpiezaJP"));
                            buttonLimpiezaJP.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                            buttonLimpiezaJP.setIconTextGap(8);
                            buttonLimpiezaJP.setToolTipText(recursos.getString("buttonLimpiezaJPTooltip"));
                            buttonLimpiezaJP.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    buttonExaminarActionPerformed(e);
                                }
                            });
                            panelPruebasSQL.add(buttonLimpiezaJP, cc.xy(9, 5));

                            //---- label24 ----
                            label24.setText(recursos.getString("label24"));
                            panelPruebasSQL.add(label24, cc.xy(2, 7));

                            //======== scrollPane13 ========
                            {
                                //---- areaSalidaJP ----
                                areaSalidaJP.setEditable(false);
                                areaSalidaJP.setLineWrap(true);
                                areaSalidaJP.setWrapStyleWord(true);
                                areaSalidaJP.setFocusable(false);
                                scrollPane13.setViewportView(areaSalidaJP);
                            }
                            panelPruebasSQL.add(scrollPane13, cc.xywh(2, 8, 8, 1));
                        }
                        tabbedPruebas.addTab(recursos.getString("tabbedPruebas2"), panelPruebasSQL);


                        //======== panelPruebasCompr ========
                        {
                            panelPruebasCompr.setLayout(new FormLayout(
                                    new ColumnSpec[]{
                                        FormFactory.UNRELATED_GAP_COLSPEC,
                                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                                        FormFactory.RELATED_GAP_COLSPEC,
                                        new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(180), FormSpec.DEFAULT_GROW),
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        new ColumnSpec(Sizes.dluX(60)),
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        new ColumnSpec(Sizes.dluX(60)),
                                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                        new ColumnSpec(Sizes.dluX(60)),
                                        FormFactory.UNRELATED_GAP_COLSPEC
                                    },
                                    new RowSpec[]{
                                        FormFactory.UNRELATED_GAP_ROWSPEC,
                                        FormFactory.DEFAULT_ROWSPEC,
                                        FormFactory.RELATED_GAP_ROWSPEC,
                                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW)
                                    }));

                            //---- label13 ----
                            label13.setText(recursos.getString("label13"));
                            panelPruebasCompr.add(label13, cc.xy(2, 2));

                            //---- comboCompr ----
                            comboCompr.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    comboComprActionPerformed();
                                }
                            });
                            panelPruebasCompr.add(comboCompr, cc.xy(4, 2));

                            //---- buttonNuevaCompr ----
                            buttonNuevaCompr.setText(recursos.getString("buttonNuevaCompr"));
                            buttonNuevaCompr.setIcon(new ImageIcon(getClass().getResource("/imagenes/add.png")));
                            buttonNuevaCompr.setToolTipText(recursos.getString("buttonNuevaComprTooltip"));
                            buttonNuevaCompr.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    buttonNuevaComprActionPerformed();
                                }
                            });
                            panelPruebasCompr.add(buttonNuevaCompr, cc.xy(6, 2));

                            //---- buttonCopiarCompr ----
                            buttonCopiarCompr.setText(recursos.getString("buttonCopiarCompr"));
                            buttonCopiarCompr.setIcon(new ImageIcon(getClass().getResource("/imagenes/copy.png")));
                            buttonCopiarCompr.setToolTipText(recursos.getString("buttonCopiarComprTooltip"));
                            buttonCopiarCompr.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    buttonCopiarComprActionPerformed();
                                }
                            });
                            panelPruebasCompr.add(buttonCopiarCompr, cc.xy(8, 2));

                            //---- buttonDelCompr ----
                            buttonDelCompr.setText(recursos.getString("buttonDelCompr"));
                            buttonDelCompr.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
                            buttonDelCompr.setToolTipText(recursos.getString("buttonDelComprTooltip"));
                            buttonDelCompr.addActionListener(new ActionListener() {

                                public void actionPerformed(ActionEvent e) {
                                    buttonDelComprActionPerformed();
                                }
                            });
                            panelPruebasCompr.add(buttonDelCompr, cc.xy(10, 2));

                            //======== tabbedCompr ========
                            {
                                tabbedCompr.setBorder(null);
                                tabbedCompr.putClientProperty(Options.EMBEDDED_TABS_KEY, Boolean.TRUE);

                                //======== panelComprDatos ========
                                {
                                    panelComprDatos.setBorder(null);
                                    panelComprDatos.setLayout(new FormLayout(
                                            new ColumnSpec[]{
                                                FormFactory.UNRELATED_GAP_COLSPEC,
                                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                                FormFactory.DEFAULT_COLSPEC,
                                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                                new ColumnSpec(ColumnSpec.LEFT, Sizes.dluX(30), FormSpec.NO_GROW),
                                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                                FormFactory.DEFAULT_COLSPEC,
                                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                                new ColumnSpec(ColumnSpec.LEFT, Sizes.dluX(30), FormSpec.NO_GROW),
                                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                                FormFactory.DEFAULT_COLSPEC,
                                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                                new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(90), FormSpec.NO_GROW),
                                                new ColumnSpec("max(default;110dlu):grow"),
                                                FormFactory.UNRELATED_GAP_COLSPEC
                                            },
                                            new RowSpec[]{
                                                FormFactory.UNRELATED_GAP_ROWSPEC,
                                                FormFactory.DEFAULT_ROWSPEC,
                                                FormFactory.LINE_GAP_ROWSPEC,
                                                new RowSpec(Sizes.DLUY14),
                                                new RowSpec(RowSpec.CENTER, Sizes.DEFAULT, 0.6),
                                                FormFactory.LINE_GAP_ROWSPEC,
                                                new RowSpec(Sizes.DLUY14),
                                                new RowSpec(RowSpec.CENTER, Sizes.DEFAULT, 0.4),
                                                FormFactory.LINE_GAP_ROWSPEC,
                                                FormFactory.DEFAULT_ROWSPEC,
                                                FormFactory.RELATED_GAP_ROWSPEC
                                            }));

                                    //---- label9 ----
                                    label9.setText(recursos.getString("label9"));
                                    panelComprDatos.add(label9, cc.xy(2, 2));

                                    //---- textNombreCompr ----
                                    textNombreCompr.addKeyListener(new KeyAdapter() {

                                        @Override
                                        public void keyReleased(KeyEvent e) {
                                            textComprobacionKeyReleased(e);
                                        }
                                    });
                                    panelComprDatos.add(textNombreCompr, cc.xywh(4, 2, 12, 1));

                                    //---- label10 ----
                                    label10.setText(recursos.getString("label10"));
                                    panelComprDatos.add(label10, cc.xy(2, 4));

                                    //======== scrollPane3 ========
                                    {
                                        //---- areaDescripCompr ----
                                        areaDescripCompr.setTabSize(4);
                                        areaDescripCompr.addKeyListener(new KeyAdapter() {

                                            @Override
                                            public void keyReleased(KeyEvent e) {
                                                textComprobacionKeyReleased(e);
                                            }
                                        });
                                        scrollPane3.setViewportView(areaDescripCompr);
                                    }
                                    panelComprDatos.add(scrollPane3, cc.xywh(4, 4, 12, 2));

                                    //---- label11 ----
                                    label11.setText(recursos.getString("label11"));
                                    panelComprDatos.add(label11, cc.xy(2, 7));

                                    //======== scrollPane4 ========
                                    {
                                        //---- areaMsgErrorCompr ----
                                        areaMsgErrorCompr.setTabSize(4);
                                        areaMsgErrorCompr.addKeyListener(new KeyAdapter() {

                                            @Override
                                            public void keyReleased(KeyEvent e) {
                                                textComprobacionKeyReleased(e);
                                            }
                                        });
                                        scrollPane4.setViewportView(areaMsgErrorCompr);
                                    }
                                    panelComprDatos.add(scrollPane4, cc.xywh(4, 7, 12, 2));

                                    //---- label39 ----
                                    label39.setText(recursos.getString("label39"));
                                    panelComprDatos.add(label39, cc.xy(4, 10));

                                    //---- checkboxbinariaCom ----
                                    binariaCom.setBackground(Color.white);
                                    binariaCom.addActionListener(new ActionListener() {

                                        public void actionPerformed(ActionEvent e) {
                                            checkBoxComActionPerformed(e);
                                        }
                                    });
                                    panelComprDatos.add(binariaCom, cc.xy(6, 10));

                                    //---- label40 ----
                                    label40.setText(recursos.getString("label40"));
                                    panelComprDatos.add(label40, cc.xy(8, 10));

                                    //---- checkboxmensajeCom ----
                                    mensajeCom.setBackground(Color.white);
                                    mensajeCom.addActionListener(new ActionListener() {

                                        public void actionPerformed(ActionEvent e) {
                                            checkBoxComActionPerformed(e);
                                        }
                                    });
                                    panelComprDatos.add(mensajeCom, cc.xy(10, 10));

                                    //---- label46 ----
                                    label46.setText(recursos.getString("label46"));
                                    panelComprDatos.add(label46, cc.xy(12, 10));

                                    //---- comboIdiomaCompr ----
                                    comboIdiomaCompr.addActionListener(new ActionListener() {

                                        public void actionPerformed(ActionEvent e) {
                                            comboIdiomaActionPerformed(e);
                                        }
                                    });
                                    panelComprDatos.add(comboIdiomaCompr, cc.xy(14, 10));

                                }
                                tabbedCompr.addTab(recursos.getString("tabbedCompr1"), panelComprDatos);


                                //======== panelComprSQL ========
                                {
                                    panelComprSQL.setLayout(new FormLayout(
                                            new ColumnSpec[]{
                                                FormFactory.UNRELATED_GAP_COLSPEC,
                                                new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                                                new ColumnSpec(Sizes.dluX(60)),
                                                FormFactory.UNRELATED_GAP_COLSPEC
                                            },
                                            new RowSpec[]{
                                                FormFactory.UNRELATED_GAP_ROWSPEC,
                                                FormFactory.DEFAULT_ROWSPEC,
                                                new RowSpec(RowSpec.FILL, Sizes.dluY(0), 0.7),
                                                FormFactory.LINE_GAP_ROWSPEC,
                                                FormFactory.DEFAULT_ROWSPEC,
                                                FormFactory.LINE_GAP_ROWSPEC,
                                                FormFactory.DEFAULT_ROWSPEC,
                                                new RowSpec(RowSpec.FILL, Sizes.dluY(0), 0.30000000000000004),
                                                FormFactory.RELATED_GAP_ROWSPEC
                                            }));

                                    //---- label28 ----
                                    label28.setText(recursos.getString("label28"));
                                    panelComprSQL.add(label28, cc.xywh(2, 2, 2, 1));

                                    //======== scrollPane18 ========
                                    {
                                        //---- areaEntradaCompr ----
                                        areaEntradaCompr.setTabSize(4);
                                        areaEntradaCompr.addKeyListener(new KeyAdapter() {

                                            @Override
                                            public void keyReleased(KeyEvent e) {
                                                textComprobacionKeyReleased(e);
                                            }
                                        });
                                        scrollPane18.setViewportView(areaEntradaCompr);
                                    }
                                    panelComprSQL.add(scrollPane18, cc.xywh(2, 3, 2, 1));

                                    //---- buttonEntradaCompr ----
                                    buttonEntradaCompr.setText(recursos.getString("buttonEntradaCompr"));
                                    buttonEntradaCompr.setIcon(new ImageIcon(getClass().getResource("/imagenes/folder_explore.png")));
                                    buttonEntradaCompr.setIconTextGap(8);
                                    buttonEntradaCompr.setToolTipText(recursos.getString("buttonEntradaComprTooltip"));
                                    buttonEntradaCompr.addActionListener(new ActionListener() {

                                        public void actionPerformed(ActionEvent e) {
                                            buttonExaminarActionPerformed(e);
                                        }
                                    });
                                    panelComprSQL.add(buttonEntradaCompr, cc.xy(3, 5));

                                    //---- label29 ----
                                    label29.setText(recursos.getString("label29"));
                                    panelComprSQL.add(label29, cc.xywh(2, 7, 2, 1));

                                    //======== scrollPane19 ========
                                    {
                                        //---- areaSalidaCompr ----
                                        areaSalidaCompr.setLineWrap(true);
                                        areaSalidaCompr.setWrapStyleWord(true);
                                        areaSalidaCompr.setEditable(false);
                                        areaSalidaCompr.setFocusable(false);
                                        scrollPane19.setViewportView(areaSalidaCompr);
                                    }
                                    panelComprSQL.add(scrollPane19, cc.xywh(2, 8, 2, 1));
                                }
                                tabbedCompr.addTab(recursos.getString("tabbedCompr2"), panelComprSQL);

                            }
                            panelPruebasCompr.add(tabbedCompr, cc.xywh(1, 4, 11, 1));
                        }
                        tabbedPruebas.addTab(recursos.getString("tabbedPruebas3"), panelPruebasCompr);

                    }
                    panelCuestionPruebas.add(tabbedPruebas, cc.xywh(1, 4, 11, 1));
                }
                tabbedCuestion.addTab(recursos.getString("tabbedCuestion3"), panelCuestionPruebas);


                //======== panelCuestionPesos ========
                {
                    panelCuestionPesos.setLayout(new FormLayout(
                            new ColumnSpec[]{
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(Sizes.dluX(60)),
                                FormFactory.UNRELATED_GAP_COLSPEC
                            },
                            new RowSpec[]{
                                FormFactory.UNRELATED_GAP_ROWSPEC,
                                new RowSpec(RowSpec.FILL, Sizes.DEFAULT, 0.4),
                                FormFactory.LINE_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                new RowSpec(RowSpec.FILL, Sizes.DEFAULT, 0.6),
                                FormFactory.RELATED_GAP_ROWSPEC
                            }));

                    //======== scrollPane17 ========
                    {
                        //---- tablePesos ----
                        tablePesos.setSortable(false);
                        tablePesos.setHighlighters(pipeline);
                        
                        scrollPane17.setViewportView(tablePesos);
                    }
                    panelCuestionPesos.add(scrollPane17, cc.xywh(2, 2, 9, 1));

                    //---- label26 ----
                    label26.setText(recursos.getString("label26"));
                    panelCuestionPesos.add(label26, cc.xy(2, 4));

                    //---- textNombrePeso ----
                    textNombrePeso.setEditable(false);
                    panelCuestionPesos.add(textNombrePeso, cc.xy(4, 4));

                    //---- label27 ----
                    label27.setText(recursos.getString("label27"));
                    panelCuestionPesos.add(label27, cc.xy(6, 4));

                    //---- spinnerPeso ----
                    spinnerPeso.setToolTipText(recursos.getString("spinnerPeso"));
                    spinnerPeso.setModel(new SpinnerNumberModel(new Float(0.0f), new Float(0.0f), new Float(1.0F), new Float(0.05F)));
                    panelCuestionPesos.add(spinnerPeso, cc.xy(8, 4));

                    //---- buttonPeso ----
                    buttonPeso.setText(recursos.getString("buttonPeso"));
                    buttonPeso.setToolTipText(recursos.getString("buttonPesoTooltip"));
                    buttonPeso.setIcon(new ImageIcon(getClass().getResource("/imagenes/edit.png")));
                    buttonPeso.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            buttonPesoActionPerformed();
                        }
                    });
                    panelCuestionPesos.add(buttonPeso, cc.xy(10, 4));
                }
                tabbedCuestion.addTab(recursos.getString("tabbedCuestion4"), panelCuestionPesos);

                //======== panelCuestionEstado ========
                {
                    panelCuestionEstado.setLayout(new FormLayout(
                            new ColumnSpec[]{
                                FormFactory.UNRELATED_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(30), FormSpec.NO_GROW),
                                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                                new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(100), FormSpec.DEFAULT_GROW),
                                FormFactory.UNRELATED_GAP_COLSPEC
                            },
                            new RowSpec[]{
                                FormFactory.UNRELATED_GAP_ROWSPEC,
                                FormFactory.DEFAULT_ROWSPEC,
                                FormFactory.LINE_GAP_ROWSPEC,
                                new RowSpec(RowSpec.FILL, Sizes.DEFAULT, 0.7),
                                FormFactory.RELATED_GAP_ROWSPEC
                            }));

                    //---- label41 ----
                    label41.setText(recursos.getString("label41"));
                    panelCuestionEstado.add(label41, cc.xy(2, 2));

                    //======== scrollPane21 ========
                    {
                        //---- areaEstado ----
                        areaEstado.setTabSize(4);
                        
                        areaEstado.addKeyListener(new KeyAdapter() {

                            @Override
                            public void keyReleased(KeyEvent e) {
                                textCuestionKeyReleased(e);
                            }
                        });
                        scrollPane21.setViewportView(areaEstado);
                    }
                    panelCuestionEstado.add(scrollPane21, cc.xywh(2, 4, 3, 2));


                }
                tabbedCuestion.addTab(recursos.getString("tabbedCuestion5"), panelCuestionEstado);


            }
            panelInfo.add(tabbedCuestion, cc.xywh(1, 1, 15, 1));
            panelInfo.add(separator3, cc.xywh(1, 3, 15, 1));

            //---- buttonCopiarCuestion ----
            buttonCopiarCuestion.setText(recursos.getString("buttonCopiarCuestion"));
            buttonCopiarCuestion.setIcon(new ImageIcon(getClass().getResource("/imagenes/copy.png")));
            buttonCopiarCuestion.setToolTipText(recursos.getString("buttonCopiarCuestionTooltip"));
            buttonCopiarCuestion.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonCopiarCuestionActionPerformed();
                }
            });
            panelInfo.add(buttonCopiarCuestion, cc.xy(2, 5));

            //---- buttonCopiarCuestionBD ----
            buttonCopiarCuestionBD.setText(recursos.getString("buttonCopiarCuestionBD"));
            buttonCopiarCuestionBD.setIcon(new ImageIcon(getClass().getResource("/imagenes/copydb.png")));
            buttonCopiarCuestionBD.setToolTipText(recursos.getString("buttonCopiarCuestionBDTooltip"));
            buttonCopiarCuestionBD.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonCopiarCuestionBDActionPerformed();
                }
            });
            panelInfo.add(buttonCopiarCuestionBD, cc.xy(4, 5));

            //---- buttonDelCuestion ----
            buttonDelCuestion.setText(recursos.getString("buttonDelCuestion"));
            buttonDelCuestion.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
            buttonDelCuestion.setToolTipText(recursos.getString("buttonDelCuestionTooltip"));
            buttonDelCuestion.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonDelCuestionActionPerformed();
                }
            });
            panelInfo.add(buttonDelCuestion, cc.xy(6, 5));

            //---- buttonListado ----
            buttonListado.setText(recursos.getString("buttonListado"));
            buttonListado.setIcon(new ImageIcon(getClass().getResource("/imagenes/play.png")));
            buttonListado.setToolTipText(recursos.getString("buttonListadoTooltip"));
            buttonListado.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonListadoActionPerformed();
                }
            });
            panelInfo.add(buttonListado, cc.xy(8, 5));

            //---- buttonBloquear ----
            buttonBloquear.setText(recursos.getString("buttonBloquear"));
            buttonBloquear.setIcon(new ImageIcon(getClass().getResource("/imagenes/bullet_red.png")));
            buttonBloquear.setToolTipText(recursos.getString("buttonBloquearTooltip"));
            buttonBloquear.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonBloquearActionPerformed();
                }
            });
            panelInfo.add(buttonBloquear, cc.xy(10, 5));

            //---- buttonGuardar ----
            buttonGuardar.setText(recursos.getString("buttonGuardar"));
            buttonGuardar.setIcon(new ImageIcon(getClass().getResource("/imagenes/disk.png")));
            buttonGuardar.setToolTipText(recursos.getString("buttonGuardarTooltip"));
            buttonGuardar.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonGuardarActionPerformed();
                }
            });
            panelInfo.add(buttonGuardar, cc.xy(12, 5));

            //---- buttonCancelar ----
            buttonCancelar.setText(recursos.getString("buttonCancelar"));
            buttonCancelar.setIcon(new ImageIcon(getClass().getResource("/imagenes/cross.png")));
            buttonCancelar.setToolTipText(recursos.getString("buttonCancelarTooltip"));
            buttonCancelar.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonCancelarActionPerformed();
                }
            });
            panelInfo.add(buttonCancelar, cc.xy(14, 5));
        }

        //======== panelEjecucion ========
        {
            panelEjecucion.setLayout(new FormLayout(
                    new ColumnSpec[]{
                        FormFactory.UNRELATED_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                        FormFactory.RELATED_GAP_COLSPEC,
                        new ColumnSpec("max(default;120dlu):grow"),
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.CENTER, Sizes.DEFAULT, FormSpec.NO_GROW),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(60)),
                        FormFactory.UNRELATED_GAP_COLSPEC
                    },
                    new RowSpec[]{
                        FormFactory.UNRELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.PARAGRAPH_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, 0.19999999999999998),
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.PARAGRAPH_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.PARAGRAPH_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, 0.4),
                        FormFactory.RELATED_GAP_ROWSPEC,
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.NO_GROW),
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, 0.4),
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.UNRELATED_GAP_ROWSPEC
                    }));

            panelEjecucion.add(separator4, cc.xywh(2, 2, 12, 1));

            //======== scrollPane14 ========
            {
                //---- listCorrectores ----
                listCorrectores.setHighlighters(pipeline);
                scrollPane14.setViewportView(listCorrectores);
            }
            panelEjecucion.add(scrollPane14, cc.xywh(4, 4, 10, 2));

            //---- buttonCorrectores ----
            buttonCorrectores.setText(recursos.getString("buttonCorrectores"));
            buttonCorrectores.setIcon(new ImageIcon(getClass().getResource("/imagenes/editlist.png")));
            buttonCorrectores.setIconTextGap(8);
            buttonCorrectores.setToolTipText(recursos.getString("buttonCorrectoresToolTip"));
            buttonCorrectores.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonCorrectoresActionPerformed();
                }
            });
            panelEjecucion.add(buttonCorrectores, cc.xy(13, 7));

            panelEjecucion.add(separator5, cc.xywh(2, 9, 12, 1));

            //======== scrollPane15 ========
            {
                //---- treeTableEjecucion ----
                treeTableEjecucion.setHighlighters(pipeline);
                treeTableEjecucion.addTreeExpansionListener(new TreeExpansionListener() {

                    public void treeCollapsed(TreeExpansionEvent e) {
                        treeTableEjecucionTreeCollapsed();
                    }

                    public void treeExpanded(TreeExpansionEvent e) {
                    }
                });
                scrollPane15.setViewportView(treeTableEjecucion);
            }
            panelEjecucion.add(scrollPane15, cc.xywh(4, 11, 10, 2));

            //---- buttonSubir ----
            buttonSubir.setText(recursos.getString("buttonSubir"));
            buttonSubir.setIcon(new ImageIcon(getClass().getResource("/imagenes/arrow_up.png")));
            buttonSubir.setIconTextGap(8);
            buttonSubir.setToolTipText(recursos.getString("buttonSubirToolTip"));
            buttonSubir.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonMoverActionPerformed(e);
                }
            });
            panelEjecucion.add(buttonSubir, cc.xy(5, 14));

            //---- buttonBajar ----
            buttonBajar.setText(recursos.getString("buttonBajar"));
            buttonBajar.setIcon(new ImageIcon(getClass().getResource("/imagenes/arrow_down.png")));
            buttonBajar.setIconTextGap(8);
            buttonBajar.setToolTipText(recursos.getString("buttonBajarToolTip"));
            buttonBajar.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonMoverActionPerformed(e);
                }
            });
            panelEjecucion.add(buttonBajar, cc.xy(7, 14));

            //---- separator6 ----
            separator6.setOrientation(SwingConstants.VERTICAL);
            panelEjecucion.add(separator6, cc.xy(9, 14));

            //---- buttonGenerar ----
            buttonGenerar.setText(recursos.getString("buttonGenerar"));
            buttonGenerar.setIcon(new ImageIcon(getClass().getResource("/imagenes/play.png")));
            buttonGenerar.setIconTextGap(8);
            buttonGenerar.setToolTipText(recursos.getString("buttonGenerarToolTip"));
            buttonGenerar.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonGenerarActionPerformed();
                }
            });
            panelEjecucion.add(buttonGenerar, cc.xy(11, 14));

            //---- buttonEjecutar ----
            buttonEjecutar.setText(recursos.getString("buttonEjecutar"));
            buttonEjecutar.setIcon(new ImageIcon(getClass().getResource("/imagenes/play.png")));
            buttonEjecutar.setIconTextGap(8);
            buttonEjecutar.setToolTipText(recursos.getString("buttonEjecutarToolTip"));
            buttonEjecutar.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonEjecutarActionPerformed();
                }
            });
            panelEjecucion.add(buttonEjecutar, cc.xy(13, 14));

            //---- label25 ----
            label25.setText(recursos.getString("label25"));
            panelEjecucion.add(label25, cc.xy(2, 16));

            //======== scrollPane16 ========
            {
                //---- editorResultados ----
                editorResultados.setWrapStyleWord(true);
                editorResultados.setLineWrap(true);
                editorResultados.setEditable(false);
                scrollPane16.setViewportView(editorResultados);
            }
            panelEjecucion.add(scrollPane16, cc.xywh(4, 16, 10, 2));

            //---- buttonLimpiar ----
            buttonLimpiar.setText(recursos.getString("buttonLimpiar"));
            buttonLimpiar.setIcon(new ImageIcon(getClass().getResource("/imagenes/reset.png")));
            buttonLimpiar.setToolTipText(recursos.getString("buttonLimpiarToolTip"));
            buttonLimpiar.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonLimpiarActionPerformed();
                }
            });
            panelEjecucion.add(buttonLimpiar, cc.xy(13, 19));
        }

        //======== panelSolucion ========
        {
            panelSolucion.setLayout(new FormLayout(
                    new ColumnSpec[]{
                        FormFactory.UNRELATED_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(70), FormSpec.NO_GROW),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                        new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(60), FormSpec.NO_GROW),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(37), FormSpec.NO_GROW),
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(5), FormSpec.NO_GROW),
                        new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(18), FormSpec.NO_GROW),
                        FormFactory.UNRELATED_GAP_COLSPEC
                    },
                    new RowSpec[]{
                        FormFactory.UNRELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        new RowSpec(RowSpec.FILL, Sizes.dluY(120), FormSpec.NO_GROW),
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
//                        FormFactory.LINE_GAP_ROWSPEC,
//                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
//                        new RowSpec(RowSpec.FILL, Sizes.dluY(40), FormSpec.DEFAULT_GROW),
                        FormFactory.UNRELATED_GAP_ROWSPEC
                    }));

            //---- label43 ----
            label43.setText(recursos.getString("label43"));
            panelSolucion.add(label43, cc.xy(2, 2));

            //======== scrollPane22 ========
            {
                //---- areaSolucionAlumno ----
                areaSolucionAlumno.setWrapStyleWord(true);
                areaSolucionAlumno.setLineWrap(true);
                areaSolucionAlumno.setTabSize(4);
                areaSolucionAlumno.addKeyListener(new KeyAdapter() {

                    @Override
                    public void keyReleased(KeyEvent e) {
                        textCuestionKeyReleased(e);
                    }
                });
                scrollPane22.setViewportView(areaSolucionAlumno);
            }
            panelSolucion.add(scrollPane22, cc.xywh(4, 2, 6, 2));

            //---- buttonLimpiezaSol ----
            buttonLimpiezaSol.setText(recursos.getString("buttonLimpiezaSol"));
            buttonLimpiezaSol.setIcon(new ImageIcon(getClass().getResource("/imagenes/reset.png")));
            buttonLimpiezaSol.setIconTextGap(8);
            buttonLimpiezaSol.setToolTipText(recursos.getString("buttonLimpiezaSolToolTip"));
            buttonLimpiezaSol.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonLimpiarSolActionPerformed();
                }
            });
            panelSolucion.add(buttonLimpiezaSol, cc.xy(5, 5));

            //---- buttonComparar ----
            buttonComparar.setText(recursos.getString("buttonComparar"));
            buttonComparar.setIcon(new ImageIcon(getClass().getResource("/imagenes/play.png")));
            buttonComparar.setIconTextGap(8);
            buttonComparar.setToolTipText(recursos.getString("buttonCompararToolTip"));
            buttonComparar.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonCompararActionPerformed();
                }
            });
            panelSolucion.add(buttonComparar, cc.xywh(7, 5, 3, 1));

            //---- label44 ----
            label44.setText(recursos.getString("label44"));
            panelSolucion.add(label44, cc.xy(2, 5));

            //======== scrollPane23 ========
            {
                //---- treeTableComparacion ----
                treeTableComparacion.setHighlighters(pipeline);
                treeTableComparacion.addTreeExpansionListener(new TreeExpansionListener() {

                    public void treeCollapsed(TreeExpansionEvent e) {
                        treeTableEjecucionTreeCollapsed();
                    }

                    public void treeExpanded(TreeExpansionEvent e) {
                    }
                });
                scrollPane23.setViewportView(treeTableComparacion);
            }
            panelSolucion.add(scrollPane23, cc.xywh(2, 7, 8, 1));

            //======== scrollPane24 ========
            {
                //---- editorAlumno ----
                editorAlumno.setWrapStyleWord(true);
                editorAlumno.setLineWrap(true);
                editorAlumno.setEditable(false);
                scrollPane24.setViewportView(editorAlumno);
            }
            panelSolucion.add(scrollPane24, cc.xywh(2, 9, 4, 5));

            //---- label48 ----
            label48.setText(recursos.getString("label48"));
            panelSolucion.add(label48, cc.xy(7, 9));
            
            //---- textNotaPublic ----
            textNotaPublic.setEditable(false);
            panelSolucion.add(textNotaPublic, cc.xy(9, 9));

            //---- label49 ----
            label49.setText(recursos.getString("label49"));
            panelSolucion.add(label49, cc.xy(7, 11));

            //---- textNotaReal ----
            textNotaReal.setEditable(false);
            panelSolucion.add(textNotaReal, cc.xy(9, 11));

            //---- label50 ----
            label50.setText(recursos.getString("label50"));
            panelSolucion.add(label50, cc.xy(7, 13));

            //---- textNotaBinaria ----
            textNotaBinaria.setEditable(false);
            panelSolucion.add(textNotaBinaria, cc.xy(9, 13));

        }

        //---- pipeline ----
        pipeline.addHighlighter(new AlternateRowHighlighter());

        //---- selectionModelPesos ----
        selectionModelPesos.addListSelectionListener(new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {
                selectionModelValueChanged(e);
            }
        });

        //---- treeSelectionModelEjec ----
        treeSelectionModelEjec.addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent e) {
                treeSelectionModelEjecValueChanged(e);
            }
        });

        //---- groupModo ----
        ButtonGroup groupModo = new ButtonGroup();
        groupModo.add(toggleInfo);
        groupModo.add(toggleEjecucion);
        groupModo.add(toggleSolucion);
        // - End of component initialization
    }
    /*********************************************************/
    /*      Variables                                        */
    /*********************************************************/
    private SimpleInternalFrame sifCuestion;
    private JToolBar toolBarModo;
    private JToggleButton toggleInfo;
    private JToggleButton toggleEjecucion;
    private JToggleButton toggleSolucion;
    private JTabbedPane tabbedCuestion;
    private JTabbedPane tabbedPruebas;
    private JTabbedPane tabbedCompr;
    private JPanel panelInfo;
    private JPanel panelCuestionDatos;
    private JPanel panelCuestionSQL;
    private JPanel panelCuestionPruebas;
    private JPanel panelPruebasDatos;
    private JPanel panelPruebasSQL;
    private JPanel panelPruebasCompr;
    private JPanel panelComprDatos;
    private JPanel panelComprSQL;
    private JPanel panelCuestionPesos;
    private JPanel panelCuestionEstado;
    private JPanel panelEjecucion;
    private JPanel panelSolucion;
    private JComponent separator1;
    private JComponent separator2;
    private JSeparator separator3;
    private JComponent separator4;
    private JComponent separator5;
    private JSeparator separator6;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JLabel label5;
    private JLabel label6;
    private JLabel label7;
    private JLabel label8;
    private JLabel label9;
    private JLabel label10;
    private JLabel label11;
    private JLabel label12;
    private JLabel label13;
    private JLabel label14;
    private JLabel label15;
    private JLabel label16;
    private JLabel label17;
    private JLabel label18;
    private JLabel label19;
    private JLabel label20;
    private JLabel label21;
    private JLabel label22;
    private JLabel label23;
    private JLabel label24;
    private JLabel label25;
    private JLabel label26;
    private JLabel label27;
    private JLabel label28;
    private JLabel label29;
    private JLabel label30;
    private JLabel label31;
    private JLabel label32;
    private JLabel label33;
    private JLabel label34;
    private JLabel label35;
    private JLabel label36;
    private JLabel label37;
    private JLabel label38;
    private JLabel label39;
    private JLabel label40;
    private JLabel label41;
    private JLabel label42;
    private JLabel label43;
    private JLabel label44;
    private JLabel label45;
    private JLabel label46;
    private JLabel label47;
    private JLabel label48;
    private JLabel label49;
    private JLabel label50;
    private JTextField textTitulo;
    private JTextField textAutor;
    private JTextField textFichero;
    private JTextField textNombreJP;
    private JTextField textNombreCompr;
    private JTextField textNombrePeso;
    private JTextField textUrl;
    private JTextField textUsuario;
    private JTextField textId;
    private JTextField textPostInits;
    private JTextField textNotaPublic;
    private JTextField textNotaReal;
    private JTextField textNotaBinaria;
    private JPasswordField textClave;
    private JButton buttonDelAdjunto;
    private JButton buttonVerAdjunto;
    private JButton buttonAdjunto;
    private JButton buttonHtml;
    private JButton buttonTematicas;
    private JButton buttonInitsCuestion;
    private JButton buttonVerSolucion;
    private JButton buttonSolucionCuestion;
    private JButton buttonLimpiezaCuestion;
    private JButton buttonPostInitsCuestion;
    private JButton buttonNuevoJP;
    private JButton buttonCopiarJP;
    private JButton buttonDelJP;
    private JButton buttonInitsJP;
    private JButton buttonEntradaJP;
    private JButton buttonLimpiezaJP;
    private JButton buttonNuevaCompr;
    private JButton buttonCopiarCompr;
    private JButton buttonDelCompr;
    private JButton buttonEntradaCompr;
    private JButton buttonPeso;
    private JButton buttonCopiarCuestion;
    private JButton buttonCopiarCuestionBD;
    private JButton buttonDelCuestion;
    private JButton buttonListado;
    private JButton buttonBloquear;
    private JButton buttonGuardar;
    private JButton buttonCancelar;
    private JButton buttonCorrectores;
    private JButton buttonSubir;
    private JButton buttonBajar;
    private JButton buttonEjecutar;
    private JButton buttonGenerar;
    private JButton buttonLimpiar;
    private JButton buttonComparar;
    private JButton buttonLimpiezaSol;
    private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;
    private JScrollPane scrollPane3;
    private JScrollPane scrollPane4;
    private JScrollPane scrollPane5;
    private JScrollPane scrollPane6;
    private JScrollPane scrollPane7;
    private JScrollPane scrollPane8;
    private JScrollPane scrollPane9;
    private JScrollPane scrollPane10;
    private JScrollPane scrollPane11;
    private JScrollPane scrollPane12;
    private JScrollPane scrollPane13;
    private JScrollPane scrollPane14;
    private JScrollPane scrollPane15;
    private JScrollPane scrollPane16;
    private JScrollPane scrollPane17;
    private JScrollPane scrollPane18;
    private JScrollPane scrollPane19;
    private JScrollPane scrollPane20;
    private JScrollPane scrollPane21;
    private JScrollPane scrollPane22;
    private JScrollPane scrollPane23;
    private JScrollPane scrollPane24;
    private JTextArea areaEnunciado;
    private JTextArea areaInitsCuestion;
    private JTextArea areaSolucionCuestion;
    private JTextArea areaLimpiezaCuestion;
    private JTextArea areaPostInitsCuestion;
    private JTextArea areaDescripJP;
    private JTextArea areaMsgErrorJP;
    private JTextArea areaInitsJP;
    private JTextArea areaEntradaJP;
    private JTextArea areaLimpiezaJP;
    private JTextArea areaSalidaJP;
    private JTextArea areaDescripCompr;
    private JTextArea areaMsgErrorCompr;
    private JTextArea areaEntradaCompr;
    private JTextArea areaSalidaCompr;
    private JTextArea editorResultados;
    private JTextArea editorAlumno;
    private JTextArea areaEstado;
    private JTextArea areaSolucionAlumno;
    private JCheckBox binaria;
    private JCheckBox binariaJP;
    private JCheckBox binariaCom;
    private JCheckBox gabia;
    private JCheckBox ssl;
    private JCheckBox mensajeJP;
    private JCheckBox mensajeCom;
    private JSpinner spinnerDificultad;
    private JSpinner spinnerPeso;
    private JXList listTematicas;
    private JXList listCorrectores;
    private JXTable tablePesos;
    private JXTreeTable treeTableComparacion;
    private JXTreeTable treeTableEjecucion;
    private JComboBox comboTipoSol;
    private JComboBox comboCategoria;
    private JComboBox comboEsquema;
    private JComboBox comboJP;
    private JComboBox comboCompr;
    private JComboBox comboIdiomaEnunciado;
    private JComboBox comboIdiomaJP;
    private JComboBox comboIdiomaCompr;
    private HighlighterPipeline pipeline;
    private DefaultListSelectionModel selectionModelPesos;
    private DefaultTreeSelectionModel treeSelectionModelEjec;

    private CellConstraints cc;
    private DefaultComponentFactory compFactory;

} // Fin clase PanelCuestion

