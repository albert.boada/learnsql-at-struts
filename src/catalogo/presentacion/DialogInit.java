//////////////////////////////////////////////////////
//  CLASSE: DialogInit.java                         //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import catalogo.global.CtrlAyuda;
import catalogo.global.CtrlModelo;
import catalogo.global.CtrlVista;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Properties;
import javax.swing.*;

public class DialogInit extends JDialog {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private CellConstraints cc1;
    private PanelPrincipal panelPrincipal;
    private FramePrincipal framePrincipal;
    private JPanel panelVisible;
    private DialogEspera dialogEspera;
    private SwingWorker workerInit;
    private SwingWorkerCompletionWaiter waiterInit;
    private boolean aceptado;
    private Properties props;
    
    public DialogInit(Frame owner, PanelPrincipal p, FramePrincipal f) {
        super(owner, true);
        initComponents();
        inicializa(p, f);
		emPassoElLoginPelFolre();
    }

    public DialogInit(Dialog owner, PanelPrincipal p, FramePrincipal f) {
        super(owner, true);
        initComponents();
        inicializa(p, f);
    }
    
    private void inicializa(PanelPrincipal p, FramePrincipal f) {
        panelPrincipal = p;
        framePrincipal = f;
        dialogEspera = null;
        props = null;
        cc1 = new CellConstraints();
        panelVisible = panelLogin;
        //muestraPanel(panelLogin);
        //setSize(500, 400);
        //this.setLocationRelativeTo(null);

        //buttonAceptar.setEnabled(false);
    }

    // manejar evento de pulsación de cualquier tecla
    private void textKeyReleased(KeyEvent e) {
        //if (textUsuario.getText().length() == 0 || passClave.getPassword().length == 0) buttonAceptar.setEnabled(false);
        //else buttonAceptar.setEnabled(true);
    }

    private void keyEnterBoto(KeyEvent e){
        /*if(e.getKeyCode()==10){
            buttonAceptarActionPerformed();
        }*/
    }
    
    private void muestraPanel(JPanel p) {
        remove(panelVisible);
        add(p, cc1.xy(2,2));
        this.validate();
        this.repaint();
        panelVisible = p;
    }

    private void buttonAceptarActionPerformed() {
        try {
            props = CtrlModelo.getCfgConexion();
			//creaWorker();
            //workerInit.execute();
			emPassoElLoginPelFolre();
            dialogEspera.setVisible(true);
        } catch (Exception e) {
            muestraPanel(panelError);
            areaInit.setText("Error durante la inicialización:\n" + e.getMessage());
            areaInit.setCaretPosition(0);
        }
    }
    
	private void emPassoElLoginPelFolre()
	{
		try
		{	
			boolean errorConexion = true;
			props = CtrlModelo.getCfgConexion();
            CtrlModelo.initConexion(props, "postgres", "ab78563421");
			errorConexion = false;
			CtrlModelo.initCtrl();
	        CtrlVista.initCtrl();
	        PanelEsquemas pEsq = new PanelEsquemas(CtrlModelo.esquemas);
	        PanelTematicas pTem = new PanelTematicas(CtrlModelo.tematicas,CtrlModelo.categorias);
	        PanelCategorias pCateg = new PanelCategorias(CtrlModelo.categorias);
        
	        PanelCuestion pCuestion = new PanelCuestion(CtrlModelo.catalogo, CtrlModelo.categorias, CtrlModelo.esquemas,
	                CtrlModelo.tematicas, CtrlModelo.tiposSolucion, CtrlModelo.idiomas, CtrlModelo.correctores, CtrlModelo.saveOnCommit);

	        PanelCorrectores pCor = new PanelCorrectores(CtrlModelo.correctores, CtrlModelo.catalogo);
	        PanelCatalogo pCat = new PanelCatalogo(pCuestion, CtrlModelo.catalogo, CtrlModelo.categorias.getAll(),
	                CtrlModelo.esquemas.getAll(), CtrlModelo.tematicas.getAll());
        
	        panelPrincipal.inicializa(pCat, pEsq, pTem, pCor, pCateg);
	        CtrlVista.initVista(framePrincipal, pCat, pCuestion);
	        pCuestion.muestraCuestion();
        
	        aceptado = true;
	        setVisible(false);
		}
		catch (Exception e) 
		{
            e.printStackTrace();
			muestraPanel(panelError);
			areaInit.setText("Error durante la inicialización:\n" + e.getMessage());
			areaInit.setCaretPosition(0);
        }	
	}

    private void creaWorker() {
        if (dialogEspera == null) {
            dialogEspera = new DialogEspera(this, "Inicializando controladores...");
            waiterInit = new SwingWorkerCompletionWaiter(dialogEspera);
        }
        dialogEspera.setLocationRelativeTo(this);
        
        workerInit = new SwingWorker<String, Void>()
        {
            @Override
            public String doInBackground() {   
                boolean errorConexion = false;
                try {
                    errorConexion = true;
                    CtrlModelo.initConexion(props, textUsuario.getText(), new String(passClave.getPassword()));
                    errorConexion = false;
                    CtrlModelo.initCtrl();
                    CtrlVista.initCtrl();
                    PanelEsquemas pEsq = new PanelEsquemas(CtrlModelo.esquemas);
                    PanelTematicas pTem = new PanelTematicas(CtrlModelo.tematicas,CtrlModelo.categorias);
                    PanelCategorias pCateg = new PanelCategorias(CtrlModelo.categorias);
                    
                    PanelCuestion pCuestion = new PanelCuestion(CtrlModelo.catalogo, CtrlModelo.categorias, CtrlModelo.esquemas,
                            CtrlModelo.tematicas, CtrlModelo.tiposSolucion, CtrlModelo.idiomas, CtrlModelo.correctores, CtrlModelo.saveOnCommit);

                    PanelCorrectores pCor = new PanelCorrectores(CtrlModelo.correctores, CtrlModelo.catalogo);
                    PanelCatalogo pCat = new PanelCatalogo(pCuestion, CtrlModelo.catalogo, CtrlModelo.categorias.getAll(),
                            CtrlModelo.esquemas.getAll(), CtrlModelo.tematicas.getAll());
                    
                    panelPrincipal.inicializa(pCat, pEsq, pTem, pCor, pCateg);
                    CtrlVista.initVista(framePrincipal, pCat, pCuestion);
                    pCuestion.muestraCuestion();
                    
                    aceptado = true;
                    setVisible(false);
                    return null;
                }
                catch (Exception e) {
                    if (errorConexion) return e.getMessage();
                    else {
                        e.printStackTrace();
                        muestraPanel(panelError);
                        areaInit.setText("Error durante la inicialización:\n" + e.getMessage());
                        areaInit.setCaretPosition(0);
                        return null;
                    }
                }
            }
            
            @Override
            public void done() {
                try { 
                    String res = get();
                    if (res != null) JOptionPane.showMessageDialog(getOwner(), res, "ERROR", JOptionPane.WARNING_MESSAGE);
                }
                catch (InterruptedException ignore) {}
                catch (java.util.concurrent.ExecutionException e) {
                    Throwable cause = e.getCause();
                    if (cause != null) System.out.println(cause.getMessage());
                    else System.out.println(e.getMessage());
                }
            }
        };
        
        workerInit.addPropertyChangeListener(waiterInit);
    }

    private void buttonCerrarActionPerformed() {
        this.setVisible(false);
        System.exit(0);// fet pel marc... que si no no tanca l'aplicacio!!!!!! impresionant
    }

    private void thisWindowOpened() {
    	aceptado = false;
    }
    
    public boolean haAceptado() {
        return aceptado;
    }
    
    private void labelHelpMouseClicked(MouseEvent e) {
    	if (e.getButton() == MouseEvent.BUTTON1) {
            try {
                CtrlAyuda.showHelp();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "ATENCIÓN: Error", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private void initComponents() {
		// Component initialization
		label3 = new JLabel();
		label4 = new JLabel();
		panelLogin = new JPanel();
		label1 = new JLabel();
		textUsuario = new JTextField();
		label2 = new JLabel();
		passClave = new JPasswordField();
		labelHelp1 = new JLabel();
		buttonAceptar = new JButton();
		buttonCancelar = new JButton();
		panelError = new JPanel();
		scrollPane1 = new JScrollPane();
		areaInit = new JTextArea();
		labelHelp2 = new JLabel();
		buttonCerrar = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle("Gestor de Cuestiones - Login");
		setResizable(false);
		setLocationByPlatform(true);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				thisWindowOpened();
			}
		});
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			"250px, 250px",
			"fill:240px, fill:140px"));

		//---- label3 ----
		label3.setIcon(new ImageIcon(getClass().getResource("/imagenes/splashscreen.png")));
		label3.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(label3, cc.xywh(1, 1, 2, 1));

		//---- label4 ----
		label4.setIcon(new ImageIcon(getClass().getResource("/imagenes/splashcorner.png")));
		contentPane.add(label4, cc.xy(1, 2));
		pack();
		setLocationRelativeTo(getOwner());

		//======== panelLogin ========
		{
			panelLogin.setBackground(new Color(87, 146, 255));
			panelLogin.setLayout(new FormLayout(
				new ColumnSpec[] {
					FormFactory.UNRELATED_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.RIGHT, Sizes.DEFAULT, FormSpec.NO_GROW),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
					FormFactory.DEFAULT_COLSPEC,
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(Sizes.dluX(50)),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(Sizes.dluX(50)),
					FormFactory.UNRELATED_GAP_COLSPEC
				},
				new RowSpec[] {
					new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.UNRELATED_GAP_ROWSPEC
				}));

			//---- label1 ----
			label1.setText("Usuario");
			label1.setForeground(Color.white);
			label1.setLabelFor(textUsuario);
			panelLogin.add(label1, cc.xy(2, 2));

			//---- textUsuario ----
			textUsuario.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased(e);
				}
			});
			panelLogin.add(textUsuario, cc.xywh(4, 2, 6, 1));

			//---- label2 ----
			label2.setText("Clave");
			label2.setForeground(Color.white);
			label2.setLabelFor(passClave);
			panelLogin.add(label2, cc.xy(2, 4));

			//---- passClave ----
			passClave.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased(e);
				}
			});
            passClave.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					keyEnterBoto(e);
				}
			});
			panelLogin.add(passClave, cc.xywh(4, 4, 6, 1));

			//---- labelHelp1 ----
			labelHelp1.setIcon(new ImageIcon(getClass().getResource("/imagenes/help.png")));
			labelHelp1.setToolTipText("Muestra los ficheros de ayuda");
			labelHelp1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					labelHelpMouseClicked(e);
				}
			});
			panelLogin.add(labelHelp1, cc.xy(5, 6));

			//---- buttonAceptar ----
			buttonAceptar.setText("Entrar");
			buttonAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonAceptarActionPerformed();
				}
			});
			panelLogin.add(buttonAceptar, cc.xy(7, 6));

			//---- buttonCancelar ----
			buttonCancelar.setText("Cerrar");
			buttonCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonCerrarActionPerformed();
				}
			});
			panelLogin.add(buttonCancelar, cc.xy(9, 6));
		}

		//======== panelError ========
		{
			panelError.setBackground(new Color(87, 146, 255));
			panelError.setLayout(new FormLayout(
				new ColumnSpec[] {
					FormFactory.UNRELATED_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
					FormFactory.DEFAULT_COLSPEC,
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(Sizes.dluX(50)),
					FormFactory.UNRELATED_GAP_COLSPEC
				},
				new RowSpec[] {
					new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.UNRELATED_GAP_ROWSPEC
				}));

			//======== scrollPane1 ========
			{

				//---- areaInit ----
				areaInit.setWrapStyleWord(true);
				areaInit.setLineWrap(true);
				areaInit.setEditable(false);
				areaInit.setBackground(Color.white);
				areaInit.setForeground(Color.red);
				scrollPane1.setViewportView(areaInit);
			}
			panelError.add(scrollPane1, cc.xywh(2, 1, 4, 1));

			//---- labelHelp2 ----
			labelHelp2.setIcon(new ImageIcon(getClass().getResource("/imagenes/help.png")));
			labelHelp2.setToolTipText("Muestra los ficheros de ayuda");
			labelHelp2.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					labelHelpMouseClicked(e);
					labelHelpMouseClicked(e);
				}
			});
			panelError.add(labelHelp2, cc.xy(3, 3));

			//---- buttonCerrar ----
			buttonCerrar.setText("Cerrar");
			buttonCerrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonCerrarActionPerformed();
				}
			});
			panelError.add(buttonCerrar, cc.xy(5, 3));
		}
		// End of component initialization
    }

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private JLabel label3;
	private JLabel label4;
	private JPanel panelLogin;
	private JLabel label1;
	private JTextField textUsuario;
	private JLabel label2;
	private JPasswordField passClave;
	private JLabel labelHelp1;
	private JButton buttonAceptar;
	private JButton buttonCancelar;
	private JPanel panelError;
	private JScrollPane scrollPane1;
	private JTextArea areaInit;
	private JLabel labelHelp2;
	private JButton buttonCerrar;


} // Fin clase DialogInit

