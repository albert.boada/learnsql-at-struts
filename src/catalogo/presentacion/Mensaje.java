//////////////////////////////////////////////////////
//  CLASSE: Mensaje.java                            //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import java.awt.*;
import java.util.*;
import javax.swing.*;


public class Mensaje {

//    private ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacastellano");

    public Mensaje(){

    }
    
    public void mensajeError(Component p, String text, Exception e, ResourceBundle recurs){
//        recursos=recurs;
        JOptionPane.showMessageDialog(p, text + e.getMessage(),
                recurs.getString("errwarning"), JOptionPane.WARNING_MESSAGE);
    }

    public void mensajeIncorrecto(Component p, String text, ResourceBundle recurs){
        JOptionPane.showMessageDialog(p, text,
                recurs.getString("errincorrect"), JOptionPane.WARNING_MESSAGE);
    }

    public int mensajeConfirmacion(Component p, String text, ResourceBundle recurs){
        return JOptionPane.showConfirmDialog(p, text,
                        recurs.getString("question"), JOptionPane.YES_NO_OPTION);
    }

    public int mensajePregunta(Component p, String text, ResourceBundle recurs){
        return JOptionPane.showConfirmDialog(p, text,
                        recurs.getString("pregunta"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
    }

    public void mensajeInformacion(Component p, String text, ResourceBundle recurs){
        JOptionPane.showMessageDialog(p, text,
                        recurs.getString("infocorrect"), JOptionPane.INFORMATION_MESSAGE);
    }


}
