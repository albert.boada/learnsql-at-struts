//////////////////////////////////////////////////////
//  CLASSE: DialogCopiaCuestionBD.java              //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import catalogo.modelo.*;
import catalogo.gestiondatos.CtrlComprobacion;
import catalogo.gestiondatos.CtrlCuestion;
import catalogo.gestiondatos.CtrlJuegoPruebas;
import catalogo.gestiondatos.WrapperConexion;
import catalogo.global.CtrlVista;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

import java.sql.*;
import java.util.*; //pel resourcebundle...

public class DialogCopiaCuestionBD extends JDialog {

    /*********************************************************/
    /*      Atributo                                         */
    /*********************************************************/

    private Cuestion cuestion;
    private Mensaje mensaje;
    ResourceBundle recursos;

    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/

    public DialogCopiaCuestionBD(Frame owner, ResourceBundle rec) {
        super(owner, true);
        recursos = rec;
        initComponents();
        inicializa();
    }

    public DialogCopiaCuestionBD(Dialog owner, ResourceBundle rec) {
        super(owner, true);
        recursos = rec;
        initComponents();
        inicializa();
    }

    public void setCuestion(Cuestion c) {
        cuestion = c;
    }

    private void inicializa() {
        cuestion = null;
        mensaje = new Mensaje();
        textHost.addMouseListener(CtrlVista.contextMouseAdapter);
        textNombre.addMouseListener(CtrlVista.contextMouseAdapter);
        textPuerto.addMouseListener(CtrlVista.contextMouseAdapter);
        textServidor.addMouseListener(CtrlVista.contextMouseAdapter);
        textUsuario.addMouseListener(CtrlVista.contextMouseAdapter);
        textKeyReleased();
    }

    private void buttonAceptarActionPerformed() {
        if (mensaje.mensajeConfirmacion(this, recursos.getString("dccaviso") + cuestion.getTitulo() + recursos.getString("dccaviso2"), recursos) == 0) {
            String sgbd;
            if (comboSGBD.getSelectedIndex() == 0) sgbd = WrapperConexion.SGBD_POSTGRESQL;
            else if (comboSGBD.getSelectedIndex() == 1) sgbd = WrapperConexion.SGBD_ORACLE;
            else if (comboSGBD.getSelectedIndex() == 2) sgbd = WrapperConexion.SGBD_INFORMIX;
            else sgbd = "";

            boolean ssl;
            if (comboSSL.getSelectedIndex() == 0) ssl = true;
            else ssl = false;

            WrapperConexion wcon = new WrapperConexion(sgbd, textHost.getText(), textPuerto.getText(), textServidor.getText(),
                    textNombre.getText(), textUsuario.getText(), new String(passClave.getPassword()), ssl);

            Connection con = null;
            PreparedStatement ps = null;
            Statement s = null;
            ResultSet rs = null;
            ArrayList<String> lista;
            CtrlCuestion ctrlCuestion;
            CtrlJuegoPruebas ctrlJuegoPruebas = null;
            CtrlComprobacion ctrlComprobacion = null;

            try
            {
                wcon.conectar();
                con = wcon.getConexion();
                ctrlCuestion = new CtrlCuestion(con);
                s = con.createStatement();
                rs = s.executeQuery("SELECT nom FROM t_esquemes");
                lista = new ArrayList<String>();
                while(rs.next()) { lista.add(rs.getString("nom")); }
                rs.close();

                if (!lista.contains(cuestion.getEsquema().getNombre()))
                {
                    ps = con.prepareStatement("INSERT INTO t_esquemes(nom, descripcio) VALUES(?, ?)");
                    ps.setString(1, cuestion.getEsquema().getNombre());
                    ps.setString(2, cuestion.getEsquema().getDescripcion());
                    ps.executeUpdate();
                    ps.close();
                }

                rs = s.executeQuery("SELECT nom FROM t_tematiques");
                lista = new ArrayList<String>();
                while(rs.next()) { lista.add(rs.getString("nom")); }
                rs.close();
                ps = con.prepareStatement("INSERT INTO t_tematiques(nom, descripcio) VALUES(?, ?)");
                for (Tematica tem : cuestion.getTematicas())  {
                    if (!lista.contains(tem.getNombre()))
                    {
                        ps.setString(1, tem.getNombre());
                        ps.setString(2, tem.getDescripcion());
                        ps.executeUpdate();
                    }
                }
                ps.close();

                rs = s.executeQuery("SELECT urlsw FROM t_urlsw");
                lista = new ArrayList<String>();
                while(rs.next()) { lista.add(rs.getString("urlsw")); }
                ps = con.prepareStatement("INSERT INTO t_urlsw(urlsw, descripcio) VALUES(?, ?)");
                Corrector corrector;
                for (Corrector cor : cuestion.getCorrectores()) {
                    if (!lista.contains(cor.getURL()))
                    {
                        ps.setString(1, cor.getURL());
                        ps.setString(2, cor.getDescripcion());
                        ps.executeUpdate();
                    }
                }

                int id = ctrlCuestion.insert(cuestion);
                ctrlCuestion.insertCorrectores(cuestion.getCorrectores(), id);

                for (JuegoPruebas jp : cuestion.getJuegosPruebas()) {
                    if (ctrlJuegoPruebas == null) ctrlJuegoPruebas = new CtrlJuegoPruebas(con);
                    ctrlJuegoPruebas.insert(jp, id);
                    for (Comprobacion compr : jp.getComprobaciones())
                    {
                        if (ctrlComprobacion == null) ctrlComprobacion = new CtrlComprobacion(con);
                        ctrlComprobacion.insert(compr, id);
                    }
                }
                con.commit();
                mensaje.mensajeInformacion(this, recursos.getString("dccaviso3"), recursos);

            }
            catch (Exception e) {
                try { if (con != null) con.rollback(); } catch (SQLException e2) {}
                mensaje.mensajeError(this, "", e, recursos);
                return;
            }
            finally {
                try { if (rs != null) rs.close(); } catch (SQLException e) {}
                try { if (s != null) rs.close(); } catch (SQLException e) {}
                try { if (ps != null) rs.close(); } catch (SQLException e) {}
                try { if (con != null) con.close(); } catch (SQLException e) {}
            }
            cuestion = null;
            this.setVisible(false);
        }
    }

    private void buttonCancelarActionPerformed() {
        this.setVisible(false);
    }

    private boolean checkTextFields() {
        boolean correcto = true;

        if (textHost.getText().length() == 0 ||
                textPuerto.getText().length() == 0 ||
                textNombre.getText().length() == 0 ||
                textUsuario.getText().length() == 0 ||
                passClave.getPassword().length == 0) correcto = false;
        else if (comboSGBD.getSelectedIndex() == 2 && textServidor.getText().length() == 0) correcto = false;

        return correcto;
    }

    private void textKeyReleased() {
        if (checkTextFields()) buttonAceptar.setEnabled(true);
        else buttonAceptar.setEnabled(false);
    }

    private void initComponents() {
		// Component initialization
		label9 = new JLabel();
		panel1 = new JPanel();
		label1 = new JLabel();
		comboSGBD = new JComboBox();
		label2 = new JLabel();
		textHost = new JTextField();
		label3 = new JLabel();
		textServidor = new JTextField();
		label4 = new JLabel();
		textPuerto = new JTextField();
		label8 = new JLabel();
		comboSSL = new JComboBox();
		label5 = new JLabel();
		textNombre = new JTextField();
		label6 = new JLabel();
		textUsuario = new JTextField();
		label7 = new JLabel();
		passClave = new JPasswordField();
		buttonAceptar = new JButton();
		buttonCancelar = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle(recursos.getString("dcctitle"));
		setResizable(false);
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			new ColumnSpec[] {
				FormFactory.UNRELATED_GAP_COLSPEC,
				new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
				new ColumnSpec(Sizes.dluX(60)),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				new ColumnSpec(Sizes.dluX(60)),
				FormFactory.UNRELATED_GAP_COLSPEC
			},
			new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.LINE_GAP_ROWSPEC,
				new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.NO_GROW),
				FormFactory.LINE_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.UNRELATED_GAP_ROWSPEC
			}));

		//---- label9 ----
		label9.setText(recursos.getString("dcclabel9"));
		contentPane.add(label9, cc.xywh(2, 2, 4, 1));

		//======== panel1 ========
		{
			panel1.setLayout(new FormLayout(
				new ColumnSpec[] {
					new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(45), FormSpec.NO_GROW),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.5),
					FormFactory.RELATED_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(45), FormSpec.NO_GROW),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.5)
				},
				new RowSpec[] {
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC
				}));
			((FormLayout)panel1.getLayout()).setColumnGroups(new int[][] {{3, 7}});

			//---- label1 ----
			label1.setText(recursos.getString("dcclabel1"));
			panel1.add(label1, cc.xy(1, 1));

			//---- comboSGBD ----
			comboSGBD.setModel(new DefaultComboBoxModel(new String[] {
				"POSTGRESQL",
				"ORACLE",
				"INFORMIX"
			}));
			panel1.add(comboSGBD, cc.xywh(3, 1, 5, 1));

			//---- label2 ----
			label2.setText(recursos.getString("dcclabel2"));
			panel1.add(label2, cc.xy(1, 3));

			//---- textHost ----
			textHost.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			panel1.add(textHost, cc.xywh(3, 3, 5, 1));

			//---- label3 ----
			label3.setText(recursos.getString("dcclabel3"));
			panel1.add(label3, cc.xy(1, 5));

			//---- textServidor ----
			textServidor.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			panel1.add(textServidor, cc.xywh(3, 5, 5, 1));

			//---- label4 ----
			label4.setText(recursos.getString("dcclabel4"));
			panel1.add(label4, cc.xy(1, 7));

			//---- textPuerto ----
			textPuerto.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			panel1.add(textPuerto, cc.xy(3, 7));

			//---- label8 ----
			label8.setText(recursos.getString("dcclabel8"));
			panel1.add(label8, cc.xy(5, 7));

			//---- comboSSL ----
			comboSSL.setModel(new DefaultComboBoxModel(new String[] {
				recursos.getString("dcccombo1"),
				recursos.getString("dcccombo2")
			}));
			panel1.add(comboSSL, cc.xy(7, 7));

			//---- label5 ----
			label5.setText(recursos.getString("dcclabel5"));
			panel1.add(label5, cc.xy(1, 9));

			//---- textNombre ----
			textNombre.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			panel1.add(textNombre, cc.xywh(3, 9, 5, 1));

			//---- label6 ----
			label6.setText(recursos.getString("dcclabel6"));
			panel1.add(label6, cc.xy(1, 11));

			//---- textUsuario ----
			textUsuario.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			panel1.add(textUsuario, cc.xywh(3, 11, 5, 1));

			//---- label7 ----
			label7.setText(recursos.getString("dcclabel7"));
			panel1.add(label7, cc.xy(1, 13));

			//---- passClave ----
			passClave.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			panel1.add(passClave, cc.xywh(3, 13, 5, 1));
		}
		contentPane.add(panel1, cc.xywh(2, 4, 4, 1));

		//---- buttonAceptar ----
		buttonAceptar.setText(recursos.getString("dccbuttonAceptar"));
		buttonAceptar.setIcon(new ImageIcon(getClass().getResource("/imagenes/tick.png")));
		buttonAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonAceptarActionPerformed();
			}
		});
		contentPane.add(buttonAceptar, cc.xy(3, 6));

		//---- buttonCancelar ----
		buttonCancelar.setText(recursos.getString("dccbuttonCancelar"));
		buttonCancelar.setIcon(new ImageIcon(getClass().getResource("/imagenes/cross.png")));
		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCancelarActionPerformed();
			}
		});
		contentPane.add(buttonCancelar, cc.xy(5, 6));
		pack();
		setLocationRelativeTo(getOwner());

    }

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private JLabel label9;
	private JPanel panel1;
	private JLabel label1;
	private JComboBox comboSGBD;
	private JLabel label2;
	private JTextField textHost;
	private JLabel label3;
	private JTextField textServidor;
	private JLabel label4;
	private JTextField textPuerto;
	private JLabel label8;
	private JComboBox comboSSL;
	private JLabel label5;
	private JTextField textNombre;
	private JLabel label6;
	private JTextField textUsuario;
	private JLabel label7;
	private JPasswordField passClave;
	private JButton buttonAceptar;
	private JButton buttonCancelar;

} // Fin clase DialogCopiaCuestionBD
