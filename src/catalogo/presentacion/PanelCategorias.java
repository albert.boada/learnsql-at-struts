//////////////////////////////////////////////////////
//  CLASSE: PanelCategorias.java                    //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import catalogo.global.CtrlVista;
import catalogo.modelo.Categoria;
//import catalogo.soporte.modelos.ArrayListModel;
import catalogo.soporte.modelos.TableModelCategorias;
import catalogo.modelo.DiccCategorias;
import catalogo.soporte.utils.TextParser;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import com.jgoodies.uif_lite.panel.*;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.AlternateRowHighlighter;
import org.jdesktop.swingx.decorator.HighlighterPipeline;

public class PanelCategorias extends JPanel {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private DiccCategorias categorias;
    private TableModelCategorias tableModel;
    private Mensaje mensaje;
    ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public PanelCategorias(DiccCategorias categorias) {
        initComponents();
        inicializa(categorias);
    }

    public void setFoco() {
        selectionModel.removeSelectionInterval(0, tableModel.getRowCount() - 1);
        textNombre.requestFocus();
    }

    public void cambioIdioma(ResourceBundle recursos){
        sifCategorias.setTitle(recursos.getString("sifCategorias"));
        label1.setText(recursos.getString("catlabel1"));
        label2.setText(recursos.getString("catlabel2"));
        buttonAdd.setText(recursos.getString("catbuttonAdd"));
        buttonMod.setText(recursos.getString("catbuttonMod"));
        buttonDel.setText(recursos.getString("catbuttonDel"));

        tableModel.setColumnName(recursos);
        tableCategorias.setModel(tableModel);
        tableCategorias.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableCategorias.getColumnModel().getColumn(1).setPreferredWidth(800);
        tableCategorias.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this.recursos = recursos;
    }

    private void inicializa(DiccCategorias categorias) {
        this.categorias = categorias;
        mensaje = new Mensaje();
        tableModel = new TableModelCategorias(categorias.getAll());
        tableCategorias.setModel(tableModel);
        tableCategorias.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableCategorias.getColumnModel().getColumn(1).setPreferredWidth(800);
        tableCategorias.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        textNombre.addMouseListener(CtrlVista.contextMouseAdapter);;
        textDescripcion.addMouseListener(CtrlVista.contextMouseAdapter);
        selectionModelValueChanged();
    }

    private void buttonAddActionPerformed() {
        textNombre.setText(TextParser.parseUTF8(textNombre.getText().trim()));
        textDescripcion.setText(TextParser.parseUTF8(textDescripcion.getText().trim()));
        try {
            Categoria cat = categorias.add(textNombre.getText(), textDescripcion.getText(), inits.isSelected(),
                    postinits.isSelected(), limpieza.isSelected(), estado.isSelected(), solucion.isSelected(),
                    initsJP.isSelected(), entradaJP.isSelected(), limpiezaJP.isSelected(), comprovaciones.isSelected());
            tableModel.fireTableDataChanged();
            selectionModel.setSelectionInterval(tableModel.getIndex(cat), tableModel.getIndex(cat));
        } catch (Exception e) {
            mensaje.mensajeError(this, recursos.getString("cataviso1"), e, recursos);
        }
    }

   private void buttonModActionPerformed() {
        textNombre.setText(TextParser.parseUTF8(textNombre.getText().trim()));
        textDescripcion.setText(TextParser.parseUTF8(textDescripcion.getText().trim()));
        Categoria cat = (Categoria) tableModel.getValueAt(tableCategorias.convertRowIndexToModel(tableCategorias.getSelectedRow()));
            if (mensaje.mensajeConfirmacion(this, recursos.getString("cataviso2") + cat.getId() + "\"?", recursos) == 0) {
                try {
                    categorias.set(cat, textNombre.getText(), textDescripcion.getText(), inits.isSelected(),
                    postinits.isSelected(), limpieza.isSelected(), estado.isSelected(), solucion.isSelected(),
                    initsJP.isSelected(), entradaJP.isSelected(), limpiezaJP.isSelected(), comprovaciones.isSelected());
                    tableModel.fireTableDataChanged();
                selectionModel.setSelectionInterval(tableModel.getIndex(cat), tableModel.getIndex(cat));
                } catch (Exception e) {
                    mensaje.mensajeError(this, recursos.getString("cataviso3"), e, recursos);
                }
                CtrlVista.creaCuestion();
            }
    }

    private void buttonDelActionPerformed() {
        int rowIndex = tableCategorias.getSelectedRow();
        Categoria cat = (Categoria) tableModel.getValueAt(rowIndex);
        if (mensaje.mensajeConfirmacion(this, recursos.getString("cataviso4") + cat.getId() + "\"?", recursos) == 0) {
            try {
                if (CtrlVista.categoriaEnUso(cat)) throw new Exception(recursos.getString("excepcat"));
                categorias.remove(cat);
                tableModel.fireTableDataChanged();
                int total = tableCategorias.getRowCount();
                if (total > 0) {
                    if (rowIndex < total) selectionModel.setSelectionInterval(rowIndex, rowIndex);
                    else selectionModel.setSelectionInterval(total - 1, total - 1);
                }
            } catch (Exception e) {
                mensaje.mensajeError(this, recursos.getString("cataviso5"), e, recursos);
            }
        }
    }

    private void selectionModelValueChanged() {
        if (tableCategorias.getSelectedRow() == -1) {
            textNombre.setText("");
            textDescripcion.setText("");
            inits.setSelected(false);
            postinits.setSelected(false);
            limpieza.setSelected(false);
            estado.setSelected(false);
            solucion.setSelected(false);
            initsJP.setSelected(false);
            entradaJP.setSelected(false);
            limpiezaJP.setSelected(false);
            comprovaciones.setSelected(false);
            buttonDel.setEnabled(false);
            buttonMod.setEnabled(false);
            buttonAdd.setEnabled(false);
        } else {
            Categoria cat = (Categoria) tableModel.getValueAt(tableCategorias.getSelectedRow());
            textNombre.setText(Integer.toString(cat.getId()));
            textDescripcion.setText(cat.getDescripcio());
            inits.setSelected(cat.getInits());
            postinits.setSelected(cat.getPostinits());
            limpieza.setSelected(cat.getLimpieza());
            estado.setSelected(cat.getEstado());
            solucion.setSelected(cat.getSolucion());
            initsJP.setSelected(cat.getInitsJP());
            entradaJP.setSelected(cat.getEntradaJP());
            limpiezaJP.setSelected(cat.getLimpiezaJP());
            comprovaciones.setSelected(cat.getComprobaciones());
            buttonDel.setEnabled(true);
            buttonMod.setEnabled(true);
            buttonAdd.setEnabled(true);
        }
    }

    private void checkBoxActionPerformed(ActionEvent e) {
        JCheckBox cb = (JCheckBox) e.getSource();
        int rowIndex = tableCategorias.getSelectedRow();
        Categoria cat = (Categoria) tableModel.getValueAt(rowIndex);
        boolean b = cb.isSelected();
        if(rowIndex != -1){
            if (cb == inits && b != cat.getInits()) {
                cat.setInits(b);
            } else {
                return;
            }
        }
    }

    private void textKeyReleased() {
        if (textNombre.getText().isEmpty() || textDescripcion.getText().isEmpty()) {
            buttonAdd.setEnabled(false);
            buttonMod.setEnabled(false);
        } else {
            buttonAdd.setEnabled(true);
            if (tableCategorias.getSelectedRow() != -1) buttonMod.setEnabled(true);
        }
    }

    private void initComponents() {
        // Component initialization
        sifCategorias = new SimpleInternalFrame();
        scrollPane1 = new JScrollPane();
        tableCategorias = new JXTable() {
            public String getToolTipText(MouseEvent e) {
                        java.awt.Point p = e.getPoint();
                return (String) getValueAt(rowAtPoint(p), columnAtPoint(p));
            }
        };
        labelinits = new JLabel();
        inits = new JCheckBox();
        labelpostinits = new JLabel();
        postinits = new JCheckBox();
        labellimpieza = new JLabel();
        limpieza = new JCheckBox();
        labelestado = new JLabel();
        estado = new JCheckBox();
        labelsolucion = new JLabel();
        solucion = new JCheckBox();
        labelinitsJP = new JLabel();
        initsJP = new JCheckBox();
        labelentradaJP = new JLabel();
        entradaJP = new JCheckBox();
        labellimpiezaJP = new JLabel();
        limpiezaJP = new JCheckBox();
        labelcomprovaciones = new JLabel();
        comprovaciones = new JCheckBox();
        label1 = new JLabel();
        textNombre = new JTextField();
        label2 = new JLabel();
        textDescripcion = new JTextField();
        buttonAdd = new JButton();
        buttonMod = new JButton();
        buttonDel = new JButton();
        selectionModel = (DefaultListSelectionModel) tableCategorias.getSelectionModel();
        pipeline = new HighlighterPipeline();
        CellConstraints cc = new CellConstraints();

        //======== this ========
        setLayout(new FormLayout(
                "default:grow",
                "fill:default:grow"));

        //======== sifCategorias ========
        {
                sifCategorias.setTitle(recursos.getString("sifCategorias"));
                sifCategorias.setFrameIcon(new ImageIcon(getClass().getResource("/imagenes/panel_categorias.png")));
                Container sifCategoriasContentPane = sifCategorias.getContentPane();
                sifCategoriasContentPane.setLayout(new FormLayout(
                    new ColumnSpec[] {
                        FormFactory.UNRELATED_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.DEFAULT, FormSpec.NO_GROW),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(7), FormSpec.NO_GROW),
                        new ColumnSpec(Sizes.dluX(15)),
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.DEFAULT, FormSpec.NO_GROW),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(7), FormSpec.NO_GROW),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.6),
                        new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.4),
                        new ColumnSpec(Sizes.dluX(65)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(65)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(65)),
                        FormFactory.UNRELATED_GAP_COLSPEC
                    },
                    new RowSpec[] {
                        FormFactory.UNRELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.UNRELATED_GAP_ROWSPEC
                    }));

                //======== scrollPane1 ========
                {
                    scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

                    //---- tableCategorias ----
                    tableCategorias.setHighlighters(pipeline);
                    tableCategorias.setSortable(false);
                    scrollPane1.setViewportView(tableCategorias);
                }
                sifCategoriasContentPane.add(scrollPane1, cc.xywh(2, 2, 17, 2));

            //---- labelinits ----
            labelinits.setText(recursos.getString("catlabelinits"));
            sifCategoriasContentPane.add(labelinits, cc.xy(4, 5));

            //---- inits ----
            inits.setBackground(Color.white);
            inits.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(inits, cc.xy(6, 5));

            //---- labelpostinits ----
            labelpostinits.setText(recursos.getString("catlabelpostinits"));
            sifCategoriasContentPane.add(labelpostinits, cc.xy(4, 7));

            //---- postinits ----
            postinits.setBackground(Color.white);
            postinits.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(postinits, cc.xy(6, 7));

            //---- labellimpieza ----
            labellimpieza.setText(recursos.getString("catlabellimpieza"));
            sifCategoriasContentPane.add(labellimpieza, cc.xy(4, 9));

            //---- limpieza ----
            limpieza.setBackground(Color.white);
            postinits.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(limpieza, cc.xy(6, 9));

            //---- labelestado ----
            labelestado.setText(recursos.getString("catlabelestado"));
            sifCategoriasContentPane.add(labelestado, cc.xy(4, 11));

            //---- estado ----
            estado.setBackground(Color.white);
            estado.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(estado, cc.xy(6, 11));

            //---- labelsolucion ----
            labelsolucion.setText(recursos.getString("catlabelsolucion"));
            sifCategoriasContentPane.add(labelsolucion, cc.xy(4, 13));

            //---- solucion ----
            solucion.setBackground(Color.white);
            solucion.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(solucion, cc.xy(6, 13));

            //---- labelinitsJP ----
            labelinitsJP.setText(recursos.getString("catlabelinitsJP"));
            sifCategoriasContentPane.add(labelinitsJP, cc.xy(8, 5));

            //---- initsJP ----
            initsJP.setBackground(Color.white);
            initsJP.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(initsJP, cc.xy(10, 5));

            //---- labelentradaJP ----
            labelentradaJP.setText(recursos.getString("catlabelentradaJP"));
            sifCategoriasContentPane.add(labelentradaJP, cc.xy(8, 7));

            //---- entradaJP ----
            entradaJP.setBackground(Color.white);
            entradaJP.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(entradaJP, cc.xy(10, 7));

            //---- labellimpiezaJP ----
            labellimpiezaJP.setText(recursos.getString("catlabellimpiezaJP"));
            sifCategoriasContentPane.add(labellimpiezaJP, cc.xy(8, 9));

            //---- limpiezaJP ----
            limpiezaJP.setBackground(Color.white);
            limpiezaJP.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(limpiezaJP, cc.xy(10, 9));

            //---- labelcomprovaciones ----
            labelcomprovaciones.setText(recursos.getString("catlabelcomprovaciones"));
            sifCategoriasContentPane.add(labelcomprovaciones, cc.xy(8, 11));

            //---- comprovaciones ----
            comprovaciones.setBackground(Color.white);
            comprovaciones.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    checkBoxActionPerformed(e);
                }
            });
            sifCategoriasContentPane.add(comprovaciones, cc.xy(10, 11));

                //---- label1 ----
                label1.setText(recursos.getString("catlabel1"));
                sifCategoriasContentPane.add(label1, cc.xy(2, 15));

                //---- textNombre ----
                textNombre.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyReleased(KeyEvent e) {
                                textKeyReleased();
                        }
                });
                sifCategoriasContentPane.add(textNombre, cc.xywh(4, 15, 9, 1));

                //---- label2 ----
                label2.setText(recursos.getString("catlabel2"));
                sifCategoriasContentPane.add(label2, cc.xy(2, 17));

                //---- textDescripcion ----
                textDescripcion.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyReleased(KeyEvent e) {
                                textKeyReleased();
                        }
                });
                sifCategoriasContentPane.add(textDescripcion, cc.xywh(4, 17, 15, 1));

                //---- buttonAdd ----
                buttonAdd.setText(recursos.getString("catbuttonAdd"));
                buttonAdd.setIcon(new ImageIcon(getClass().getResource("/imagenes/add.png")));
                buttonAdd.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                                buttonAddActionPerformed();
                        }
                });
                sifCategoriasContentPane.add(buttonAdd, cc.xy(14, 19));

                //---- buttonMod ----
                buttonMod.setText(recursos.getString("catbuttonMod"));
                buttonMod.setIcon(new ImageIcon(getClass().getResource("/imagenes/edit.png")));
                buttonMod.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                                buttonModActionPerformed();
                        }
                });
                sifCategoriasContentPane.add(buttonMod, cc.xy(16, 19));

                //---- buttonDel ----
                buttonDel.setText(recursos.getString("catbuttonDel"));
                buttonDel.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
                buttonDel.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                                buttonDelActionPerformed();
                        }
                });
                sifCategoriasContentPane.add(buttonDel, cc.xy(18, 19));
		}
		add(sifCategorias, cc.xy(1, 1));

		//---- selectionModel ----
		selectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				selectionModelValueChanged();
			}
		});

		//---- pipeline ----
		pipeline.addHighlighter(new AlternateRowHighlighter());
		// End of component initialization
    }

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

    private SimpleInternalFrame sifCategorias;
    private JScrollPane scrollPane1;
    private JXTable tableCategorias;
    private JLabel labelinits;
    private JCheckBox inits;
    private JLabel labelpostinits;
    private JCheckBox postinits;
    private JLabel labellimpieza;
    private JCheckBox limpieza;
    private JLabel labelestado;
    private JCheckBox estado;
    private JLabel labelsolucion;
    private JCheckBox solucion;
    private JLabel labelinitsJP;
    private JCheckBox initsJP;
    private JLabel labelentradaJP;
    private JCheckBox entradaJP;
    private JLabel labellimpiezaJP;
    private JCheckBox limpiezaJP;
    private JLabel labelcomprovaciones;
    private JCheckBox comprovaciones;
    private JLabel label1;
    private JTextField textNombre;
    private JLabel label2;
    private JTextField textDescripcion;
    private JButton buttonAdd;
    private JButton buttonMod;
    private JButton buttonDel;
    private DefaultListSelectionModel selectionModel;
    private HighlighterPipeline pipeline;

} // Fin clase PanelCategorias
