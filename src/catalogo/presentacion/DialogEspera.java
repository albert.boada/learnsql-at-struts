//////////////////////////////////////////////////////
//  CLASSE: DialogEspera.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import java.awt.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

public class DialogEspera extends JDialog {

	public DialogEspera(Frame owner, String operacion) {
		super(owner);
		initComponents();
        inicializa(operacion);
	}

	public DialogEspera(Dialog owner, String operacion) {
		super(owner);
		initComponents();
        inicializa(operacion);
	}
        
    public void inicializa(String operacion) {
        setModal(true);
        labelOperacion.setText(operacion);
    }

	private void initComponents() {
		// Component initialization
		labelOperacion = new JLabel();
		progressEjecucion = new JProgressBar();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle("Espere...");
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			new ColumnSpec[] {
				FormFactory.UNRELATED_GAP_COLSPEC,
				new ColumnSpec(Sizes.dluX(180)),
				FormFactory.UNRELATED_GAP_COLSPEC
			},
			new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.LINE_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.UNRELATED_GAP_ROWSPEC
			}));

		//---- labelOperacion ----
		labelOperacion.setText("Realizando operaci\u00f3n...");
		contentPane.add(labelOperacion, cc.xy(2, 2));

		//---- progressEjecucion ----
		progressEjecucion.setIndeterminate(true);
		contentPane.add(progressEjecucion, cc.xy(2, 4));
		pack();
		setLocationRelativeTo(getOwner());

	}

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private JLabel labelOperacion;
	private JProgressBar progressEjecucion;

} // Fin clase DialogEspera

