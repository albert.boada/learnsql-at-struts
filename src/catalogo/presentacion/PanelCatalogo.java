//////////////////////////////////////////////////////
//  CLASSE: PanelCatologo.java                      //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import catalogo.modelo.Esquema;
import catalogo.modelo.Cuestion;
import catalogo.modelo.DiccCatalogo;
import catalogo.modelo.Categoria;
import catalogo.modelo.Tematica;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.JSpinner.DefaultEditor;
import java.util.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import com.jgoodies.uif_lite.component.*;
import com.jgoodies.uif_lite.panel.*;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
//import java.text.ParseException;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.AlternateRowHighlighter;
import org.jdesktop.swingx.decorator.HighlighterPipeline;
import java.text.ParseException;

public class PanelCatalogo extends JPanel {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private PanelCuestion panelCuestion;
    private DiccCatalogo catalogo;
    private Mensaje mensaje;
    ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/
    
    public PanelCatalogo(PanelCuestion panel, DiccCatalogo cat, ListModel categorias, ListModel esquemas, ListModel tematicas) {
        initComponents();
        inicializa(panel, cat, categorias, esquemas, tematicas);
    }

    public void cambioIdioma(ResourceBundle recursos){
        sifListado.setTitle(recursos.getString("sifListado"));
        label6.setText(recursos.getString("listlabel6"));
        label7.setText(recursos.getString("listlabel7"));
        label8.setText(recursos.getString("listlabel8"));
        label9.setText(recursos.getString("listlabel9"));
        label10.setText(recursos.getString("listlabel10"));
        label11.setText(recursos.getString("listlabel11"));
        label12.setText(recursos.getString("listlabel12"));
        label13.setText(recursos.getString("listlabel13"));
        comboEstado.setModel(new DefaultComboBoxModel(new String[] {
            recursos.getString("cualquiera"),
            recursos.getString("resueltas"),
            recursos.getString("sinresolver")
        }));
        spinnerDificultad1.setToolTipText(recursos.getString("spinnerDificultad"));
        spinnerDificultad2.setToolTipText(recursos.getString("spinnerDificultad"));
        buttonReset.setText(recursos.getString("listbuttonReset"));
        buttonReset.setToolTipText(recursos.getString("listbuttonResetTooltip"));
        buttonFiltrar.setText(recursos.getString("listbuttonFiltrar"));
        buttonFiltrar.setToolTipText(recursos.getString("listbuttonFiltrarTooltip"));
        buttonTodas.setText(recursos.getString("listbuttonTodas"));
        buttonTodas.setToolTipText(recursos.getString("listbuttonTodasTooltip"));

        panelCuestion.cambioIdioma(recursos);

        this.recursos = recursos;
    }
    
    private void inicializa(PanelCuestion panel, DiccCatalogo cat, ListModel categorias, ListModel esquemas, ListModel tematicas) {
        panelCuestion = panel;
        catalogo = cat;
        mensaje = new Mensaje();
        listCategorias.setModel(categorias);
        listEsquemas.setModel(esquemas);
        listTematicas.setModel(tematicas);
        listCuestiones.setModel(cat.getAll());
        splitCatalogo.setRightComponent(panelCuestion);

        // ********************************************************************
        // Se define y asigna un KeyListener al editor del JSpinner para que
        // se reconozca la pulsación de teclas.
        final JFormattedTextField spinnerEditorText = ((DefaultEditor) spinnerDificultad1.getEditor()).getTextField();
        spinnerEditorText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int pos = spinnerEditorText.getCaretPosition();
                float dificultad = ((Float) spinnerDificultad1.getValue()).floatValue();
                dificultad *= 1000;
                dificultad = (float) Math.round(dificultad) / 1000;
                spinnerDificultad1.setValue(dificultad);

                try {
                    spinnerDificultad1.commitEdit();
                } catch (ParseException e1) {
                    spinnerEditorText.setValue(spinnerDificultad1.getValue());
                }

                if (pos <= spinnerEditorText.getText().length()) spinnerEditorText.setCaretPosition(pos);
                else spinnerEditorText.setCaretPosition(spinnerEditorText.getText().length());
            }
        });

        final JFormattedTextField spinnerEditorText2 = ((DefaultEditor) spinnerDificultad2.getEditor()).getTextField();
        spinnerEditorText2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int pos = spinnerEditorText2.getCaretPosition();
                float dificultad = ((Float) spinnerDificultad2.getValue()).floatValue();
                dificultad *= 1000;
                dificultad = (float) Math.round(dificultad) / 1000;
                spinnerDificultad2.setValue(dificultad);

                try {
                    spinnerDificultad2.commitEdit();
                } catch (ParseException e1) {
                    spinnerEditorText2.setValue(spinnerDificultad2.getValue());
                }

                if (pos <= spinnerEditorText2.getText().length()) spinnerEditorText2.setCaretPosition(pos);
                else spinnerEditorText2.setCaretPosition(spinnerEditorText2.getText().length());
            }
        });

    }

    private void buttonResetActionPerformed() {
        textTitulo.setText("");
        textAutor.setText("");
        textId.setText("");
        comboEstado.setSelectedIndex(0);
        spinnerDificultad1.setValue(0.0f);
        spinnerDificultad2.setValue(1.0f);
        listCategorias.clearSelection();
        listEsquemas.clearSelection();
        listTematicas.clearSelection();
//        catalogo.getAll().clear();
//        listCuestiones.setModel(catalogo.getAll().EMPTY_LIST);
//        listCuestiones.setModel(catalogo.getAll());
    }
    
    private void buttonFiltrarActionPerformed() {
        listCuestiones.clearSelection();
        
        String titulo = null;
        String autor = null;
        int id = 0;
        int estado = 0;
        float mindificultad = 0;
        float maxdificultad = 1;
        List<Categoria> categorias = null;
        List<Esquema> esquemas = null;
        List<Tematica> tematicas = null;
        
        textAutor.setText(textAutor.getText().trim());
        textTitulo.setText(textTitulo.getText().trim());
        textId.setText(textId.getText().trim());
        
        if (textTitulo.getText().length() > 0) titulo = textTitulo.getText();
        if (textAutor.getText().length() > 0) autor = textAutor.getText();

        estado = comboEstado.getSelectedIndex();
        if (textId.getText().length() > 0){
            try {
                id= Integer.parseInt(textId.getText());
            } catch (NumberFormatException nfe){
                mensaje.mensajeError(this, recursos.getString("catalogoerror"), nfe, recursos);
            }
        }

        mindificultad = ((Float) spinnerDificultad1.getValue()).floatValue();
        maxdificultad = ((Float) spinnerDificultad2.getValue()).floatValue();


        if (!listCategorias.isSelectionEmpty()) {
            categorias = new ArrayList<Categoria>();
            for (Object obj : listCategorias.getSelectedValues()) categorias.add((Categoria) obj);
        }
        
        if (!listEsquemas.isSelectionEmpty()) {
            esquemas = new ArrayList<Esquema>();
            for (Object obj : listEsquemas.getSelectedValues()) esquemas.add((Esquema) obj);
        }
        
        if (!listTematicas.isSelectionEmpty()) {
            tematicas = new ArrayList<Tematica>();
            for (Object obj : listTematicas.getSelectedValues()) tematicas.add((Tematica) obj);
        }
        
        try {
            catalogo.search(id, estado, titulo, autor, mindificultad, maxdificultad, categorias, esquemas, tematicas);
        }
        catch (Exception e) {
            mensaje.mensajeError(getTopLevelAncestor(), "", e, recursos);
        }
    }
    
    public void setSelected(Cuestion c) {
        if (c == null) {
            listCuestiones.clearSelection();
            catalogo.setCuestiones();
            listCuestiones.setModel(catalogo.getAll());
        }
        else if (c.getId() == 0) {
            listCuestiones.clearSelection();
            panelCuestion.muestraCuestion(c);
        }
        else listCuestiones.setSelectedValue(c, true);
    }
 
    private void buttonTodasActionPerformed() {
        try {
            catalogo.search();
        }
        catch (Exception e) {
            mensaje.mensajeError(getTopLevelAncestor(), "", e, recursos);
        }
    }
    
    private void listCuestionesValueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting() && listCuestiones.getSelectedIndex() != -1)
            panelCuestion.muestraCuestion((Cuestion) listCuestiones.getSelectedValue());
    }

    private void spinnerDificultadStateChanged() {
//        float dificultad = ((Float) spinnerDificultad.getValue()).floatValue();
//        dificultad *= 100;
//        dificultad = (float) Math.round(dificultad) / 100;
//        spinnerDificultad.setValue(dificultad);
//        resetdificultad = false;
//        if (dificultad != cuestion.getDificultad()) {
//            soc.setCuestionModif();
//            setControlesModif(true);
//        }
    }

    public void setEnabledControlesBusqueda(boolean enabled) {
        textAutor.setEnabled(enabled);
        textTitulo.setEnabled(enabled);
        textId.setEnabled(enabled);
        comboEstado.setEnabled(enabled);
        spinnerDificultad1.setEnabled(enabled);
        spinnerDificultad2.setEnabled(enabled);
        listEsquemas.setEnabled(enabled);
        listCategorias.setEnabled(enabled);
        listTematicas.setEnabled(enabled);
        buttonReset.setEnabled(enabled);
        buttonFiltrar.setEnabled(enabled);
        buttonTodas.setEnabled(enabled);
        listCuestiones.setEnabled(enabled);
    }
    
    private void initComponents() {
		// Component initialization
		splitCatalogo = new UIFSplitPane();
		sifListado = new SimpleInternalFrame();
		panelListado = new JPanel();
		splitListado = new UIFSplitPane();
		panelSplit1 = new JPanel();
		label6 = new JLabel();
		textTitulo = new JTextField();
		label7 = new JLabel();
		textAutor = new JTextField();
		label8 = new JLabel();
		comboEstado = new JComboBox();
        label14 = new JLabel();
        textId = new JTextField();
		label10 = new JLabel();
		scrollPane2 = new JScrollPane();
		listCategorias = new JXList();
		label9 = new JLabel();
		scrollPane3 = new JScrollPane();
		listEsquemas = new JXList();
		label11 = new JLabel();
		scrollPane1 = new JScrollPane();
        label12 = new JLabel();
        label13 = new JLabel();
        spinnerDificultad1 = new JSpinner();
        spinnerDificultad2 = new JSpinner();
		listTematicas = new JXList();
		buttonReset = new JButton();
		buttonFiltrar = new JButton();
		buttonTodas = new JButton();
		separator2 = new JSeparator();
		panelSplit2 = new JPanel();
		scrollPane14 = new JScrollPane();
		listCuestiones = new JXList();
		pipeline = new HighlighterPipeline();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setLayout(new FormLayout(
			"default:grow",
			"fill:default:grow"));

		//======== splitCatalogo ========
		{
			splitCatalogo.setOneTouchExpandable(true);
			splitCatalogo.setBorder(null);
			splitCatalogo.setResizeWeight(0.2);

			//======== sifListado ========
			{
				sifListado.setTitle(recursos.getString("sifListado"));
				sifListado.setFrameIcon(new ImageIcon(getClass().getResource("/imagenes/panel_catalogo.png")));
				Container sifListadoContentPane = sifListado.getContentPane();
				sifListadoContentPane.setLayout(new FormLayout(
					"default:grow",
					"fill:default:grow"));

				//======== panelListado ========
				{
					panelListado.setLayout(new FormLayout(
						"default:grow",
						"top:2dlu, fill:default:grow"));

					//======== splitListado ========
					{
						splitListado.setOrientation(JSplitPane.VERTICAL_SPLIT);
						splitListado.setOneTouchExpandable(true);
						splitListado.setBorder(null);
						splitListado.setResizeWeight(0.5);

						//======== panelSplit1 ========
						{
							panelSplit1.setLayout(new FormLayout(
								new ColumnSpec[] {
									FormFactory.UNRELATED_GAP_COLSPEC,
									new ColumnSpec(ColumnSpec.RIGHT, Sizes.MINIMUM, FormSpec.NO_GROW),
									FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
									new ColumnSpec(Sizes.dluX(45)),
                                    FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
									new ColumnSpec(ColumnSpec.CENTER, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
									FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
									new ColumnSpec(Sizes.dluX(45)),
									FormFactory.UNRELATED_GAP_COLSPEC
								},
								new RowSpec[] {
									new RowSpec(RowSpec.TOP, Sizes.DLUY5, FormSpec.NO_GROW),
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
                                    FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									new RowSpec(Sizes.DLUY14),
									new RowSpec(RowSpec.CENTER, Sizes.dluY(36), 0.19999999999999998),
									FormFactory.LINE_GAP_ROWSPEC,
									new RowSpec(Sizes.DLUY14),
									new RowSpec("max(default;36dlu):grow(0.19999999999999998)"),
									FormFactory.LINE_GAP_ROWSPEC,
									new RowSpec(Sizes.DLUY14),
									new RowSpec("fill:max(default;52dlu):grow(0.6)"),
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.UNRELATED_GAP_ROWSPEC,
									new RowSpec(RowSpec.BOTTOM, Sizes.DEFAULT, FormSpec.NO_GROW),
									new RowSpec(RowSpec.TOP, Sizes.DLUY2, FormSpec.NO_GROW)
								}));

							//---- label6 ----
							label6.setText(recursos.getString("listlabel6"));
							panelSplit1.add(label6, cc.xy(2, 2));
							panelSplit1.add(textTitulo, cc.xywh(4, 2, 5, 1));

							//---- label7 ----
							label7.setText(recursos.getString("listlabel7"));
							panelSplit1.add(label7, cc.xy(2, 4));
							panelSplit1.add(textAutor, cc.xywh(4, 4, 5, 1));

							//---- label8 ----
							label8.setText(recursos.getString("listlabel8"));
							panelSplit1.add(label8, cc.xy(2, 6));

							//---- comboEstado ----
							comboEstado.setModel(new DefaultComboBoxModel(new String[] {
								recursos.getString("cualquiera"),
								recursos.getString("resueltas"),
								recursos.getString("sinresolver")
							}));
							panelSplit1.add(comboEstado, cc.xywh(4, 6, 2, 1));

                            //---- label14 ----
							label14.setText("Id");//recursos.getString("listlabel7"));
							panelSplit1.add(label14, cc.xy(6, 6));
                            panelSplit1.add(textId, cc.xywh(8, 6, 1, 1));

                            //---- label12 ----
							label12.setText(recursos.getString("listlabel12"));
							panelSplit1.add(label12, cc.xy(2, 8));

                            //---- spinnerDificultad1 ----
                            spinnerDificultad1.setToolTipText(recursos.getString("spinnerDificultad"));
                            spinnerDificultad1.addChangeListener(new ChangeListener() {
                                public void stateChanged(ChangeEvent e) {
                                    spinnerDificultadStateChanged();
                                }
                            });
                            spinnerDificultad1.setModel(new SpinnerNumberModel(new Float(0.0f), new Float(0.0f), new Float(1.0f), new Float(0.1f)));
                            panelSplit1.add(spinnerDificultad1, cc.xy(4, 8));

                            //---- label13 ----
							label13.setText(recursos.getString("listlabel13"));
							panelSplit1.add(label13, cc.xy(6, 8));

                            //---- spinnerDificultad2 ----
                            spinnerDificultad2.setToolTipText(recursos.getString("spinnerDificultad"));
                            spinnerDificultad2.addChangeListener(new ChangeListener() {
                                public void stateChanged(ChangeEvent e) {
                                    spinnerDificultadStateChanged();
                                }
                            });
                            spinnerDificultad2.setModel(new SpinnerNumberModel(new Float(1.0f), new Float(0.0f), new Float(1.0f), new Float(0.1f)));
                            panelSplit1.add(spinnerDificultad2, cc.xy(8, 8));

							//---- label10 ----
							label10.setText(recursos.getString("listlabel10"));
							panelSplit1.add(label10, cc.xy(2, 10));

							//======== scrollPane2 ========
							{

								//---- listCategorias ----
								listCategorias.setHighlighters(pipeline);
								scrollPane2.setViewportView(listCategorias);
							}
							panelSplit1.add(scrollPane2, cc.xywh(4, 10, 5, 2));

							//---- label9 ----
							label9.setText(recursos.getString("listlabel9"));
							panelSplit1.add(label9, cc.xy(2, 13));

							//======== scrollPane3 ========
							{

								//---- listEsquemas ----
								listEsquemas.setHighlighters(pipeline);
								scrollPane3.setViewportView(listEsquemas);
							}
							panelSplit1.add(scrollPane3, cc.xywh(4, 13, 5, 2));

							//---- label11 ----
							label11.setText(recursos.getString("listlabel11"));
							panelSplit1.add(label11, cc.xy(2, 16));

							//======== scrollPane1 ========
							{

								//---- listTematicas ----
								listTematicas.setHighlighters(pipeline);
								scrollPane1.setViewportView(listTematicas);
							}
							panelSplit1.add(scrollPane1, cc.xywh(4, 16, 5, 2));

							//---- buttonReset ----
							buttonReset.setText(recursos.getString("listbuttonReset"));
							buttonReset.setIcon(new ImageIcon(getClass().getResource("/imagenes/reset.png")));
							buttonReset.setToolTipText(recursos.getString("listbuttonResetTooltip"));
							buttonReset.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									buttonResetActionPerformed();
								}
							});
							panelSplit1.add(buttonReset, cc.xy(4, 19));

							//---- buttonFiltrar ----
							buttonFiltrar.setText(recursos.getString("listbuttonFiltrar"));
							buttonFiltrar.setIcon(new ImageIcon(getClass().getResource("/imagenes/zoom.png")));
							buttonFiltrar.setIconTextGap(8);
							buttonFiltrar.setToolTipText(recursos.getString("listbuttonFiltrarTooltip"));
							buttonFiltrar.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									buttonFiltrarActionPerformed();
								}
							});
							panelSplit1.add(buttonFiltrar, cc.xy(8, 19));

							//---- buttonTodas ----
							buttonTodas.setText(recursos.getString("listbuttonTodas"));
							buttonTodas.setToolTipText(recursos.getString("listbuttonTodasTooltip"));
							buttonTodas.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									buttonTodasActionPerformed();
								}
							});
							panelSplit1.add(buttonTodas, cc.xywh(4, 21, 5, 1));
							panelSplit1.add(separator2, cc.xywh(1, 23, 9, 1));
						}
						splitListado.setTopComponent(panelSplit1);

						//======== panelSplit2 ========
						{
							panelSplit2.setLayout(new FormLayout(
								"default:grow",
								"top:2dlu, fill:max(default;100dlu):grow"));

							//======== scrollPane14 ========
							{

								//---- listCuestiones ----
								listCuestiones.setHighlighters(pipeline);
								listCuestiones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
								listCuestiones.addListSelectionListener(new ListSelectionListener() {
									public void valueChanged(ListSelectionEvent e) {
										listCuestionesValueChanged(e);
									}
								});
								scrollPane14.setViewportView(listCuestiones);
							}
							panelSplit2.add(scrollPane14, cc.xy(1, 2));
						}
						splitListado.setBottomComponent(panelSplit2);
					}
					panelListado.add(splitListado, cc.xy(1, 2));
				}
				sifListadoContentPane.add(panelListado, cc.xy(1, 1));
			}
			splitCatalogo.setLeftComponent(sifListado);
		}
		add(splitCatalogo, cc.xy(1, 1));

		//---- pipeline ----
		pipeline.addHighlighter(new AlternateRowHighlighter());
		// End of component initialization
    }
    
	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private UIFSplitPane splitCatalogo;
	private SimpleInternalFrame sifListado;
	private JPanel panelListado;
	private UIFSplitPane splitListado;
	private JPanel panelSplit1;
	private JLabel label6;
	private JTextField textTitulo;
	private JLabel label7;
	private JTextField textAutor;
	private JLabel label8;
	private JComboBox comboEstado;
    private JLabel label14;
    private JTextField textId;
	private JLabel label10;
	private JScrollPane scrollPane2;
	private JXList listCategorias;
	private JLabel label9;
	private JScrollPane scrollPane3;
	private JXList listEsquemas;
	private JLabel label11;
    private JLabel label12;
    private JLabel label13;
    private JSpinner spinnerDificultad1;
    private JSpinner spinnerDificultad2;
	private JScrollPane scrollPane1;
	private JXList listTematicas;
	private JButton buttonReset;
	private JButton buttonFiltrar;
	private JButton buttonTodas;
	private JSeparator separator2;
	private JPanel panelSplit2;
	private JScrollPane scrollPane14;
	private JXList listCuestiones;
	private HighlighterPipeline pipeline;

} // Fin clase PanelCatalogo
