//////////////////////////////////////////////////////
//  CLASSE: DialogHtml.java                         //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import javax.swing.*;
import java.util.*; //pel resourcebundle...
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import java.awt.event.*;

public class DialogHtml extends JDialog {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    ResourceBundle recursos;

    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/

	public DialogHtml(Frame owner, ResourceBundle rec) {
		super(owner, true);
        recursos = rec;
		initComponents();
	}

	public DialogHtml(Dialog owner, ResourceBundle rec) {
		super(owner, true);
        recursos = rec;
		initComponents();
	}

    public void setTextEnunciado(String enunciat){
        areaenunciado.setText(enunciat);
    }

    private void buttonAceptarActionPerformed(){
        this.setVisible(false);
    }

	private void initComponents() {

        panel1 = new JPanel();
        scrollPane1 = new JScrollPane();
		areaenunciado = new JEditorPane();
        buttonAceptar = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle(recursos.getString("dhttitle"));
		setResizable(true);
		setLocationByPlatform(true);
		setModal(true);
		Container contentPane = getContentPane();
        contentPane.setLayout(new FormLayout(
			"400px:grow",
			"fill:380px:grow"));

        //======== panel1 ========
		{
			panel1.setLayout(new FormLayout(
                new ColumnSpec[] {
                    FormFactory.UNRELATED_GAP_COLSPEC,
                    new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                    FormFactory.DEFAULT_COLSPEC,
                    FormFactory.UNRELATED_GAP_COLSPEC
                    },
                new RowSpec[] {
                    FormFactory.UNRELATED_GAP_ROWSPEC,
                    new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.UNRELATED_GAP_ROWSPEC
            }));

            //======== scrollPane1 ========
            {
                //---- areaenunciado ----
                areaenunciado.setContentType("text/html");
                areaenunciado.setText("");
                areaenunciado.setEditable(false);
                scrollPane1.setViewportView(areaenunciado);
            }
            panel1.add(scrollPane1, cc.xywh(2, 2, 2, 1));

            //---- buttonAceptar ----
            buttonAceptar.setText(recursos.getString("dhtbuttonAceptar"));
            buttonAceptar.setIcon(new ImageIcon(getClass().getResource("/imagenes/tick.png")));
            buttonAceptar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    buttonAceptarActionPerformed();
                }
            });
            panel1.add(buttonAceptar, cc.xy(3, 4));
        }
        contentPane.add(panel1, cc.xy(1, 1));
		pack();
		setLocationRelativeTo(getOwner());
		// End of component initialization
	}

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

    private JPanel panel1;
    private JScrollPane scrollPane1;
	private JEditorPane areaenunciado;
    private JButton buttonAceptar;

} // Fin clase DialogHtml
