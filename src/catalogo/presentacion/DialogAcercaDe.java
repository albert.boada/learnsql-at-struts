//////////////////////////////////////////////////////
//  CLASSE: DialogAcercaDe.java                     //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import java.awt.*;
import javax.swing.*;
import java.util.*; //pel resourcebundle...
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

public class DialogAcercaDe extends JDialog {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    ResourceBundle recursos;

    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/

	public DialogAcercaDe(Frame owner, ResourceBundle rec) {
		super(owner, true);
        recursos = rec;
		initComponents();
	}

	public DialogAcercaDe(Dialog owner, ResourceBundle rec) {
		super(owner, true);
        recursos = rec;
		initComponents();
	}
        
    private void inicializa() {
        setSize(500, 400);
        setLocationRelativeTo(null);
    }

	private void initComponents() {
		// Component initialization
		label3 = new JLabel();
		label4 = new JLabel();
		panel1 = new JPanel();
		label5 = new JLabel();
		separator2 = new JSeparator();
		textArea1 = new JTextArea();
		separator3 = new JSeparator();
		label6 = new JLabel();
		label7 = new JLabel();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle(recursos.getString("dadtitle"));
		setResizable(false);
		setLocationByPlatform(true);
		setModal(true);
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			"250px, 250px",
			"fill:240px, fill:140px"));

		//---- label3 ----
		label3.setIcon(new ImageIcon(getClass().getResource("/imagenes/splashscreen.png")));
		label3.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(label3, cc.xywh(1, 1, 2, 1));

		//---- label4 ----
		label4.setIcon(new ImageIcon(getClass().getResource("/imagenes/splashcorner.png")));
		contentPane.add(label4, cc.xy(1, 2));

		//======== panel1 ========
		{
			panel1.setBackground(new Color(87, 146, 255));
			panel1.setLayout(new FormLayout(
				new ColumnSpec[] {
					FormFactory.UNRELATED_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
					FormFactory.DEFAULT_COLSPEC,
					FormFactory.UNRELATED_GAP_COLSPEC
				},
				new RowSpec[] {
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					new RowSpec(RowSpec.TOP, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
					FormFactory.UNRELATED_GAP_ROWSPEC
				}));

			//---- label5 ----
			label5.setText(recursos.getString("dadlabel05"));
			label5.setForeground(Color.white);
			label5.setFont(new Font("Tahoma", Font.BOLD, 11));
			panel1.add(label5, cc.xywh(2, 1, 2, 1));
			panel1.add(separator2, cc.xywh(2, 3, 2, 1));

			//---- textArea1 ----
			textArea1.setWrapStyleWord(true);
			textArea1.setLineWrap(true);
			textArea1.setText(recursos.getString("dadtextArea1"));
			textArea1.setForeground(Color.white);
			textArea1.setBorder(null);
			textArea1.setBackground(new Color(87, 146, 255));
			textArea1.setEditable(false);
			panel1.add(textArea1, cc.xywh(2, 5, 2, 1));
			panel1.add(separator3, cc.xywh(2, 7, 2, 1));

			//---- label6 ----
			label6.setForeground(Color.white);
			label6.setText(recursos.getString("dadlabel06"));
			panel1.add(label6, cc.xy(2, 9));

			//---- label7 ----
			label7.setText(recursos.getString("dadlabel07"));
			label7.setHorizontalAlignment(SwingConstants.RIGHT);
			label7.setForeground(Color.white);
			panel1.add(label7, cc.xy(3, 9));
		}
		contentPane.add(panel1, cc.xy(2, 2));
		pack();
		setLocationRelativeTo(getOwner());
		// End of component initialization
	}

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private JLabel label3;
	private JLabel label4;
	private JPanel panel1;
	private JLabel label5;
	private JSeparator separator2;
	private JTextArea textArea1;
	private JSeparator separator3;
	private JLabel label6;
	private JLabel label7;

} // Fin clase DialogAcercaDe
