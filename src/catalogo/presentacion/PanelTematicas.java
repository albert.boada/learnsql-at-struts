//////////////////////////////////////////////////////
//  CLASSE: PanelTematicas.java                     //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import java.awt.event.*;
import catalogo.global.CtrlVista;
import catalogo.soporte.modelos.TableModelTematicas;
import catalogo.soporte.modelos.ArrayListModel;
import catalogo.modelo.*;
import catalogo.soporte.utils.TextParser;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.*;
import com.jgoodies.uif_lite.panel.SimpleInternalFrame;
import java.awt.Frame;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;
import java.util.Collections;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.AlternateRowHighlighter;
import org.jdesktop.swingx.decorator.HighlighterPipeline;

public class PanelTematicas extends JPanel{
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private DiccCategorias categorias;
    private DiccTematicas tematicas;
    private TableModelTematicas tableModel;
    private Mensaje mensaje;
    private DialogCategorias dialogCategorias;
    private ArrayListModel<Categoria> listacats;
    ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public PanelTematicas(DiccTematicas tematicas, DiccCategorias categorias) {
        initComponents();
        inicializa(tematicas, categorias);
    }
    
    public void setFoco() {
        selectionModel.removeSelectionInterval(0, tableModel.getRowCount() - 1);
        textNombre.requestFocus();
    }

    public void cambioIdioma(ResourceBundle recursos){
        sifTematicas.setTitle(recursos.getString("sifTematicas"));
        label.setText(recursos.getString("temlabel"));
        label1.setText(recursos.getString("temlabel1"));
        label2.setText(recursos.getString("temlabel2"));
        buttonCateg.setText(recursos.getString("tembuttonCateg"));
        buttonCateg.setToolTipText(recursos.getString("tembuttonCategTooltip"));
        buttonAdd.setText(recursos.getString("tembuttonAdd"));
        buttonMod.setText(recursos.getString("tembuttonMod"));
        buttonDel.setText(recursos.getString("tembuttonDel"));

        tableModel.setColumnName(recursos);
        tableTematicas.setModel(tableModel);
        tableTematicas.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableTematicas.getColumnModel().getColumn(1).setPreferredWidth(800);
        tableTematicas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        dialogCategorias.setRecursos(recursos);

        this.recursos = recursos;
    }
    
    private void inicializa(DiccTematicas tematicas, DiccCategorias diccategorias) {
        this.tematicas = tematicas;
        this.categorias = diccategorias;
        mensaje = new Mensaje();
        listacats = new ArrayListModel<Categoria>();
        dialogCategorias = new DialogCategorias((Frame) getTopLevelAncestor(), diccategorias, recursos);
        tableModel = new TableModelTematicas(tematicas.getAll());
        tableTematicas.setModel(tableModel);
        tableTematicas.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableTematicas.getColumnModel().getColumn(1).setPreferredWidth(800);
        tableTematicas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        textNombre.addMouseListener(CtrlVista.contextMouseAdapter);
        textDescripcion.addMouseListener(CtrlVista.contextMouseAdapter);
        selectionModelValueChanged();
    }

    private void buttonCategoriasActionPerformed() {
        dialogCategorias.setLocationRelativeTo(getTopLevelAncestor());
        dialogCategorias.setSeleccion(listacats);
        dialogCategorias.firePropertyChange("visible", 0, 1);
        dialogCategorias.setVisible(true);
        List<Categoria> seleccion = dialogCategorias.getSeleccion();
        //Collections.sort(seleccion);
        try{
            if (dialogCategorias.haAceptado() && !seleccion.equals(listacats)) {
                setLista(seleccion);
                listCategorias.setModel(listacats);
                textKeyReleased();
            }
        }catch(Exception e){
            mensaje.mensajeError(this, recursos.getString("temaviso1"), e, recursos);
        }
    }

    private void buttonAddActionPerformed() {
        textNombre.setText(TextParser.parseUTF8(textNombre.getText().trim()));
        textDescripcion.setText(TextParser.parseUTF8(textDescripcion.getText().trim()));
        try {
            Tematica tem = tematicas.add(textNombre.getText(), textDescripcion.getText(), listacats);
//            tableModel.fireTableDataChanged();
            tableModel.fireTableRowsUpdated(0, tableModel.getRowCount());
            selectionModel.setSelectionInterval(tableModel.getIndex(tem), tableModel.getIndex(tem));
        } catch (Exception e) {
            mensaje.mensajeError(this, recursos.getString("temaviso2"), e, recursos);
        }
    }
    
    private void buttonModActionPerformed() {
        textNombre.setText(TextParser.parseUTF8(textNombre.getText().trim()));
        textDescripcion.setText(TextParser.parseUTF8(textDescripcion.getText().trim()));
        Tematica tem = (Tematica) tableModel.getValueAt(tableTematicas.convertRowIndexToModel(tableTematicas.getSelectedRow()));
        if (!textNombre.getText().equals(tem.getNombre()) || !textDescripcion.getText().equals(tem.getDescripcion())
                || !listacats.equals(tem.getCategorias())) {
            if (mensaje.mensajeConfirmacion(this, recursos.getString("temaviso3") + tem.getNombre() + "\"?", recursos) == 0) {
                try {
                    tematicas.set(tem, textNombre.getText(), textDescripcion.getText(), listacats);
                    tableModel.fireTableRowsUpdated(0, tableModel.getRowCount());
//                    tableModel.fireTableDataChanged();
                    selectionModel.setSelectionInterval(tableModel.getIndex(tem), tableModel.getIndex(tem));
                } catch (Exception e) {
                    mensaje.mensajeError(this, recursos.getString("temaviso4"), e, recursos);
                }
            }
        }
    }
    
    private void buttonDelActionPerformed() {
        int rowIndex = tableTematicas.getSelectedRow();
        Tematica tem = (Tematica) tableModel.getValueAt(rowIndex);
        if (mensaje.mensajeConfirmacion(this, recursos.getString("temaviso5") + tem.getNombre() + "\"?", recursos) == 0) {
            try {
                if (CtrlVista.tematicaEnUso(tem)) throw new Exception(recursos.getString("exceptem"));
                tableModel.fireTableRowsUpdated(0, tableModel.getRowCount());
                tematicas.remove(tem);
//                tableModel.fireTableDataChanged();
                int total = tableTematicas.getRowCount();
                if (total > 0) {
                    if (rowIndex < total) selectionModel.setSelectionInterval(rowIndex, rowIndex);
                    else selectionModel.setSelectionInterval(total - 1, total - 1);
                }
            } catch (Exception e) {
                mensaje.mensajeError(this, recursos.getString("temaviso6"), e, recursos);
            }
        }
    }

    private void setLista(List<Categoria> lista) {
        if (!listacats.isEmpty()) listacats.clear();
        listacats.addAll(lista);
    }
    
    private void selectionModelValueChanged() {
        if (tableTematicas.getSelectedRow() == -1) {
            textNombre.setText("");
            textDescripcion.setText("");
            listacats.clear();
            listCategorias.setModel(listacats);
            buttonDel.setEnabled(false);
            buttonMod.setEnabled(false);
            buttonAdd.setEnabled(false);
        } else {
            Tematica tem = (Tematica) tableModel.getValueAt(tableTematicas.getSelectedRow());
            textNombre.setText(tem.getNombre());
            textDescripcion.setText(tem.getDescripcion());
            setLista(tem.getCategorias());
            listCategorias.setModel(listacats);
            buttonDel.setEnabled(true);
            buttonMod.setEnabled(true);
            buttonAdd.setEnabled(true);
        }
    }
    
    private void textKeyReleased() {
        if (textNombre.getText().isEmpty() || textDescripcion.getText().isEmpty() || listacats.isEmpty()) {
            buttonAdd.setEnabled(false);
            buttonMod.setEnabled(false);
        } else {
            buttonAdd.setEnabled(true);
            if (tableTematicas.getSelectedRow() != -1) buttonMod.setEnabled(true);
        }
    }
    
    private void initComponents() {
		// Component initialization
		sifTematicas = new SimpleInternalFrame();
		scrollPane1 = new JScrollPane();
        scrollPane2 = new JScrollPane();
        label = new JLabel();
        listCategorias = new JXList();
		tableTematicas = new JXTable() {
		    public String getToolTipText(MouseEvent e) {
				java.awt.Point p = e.getPoint();
		        return (String) getValueAt(rowAtPoint(p), columnAtPoint(p));
		    }
		};
		label1 = new JLabel();
		textNombre = new JTextField();
		label2 = new JLabel();
		textDescripcion = new JTextField();
		buttonCateg = new JButton();
        buttonAdd = new JButton();
		buttonMod = new JButton();
		buttonDel = new JButton();
		selectionModel = (DefaultListSelectionModel) tableTematicas.getSelectionModel();
		pipeline = new HighlighterPipeline();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setLayout(new FormLayout(
			"default:grow",
			"fill:default:grow"));

		//======== sifTematicas ========
		{
			sifTematicas.setTitle(recursos.getString("sifTematicas"));
			sifTematicas.setFrameIcon(new ImageIcon(getClass().getResource("/imagenes/panel_tematicas.png")));
			Container sifTematicasContentPane = sifTematicas.getContentPane();
			sifTematicasContentPane.setLayout(new FormLayout(
				new ColumnSpec[] {
					FormFactory.UNRELATED_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                    new ColumnSpec(ColumnSpec.FILL, Sizes.dluX(150), FormSpec.NO_GROW),
                    FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                    new ColumnSpec(Sizes.dluX(85)),
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.6),
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.4),
					new ColumnSpec(Sizes.dluX(65)),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(Sizes.dluX(65)),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(Sizes.dluX(65)),
					FormFactory.UNRELATED_GAP_COLSPEC
				},
				new RowSpec[] {
					FormFactory.UNRELATED_GAP_ROWSPEC,
					new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                    FormFactory.LINE_GAP_ROWSPEC,
					new RowSpec(RowSpec.TOP, Sizes.dluY(60), FormSpec.NO_GROW),
					FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.UNRELATED_GAP_ROWSPEC
				}));

			//======== scrollPane1 ========
			{
				scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

				//---- tableTematicas ----
				tableTematicas.setSortable(false);
				tableTematicas.setHighlighters(pipeline);
				scrollPane1.setViewportView(tableTematicas);
			}
			sifTematicasContentPane.add(scrollPane1, cc.xywh(2, 2, 12, 1));

            //---- label ----
            label.setText(recursos.getString("temlabel"));
            sifTematicasContentPane.add(label, cc.xy(2, 4));

            //======== scrollPane2 ========
            {
                //---- listCategorias ----
                listCategorias.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                listCategorias.setHighlighters(pipeline);
                listCategorias.setFocusable(false);
                scrollPane2.setViewportView(listCategorias);
            }
            sifTematicasContentPane.add(scrollPane2, cc.xywh(4, 4, 1, 2));

            //---- buttonCategorias ----
            buttonCateg.setText(recursos.getString("tembuttonCateg"));
            buttonCateg.setIcon(new ImageIcon(getClass().getResource("/imagenes/editlist.png")));
            buttonCateg.setIconTextGap(8);
            buttonCateg.setToolTipText(recursos.getString("tembuttonCategTooltip"));
            buttonCateg.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    buttonCategoriasActionPerformed();
                }
            });
            sifTematicasContentPane.add(buttonCateg, cc.xy(6, 5));

			//---- label1 ----
			label1.setText(recursos.getString("temlabel1"));
			sifTematicasContentPane.add(label1, cc.xy(2, 7));

			//---- textNombre ----
			textNombre.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			sifTematicasContentPane.add(textNombre, cc.xywh(4, 7, 4, 1));

			//---- label2 ----
			label2.setText(recursos.getString("temlabel2"));
			sifTematicasContentPane.add(label2, cc.xy(2, 9));

			//---- textDescripcion ----
			textDescripcion.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			sifTematicasContentPane.add(textDescripcion, cc.xywh(4, 9, 10, 1));

			//---- buttonAdd ----
			buttonAdd.setText(recursos.getString("tembuttonAdd"));
			buttonAdd.setIcon(new ImageIcon(getClass().getResource("/imagenes/add.png")));
			buttonAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonAddActionPerformed();
				}
			});
			sifTematicasContentPane.add(buttonAdd, cc.xy(9, 11));

			//---- buttonMod ----
			buttonMod.setText(recursos.getString("tembuttonMod"));
			buttonMod.setIcon(new ImageIcon(getClass().getResource("/imagenes/edit.png")));
			buttonMod.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonModActionPerformed();
				}
			});
			sifTematicasContentPane.add(buttonMod, cc.xy(11, 11));

			//---- buttonDel ----
			buttonDel.setText(recursos.getString("tembuttonDel"));
			buttonDel.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
			buttonDel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonDelActionPerformed();
				}
			});
			sifTematicasContentPane.add(buttonDel, cc.xy(13, 11));
		}
		add(sifTematicas, cc.xy(1, 1));

		//---- selectionModel ----
		selectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				selectionModelValueChanged();
			}
		});

		//---- pipeline ----
		pipeline.addHighlighter(new AlternateRowHighlighter());
		// End of component initialization
    }

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private SimpleInternalFrame sifTematicas;
	private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;
    private JLabel label;
    private JXList listCategorias;
	private JXTable tableTematicas;
	private JLabel label1;
	private JTextField textNombre;
	private JLabel label2;
	private JTextField textDescripcion;
    private JButton buttonCateg;
	private JButton buttonAdd;
	private JButton buttonMod;
	private JButton buttonDel;
	private DefaultListSelectionModel selectionModel;
	private HighlighterPipeline pipeline;

} // Fin clase PanelTematicas
