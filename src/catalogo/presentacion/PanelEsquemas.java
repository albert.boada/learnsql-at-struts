//////////////////////////////////////////////////////
//  CLASSE: PanelEsquemas.java                      //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import catalogo.global.CtrlVista;
import catalogo.modelo.Esquema;
import catalogo.soporte.modelos.TableModelEsquemas;
import catalogo.modelo.DiccEsquemas;
import catalogo.soporte.utils.TextParser;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import com.jgoodies.uif_lite.panel.*;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.AlternateRowHighlighter;
import org.jdesktop.swingx.decorator.HighlighterPipeline;

public class PanelEsquemas extends JPanel {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private DiccEsquemas esquemas;
    private TableModelEsquemas tableModel;
    private Mensaje mensaje;
    private ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public PanelEsquemas(DiccEsquemas esquemas) {
        initComponents();
        inicializa(esquemas);
    }
    
    public void setFoco() {
        selectionModel.removeSelectionInterval(0, tableModel.getRowCount() - 1);
        textNombre.requestFocus();
    }

    public void cambioIdioma(ResourceBundle recursos){
        sifEsquemas.setTitle(recursos.getString("sifEsquemas"));
        label1.setText(recursos.getString("esqlabel1"));
        label2.setText(recursos.getString("esqlabel2"));
        buttonAdd.setText(recursos.getString("esqbuttonAdd"));
        buttonMod.setText(recursos.getString("esqbuttonMod"));
        buttonDel.setText(recursos.getString("esqbuttonDel"));

        tableModel.setColumnName(recursos);
        tableEsquemas.setModel(tableModel);
        tableEsquemas.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableEsquemas.getColumnModel().getColumn(1).setPreferredWidth(800);
        tableEsquemas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this.recursos = recursos;
    }
    
    private void inicializa(DiccEsquemas esquemas) {
        this.esquemas = esquemas;
        mensaje = new Mensaje();
        tableModel = new TableModelEsquemas(esquemas.getAll());
        tableEsquemas.setModel(tableModel);
        tableEsquemas.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableEsquemas.getColumnModel().getColumn(1).setPreferredWidth(800);
        tableEsquemas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        textNombre.addMouseListener(CtrlVista.contextMouseAdapter);;
        textDescripcion.addMouseListener(CtrlVista.contextMouseAdapter);
        selectionModelValueChanged();
    }
    
    private void buttonAddActionPerformed() {
        textNombre.setText(TextParser.parseUTF8(textNombre.getText().trim()));
        textDescripcion.setText(TextParser.parseUTF8(textDescripcion.getText().trim()));
        try {
            Esquema esq = esquemas.add(textNombre.getText(), textDescripcion.getText());
            tableModel.fireTableDataChanged();
            selectionModel.setSelectionInterval(tableModel.getIndex(esq), tableModel.getIndex(esq));
        } catch (Exception e) {
            mensaje.mensajeError(this, recursos.getString("esqaviso1"), e, recursos);
        }
    }
    
    private void buttonModActionPerformed() {
        textNombre.setText(TextParser.parseUTF8(textNombre.getText().trim()));
        textDescripcion.setText(TextParser.parseUTF8(textDescripcion.getText().trim()));
        Esquema esq = (Esquema) tableModel.getValueAt(tableEsquemas.convertRowIndexToModel(tableEsquemas.getSelectedRow()));
        if (!textNombre.getText().equals(esq.getNombre()) || !textDescripcion.getText().equals(esq.getDescripcion())) {
            if (mensaje.mensajeConfirmacion(this, recursos.getString("esqaviso2") + esq.getNombre() + "\"?", recursos) == 0) {
                try {
                    esquemas.set(esq, textNombre.getText(), textDescripcion.getText());
                    tableModel.fireTableDataChanged();
                    selectionModel.setSelectionInterval(tableModel.getIndex(esq), tableModel.getIndex(esq));

                } catch (Exception e) {
                    mensaje.mensajeError(this, recursos.getString("esqaviso3"), e, recursos);
                }
            }
        }
    }
    
    private void buttonDelActionPerformed() {
        int rowIndex = tableEsquemas.getSelectedRow();
        Esquema esq = (Esquema) tableModel.getValueAt(rowIndex);
        if (mensaje.mensajeConfirmacion(this, recursos.getString("esqaviso4") + esq.getNombre() + "\"?", recursos) == 0) {
            try {
                if (CtrlVista.esquemaEnUso(esq)) throw new Exception(recursos.getString("excepesq"));
                esquemas.remove(esq);
                tableModel.fireTableDataChanged();
                int total = tableEsquemas.getRowCount();
                if (total > 0) {
                    if (rowIndex < total) selectionModel.setSelectionInterval(rowIndex, rowIndex);
                    else selectionModel.setSelectionInterval(total - 1, total - 1);
                }
            } catch (Exception e) {
                mensaje.mensajeError(this, recursos.getString("esqaviso5"), e, recursos);
            }
        }
    }
    
    private void selectionModelValueChanged() {
        if (tableEsquemas.getSelectedRow() == -1) {
            textNombre.setText("");
            textDescripcion.setText("");
            buttonDel.setEnabled(false);
            buttonMod.setEnabled(false);
            buttonAdd.setEnabled(false);
        } else {
            Esquema esq = (Esquema) tableModel.getValueAt(tableEsquemas.getSelectedRow());
            textNombre.setText(esq.getNombre());
            textDescripcion.setText(esq.getDescripcion());
            buttonDel.setEnabled(true);
            buttonMod.setEnabled(true);
            buttonAdd.setEnabled(true);
        }
    }
    
    private void textKeyReleased() {
        if (textNombre.getText().isEmpty() || textDescripcion.getText().isEmpty()) {
            buttonAdd.setEnabled(false);
            buttonMod.setEnabled(false);
        } else {
            buttonAdd.setEnabled(true);
            if (tableEsquemas.getSelectedRow() != -1) buttonMod.setEnabled(true);
        }
    }
    
    private void initComponents() {
		// Component initialization

		sifEsquemas = new SimpleInternalFrame();
		scrollPane1 = new JScrollPane();
		tableEsquemas = new JXTable() {
		    public String getToolTipText(MouseEvent e) {
				java.awt.Point p = e.getPoint();
		        return (String) getValueAt(rowAtPoint(p), columnAtPoint(p));
		    }
		};
		label1 = new JLabel();
		textNombre = new JTextField();
		label2 = new JLabel();
		textDescripcion = new JTextField();
		buttonAdd = new JButton();
		buttonMod = new JButton();
		buttonDel = new JButton();
		selectionModel = (DefaultListSelectionModel) tableEsquemas.getSelectionModel();
		pipeline = new HighlighterPipeline();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setLayout(new FormLayout(
			"default:grow",
			"fill:default:grow"));

		//======== sifEsquemas ========
		{
			sifEsquemas.setTitle(recursos.getString("sifEsquemas"));
			sifEsquemas.setFrameIcon(new ImageIcon(getClass().getResource("/imagenes/panel_esquemas.png")));
			Container sifEsquemasContentPane = sifEsquemas.getContentPane();
			sifEsquemasContentPane.setLayout(new FormLayout(
				new ColumnSpec[] {
					FormFactory.UNRELATED_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.6),
					new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, 0.4),
					new ColumnSpec(Sizes.dluX(65)),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(Sizes.dluX(65)),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
					new ColumnSpec(Sizes.dluX(65)),
					FormFactory.UNRELATED_GAP_COLSPEC
				},
				new RowSpec[] {
					FormFactory.UNRELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.UNRELATED_GAP_ROWSPEC
				}));

			//======== scrollPane1 ========
			{
				scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

				//---- tableEsquemas ----
				tableEsquemas.setHighlighters(pipeline);
				tableEsquemas.setSortable(false);
				scrollPane1.setViewportView(tableEsquemas);
			}
			sifEsquemasContentPane.add(scrollPane1, cc.xywh(2, 2, 9, 2));

			//---- label1 ----
			label1.setText(recursos.getString("esqlabel1"));
			sifEsquemasContentPane.add(label1, cc.xy(2, 5));

			//---- textNombre ----
			textNombre.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			sifEsquemasContentPane.add(textNombre, cc.xy(4, 5));

			//---- label2 ----
			label2.setText(recursos.getString("esqlabel2"));
			sifEsquemasContentPane.add(label2, cc.xy(2, 7));

			//---- textDescripcion ----
			textDescripcion.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					textKeyReleased();
				}
			});
			sifEsquemasContentPane.add(textDescripcion, cc.xywh(4, 7, 7, 1));

			//---- buttonAdd ----
			buttonAdd.setText(recursos.getString("esqbuttonAdd"));
			buttonAdd.setIcon(new ImageIcon(getClass().getResource("/imagenes/add.png")));
			buttonAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonAddActionPerformed();
				}
			});
			sifEsquemasContentPane.add(buttonAdd, cc.xy(6, 9));

			//---- buttonMod ----
			buttonMod.setText(recursos.getString("esqbuttonMod"));
			buttonMod.setIcon(new ImageIcon(getClass().getResource("/imagenes/edit.png")));
			buttonMod.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonModActionPerformed();
				}
			});
			sifEsquemasContentPane.add(buttonMod, cc.xy(8, 9));

			//---- buttonDel ----
			buttonDel.setText(recursos.getString("esqbuttonDel"));
			buttonDel.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
			buttonDel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					buttonDelActionPerformed();
				}
			});
			sifEsquemasContentPane.add(buttonDel, cc.xy(10, 9));
		}
		add(sifEsquemas, cc.xy(1, 1));

		//---- selectionModel ----
		selectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				selectionModelValueChanged();
			}
		});

		//---- pipeline ----
		pipeline.addHighlighter(new AlternateRowHighlighter());
		// End of component initialization
    }
    
	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private SimpleInternalFrame sifEsquemas;
	private JScrollPane scrollPane1;
	private JXTable tableEsquemas;
	private JLabel label1;
	private JTextField textNombre;
	private JLabel label2;
	private JTextField textDescripcion;
	private JButton buttonAdd;
	private JButton buttonMod;
	private JButton buttonDel;
	private DefaultListSelectionModel selectionModel;
	private HighlighterPipeline pipeline;
        
} // Fin clase PanelEsquemas
