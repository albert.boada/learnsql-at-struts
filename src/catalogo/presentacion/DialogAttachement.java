//////////////////////////////////////////////////////
//  CLASSE: DialogAttachement.java                  //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////
package catalogo.presentacion;

import catalogo.modelo.JuegoPruebas;
import catalogo.modelo.Comprobacion;
import catalogo.modelo.Cuestion;
import catalogo.soporte.modelos.TableModelJP;
import catalogo.soporte.modelos.TableModelComprobaciones;
import java.util.*; //pel resourcebundle...
import java.io.*;
import java.beans.*;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.AlternateRowHighlighter;
import org.jdesktop.swingx.decorator.HighlighterPipeline;
import java.awt.event.*;
import javax.swing.event.*;


public class DialogAttachement extends JDialog {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    private TableModelJP tableModeljp;
    private TableModelComprobaciones tableModelcom;
    private Cuestion cuestion;
    private Boolean haAceptado;
    private File fichero = new File("attachment.txt");
    private FileFilter filterTxt;
    private Mensaje mensaje;
    ResourceBundle recursos;// = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/
    public DialogAttachement(Frame owner, Cuestion c, ResourceBundle rec) {
        super(owner, true);
        cuestion = c;
        recursos = rec;
        haAceptado = false;
        initComponents();
        inicializa();
    }

    public DialogAttachement(Dialog owner, Cuestion c, ResourceBundle rec) {
        super(owner, true);
        cuestion = c;
        recursos = rec;
        haAceptado = false;
        initComponents();
        inicializa();
    }

    public Boolean haAceptado() {
        return haAceptado;
    }

    public File ficheroGenerado() {
        return fichero;
    }

    private void inicializa() {
        setSize(700, 500);

        mensaje = new Mensaje();
        tableModeljp = new TableModelJP(cuestion.getJuegosPruebas());
        tableModeljp.setColumnName(recursos);
        tableJP.setModel(tableModeljp);
        tableJP.getColumnModel().getColumn(0).setPreferredWidth(130);
        tableJP.getColumnModel().getColumn(1).setPreferredWidth(150);
        tableJP.getColumnModel().getColumn(2).setPreferredWidth(120);
        tableJP.getColumnModel().getColumn(3).setPreferredWidth(120);
        tableJP.getColumnModel().getColumn(4).setPreferredWidth(120);

        tableModelcom = new TableModelComprobaciones(cuestion.getJuegosPruebas().EMPTY_LIST);
        tableModelcom.setColumnName(recursos);
        tableComp.setModel(tableModelcom);
        tableComp.getColumnModel().getColumn(0).setPreferredWidth(180);
        tableComp.getColumnModel().getColumn(1).setPreferredWidth(200);
        tableComp.getColumnModel().getColumn(2).setPreferredWidth(200);

        checkinits.setSelected(true);
        checklimpieza.setSelected(true);
    }

    private String calculaCaracteres(String text, String var) {
        String guiones;
        int num = text.length() + var.length();

        guiones = "--";
        for (int i = 0; i < num; i++) {
            guiones = guiones + "-";
        }
        guiones = guiones + "--\n";

        return guiones;
    }

    private void selectionModelValueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            int rowIndex = tableJP.getSelectedRow();
            if (rowIndex != -1) {
                JuegoPruebas jp = (JuegoPruebas) ((TableModelJP) tableJP.getModel()).getValueAt(tableJP.convertRowIndexToModel(rowIndex));
                tableModelcom.setModel(jp.getComprobaciones());
                tableComp.setModel(tableModelcom);
                tableModeljp.setEntradaComp(tableModelcom.getEntrada());
                tableModeljp.setSalidaComp(tableModelcom.getSalida());
            }
        }
    }

    private void buttonAceptarActionPerformed(ActionEvent e) {

        String inits = recursos.getString("inits");
        String postinits = recursos.getString("postinits");
        String limpieza = recursos.getString("limpieza");
        String tjp = recursos.getString("tjp");
        String jpinits = recursos.getString("jpinits");
        String jpentrada = recursos.getString("jpentrada");
        String jpresultado = recursos.getString("jpresultado");
        String jplimpieza = recursos.getString("jplimpieza");
        String tcomp = recursos.getString("tcomp");
        String compentrada = recursos.getString("compentrada");
        String compsalida = recursos.getString("compsalida");

        Boolean hayjpinits =true;
        Boolean hayjpentrada =true;
        Boolean hayjpresultado =true;
        Boolean hayjplimpieza =true;
        Boolean haycompentrada =true;
        Boolean haycompsalida =true;

        if (e.getSource() == buttonGuardar) {

            filterTxt = new FileNameExtensionFilter("SQL/Texto (*.sql, *.txt)", "sql", "txt");
            fileChooserguardar.setFileFilter(filterTxt);
            fileChooserguardar.setSelectedFile(fichero);
            int respuesta = fileChooserguardar.showSaveDialog(this);
            if (respuesta == JFileChooser.APPROVE_OPTION) {
                if(fichero.exists()){
                    int response= mensaje.mensajePregunta(this,recursos.getString("dataviso2"), recursos);
                    if(response==0) fichero = fileChooserguardar.getSelectedFile();
                    else return;
                }
                else fichero = fileChooserguardar.getSelectedFile();
            }

        }

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fichero.getAbsolutePath()));

            if (checkinits.isSelected()) {
                bw.write(inits + "\n" + cuestion.getInits() + "\n\n");
            }

            if (checkpostinits.isSelected()) {
                bw.write(postinits + "\n" + cuestion.getPostInits() + "\n\n");
            }

            if (checklimpieza.isSelected()) {
                bw.write(limpieza + "\n" + cuestion.getLimpieza() + "\n\n");
            }

            //Escribimos en el buffer los juegos de pruebas
            for (JuegoPruebas todos : cuestion.getJuegosPruebas()) {
                if(tableModeljp.getInits().contains(todos)) hayjpinits = true;
                else hayjpinits = false;

                if(tableModeljp.getEntrada().contains(todos)) hayjpentrada = true;
                else hayjpentrada = false;

                if(tableModeljp.getResultado().contains(todos)) hayjpresultado = true;
                else hayjpresultado = false;

                if(tableModeljp.getLimpieza().contains(todos)) hayjplimpieza = true;
                else hayjplimpieza = false;

                if(hayjpinits || hayjpentrada || hayjpresultado || hayjplimpieza){
                    bw.write(calculaCaracteres(tjp, todos.getNombre()));
                    bw.write(tjp + " " + todos.getNombre() + "\n");
                    bw.write(calculaCaracteres(tjp, todos.getNombre()) + "\n");
                }

                if (hayjpinits) {
                    bw.write(jpinits + "\n");
                    for (JuegoPruebas jp : tableModeljp.getInits()) {
                        if (todos.getNombre().equals(jp.getNombre())) {
                            bw.write(jp.getInits() + "\n");
                        }
                    }
                    bw.write("\n");
                }
                if (hayjpentrada) {
                    bw.write(jpentrada + "\n");
                    for (JuegoPruebas jp : tableModeljp.getEntrada()) {
                        if (todos.getNombre().equals(jp.getNombre())) {
                            bw.write(jp.getEntrada() + "\n");
                        }
                    }
                    bw.write("\n");
                }

                if (hayjpresultado) {
                    bw.write(jpresultado + "\n");
                    for (JuegoPruebas jp : tableModeljp.getResultado()) {
                        if (todos.getNombre().equals(jp.getNombre())) {
                            bw.write(jp.getSalida() + "\n");
                        }
                    }
                    bw.write("\n");
                }
                if (hayjplimpieza) {
                    bw.write(jplimpieza + "\n");
                    for (JuegoPruebas jp : tableModeljp.getLimpieza()) {
                        if (todos.getNombre().equals(jp.getNombre())) {
                            bw.write(jp.getLimpieza() + "\n");
                        }
                    }
                    bw.write("\n");
                }
                bw.write("\n");

                //Escribimos en el buffer las comprobaciones
                for (Comprobacion all : todos.getComprobaciones()) {

                    if(tableModelcom.getEntrada().contains(all)) haycompentrada = true;
                    else haycompentrada = false;

                    if(tableModelcom.getSalida().contains(all)) haycompsalida = true;
                    else haycompsalida = false;

                    if(haycompentrada || haycompsalida){
                        if(!hayjpinits && !hayjpentrada && !hayjpresultado && !hayjplimpieza){
                            bw.write(calculaCaracteres(tjp, todos.getNombre()));
                            bw.write(tjp + " " + todos.getNombre() + "\n");
                            bw.write(calculaCaracteres(tjp, todos.getNombre()) + "\n");
                        }
                        bw.write(calculaCaracteres(tcomp, all.getNombre()));
                        bw.write(tcomp + " " + all.getNombre() + "\n");
                        bw.write(calculaCaracteres(tcomp, all.getNombre()) + "\n");
                    }

                    if (haycompentrada) {
                        bw.write(compentrada + "\n");
                        for (Comprobacion com : tableModelcom.getEntrada()) {
                            if (all.getNombre().equals(com.getNombre())) {
                                bw.write(com.getEntrada() + "\n");
                            }
                        }
                        bw.write("\n");
                    }

                    if (haycompsalida) {
                        bw.write(compsalida + "\n");
                        for (Comprobacion com : tableModelcom.getSalida()) {
                            if (all.getNombre().equals(com.getNombre())) {
                                bw.write(com.getSalida() + "\n");
                            }
                        }
                        bw.write("\n");
                    }
                    bw.write("\n");
                }
                bw.write("\n");
            }

            // Hay que cerrar el fichero
            bw.close();

            if (e.getSource() == buttonAceptar) {
                haAceptado = true;
                mensaje.mensajeInformacion(this, recursos.getString("dataviso"), recursos);
                dispose();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void buttonCancelarActionPerformed() {
        dispose();
    }

    private void thisPropertyChange() {
//        aceptado = false;
    }

    private void initComponents() {
        // Component initialization
        DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
        fileChooserguardar = new JFileChooser();
        scrollPane1 = new JScrollPane();
        scrollPane2 = new JScrollPane();
        separator1 = compFactory.createSeparator(recursos.getString("datseparator1"));
        separator2 = compFactory.createSeparator(recursos.getString("datseparator2"));
        separator3 = compFactory.createSeparator(recursos.getString("datseparator3"));
        tableJP = new JXTable() {

            public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColIndex = convertColumnIndexToModel(colIndex);
                if (realColIndex != 0) {
                    tip = ((Boolean) getValueAt(rowIndex, colIndex)) ? recursos.getString("seleccionado") : recursos.getString("noseleccionado");
                } else {
                    tip = (String) getValueAt(rowIndex, colIndex);
                }
                return tip;
            }
        };
        tableComp = new JXTable() {

            public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColIndex = convertColumnIndexToModel(colIndex);
                if (realColIndex != 0) {
                    tip = ((Boolean) getValueAt(rowIndex, colIndex)) ? recursos.getString("seleccionado") : recursos.getString("noseleccionado");
                } else {
                    tip = (String) getValueAt(rowIndex, colIndex);
                }
                return tip;
            }
        };
        labelinits = new JLabel();
        labelpostinits = new JLabel();
        labellimpieza = new JLabel();
        checkinits = new JCheckBox();
        checkpostinits = new JCheckBox();
        checklimpieza = new JCheckBox();
        buttonAceptar = new JButton();
        buttonGuardar = new JButton();
        buttonCancelar = new JButton();
        selectionModelJP = (DefaultListSelectionModel) tableJP.getSelectionModel();
        pipeline = new HighlighterPipeline();
        CellConstraints cc = new CellConstraints();

        //======== this ========
        setTitle(recursos.getString("dattitle"));
        addPropertyChangeListener("visible", new PropertyChangeListener() {

            public void propertyChange(PropertyChangeEvent e) {
                thisPropertyChange();
            }
        });
        Container contentPane = getContentPane();
        contentPane.setLayout(new FormLayout(
                new ColumnSpec[]{
                    FormFactory.UNRELATED_GAP_COLSPEC,
//                    				
//                    new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(60), FormSpec.NO_GROW),
                    FormFactory.DEFAULT_COLSPEC,
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
//                    new ColumnSpec(ColumnSpec.LEFT, Sizes.dluX(50), FormSpec.NO_GROW),
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
//                    new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(80), FormSpec.NO_GROW),
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
//                    new ColumnSpec(ColumnSpec.LEFT, Sizes.dluX(50), FormSpec.NO_GROW),
                    new ColumnSpec("max(default;10dlu):grow"),
                    new ColumnSpec(Sizes.dluX(60)),
                    FormFactory.RELATED_GAP_COLSPEC,
                    new ColumnSpec(Sizes.dluX(60)),
                    FormFactory.RELATED_GAP_COLSPEC,
                    new ColumnSpec(Sizes.dluX(60)),
                    FormFactory.UNRELATED_GAP_COLSPEC
                },
                new RowSpec[]{
                    FormFactory.UNRELATED_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                    FormFactory.RELATED_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.UNRELATED_GAP_ROWSPEC
                }));

        contentPane.add(separator1, cc.xywh(2, 2, 17, 1));

        //---- labelinits ----
        labelinits.setText(recursos.getString("datlabelinits"));
        contentPane.add(labelinits, cc.xy(2, 4));

        //---- checkinits ----
        checkinits.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
//                checkBoxComActionPerformed(e);
            }
        });
        contentPane.add(checkinits, cc.xy(4, 4));

        //---- labelpostinits ----
        labelpostinits.setText(recursos.getString("datlabelpostinits"));
        contentPane.add(labelpostinits, cc.xy(6, 4));

        //---- checkpostinits ----
        checkpostinits.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
//                checkBoxComActionPerformed(e);
            }
        });
        contentPane.add(checkpostinits, cc.xy(8, 4));

        //---- labellimpieza ----
        labellimpieza.setText(recursos.getString("datlabellimpieza"));
        contentPane.add(labellimpieza, cc.xy(10, 4));

        //---- checklimpieza ----
        checklimpieza.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
//                checkBoxComActionPerformed(e);
            }
        });
        contentPane.add(checklimpieza, cc.xy(12, 4));

        contentPane.add(separator2, cc.xywh(2, 6, 17, 1));

        //======== scrollPane1 ========
        {
            //---- tableJP ----
            tableJP.setSortable(false);
            tableJP.setHighlighters(pipeline);
            scrollPane1.setViewportView(tableJP);
        }
        contentPane.add(scrollPane1, cc.xywh(2, 8, 17, 1));

        contentPane.add(separator3, cc.xywh(2, 10, 17, 1));

        //======== scrollPane2 ========
        {
            //---- tableComp ----
            tableComp.setSortable(false);
            tableComp.setHighlighters(pipeline);
            scrollPane2.setViewportView(tableComp);
        }
        contentPane.add(scrollPane2, cc.xywh(2, 12, 17, 1));

        //---- buttonAceptar ----
        buttonAceptar.setText(recursos.getString("datbuttonAceptar"));
        buttonAceptar.setIcon(new ImageIcon(getClass().getResource("/imagenes/tick.png")));
        buttonAceptar.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buttonAceptarActionPerformed(e);
            }
        });
        contentPane.add(buttonAceptar, cc.xy(14, 14));

        //---- buttonGuardar ----
        buttonGuardar.setText(recursos.getString("datbuttonGuardar"));
        buttonGuardar.setIcon(new ImageIcon(getClass().getResource("/imagenes/tick.png")));
        buttonGuardar.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buttonAceptarActionPerformed(e);
            }
        });
        contentPane.add(buttonGuardar, cc.xy(16, 14));

        //---- buttonCancelar ----
        buttonCancelar.setText(recursos.getString("datbuttonCancelar"));
        buttonCancelar.setIcon(new ImageIcon(getClass().getResource("/imagenes/cross.png")));
        buttonCancelar.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buttonCancelarActionPerformed();
            }
        });
        contentPane.add(buttonCancelar, cc.xy(18, 14));

        pack();
        setLocationRelativeTo(getOwner());

        //---- selectionModelJP ----
        selectionModelJP.addListSelectionListener(new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {
                selectionModelValueChanged(e);
            }
        });

        //---- pipeline ----
        pipeline.addHighlighter(new AlternateRowHighlighter());
        // End of component initialization
    }
    /*********************************************************/
    /*      Variables                                        */
    /*********************************************************/
    private JFileChooser fileChooserguardar;
    private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;
    private JComponent separator1;
    private JComponent separator2;
    private JComponent separator3;
    private JXTable tableJP;
    private JXTable tableComp;
    private JLabel labelinits;
    private JLabel labelpostinits;
    private JLabel labellimpieza;
    private JCheckBox checkinits;
    private JCheckBox checkpostinits;
    private JCheckBox checklimpieza;
    private JButton buttonAceptar;
    private JButton buttonGuardar;
    private JButton buttonCancelar;
    private DefaultListSelectionModel selectionModelJP;
    private HighlighterPipeline pipeline;
} // Fin clase DialogAttachement

