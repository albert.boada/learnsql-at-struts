//////////////////////////////////////////////////////
//  CLASSE: SwingWorkerCompletionWaiter.java        //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JDialog;
import javax.swing.SwingWorker;

public class SwingWorkerCompletionWaiter implements PropertyChangeListener {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private JDialog dialog;
 
     public SwingWorkerCompletionWaiter(JDialog dialog) {
         this.dialog = dialog;
     }
 
     public void propertyChange(PropertyChangeEvent event) {
         if ("state".equals(event.getPropertyName())
                 && SwingWorker.StateValue.DONE == event.getNewValue()) {
             dialog.setVisible(false);
             dialog.dispose();
         }
     }
 } // Fin clase SwingWorkerCompletionWaiter
