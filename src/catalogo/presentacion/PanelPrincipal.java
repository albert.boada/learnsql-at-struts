//////////////////////////////////////////////////////
//  CLASSE: PanelPrincipal.java                     //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.presentacion;

import catalogo.global.CtrlVista;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import com.l2fprod.common.swing.*;

public class PanelPrincipal extends JPanel {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private CellConstraints cc1;
    private JPanel panelVisible;
    private PanelCatalogo panelCatalogo;
    private PanelTematicas panelTematicas;
    private PanelEsquemas panelEsquemas;
    private PanelCorrectores panelCorrectores;
    private PanelCategorias panelCategorias;
    ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

	public PanelPrincipal() {
        initComponents();
	}
        
    public void inicializa(PanelCatalogo cat, PanelEsquemas esq, PanelTematicas tem, PanelCorrectores cor, PanelCategorias categ) {
        cc1 = new CellConstraints();
        panelCatalogo = cat;
        panelTematicas = tem;
        panelEsquemas = esq;
        panelCorrectores = cor;
        panelCategorias = categ;
        panelVisible = panelCatalogo;
        toggleCatalogo.setSelected(true);
    }

    public void setFocoCuestion() {
        toggleCatalogo.setSelected(true);
        CtrlVista.creaCuestion();
    }

    public void setFocoCategorias() {
        toggleCategorias.setSelected(true);
        panelCategorias.setFoco();
    }

    public void setFocoTematicas() {
        toggleTematicas.setSelected(true);
        panelTematicas.setFoco();
    }

    public void setFocoEsquemas() {
        toggleEsquemas.setSelected(true);
        panelEsquemas.setFoco();
    }

    public void setFocoCorrectores() {
        toggleCorrectores.setSelected(true);
        panelCorrectores.setFoco();
    }
    
    public void cambioIdioma(ResourceBundle recursos){
        toggleCatalogo.setText(recursos.getString("toggleCatalogo"));
        toggleTematicas.setText(recursos.getString("toggleTematicas"));
        toggleEsquemas.setText(recursos.getString("toggleEsquemas"));
        toggleCorrectores.setText(recursos.getString("toggleCorrectores"));
        toggleCategorias.setText(recursos.getString("toggleCategorias"));
        
        toggleCatalogo.setToolTipText(recursos.getString("toggleCatalogoTooltip"));
        toggleTematicas.setToolTipText(recursos.getString("toggleTematicasTooltip"));
        toggleEsquemas.setToolTipText(recursos.getString("toggleEsquemasTooltip"));
        toggleCorrectores.setToolTipText(recursos.getString("toggleCorrectoresTooltip"));
        toggleCategorias.setToolTipText(recursos.getString("toggleCategoriasTooltip"));

        panelCatalogo.cambioIdioma(recursos);
        panelCategorias.cambioIdioma(recursos);
        panelTematicas.cambioIdioma(recursos);
        panelEsquemas.cambioIdioma(recursos);
        panelCorrectores.cambioIdioma(recursos);
    }

    private void muestraSeccion(JPanel seccion) {
        this.remove(panelVisible);
        panelVisible = seccion;
        this.add(seccion, cc1.xy(3, 1));
        this.validate();
        this.repaint();
    }

	private void toggleCatalogoItemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) muestraSeccion(panelCatalogo);
	}

	private void toggleTematicasItemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) muestraSeccion(panelTematicas);
	}

	private void toggleEsquemasItemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) muestraSeccion(panelEsquemas);
	}

	private void toggleCorrectoresItemStateChanged(ItemEvent e) {
             if (e.getStateChange() == ItemEvent.SELECTED) {
                muestraSeccion(panelCorrectores);
                panelCorrectores.setPropiedades(false);
             }
	}

    private void toggleCategoriasItemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) muestraSeccion(panelCategorias);
	}

	private void initComponents() {
		// Component initialization
		buttonBarSecciones = new JButtonBar();
		toggleCatalogo = new JToggleButton();
		toggleTematicas = new JToggleButton();
		toggleEsquemas = new JToggleButton();
		toggleCorrectores = new JToggleButton();
        toggleCategorias = new JToggleButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setBorder(null);
		setLayout(new FormLayout(
			new ColumnSpec[] {
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW)
			},
			RowSpec.decodeSpecs("fill:default:grow")));

		//======== buttonBarSecciones ========
		{
			buttonBarSecciones.setLayout(new FormLayout(
				ColumnSpec.decodeSpecs("45dlu"),
				new RowSpec[] {
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.LINE_GAP_ROWSPEC,
					new RowSpec(RowSpec.TOP, Sizes.DEFAULT, FormSpec.DEFAULT_GROW)
				}));

			//---- toggleCatalogo ----
			toggleCatalogo.setText(recursos.getString("toggleCatalogo"));
			toggleCatalogo.setIcon(new ImageIcon(getClass().getResource("/imagenes/seccion_catalogo.png")));
			toggleCatalogo.setPreferredSize(new Dimension(49, 60));
			toggleCatalogo.setToolTipText(recursos.getString("toggleCatalogoTooltip"));
			toggleCatalogo.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					toggleCatalogoItemStateChanged(e);
				}
			});
			buttonBarSecciones.add(toggleCatalogo, cc.xy(1, 1));

            //---- toggleCategorias ----
			toggleCategorias.setText(recursos.getString("toggleCategorias"));
			toggleCategorias.setIcon(new ImageIcon(getClass().getResource("/imagenes/seccion_categorias.png")));
			toggleCategorias.setPreferredSize(new Dimension(49, 60));
			toggleCategorias.setToolTipText(recursos.getString("toggleCategoriasTooltip"));
			toggleCategorias.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					toggleCategoriasItemStateChanged(e);
				}
			});
			buttonBarSecciones.add(toggleCategorias, cc.xy(1, 3));

			//---- toggleTematicas ----
			toggleTematicas.setText(recursos.getString("toggleTematicas"));
			toggleTematicas.setIcon(new ImageIcon(getClass().getResource("/imagenes/seccion_tematicas.png")));
			toggleTematicas.setPreferredSize(new Dimension(49, 60));
			toggleTematicas.setToolTipText(recursos.getString("toggleTematicasTooltip"));
			toggleTematicas.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					toggleTematicasItemStateChanged(e);
				}
			});
			buttonBarSecciones.add(toggleTematicas, cc.xy(1, 5));

			//---- toggleEsquemas ----
			toggleEsquemas.setText(recursos.getString("toggleEsquemas"));
			toggleEsquemas.setIcon(new ImageIcon(getClass().getResource("/imagenes/seccion_esquemas.png")));
			toggleEsquemas.setPreferredSize(new Dimension(49, 60));
			toggleEsquemas.setToolTipText(recursos.getString("toggleEsquemasTooltip"));
			toggleEsquemas.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					toggleEsquemasItemStateChanged(e);
				}
			});
			buttonBarSecciones.add(toggleEsquemas, cc.xy(1, 7));

			//---- toggleCorrectores ----
			toggleCorrectores.setText(recursos.getString("toggleCorrectores"));
			toggleCorrectores.setIcon(new ImageIcon(getClass().getResource("/imagenes/seccion_correctores.png")));
			toggleCorrectores.setPreferredSize(new Dimension(49, 60));
			toggleCorrectores.setToolTipText(recursos.getString("toggleCorrectoresTooltip"));
			toggleCorrectores.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					toggleCorrectoresItemStateChanged(e);
				}
			});
			buttonBarSecciones.add(toggleCorrectores, cc.xy(1, 9));

		}
		add(buttonBarSecciones, cc.xy(1, 1));

		//---- groupSecciones ----
		ButtonGroup groupSecciones = new ButtonGroup();
		groupSecciones.add(toggleCatalogo);
		groupSecciones.add(toggleTematicas);
		groupSecciones.add(toggleEsquemas);
		groupSecciones.add(toggleCorrectores);
        groupSecciones.add(toggleCategorias);
		// End of component initialization
	}

	/*********************************************************/
    /*      Variables                                        */
    /*********************************************************/

	private JButtonBar buttonBarSecciones;
	private JToggleButton toggleCatalogo;
	private JToggleButton toggleTematicas;
	private JToggleButton toggleEsquemas;
	private JToggleButton toggleCorrectores;
    private JToggleButton toggleCategorias;
        
} // Fin clase PanelPrincipal
