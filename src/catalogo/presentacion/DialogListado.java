//////////////////////////////////////////////////////
//  CLASSE: DialogListado.java                      //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////
package catalogo.presentacion;

import catalogo.modelo.JuegoPruebas;
import catalogo.modelo.Comprobacion;
import catalogo.modelo.Cuestion;
import catalogo.modelo.Tematica;
import catalogo.soporte.modelos.TableModelJP;
import catalogo.soporte.modelos.TableModelComprobaciones;
import java.util.*; //pel resourcebundle...
import java.io.*;
import java.beans.*;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.AlternateRowHighlighter;
import org.jdesktop.swingx.decorator.HighlighterPipeline;
import java.awt.event.*;
import javax.swing.event.*;

public class DialogListado extends JDialog {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    private TableModelJP tableModeljp;
    private TableModelComprobaciones tableModelcom;
    private Cuestion cuestion;
    private Boolean haAceptado;
    private File fichero;
    private FileFilter filterTxt;
    private Mensaje mensaje;
    ResourceBundle recursos;

    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/
    public DialogListado(Frame owner, Cuestion c, ResourceBundle rec) {
        super(owner, true);
        cuestion = c;
        recursos = rec;
        haAceptado = false;
        initComponents();
        inicializa();
    }

    public DialogListado(Dialog owner, Cuestion c, ResourceBundle rec) {
        super(owner, true);
        cuestion = c;
        recursos = rec;
        haAceptado = false;
        initComponents();
        inicializa();
    }

    public Boolean haAceptado() {
        return haAceptado;
    }

    public File ficheroGenerado() {
        return fichero;
    }

    private void inicializa() {
        setSize(700, 500);

        mensaje = new Mensaje();
        tableModeljp = new TableModelJP(cuestion.getJuegosPruebas());
        tableModeljp.setColumnName(recursos);
        tableJP.setModel(tableModeljp);
        tableJP.getColumnModel().getColumn(0).setPreferredWidth(130);
        tableJP.getColumnModel().getColumn(1).setPreferredWidth(150);
        tableJP.getColumnModel().getColumn(2).setPreferredWidth(120);
        tableJP.getColumnModel().getColumn(3).setPreferredWidth(120);
        tableJP.getColumnModel().getColumn(4).setPreferredWidth(120);

        tableModelcom = new TableModelComprobaciones(cuestion.getJuegosPruebas().EMPTY_LIST);
        tableModelcom.setColumnName(recursos);
        tableComp.setModel(tableModelcom);
        tableComp.getColumnModel().getColumn(0).setPreferredWidth(180);
        tableComp.getColumnModel().getColumn(1).setPreferredWidth(200);
        tableComp.getColumnModel().getColumn(2).setPreferredWidth(200);

        checkenunciado.setSelected(true);
        checkesquema.setSelected(true);
        checkcategoria.setSelected(true);
        checktematicas.setSelected(true);
        checkdificultad.setSelected(true);
        checktipo.setSelected(true);
        checkestado.setSelected(true);
        checkinits.setSelected(true);
        checksolucion.setSelected(true);
        checkpostinits.setSelected(true);
        checklimpieza.setSelected(true);

    }

    private String calculaCaracteres(String text, String var) {
        String guiones;
        int num = text.length() + var.length();

        guiones = "--";
        for (int i = 0; i < num; i++) {
            guiones = guiones + "-";
        }
//        guiones = guiones + "--\n";
        guiones = guiones + "\n";

        return guiones;
    }

    private void selectionModelValueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            int rowIndex = tableJP.getSelectedRow();
            if (rowIndex == -1) {
//                textNombrePeso.setText("");
//                spinnerPeso.setValue(0);
            } else {
                JuegoPruebas jp = (JuegoPruebas) ((TableModelJP) tableJP.getModel()).getValueAt(tableJP.convertRowIndexToModel(rowIndex));
                tableModelcom.setModel(jp.getComprobaciones());
                tableComp.setModel(tableModelcom);
                tableModeljp.setEntradaComp(tableModelcom.getEntrada());
                tableModeljp.setSalidaComp(tableModelcom.getSalida());
            }
        }
    }

    private void buttonGuardarActionPerformed(ActionEvent e) {

        String tcuestion = recursos.getString("tcuestion");
        String tenunciado = recursos.getString("tenunciado");
        String tcategoria = recursos.getString("tcategoria");
        String tesquema = recursos.getString("tesquema");
        String ttematicas = recursos.getString("ttematicas");
        String tdificultad = recursos.getString("tdificultad");
        String ttipo = recursos.getString("ttipo");
        String testado = recursos.getString("testado");
        String solucion = recursos.getString("solucion");
        String inits = recursos.getString("inits");
        String postinits = recursos.getString("postinits");
        String limpieza = recursos.getString("limpieza");
        String tjp = recursos.getString("tjp");
        String jpinits = recursos.getString("jpinits");
        String jpentrada = recursos.getString("jpentrada");
        String jpresultado = recursos.getString("jpresultado");
        String jplimpieza = recursos.getString("jplimpieza");
        String tcomp = recursos.getString("tcomp");
        String compentrada = recursos.getString("compentrada");
        String compsalida = recursos.getString("compsalida");
        String cat = recursos.getString("itemIdiomacat");
        String esp = recursos.getString("itemIdiomaesp");
        String ing = recursos.getString("itemIdiomaing");

        Boolean hayjpinits =true;
        Boolean hayjpentrada =true;
        Boolean hayjpresultado =true;
        Boolean hayjplimpieza =true;
        Boolean haycompentrada =true;
        Boolean haycompsalida =true;

        fichero = new File("question"+cuestion.getId()+".txt");
        filterTxt = new FileNameExtensionFilter("SQL/Texto (*.sql, *.txt)", "sql", "txt");
        fileChooserguardar.setSelectedFile(fichero);
        fileChooserguardar.setFileFilter(filterTxt);
        int respuesta = fileChooserguardar.showSaveDialog(this);
        if (respuesta == JFileChooser.APPROVE_OPTION) {
            if(fichero.exists()){
                int response= mensaje.mensajePregunta(this,recursos.getString("dlistaviso2"), recursos);
                if(response==0) fichero = fileChooserguardar.getSelectedFile();
                else return;
            }
            else fichero = fileChooserguardar.getSelectedFile();
        

            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(fichero.getAbsolutePath()));

                //Escribimos en el buffer atributos cuestion
                bw.write("--" + calculaCaracteres(tcuestion, cuestion.getTitulo()));
                bw.write(tcuestion + " " + cuestion.getTitulo() + " --\n");
                bw.write("--" + calculaCaracteres(tcuestion, cuestion.getTitulo()) + "\n");

                if (checkenunciado.isSelected()){
                    bw.write(tenunciado + cat + "\n");
                    bw.write(calculaCaracteres(tenunciado, cat));
                    bw.write(cuestion.getEnunciado() + "\n\n");
                    bw.write(tenunciado + esp + "\n");
                    bw.write(calculaCaracteres(tenunciado, esp));
                    bw.write(cuestion.getEnunciadoEsp() + "\n\n");
                    bw.write(tenunciado + ing + "\n");
                    bw.write(calculaCaracteres(tenunciado, ing));
                    bw.write(cuestion.getEnunciadoIng() + "\n\n");
                }

                if (checkcategoria.isSelected()) {
                    bw.write(tcategoria + "\n");
                    bw.write(calculaCaracteres(tcategoria, ""));
                    bw.write(cuestion.getCategoria().getDescripcio() + "\n\n");
                }

                if (checkesquema.isSelected()) {
                    bw.write(tesquema + "\n");
                    bw.write(calculaCaracteres(tesquema, ""));
                    bw.write(cuestion.getEsquema().getNombre() + "\n\n");
                }

                if (checktematicas.isSelected()) {
                    bw.write(ttematicas + "\n");
                    bw.write(calculaCaracteres(ttematicas, ""));
                    for(Tematica t : cuestion.getTematicas()){
                        bw.write(t.getNombre() + "\n");
                    }
                    bw.write("\n");
                }

                if (checkdificultad.isSelected()) {
                    bw.write(tdificultad + "\n");
                    bw.write(calculaCaracteres(tdificultad, ""));
                    bw.write(cuestion.getDificultad() + "\n\n");
                }

                if (checktipo.isSelected()) {
                    bw.write(ttipo + "\n");
                    bw.write(calculaCaracteres(ttipo, ""));
                    bw.write(cuestion.getTipoSolucion().getNombre() + "\n\n");
                }

                if (checkestado.isSelected()) {
                    bw.write(testado + "\n");
                    bw.write(calculaCaracteres(testado, ""));
                    bw.write(cuestion.getEstado()+ "\n\n");
                }

                if (checkinits.isSelected()) {
                    bw.write(inits + "\n");
                    bw.write(calculaCaracteres(inits, ""));
                    bw.write(cuestion.getInits() + "\n\n");
                }

                if (checksolucion.isSelected()) {
                    bw.write(solucion + "\n");
                    bw.write(calculaCaracteres(solucion, ""));
                    bw.write(cuestion.getSolucion() + "\n\n");
                }

                if (checkpostinits.isSelected()) {
                    bw.write(postinits + "\n");
                    bw.write(calculaCaracteres(postinits, ""));
                    bw.write(cuestion.getPostInits() + "\n\n");
                }

                if (checklimpieza.isSelected()) {
                    bw.write(limpieza + "\n");
                    bw.write(calculaCaracteres(limpieza, ""));
                    bw.write(cuestion.getLimpieza() + "\n\n");
                }

                //Escribimos en el buffer los juegos de pruebas
                for (JuegoPruebas todos : cuestion.getJuegosPruebas()) {

                    if(tableModeljp.getInits().contains(todos)) hayjpinits = true;
                    else hayjpinits = false;

                    if(tableModeljp.getEntrada().contains(todos)) hayjpentrada = true;
                    else hayjpentrada = false;

                    if(tableModeljp.getResultado().contains(todos)) hayjpresultado = true;
                    else hayjpresultado = false;

                    if(tableModeljp.getLimpieza().contains(todos)) hayjplimpieza = true;
                    else hayjplimpieza = false;

                    if(hayjpinits || hayjpentrada || hayjpresultado || hayjplimpieza){
                        bw.write(calculaCaracteres(tjp, todos.getNombre()));
                        bw.write(tjp + " " + todos.getNombre() + "\n");
                        bw.write(calculaCaracteres(tjp, todos.getNombre()) + "\n");
                    }

                    if (hayjpinits) {
                        bw.write(jpinits + "\n");
                        bw.write(calculaCaracteres(jpinits, ""));
                        for (JuegoPruebas jp : tableModeljp.getInits()) {
                            if (todos.getNombre().equals(jp.getNombre())) {
                                bw.write(jp.getInits() + "\n");
                            }
                        }
                        bw.write("\n");
                    }
                    if (hayjpentrada) {
                        bw.write(jpentrada + "\n");
                        bw.write(calculaCaracteres(jpentrada, ""));
                        for (JuegoPruebas jp : tableModeljp.getEntrada()) {
                            if (todos.getNombre().equals(jp.getNombre())) {
                                bw.write(jp.getEntrada() + "\n");
                            }
                        }
                        bw.write("\n");
                    }

                    if (hayjpresultado) {
                        bw.write(jpresultado + "\n");
                        bw.write(calculaCaracteres(jpresultado, ""));
                        for (JuegoPruebas jp : tableModeljp.getResultado()) {
                            if (todos.getNombre().equals(jp.getNombre())) {
                                bw.write(jp.getSalida() + "\n");
                            }
                        }
                        bw.write("\n");
                    }
                    if (hayjplimpieza) {
                        bw.write(jplimpieza + "\n");
                        bw.write(calculaCaracteres(jplimpieza, ""));
                        for (JuegoPruebas jp : tableModeljp.getLimpieza()) {
                            if (todos.getNombre().equals(jp.getNombre())) {
                                bw.write(jp.getLimpieza() + "\n");
                            }
                        }
                        bw.write("\n");
                    }
                    bw.write("\n");

                    //Escribimos en el buffer las comprobaciones
                    for (Comprobacion all : todos.getComprobaciones()) {

                        if(tableModelcom.getEntrada().contains(all)) haycompentrada = true;
                        else haycompentrada = false;

                        if(tableModelcom.getSalida().contains(all)) haycompsalida = true;
                        else haycompsalida = false;

                        if(haycompentrada || haycompsalida){
                            if(!hayjpinits && !hayjpentrada && !hayjpresultado && !hayjplimpieza){
                                bw.write(calculaCaracteres(tjp, todos.getNombre()));
                                bw.write(tjp + " " + todos.getNombre() + "\n");
                                bw.write(calculaCaracteres(tjp, todos.getNombre()) + "\n");
                            }
                            bw.write(calculaCaracteres(tcomp, all.getNombre()));
                            bw.write(tcomp + " " + all.getNombre() + "\n");
                            bw.write(calculaCaracteres(tcomp, all.getNombre()) + "\n");
                        }

                        if (haycompentrada) {
                            bw.write(compentrada + "\n");
                            bw.write(calculaCaracteres(compentrada, ""));
                            for (Comprobacion com : tableModelcom.getEntrada()) {
                                if (all.getNombre().equals(com.getNombre())) {
                                    bw.write(com.getEntrada() + "\n");
                                }
                            }
                            bw.write("\n");
                        }

                        if (haycompsalida) {
                            bw.write(compsalida + "\n");
                            bw.write(calculaCaracteres(compsalida, ""));
                            for (Comprobacion com : tableModelcom.getSalida()) {
                                if (all.getNombre().equals(com.getNombre())) {
                                    bw.write(com.getSalida() + "\n");
                                }
                            }
                            bw.write("\n");
                        }
                        bw.write("\n");
                    }
                    bw.write("\n");
                }

                // Hay que cerrar el fichero
                bw.close();


//                haAceptado = true;
                mensaje.mensajeInformacion(this, recursos.getString("dlistaviso"), recursos);
                dispose();

            }catch (IOException ioe) {
                ioe.printStackTrace();

            }
        }
    }

    private void buttonCancelarActionPerformed() {
        dispose();
    }

    private void initComponents() {
        // Component initialization
        DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
        fileChooserguardar = new JFileChooser();
        scrollPane1 = new JScrollPane();
        scrollPane2 = new JScrollPane();
        separator1 = compFactory.createSeparator(recursos.getString("dlistseparator1"));
        separator2 = compFactory.createSeparator(recursos.getString("dlistseparator2"));
        separator3 = compFactory.createSeparator(recursos.getString("dlistseparator3"));
        tableJP = new JXTable() {

            public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColIndex = convertColumnIndexToModel(colIndex);
                if (realColIndex != 0) {
                    tip = ((Boolean) getValueAt(rowIndex, colIndex)) ? recursos.getString("seleccionado") : recursos.getString("noseleccionado");
                } else {
                    tip = (String) getValueAt(rowIndex, colIndex);
                }
                return tip;
            }
        };
        tableComp = new JXTable() {

            public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColIndex = convertColumnIndexToModel(colIndex);
                if (realColIndex != 0) {
                    tip = ((Boolean) getValueAt(rowIndex, colIndex)) ? recursos.getString("seleccionado") : recursos.getString("noseleccionado");
                } else {
                    tip = (String) getValueAt(rowIndex, colIndex);
                }
                return tip;
            }
        };
        labelenunciado = new JLabel();
        labelesquema = new JLabel();
        labelcategoria = new JLabel();
        labeltematicas = new JLabel();
        labeldificultad = new JLabel();
        labeltipo = new JLabel();
        labelestado = new JLabel();
        labelinits = new JLabel();
        labelsolucion = new JLabel();
        labelpostinits = new JLabel();
        labellimpieza = new JLabel();
        checkenunciado = new JCheckBox();
        checkesquema = new JCheckBox();
        checkcategoria = new JCheckBox();
        checktematicas = new JCheckBox();
        checkdificultad = new JCheckBox();
        checktipo = new JCheckBox();
        checkestado = new JCheckBox();
        checkinits = new JCheckBox();
        checksolucion = new JCheckBox();
        checkpostinits = new JCheckBox();
        checklimpieza = new JCheckBox();
//        buttonAceptar = new JButton();
        buttonGuardar = new JButton();
        buttonCancelar = new JButton();
        selectionModelJP = (DefaultListSelectionModel) tableJP.getSelectionModel();
        pipeline = new HighlighterPipeline();
        CellConstraints cc = new CellConstraints();

        //======== this ========
        setTitle(recursos.getString("dlisttitle"));
//        addPropertyChangeListener("visible", new PropertyChangeListener() {
//
//            public void propertyChange(PropertyChangeEvent e) {
//                thisPropertyChange();
//            }
//        });
        Container contentPane = getContentPane();
        contentPane.setLayout(new FormLayout(
                new ColumnSpec[]{
                    FormFactory.UNRELATED_GAP_COLSPEC,
                    new ColumnSpec(ColumnSpec.RIGHT, Sizes.DEFAULT, FormSpec.NO_GROW),
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
                    FormFactory.RELATED_GAP_COLSPEC,
                    new ColumnSpec(ColumnSpec.RIGHT, Sizes.DEFAULT, FormSpec.NO_GROW),
//                    new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(80), FormSpec.NO_GROW),
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
                    FormFactory.RELATED_GAP_COLSPEC,
                    new ColumnSpec(ColumnSpec.RIGHT, Sizes.DEFAULT, FormSpec.NO_GROW),
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
                    FormFactory.RELATED_GAP_COLSPEC,
                    new ColumnSpec(ColumnSpec.RIGHT, Sizes.DEFAULT, FormSpec.NO_GROW),
                    FormFactory.RELATED_GAP_COLSPEC,
                    FormFactory.DEFAULT_COLSPEC,
                    new ColumnSpec("max(default;10dlu):grow"),
                    new ColumnSpec(Sizes.dluX(60)),
                    FormFactory.RELATED_GAP_COLSPEC,
                    new ColumnSpec(Sizes.dluX(60)),
                    FormFactory.UNRELATED_GAP_COLSPEC
                },
                new RowSpec[]{
                    FormFactory.UNRELATED_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                    FormFactory.LINE_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.LINE_GAP_ROWSPEC,
                    new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                    FormFactory.RELATED_GAP_ROWSPEC,
                    FormFactory.DEFAULT_ROWSPEC,
                    FormFactory.UNRELATED_GAP_ROWSPEC
                }));

        contentPane.add(separator1, cc.xywh(2, 2, 19, 1));

        //---- labelenunciado ----
        labelenunciado.setText(recursos.getString("dlistlabelenunciado"));
        contentPane.add(labelenunciado, cc.xy(2, 4));

        //---- checkenunciado ----
        contentPane.add(checkenunciado, cc.xy(4, 4));

        //---- labelcategoria ----
        labelcategoria.setText(recursos.getString("dlistlabelcategoria"));
        contentPane.add(labelcategoria, cc.xy(6, 4));

        //---- checkcategoria ----
        contentPane.add(checkcategoria, cc.xy(8, 4));

        //---- labelesquema ----
        labelesquema.setText(recursos.getString("dlistlabelesquema"));
        contentPane.add(labelesquema, cc.xy(10, 4));

        //---- checkesquema ----
        contentPane.add(checkesquema, cc.xy(12, 4));

        //---- labeltematicas ----
        labeltematicas.setText(recursos.getString("dlistlabeltematicas"));
        contentPane.add(labeltematicas, cc.xy(14, 4));

        //---- checktematicas ----
        contentPane.add(checktematicas, cc.xy(16, 4));

        //---- labeldificultad ----
        labeldificultad.setText(recursos.getString("dlistlabeldificultad"));
        contentPane.add(labeldificultad, cc.xy(2, 6));

        //---- checkdificultad ----
        contentPane.add(checkdificultad, cc.xy(4, 6));

        //---- labeltipo ----
        labeltipo.setText(recursos.getString("dlistlabeltipo"));
        contentPane.add(labeltipo, cc.xy(6, 6));

        //---- checktipo ----
        contentPane.add(checktipo, cc.xy(8, 6));

        //---- labelestado ----
        labelestado.setText(recursos.getString("dlistlabelestado"));
        contentPane.add(labelestado, cc.xy(10, 6));

        //---- checkestado ----
        contentPane.add(checkestado, cc.xy(12, 6));

        //---- labelsolucion ----
        labelsolucion.setText(recursos.getString("dlistlabelsolucion"));
        contentPane.add(labelsolucion, cc.xy(14, 6));

        //---- checksolucion ----
        contentPane.add(checksolucion, cc.xy(16, 6));

        //---- labelinits ----
        labelinits.setText(recursos.getString("dlistlabelinits"));
        contentPane.add(labelinits, cc.xy(2, 8));

        //---- checkinits ----
        contentPane.add(checkinits, cc.xy(4, 8));

        //---- labelpostinits ----
        labelpostinits.setText(recursos.getString("dlistlabelpostinits"));
        contentPane.add(labelpostinits, cc.xy(6, 8));

        //---- checkpostinits ----
        contentPane.add(checkpostinits, cc.xy(8, 8));

        //---- labellimpieza ----
        labellimpieza.setText(recursos.getString("dlistlabellimpieza"));
        contentPane.add(labellimpieza, cc.xy(10, 8));

        //---- checklimpieza ----
        contentPane.add(checklimpieza, cc.xy(12, 8));

        contentPane.add(separator2, cc.xywh(2, 10, 19, 1));

        //======== scrollPane1 ========
        {
            //---- tableJP ----
            tableJP.setSortable(false);
            tableJP.setHighlighters(pipeline);
            scrollPane1.setViewportView(tableJP);
        }
        contentPane.add(scrollPane1, cc.xywh(2, 12, 19, 1));

        contentPane.add(separator3, cc.xywh(2, 14, 19, 1));

        //======== scrollPane2 ========
        {
            //---- tableComp ----
            tableComp.setSortable(false);
            tableComp.setHighlighters(pipeline);
            scrollPane2.setViewportView(tableComp);
        }
        contentPane.add(scrollPane2, cc.xywh(2, 16, 19, 1));

//        //---- buttonAceptar ----
//        buttonAceptar.setText(recursos.getString("datbuttonAceptar"));
//        buttonAceptar.setIcon(new ImageIcon(getClass().getResource("/imagenes/tick.png")));
//        buttonAceptar.addActionListener(new ActionListener() {
//
//            public void actionPerformed(ActionEvent e) {
//                buttonAceptarActionPerformed(e);
//            }
//        });
//        contentPane.add(buttonAceptar, cc.xy(14, 14));

        //---- buttonGuardar ----
        buttonGuardar.setText(recursos.getString("dlistbuttonGuardar"));
        buttonGuardar.setIcon(new ImageIcon(getClass().getResource("/imagenes/tick.png")));
        buttonGuardar.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buttonGuardarActionPerformed(e);
            }
        });
        contentPane.add(buttonGuardar, cc.xy(18, 18));

        //---- buttonCancelar ----
        buttonCancelar.setText(recursos.getString("dlistbuttonCancelar"));
        buttonCancelar.setIcon(new ImageIcon(getClass().getResource("/imagenes/cross.png")));
        buttonCancelar.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buttonCancelarActionPerformed();
            }
        });
        contentPane.add(buttonCancelar, cc.xy(20, 18));

        pack();
        setLocationRelativeTo(getOwner());

        //---- selectionModelJP ----
        selectionModelJP.addListSelectionListener(new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {
                selectionModelValueChanged(e);
            }
        });

        //---- pipeline ----
        pipeline.addHighlighter(new AlternateRowHighlighter());
        // End of component initialization
    }
    /*********************************************************/
    /*      Variables                                        */
    /*********************************************************/
    private JFileChooser fileChooserguardar;
    private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;
    private JComponent separator1;
    private JComponent separator2;
    private JComponent separator3;
    private JXTable tableJP;
    private JXTable tableComp;    
    private JLabel labelenunciado;
    private JLabel labelesquema;
    private JLabel labelcategoria;
    private JLabel labeltematicas;
    private JLabel labeldificultad;
    private JLabel labeltipo;
    private JLabel labelestado;
    private JLabel labelinits;
    private JLabel labelsolucion;
    private JLabel labelpostinits;
    private JLabel labellimpieza;
    private JCheckBox checkenunciado;
    private JCheckBox checkesquema;
    private JCheckBox checkcategoria;
    private JCheckBox checktematicas;
    private JCheckBox checkdificultad;
    private JCheckBox checktipo;
    private JCheckBox checkestado;
    private JCheckBox checkinits;
    private JCheckBox checksolucion;
    private JCheckBox checkpostinits;
    private JCheckBox checklimpieza;
//    private JButton buttonAceptar;
    private JButton buttonGuardar;
    private JButton buttonCancelar;
    private DefaultListSelectionModel selectionModelJP;
    private HighlighterPipeline pipeline;

} // Fin clase DialogListado