//////////////////////////////////////////////////////
//  CLASSE: PanelCorrectores.java                   //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////
package catalogo.presentacion;

import catalogo.global.CtrlVista;
import catalogo.modelo.DiccCatalogo;
import catalogo.modelo.Corrector;
import catalogo.modelo.Cuestion;
import catalogo.modelo.DiccCorrectores;
import catalogo.soporte.modelos.ArrayListModel;
import catalogo.soporte.modelos.TableModelCorrectores;
import catalogo.soporte.modelos.TableModelGestioCorrectores;
import catalogo.soporte.utils.TextParser;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import com.jgoodies.uif_lite.panel.*;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.decorator.*;

public class PanelCorrectores extends JPanel {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    private DiccCorrectores correctores;
    private DiccCatalogo catalogo;
    private TableModelCorrectores tableModel;
    private TableModelGestioCorrectores tableModel2;
    private DialogEspera dialogEspera;
    private SwingWorker workerProp;
    private SwingWorkerCompletionWaiter waiterProp;
    private Mensaje mensaje;
    private ResourceBundle recursos = ResourceBundle.getBundle("config/idiomacatala");

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/
    public PanelCorrectores(DiccCorrectores correctores, DiccCatalogo catalogo) {
        initComponents();
        inicializa(correctores, catalogo);
    }

    public void setFoco() {
        selectionModel.removeSelectionInterval(0, tableModel.getRowCount() - 1);
        textURL.requestFocus();
    }

    public void setPropiedades(boolean todos) {
        boolean faltaInit = false;
        if (!todos) {
            for (Corrector cor : correctores.getAll()) {
                if (cor.getEstado() == Corrector.DESCONOCIDO) {
                    faltaInit = true;
                    break;
                }
            }
        }
        if (faltaInit || todos) {
            creaWorker(todos);
            workerProp.execute();
            dialogEspera.setVisible(true);
        }
    }

    public void cambioIdioma(ResourceBundle recursos) {
        sifCorrectores.setTitle(recursos.getString("sifCorrectores"));
        label1.setText(recursos.getString("corlabel1"));
        label2.setText(recursos.getString("corlabel2"));
        buttonAdd.setText(recursos.getString("corbuttonAdd"));
        buttonMod.setText(recursos.getString("corbuttonMod"));
        buttonDel.setText(recursos.getString("corbuttonDel"));
        buttonActualizar.setText(recursos.getString("corbuttonActualizar"));
        buttonActualizar.setToolTipText(recursos.getString("corbuttonActualizarTooltip"));
        buttonEjecutar.setText(recursos.getString("corbuttonEjecutar"));
        buttonEjecutar.setToolTipText(recursos.getString("corbuttonEjecutarTooltip"));
        buttonSeleccionar.setText(recursos.getString("corbuttonSeleccionar"));
        buttonSeleccionar.setToolTipText(recursos.getString("corbuttonSeleccionarTooltip"));
        buttonDesSeleccionar.setText(recursos.getString("corbuttonDesSeleccionar"));
        buttonDesSeleccionar.setToolTipText(recursos.getString("corbuttonDesSeleccionarTooltip"));

        tableModel.setColumnName(recursos);
        tableCorrectores.setModel(tableModel);
        tableCorrectores.getColumnModel().getColumn(0).setPreferredWidth(400);
        tableCorrectores.getColumnModel().getColumn(1).setPreferredWidth(125);
        tableCorrectores.getColumnModel().getColumn(2).setPreferredWidth(125);
        tableCorrectores.getColumnModel().getColumn(3).setPreferredWidth(175);
        tableCorrectores.getColumnModel().getColumn(4).setPreferredWidth(425);
        tableCorrectores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        tableModel2 = new TableModelGestioCorrectores();
        tableModel2.setColumnName(recursos);
        tableGestionCorrectores.setModel(tableModel2);
        tableGestionCorrectores.getColumnModel().getColumn(0).setPreferredWidth(75);
        tableGestionCorrectores.getColumnModel().getColumn(1).setPreferredWidth(40);
        tableGestionCorrectores.getColumnModel().getColumn(2).setPreferredWidth(550);
        tableGestionCorrectores.getColumnModel().getColumn(3).setPreferredWidth(260);
        tableGestionCorrectores.getColumnModel().getColumn(4).setPreferredWidth(225);
        tableGestionCorrectores.getColumnModel().getColumn(5).setPreferredWidth(50);
        tableGestionCorrectores.getColumnModel().getColumn(6).setPreferredWidth(50);
        tableGestionCorrectores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this.recursos = recursos;
    }

    private void inicializa(DiccCorrectores correctores, DiccCatalogo catalogo) {
        this.correctores = correctores;
        this.catalogo = catalogo;
        dialogEspera = null;
        mensaje = new Mensaje();
        tableModel = new TableModelCorrectores(correctores.getAll());
        tableCorrectores.setModel(tableModel);
        tableCorrectores.getColumnModel().getColumn(0).setPreferredWidth(400);
        tableCorrectores.getColumnModel().getColumn(1).setPreferredWidth(125);
        tableCorrectores.getColumnModel().getColumn(2).setPreferredWidth(125);
        tableCorrectores.getColumnModel().getColumn(3).setPreferredWidth(175);
        tableCorrectores.getColumnModel().getColumn(4).setPreferredWidth(425);
        tableCorrectores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        tableModel2 = new TableModelGestioCorrectores();
        tableGestionCorrectores.setModel(tableModel2);        
        tableGestionCorrectores.getColumnModel().getColumn(0).setPreferredWidth(75);
        tableGestionCorrectores.getColumnModel().getColumn(1).setPreferredWidth(40);
//        tableGestionCorrectores.getColumnModel().getColumn(1).setCellRenderer(new TableGestionCellRender());
        tableGestionCorrectores.getColumnModel().getColumn(2).setPreferredWidth(550);
        tableGestionCorrectores.getColumnModel().getColumn(3).setPreferredWidth(260);
        tableGestionCorrectores.getColumnModel().getColumn(4).setPreferredWidth(225);
        tableGestionCorrectores.getColumnModel().getColumn(5).setPreferredWidth(50);
        tableGestionCorrectores.getColumnModel().getColumn(6).setPreferredWidth(50);
        tableGestionCorrectores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        textURL.addMouseListener(CtrlVista.contextMouseAdapter);
        textDescripcion.addMouseListener(CtrlVista.contextMouseAdapter);

        buttonEjecutar.setEnabled(false);
        buttonSeleccionar.setEnabled(false);
        buttonDesSeleccionar.setEnabled(false);
//        selectionModelValueChanged();
    }

    private void buttonAddActionPerformed() {
        textURL.setText(TextParser.parseUTF8(textURL.getText().trim()));
        textDescripcion.setText(TextParser.parseUTF8(textDescripcion.getText().trim()));
        try {
            Corrector cor = correctores.add(textURL.getText(), textDescripcion.getText());
            setPropiedades(false);
            tableModel.fireTableDataChanged();
            selectionModel.setSelectionInterval(tableModel.getIndex(cor), tableModel.getIndex(cor));
        } catch (Exception e) {
            mensaje.mensajeError(this, recursos.getString("coraviso1"), e, recursos);
        }
    }

    private void buttonModActionPerformed() {
        textURL.setText(TextParser.parseUTF8(textURL.getText().trim()));
        textDescripcion.setText(TextParser.parseUTF8(textDescripcion.getText().trim()));
        Corrector cor = (Corrector) tableModel.getValueAt(tableCorrectores.convertRowIndexToModel(tableCorrectores.getSelectedRow()));
            if (mensaje.mensajeConfirmacion(this, recursos.getString("coraviso2") + cor.getURL() + "\"?", recursos) == 0) {
                try {
                    boolean urlChanged = correctores.set(cor, textURL.getText(), textDescripcion.getText());
                    if (urlChanged) {
                        setPropiedades(false);
                    }

                    java.util.List<Cuestion> todas = tableModel2.getDatos();
                    java.util.List<Cuestion> seleccionadas = tableModel2.getSeleccion();

                    for (Cuestion actual : todas) {
                        if (seleccionadas.contains(actual)) {
                            if(!(actual.getCorrectores().contains(cor))){
                                actual.addCorrector(cor);
                                catalogo.actualizarCorrectores(actual);
                                CtrlVista.setTituloModif(actual);
                            }
                        }
                        else{
                            if(actual.getCorrectores().contains(cor)){
                                actual.removeCorrector(cor);
                                catalogo.actualizarCorrectores(actual);
                                CtrlVista.setTituloModif(actual);
                            }
                        }
                    }
                    CtrlVista.creaCuestion();
                    catalogo.clearSalidas(cor);
                    CtrlVista.clearSalidas(cor);
                    tableModel.fireTableDataChanged();
                    tableModel2.fireTableDataChanged();
                    tableModel2.setEjecutado(false);
                    selectionModel.setSelectionInterval(tableModel.getIndex(cor), tableModel.getIndex(cor));
                    buttonEjecutar.setEnabled(true);

                } catch (Exception e) {
                    mensaje.mensajeError(this, recursos.getString("coraviso3"), e, recursos);
                }
            }
//        }
    }

    private void buttonDelActionPerformed() {
        int rowIndex = tableCorrectores.getSelectedRow();
        Corrector cor = (Corrector) tableModel.getValueAt(rowIndex);
        if (mensaje.mensajeConfirmacion(this, recursos.getString("coraviso4") + cor.getURL() + "\"?", recursos) == 0) {
            try {
                correctores.remove(cor);
                tableModel.fireTableDataChanged();
                int total = tableCorrectores.getRowCount();
                if (total > 0) {
                    if (rowIndex < total) {
                        selectionModel.setSelectionInterval(rowIndex, rowIndex);
                    } else {
                        selectionModel.setSelectionInterval(total - 1, total - 1);
                    }
                }
            } catch (Exception e) {
                mensaje.mensajeError(this, recursos.getString("coraviso5"), e, recursos);
            }
        }

    }

    private void selectionModelValueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            if (tableCorrectores.getSelectedRow() == -1) {
                textURL.setText("");
                textDescripcion.setText("");
                buttonEjecutar.setEnabled(false);
                buttonSeleccionar.setEnabled(false);
                buttonDesSeleccionar.setEnabled(false);
                buttonDel.setEnabled(false);
                buttonMod.setEnabled(false);
                buttonAdd.setEnabled(false);
            } else {
                Corrector cor = (Corrector) tableModel.getValueAt(tableCorrectores.getSelectedRow());
                try {
                    ArrayListModel<Cuestion> todas = catalogo.getTodasCuestiones();
                    tableModel2.setModel(todas, correctores.getCuestiones(todas, cor),cor);
                } catch (Exception error) {
                    mensaje.mensajeError(this, "", error, recursos);
                }
                tableGestionCorrectores.setModel(tableModel2);
                tableGestionCorrectores.setSortable(true);
                tableGestionCorrectores.setAutoCreateRowSorter(true);
                textURL.setText(cor.getURL());
                textDescripcion.setText(cor.getDescripcion());
                if(cor.getEstado()== cor.INACCESIBLE){
                    buttonEjecutar.setEnabled(false);
                    buttonSeleccionar.setEnabled(false);
                    buttonDesSeleccionar.setEnabled(false);
                }
                else{
                    buttonEjecutar.setEnabled(true);
                    buttonSeleccionar.setEnabled(true);
                    buttonDesSeleccionar.setEnabled(true);
                }
                buttonDel.setEnabled(true);
                buttonMod.setEnabled(true);
                buttonAdd.setEnabled(true);
                tableModel2.setEjecutado(false);
            }
        }
    }

    private void selectionModel2ValueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            if (tableGestionCorrectores.getSelectedRow() != -1) {
                buttonEjecutar.setEnabled(false);
            }
        }
    }

    private void textKeyReleased() {
        if (textURL.getText().isEmpty() || textDescripcion.getText().isEmpty()) {
            buttonAdd.setEnabled(false);
            buttonMod.setEnabled(false);
        } else {
            buttonAdd.setEnabled(true);
            if (tableCorrectores.getSelectedRow() != -1) {
                buttonMod.setEnabled(true);
            }
        }
    }

    private void creaWorker(boolean getPropsTodos) {
        final boolean todos = getPropsTodos;
        if (dialogEspera == null) {
            dialogEspera = new DialogEspera((Frame) getTopLevelAncestor(), recursos.getString("corworkerespera1"));
            waiterProp = new SwingWorkerCompletionWaiter(dialogEspera);
        }

        workerProp = new SwingWorker<Boolean, Void>() {

            @Override
            public Boolean doInBackground() {
                correctores.setPropiedades(todos);
                return new Boolean(todos);
            }

            @Override
            public void done() {
                try {
                    get();
                    tableModel.fireTableDataChanged();
                } catch (InterruptedException ignore) {
                } catch (java.util.concurrent.ExecutionException e) {
                    Throwable cause = e.getCause();
                    if (cause != null) {
                        System.err.println(cause.getMessage());
                    } else {
                        System.err.println(e.getMessage());
                    }
                }
            }
        };

        workerProp.addPropertyChangeListener(waiterProp);
    }

    private void creaWorkerEjecucion() {
        if (dialogEspera == null) {
            dialogEspera = new DialogEspera((Frame) getTopLevelAncestor(), recursos.getString("corworkerespera2"));
            waiterProp = new SwingWorkerCompletionWaiter(dialogEspera);
        }

        workerProp = new SwingWorker<Boolean, Void>() {

            @Override
            public Boolean doInBackground() {

                try {
                    tableModel2.setEjecutado(true);
                    java.util.List<Cuestion> todas = tableModel2.getDatos();
                    java.util.List<Cuestion> seleccionadas = tableModel2.getSeleccion();

                    for (Cuestion actual : todas) {
                        if (seleccionadas.contains(actual)) {
                            try {
                                Thread.sleep (1*1000);
                            }catch (Exception e) {
                                System.out.println("Espera error: " +e);
                            }
                            CtrlVista.ejecuta(actual);
                        }
                    }
                    CtrlVista.creaCuestion();

                } catch (Exception e) { }

                return false;
            }

            @Override
            public void done() {
                try {
                    get();
                    tableModel2.fireTableDataChanged();
                } catch (InterruptedException ignore) {
                } catch (java.util.concurrent.ExecutionException e) {
                    Throwable cause = e.getCause();
                    if (cause != null) {
                        System.err.println(cause.getMessage());
                    } else {
                        System.err.println(e.getMessage());
                    }
                }
            }
        };

        workerProp.addPropertyChangeListener(waiterProp);

    }

    private void buttonActualizarActionPerformed() {
        setPropiedades(true);

        tableModel2 = new TableModelGestioCorrectores();
        tableGestionCorrectores.setModel(tableModel2);
        tableGestionCorrectores.getColumnModel().getColumn(0).setPreferredWidth(75);
        tableGestionCorrectores.getColumnModel().getColumn(1).setPreferredWidth(40);
        tableGestionCorrectores.getColumnModel().getColumn(1).setPreferredWidth(40);
        tableGestionCorrectores.getColumnModel().getColumn(2).setPreferredWidth(550);
        tableGestionCorrectores.getColumnModel().getColumn(3).setPreferredWidth(260);
        tableGestionCorrectores.getColumnModel().getColumn(4).setPreferredWidth(225);
        tableGestionCorrectores.getColumnModel().getColumn(5).setPreferredWidth(50);
        tableGestionCorrectores.getColumnModel().getColumn(6).setPreferredWidth(50);
        tableGestionCorrectores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    private void buttonEjecutarActionPerformed() {
        creaWorkerEjecucion();
        workerProp.execute();
        dialogEspera.setVisible(true);
    }

    private void buttonSeleccionarActionPerformed(ActionEvent e){
        JButton boton = (JButton) e.getSource();
        try{
            if(boton == buttonSeleccionar){
                tableModel2.setTodos(true);
            }
            else if(boton == buttonDesSeleccionar){
                tableModel2.setTodos(false);
            }
            buttonEjecutar.setEnabled(false);
        }catch(Exception ev){
            mensaje.mensajeError(this, "", ev, recursos);
        }
    }

    private void initComponents() {
        // Component initialization
        sifCorrectores = new SimpleInternalFrame();
        scrollPane1 = new JScrollPane();
        tableCorrectores = new JXTable() {
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                return (String) getValueAt(rowAtPoint(p), columnAtPoint(p));
            }
        };
        scrollPane2 = new JScrollPane();
        tableGestionCorrectores = new JXTable();
        label1 = new JLabel();
        textURL = new JTextField();
        label2 = new JLabel();
        textDescripcion = new JTextField();
        buttonAdd = new JButton();
        buttonMod = new JButton();
        buttonDel = new JButton();
        separator1 = new JSeparator();
        panel1 = new JPanel();
        buttonActualizar = new JButton();
        buttonEjecutar = new JButton();
        buttonSeleccionar = new JButton();
        buttonDesSeleccionar = new JButton();
        selectionModel = (DefaultListSelectionModel) tableCorrectores.getSelectionModel();
        selectionModel2 = (DefaultListSelectionModel) tableGestionCorrectores.getSelectionModel();
        pipeline = new HighlighterPipeline();
        CellConstraints cc = new CellConstraints();

        //======== this ========
        setLayout(new FormLayout(
                "default:grow",
                "fill:default:grow"));

        //======== sifCorrectores ========
        {
            sifCorrectores.setTitle(recursos.getString("sifCorrectores"));
            sifCorrectores.setFrameIcon(new ImageIcon(getClass().getResource("/imagenes/panel_correctores.png")));
            Container sifCorrectoresContentPane = sifCorrectores.getContentPane();
            sifCorrectoresContentPane.setLayout(new FormLayout(
                    new ColumnSpec[]{
                        FormFactory.UNRELATED_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(65), FormSpec.NO_GROW),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
//                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
//                        new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                        new ColumnSpec(Sizes.dluX(65)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(65)),
                        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                        new ColumnSpec(Sizes.dluX(65)),
                        FormFactory.UNRELATED_GAP_COLSPEC
                    },
                    new RowSpec[]{
                        FormFactory.UNRELATED_GAP_ROWSPEC,
                        new RowSpec(Sizes.dluY(80)),
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                        FormFactory.LINE_GAP_ROWSPEC,
                        new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW),
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.LINE_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.UNRELATED_GAP_ROWSPEC
                    }));

            //======== scrollPane1 ========
            {
                scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

                //---- tableCorrectores ----
                tableCorrectores.setHighlighters(pipeline);
                tableCorrectores.setSortable(false);
                scrollPane1.setViewportView(tableCorrectores);
            }
            sifCorrectoresContentPane.add(scrollPane1, cc.xywh(2, 2, 8, 2));

            //======== scrollPane2 ========
            {
                scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

                //---- tableGestionCorrectores ----
                tableGestionCorrectores.setHighlighters(pipeline);
                tableGestionCorrectores.setSortable(false);
                scrollPane2.setViewportView(tableGestionCorrectores);
            }
            sifCorrectoresContentPane.add(scrollPane2, cc.xywh(2, 5, 8, 1));

            //---- label1 ----
            label1.setText(recursos.getString("corlabel1"));
            sifCorrectoresContentPane.add(label1, cc.xy(2, 7));

            //---- textURL ----
            textURL.addKeyListener(new KeyAdapter() {

                @Override
                public void keyReleased(KeyEvent e) {
                    textKeyReleased();
                }
            });
            sifCorrectoresContentPane.add(textURL, cc.xy(4, 7));

            //---- buttonSeleccionar ----
            buttonSeleccionar.setText(recursos.getString("corbuttonSeleccionar"));
            buttonSeleccionar.setToolTipText(recursos.getString("corbuttonSeleccionarTooltip"));
            buttonSeleccionar.setIcon(new ImageIcon(getClass().getResource("/imagenes/editlist.png")));
            buttonSeleccionar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    buttonSeleccionarActionPerformed(e);
                }
            });
            sifCorrectoresContentPane.add(buttonSeleccionar, cc.xy(7, 7));

            //---- buttonDesSeleccionar ----
            buttonDesSeleccionar.setText(recursos.getString("corbuttonDesSeleccionar"));
            buttonDesSeleccionar.setToolTipText(recursos.getString("corbuttonDesSeleccionarTooltip"));
            buttonDesSeleccionar.setIcon(new ImageIcon(getClass().getResource("/imagenes/editlist.png")));
            buttonDesSeleccionar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    buttonSeleccionarActionPerformed(e);
                }
            });
            sifCorrectoresContentPane.add(buttonDesSeleccionar, cc.xy(9, 7));

            //---- label2 ----
            label2.setText(recursos.getString("corlabel2"));
            sifCorrectoresContentPane.add(label2, cc.xy(2, 9));

            //---- textDescripcion ----
            textDescripcion.addKeyListener(new KeyAdapter() {

                @Override
                public void keyReleased(KeyEvent e) {
                    textKeyReleased();
                }
            });
            sifCorrectoresContentPane.add(textDescripcion, cc.xywh(4, 9, 6, 1));

            sifCorrectoresContentPane.add(separator1, cc.xywh(2, 13, 8, 1));

            //---- buttonAdd ----
            buttonAdd.setText(recursos.getString("corbuttonAdd"));
            buttonAdd.setIcon(new ImageIcon(getClass().getResource("/imagenes/add.png")));
            buttonAdd.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonAddActionPerformed();
                }
            });
            sifCorrectoresContentPane.add(buttonAdd, cc.xy(5, 15));

            //---- buttonMod ----
            buttonMod.setText(recursos.getString("corbuttonMod"));
            buttonMod.setIcon(new ImageIcon(getClass().getResource("/imagenes/edit.png")));
            buttonMod.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonModActionPerformed();
                }
            });
            sifCorrectoresContentPane.add(buttonMod, cc.xy(7, 15));

            //---- buttonDel ----
            buttonDel.setText(recursos.getString("corbuttonDel"));
            buttonDel.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
            buttonDel.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    buttonDelActionPerformed();
                }
            });
            sifCorrectoresContentPane.add(buttonDel, cc.xy(9, 15));

            //======== panel1 ========
            {
                panel1.setLayout(new FormLayout(
                        "65dlu, 3dlu, 65dlu, 3dlu, 65dlu, default:grow",
                        "default"));

                //---- buttonActualizar ----
                buttonActualizar.setText(recursos.getString("corbuttonActualizar"));
                buttonActualizar.setToolTipText(recursos.getString("corbuttonActualizarTooltip"));
                buttonActualizar.setIcon(new ImageIcon(getClass().getResource("/imagenes/play.png")));
                buttonActualizar.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        buttonActualizarActionPerformed();
                    }
                });
                panel1.add(buttonActualizar, cc.xy(1, 1));

                //---- buttonEjecutar ----
                buttonEjecutar.setText(recursos.getString("corbuttonEjecutar"));
                buttonEjecutar.setToolTipText(recursos.getString("corbuttonEjecutarTooltip"));
                buttonEjecutar.setIcon(new ImageIcon(getClass().getResource("/imagenes/play.png")));
                buttonEjecutar.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        buttonEjecutarActionPerformed();
                    }
                });
                panel1.add(buttonEjecutar, cc.xy(3, 1));
            }
            sifCorrectoresContentPane.add(panel1, cc.xywh(2, 15, 8, 1));

        }
        add(sifCorrectores, cc.xy(1, 1));

        //---- selectionModel ----
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                selectionModelValueChanged(e);
            }
        });

        //---- selectionModel ----
        selectionModel2.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                selectionModel2ValueChanged(e);
            }
        });

        //---- pipeline ----
        pipeline.addHighlighter(new AlternateRowHighlighter());
        // End of component initialization
    }
    
    /*********************************************************/
    /*      Variables                                        */
    /*********************************************************/
    
    private SimpleInternalFrame sifCorrectores;
    private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;
    private JXTable tableCorrectores;
    private JXTable tableGestionCorrectores;
    private JLabel label1;
    private JTextField textURL;
    private JLabel label2;
    private JTextField textDescripcion;
    private JButton buttonAdd;
    private JButton buttonMod;
    private JButton buttonDel;
    private JSeparator separator1;
    private JPanel panel1;
    private JButton buttonActualizar;
    private JButton buttonEjecutar;
    private JButton buttonSeleccionar;
    private JButton buttonDesSeleccionar;
    private DefaultListSelectionModel selectionModel;
    private DefaultListSelectionModel selectionModel2;
    private HighlighterPipeline pipeline;

} // Fin clase PanelCorrectores
