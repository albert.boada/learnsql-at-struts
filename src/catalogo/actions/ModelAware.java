package catalogo.actions;

import catalogo.modelo.DataModel;

public interface ModelAware 
{
	public void initModel(DataModel data);
}
