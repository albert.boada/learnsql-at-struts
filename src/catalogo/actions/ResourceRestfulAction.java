package catalogo.actions;

public class ResourceRestfulAction extends RestfulAction
{
	private static final long serialVersionUID = -6304217764896715791L;

	@Override
	protected String execute2() throws Exception
    {
	    if      (this._isGet())    { return this.get(); }
        else if (this._isPut())    { return this.update(); }
        else if (this._isDelete()) { return this.delete(); }
        else {
            this.getResponse().setStatus(500);
            return this.respondAsJson(null);
        }
    }

    
    protected String get() throws Exception
    {
        this.getResponse().setStatus(500);
        return this.respondAsJson(null);
    }

    /**
     *
     */
    protected String update() throws Exception
    {
        this.getResponse().setStatus(500);
        return this.respondAsJson(null);
    }

    /**
     *
     */
    protected String delete() throws Exception
    {
        this.getResponse().setStatus(500);
        return this.respondAsJson(null);
    }
}
