package catalogo.actions.questions;

import java.util.ArrayList;
import java.util.List;

import catalogo.actions.ResourcesRestfulAction;
import catalogo.dtos.CuestionDTO;
import catalogo.modelo.Cuestion;

public class QuestionsAction extends ResourcesRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    public QuestionsAction() {
    	super();
    	this.setRequestBody(new CuestionDTO());
    }

    /**
     *
     */
    @Override
    protected String index() throws Exception
    {
    	List<Cuestion> questions = this.model.cuestiones.getAll();
    	List<CuestionDTO> questionsForms = new ArrayList<CuestionDTO>();
    	int i = 0;
    	int j = 0;
    	//for (i=0;i<35;i++) {
	    	for (Cuestion c : questions) {
	    		if (i!=0) {
	    			c.setId(1000+(++j));	    			
	    		}
	    		questionsForms.add(this.model.cuestiones.entityToDTO(c));
	    	}
    	//}
        return this.respondAsJson(questionsForms);
    }

    /**
     *
     */
    @Override
    protected String create() throws Exception
    {
        CuestionDTO cf = (CuestionDTO) this.getRequestBody();
        Cuestion c = this.model.cuestiones.DTOToEntity(cf);
        
        if (c.getId() != 0) {
        	this.getResponse().setStatus(400);
        	return this.respondAsJson("invalid id");
        }
        
        Cuestion c_new =
        this.model.cuestiones.create(c);
        
        cf = this.model.cuestiones.entityToDTO(c_new);
        return this.respondAsJson(cf);
    }
}
