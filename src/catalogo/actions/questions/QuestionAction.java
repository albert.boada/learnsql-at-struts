package catalogo.actions.questions;

import catalogo.actions.ResourceRestfulAction;
import catalogo.dtos.CuestionDTO;
import catalogo.modelo.Cuestion;

public class QuestionAction extends ResourceRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    // /questions/${id}
    private int id;
    public void setId(int id) { this.id = id; }
    public int getId() { return this.id; }

    public QuestionAction() {
    	super();
    	this.setRequestBody(new CuestionDTO());
    }

    /**
     *
     */
    @Override
    protected String update() throws Exception
    {
        CuestionDTO cf = (CuestionDTO) this.getRequestBody();
        if (cf.getId() != this.getId()) {
            this.getResponse().setStatus(400);
            return this.respondAsJson("ids don't match");
        }
        Cuestion c = this.model.cuestiones.DTOToEntity(cf);

        Cuestion c_new =
        this.model.cuestiones.update(c);

        cf = this.model.cuestiones.entityToDTO(c_new);
        return this.respondAsJson(cf);
    }

    /**
     *
     */
    @Override
    protected String delete() throws Exception
    {
    	Cuestion c = this.model.cuestiones.find(this.getId());
    	this.model.cuestiones.remove(c);
        return this.respondAsJson(null);
    }
}
