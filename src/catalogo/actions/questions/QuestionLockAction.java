package catalogo.actions.questions;

import catalogo.actions.ResourceRestfulAction;
import catalogo.dtos.CuestionDTO;
import catalogo.modelo.Cuestion;

public class QuestionLockAction extends ResourceRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    // /questions/${id}/copy
    private int id;
    public void setId(int id) { this.id = id; }
    public int getId() { return this.id; }
    
    @Override
    protected String execute2() throws Exception {
    	if (this._isPost()) {
    		return this.lock();
    	} else {
    		this.getResponse().setStatus(500);
            return this.respondAsJson(null);
    	}
    }

    /**
     *
     */
    protected String lock() throws Exception
    {
    	Cuestion c = this.model.cuestiones.find(this.getId());
    	
    	this.model.cuestiones.lock(c);

		return this.respondAsJson(c);
    }
}
