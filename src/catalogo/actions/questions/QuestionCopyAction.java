package catalogo.actions.questions;

import catalogo.actions.ResourceRestfulAction;
import catalogo.dtos.CuestionDTO;
import catalogo.modelo.Cuestion;

public class QuestionCopyAction extends ResourceRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    // /questions/${id}/copy
    private int id;
    public void setId(int id) { this.id = id; }
    public int getId() { return this.id; }
    
    @Override
    protected String execute2() throws Exception {
    	if (this._isPost()) {
    		return this.copy();
    	} else {
    		this.getResponse().setStatus(500);
            return this.respondAsJson(null);
    	}
    }

    /**
     *
     */
    protected String copy() throws Exception
    {
    	Cuestion c = this.model.cuestiones.find(this.getId());
    	
    	Cuestion c_new = this.model.cuestiones.copy(c);

		CuestionDTO cf = this.model.cuestiones.entityToDTO(c_new);
		return this.respondAsJson(cf);
    }
}
