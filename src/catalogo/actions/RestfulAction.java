package catalogo.actions;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.HashMap;

public class RestfulAction extends BaseAction
{
	private static final long serialVersionUID = -6304217764896715791L;

	public String execute() throws Exception
    {
    	try {
	        if (this._isOptions()){
	        	return this.respondAsJson(null);
	        }
	        else {
	            return this.execute2();
	        }
    	}
    	catch (Exception e) {
    		this.getResponse().setStatus(400);
    		
    		HashMap response = new HashMap();
    		if (e instanceof RestfulValidationException) {
    			response.put("validation", ((RestfulValidationException) e).getValidation());
    		} else {
    			String exception = e.getClass()+"\n"+e.getMessage();
    			
    			if (!(e instanceof SQLException)) {
    				StringWriter sw = new StringWriter();
    				PrintWriter pw = new PrintWriter(sw);
    				e.printStackTrace(pw);
    				exception += "\n"+sw.toString();
    			}
    			
    			response.put("exception", exception);
    		}
    		
        	return this.respondAsJson(response);
        }
    }
	
	
   /**
    *
    */
   protected String execute2() throws Exception
   {
       this.getResponse().setStatus(500);
       return this.respondAsJson(null);
   }
	
}

