package catalogo.actions.tipussolucio;

import java.util.List;

import catalogo.actions.ResourcesRestfulAction;
import catalogo.modelo.Categoria;
import catalogo.modelo.TipoSolucion;

public class TipussolucioIndexAction extends ResourcesRestfulAction
{
    private static final long serialVersionUID = 6665361821971022864L;

    /**
     *
     */
    @Override
    protected String index() throws Exception
    {
        List<TipoSolucion> tipussolucio = this.model.tiposSolucion.getAll();
        return this.respondAsJson(tipussolucio);
    }
}
