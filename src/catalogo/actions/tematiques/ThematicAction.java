package catalogo.actions.tematiques;

import org.apache.commons.lang3.StringEscapeUtils;

import catalogo.actions.ResourceRestfulAction;
import catalogo.dtos.TematicaDTO;
import catalogo.modelo.Tematica;

public class ThematicAction extends ResourceRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    // /thematics/${id}
    private String id;
    public void setId(String id) { this.id = StringEscapeUtils.unescapeHtml4(id); }
    public String getId() { return this.id; }

    public ThematicAction() {
        super();
        this.setRequestBody(new TematicaDTO());
    }

    /**
     *
     */
    @Override
    protected String update() throws Exception
    {
    	TematicaDTO tem_form = (TematicaDTO) this.getRequestBody();
        if (!tem_form.getId().equals(this.getId())) {
            this.getResponse().setStatus(400);
            return this.respondAsJson("ids don't match");
        }
        Tematica tem = this.model.tematicas.formToEntity(tem_form);

        Tematica tem_new =
        this.model.tematicas.update(tem, getId());

        tem_form = this.model.tematicas.entityToForm(tem_new);
        return this.respondAsJson(tem_form);
    }

    /**
     *
     */
    @Override
    protected String delete() throws Exception
    {
        Tematica tem = this.model.tematicas.find(this.getId());
        this.model.tematicas.remove(tem);
        return this.respondAsJson(null);
    }
}
