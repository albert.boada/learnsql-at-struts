package catalogo.actions.tematiques;

import java.util.ArrayList;
import java.util.List;

import catalogo.actions.ResourcesRestfulAction;
import catalogo.dtos.CategoriaDTO;
import catalogo.dtos.CuestionDTO;
import catalogo.dtos.TematicaDTO;
import catalogo.modelo.Categoria;
import catalogo.modelo.Cuestion;
import catalogo.modelo.Tematica;

public class ThematicsAction extends ResourcesRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    public ThematicsAction() {
        super();
        this.setRequestBody(new TematicaDTO());
    }
    
    /**
     *
     */
    @Override
    protected String index() throws Exception
    {
        List<Tematica> thematics = this.model.tematicas.getAll();
        
        List<TematicaDTO> thematicsForms = new ArrayList<TematicaDTO>();
    	for (Tematica t : thematics) {
    		thematicsForms.add(this.model.tematicas.entityToForm(t));
    	}
        
    	return this.respondAsJson(thematicsForms);
    }

    /**
     *
     */
    @Override
    protected String create() throws Exception
    {
    	TematicaDTO tem_form = (TematicaDTO) this.getRequestBody();
        if (!tem_form.getId().equals("0")) {
        	this.getResponse().setStatus(400);
        	return this.respondAsJson("invalid id");
        }
        Tematica tem = this.model.tematicas.formToEntity(tem_form);

        Tematica tem_new =
        this.model.tematicas.create(tem);

        tem_form = this.model.tematicas.entityToForm(tem_new);
        return this.respondAsJson(tem_form);
    }
}
