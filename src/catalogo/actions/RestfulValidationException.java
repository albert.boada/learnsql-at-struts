package catalogo.actions;

public class RestfulValidationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3425397091621012937L;
	
	private Object validation;
	
	public RestfulValidationException(Object validation) {
		super();
		this.setValidation(validation);
	}

	public Object getValidation() {
		return validation;
	}

	public void setValidation(Object validation) {
		this.validation = validation;
	}

}
