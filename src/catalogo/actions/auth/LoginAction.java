package catalogo.actions.auth;

import java.util.Map;

import catalogo.actions.BaseAction;
import catalogo.auth.Login;
import catalogo.auth.LoginService;

public class LoginAction extends BaseAction
{
	private static final long serialVersionUID = 4690536483815559242L;
	
	private Login login;
	public Login getLogin() { return login; }
	public void setLogin(Login login) { this.login = login; }
	
	public LoginAction() throws Exception {
		super();
		this.setView("login"); //Default Render View
		this.setTitleForLayout("Login"); //Default Render View
	}
	
	public String execute() {
		//si ja estem loginats, no deixem accedir al formulari de login
		if (session.get("login") != null) {
			return SUCCESS;
		}
		
		// this.setLayout("login"); //definim login layout
		
		//si ens arriben dades de login a través del formulari...
		if (this.login != null) {
			LoginService service = new LoginService();
			
			//validate input
			//validate credentials
			if (service.doLogin(this.login)) {
				//save credentials to session
				this.session.put("login", this.login);
				
				String loginRedirect = (String)this.session.get("loginRedirect");
				this.session.remove("loginRedirect");
				if (loginRedirect != null) {
					return this.redirect(loginRedirect);
				}
				
				return SUCCESS;
			}
			else {
				Map<String,String> validation = service.getValidation();
				if (!validation.isEmpty()) {
					for (Map.Entry<String, String> entry : validation.entrySet()) {
						this.addFieldError(entry.getKey(),entry.getValue());
					}
				}
				else {
					//error connection
					this.addActionMessage("Credencials no vàlides");
				}
				//return this.render();
			}
		}
		
		return this.render();
	}
}
