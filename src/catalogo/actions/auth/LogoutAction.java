package catalogo.actions.auth;

import catalogo.actions.BaseAction;

public class LogoutAction extends BaseAction
{
	private static final long serialVersionUID = 4690536483815559242L;
	
	public String execute() {
		this.session.remove("login");
		//this.redirect="/login";
		return SUCCESS;
	}
}
