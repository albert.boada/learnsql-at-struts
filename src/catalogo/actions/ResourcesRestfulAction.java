package catalogo.actions;

public class ResourcesRestfulAction extends RestfulAction
{
	private static final long serialVersionUID = 5083149093141967249L;

	@Override
	protected String execute2() throws Exception
    {
        if      (this._isGet())  { return this.index(); }
        else if (this._isPost()) { return this.create(); }
        else {
            this.getResponse().setStatus(500);
            return this.respondAsJson(null);
        }
    }

    /**
     * override, plz
     */
    protected String index() throws Exception
    {
        this.getResponse().setStatus(500);
        return this.respondAsJson(null);
    }

    /**
     * override, plz
     */
    protected String create() throws Exception
    {
        this.getResponse().setStatus(500);
        return this.respondAsJson(null);
    }
}
