package catalogo.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import catalogo.modelo.DataModel;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

public class BaseAction extends ActionSupport implements ModelAware, SessionAware, ServletRequestAware, ServletResponseAware
{
	private static final long serialVersionUID = 5764795954923715072L;

	public static final String RENDER   = "render";
	public static final String REDIRECT = "redirect";
	public static final String JSON     = "json";

	/**
	 * DatalayerAware
	 */
	protected DataModel model;
	public void initModel(DataModel model) { this.model = model; }

	/**
	 * SessionAware
	 */
	protected Map session;
	public void setSession(Map<String, Object> session) { this.session=session; }

	/**
	 * ServletRequestAware
	 */
	private HttpServletRequest __request;
	public HttpServletRequest getRequest() { return this.__request; }
	public void setServletRequest(HttpServletRequest request) { this.__request = request; }

	/**
	 * ServletResponseAware
	 */
	private HttpServletResponse __response;
	public HttpServletResponse getResponse() { return this.__response; }
	public void setServletResponse(HttpServletResponse response) { this.__response = response; }

	private String view="index";
	public String getView() { return this.view; }
	public void setView(String view) { this.view=view; }

	private String layout="default";
	public String getLayout() { return this.layout; }
	public void setLayout(String layout) { this.layout=layout; }

	private String layoutview;
	public String getLayoutview() { return this.layoutview; }
	public void setLayoutview(String layoutview) { this.layoutview=layoutview; }

	private String title_for_layout="";
	public String getTitleForLayout() { return this.title_for_layout; }
	public void setTitleForLayout(String title) { this.title_for_layout=title; }

	private String redirect="/";
	public String getRedirect() { return this.redirect; }
	public void setRedirect(String redirect) { this.redirect=redirect; }

	protected ArrayList<Map<String,String>> breadcrumb;
	public ArrayList<Map<String,String>> getBreadcrumb() { return this.breadcrumb; }
	public void setBreadcrumb(ArrayList<Map<String,String>> breadcrumb) { this.breadcrumb=breadcrumb; }

	private Object __result;
	public Object getResult() { return this.__result; }
	public void setResult(Object result) { this.__result = result; }
	
    private Object __requestBody = "null"; // never be NULL 
    public void setRequestBody(Object data) { this.__requestBody = data; }
    public Object getRequestBody() { return this.__requestBody; }

	protected ActionContext context;
	protected ValueStack stack;

	//* Render View
	public String render()
	{
		this.layoutview = this.view;
		return RENDER;
	}
	public String render(String view)
	{
		this.layoutview = view;
		return RENDER;
	}

	//* Redirect
	public String redirect()
	{
		return REDIRECT;
	}
	public String redirect(String url)
	{
		this.redirect=url;
		return REDIRECT;
	}

	//* JSON response
	public String respondAsJson(Object result)
	{
		this.getResponse().addHeader("Access-Control-Allow-Origin", "*");
		this.getResponse().addHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
		this.getResponse().addHeader("Access-Control-Allow-Credentials", "*");
	    this.getResponse().addHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

		this.__result = result;
		return JSON;
	}

	/**
	 *
	 */
	protected Boolean _isGet()     { return this.getRequest().getMethod().equals("GET"); }
	protected Boolean _isPost()    { return this.getRequest().getMethod().equals("POST"); }
	protected Boolean _isPut()     { return this.getRequest().getMethod().equals("PUT"); }
	protected Boolean _isDelete()  { return this.getRequest().getMethod().equals("DELETE"); }
	protected Boolean _isOptions() { return this.getRequest().getMethod().equals("OPTIONS"); }
}
