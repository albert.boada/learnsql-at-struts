package catalogo.actions.categories;

import java.lang.reflect.Field;
import java.util.List;

import catalogo.actions.ResourceRestfulAction;
import catalogo.dtos.CategoriaDTO;
import catalogo.modelo.Categoria;

public class CategoryAction extends ResourceRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    // /categories/${id}
    private int id;
    public void setId(int id) { this.id = id; }
    public int getId() { return this.id; }

    public CategoryAction() {
        super();
        this.setRequestBody(new CategoriaDTO());
    }

    /**
     *
     */
    @Override
    protected String update() throws Exception
    {
        CategoriaDTO cat_form = (CategoriaDTO) this.getRequestBody();
        if (cat_form.getId() != this.getId()) {
            this.getResponse().setStatus(400);
            return this.respondAsJson("ids don't match");
        }
        Categoria cat = this.model.categorias.formToEntity(cat_form);

        Categoria cat_new =
        this.model.categorias.update(cat, cat_form.getId());

        
        cat_form = this.model.categorias.entityToForm(cat_new);
        return this.respondAsJson(cat_form);
    }

    /**
     *
     */
    @Override
    protected String delete() throws Exception
    {
        Categoria cat = this.model.categorias.find(this.getId());
        this.model.categorias.remove(cat);
        return this.respondAsJson(null);
    }
}
