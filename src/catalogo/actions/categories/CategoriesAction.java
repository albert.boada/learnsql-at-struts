package catalogo.actions.categories;

import java.util.ArrayList;
import java.util.List;

import catalogo.actions.ResourcesRestfulAction;
import catalogo.dtos.CategoriaDTO;
import catalogo.dtos.CuestionDTO;
import catalogo.modelo.Categoria;
import catalogo.modelo.Cuestion;

public class CategoriesAction extends ResourcesRestfulAction
{
    private static final long serialVersionUID = 6665361821971022864L;

    public CategoriesAction() {
    	super();
    	this.setRequestBody(new CategoriaDTO());
    }

    /**
     *
     */
    @Override
    protected String index() throws Exception
    {
    	List<Categoria> categorias = this.model.categorias.getAll();
    	List<CategoriaDTO> categoriasForms = new ArrayList<CategoriaDTO>();
    	for (Categoria cat : categorias) {
    		categoriasForms.add(this.model.categorias.entityToForm(cat));
    	}

        return this.respondAsJson(categoriasForms);
    }

    /**
     *
     */
    @Override
    protected String create() throws Exception
    {
    	CategoriaDTO cat_form = (CategoriaDTO) this.getRequestBody();
        if (cat_form.getId() != 0) {
        	this.getResponse().setStatus(400);
        	return this.respondAsJson("invalid id");
        }
        Categoria cat = this.model.categorias.formToEntity(cat_form);

        Categoria cat_new =
        this.model.categorias.create(cat);

        cat_form = this.model.categorias.entityToForm(cat_new);
        return this.respondAsJson(cat_form);
    }
}
