package catalogo.actions.esquemes;

import org.apache.commons.lang3.StringEscapeUtils;

import catalogo.actions.ResourceRestfulAction;
import catalogo.dtos.EsquemaDTO;
import catalogo.modelo.Esquema;

public class SchemaAction extends ResourceRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    // /schemas/${id}
    private String id;
    public void setId(String id) { this.id = StringEscapeUtils.unescapeHtml4(id); }
    public String getId() { return this.id; }

    public SchemaAction() {
        super();
        this.setRequestBody(new EsquemaDTO());
    }

    /**
     *
     */
    @Override
    protected String update() throws Exception
    {
        EsquemaDTO esq_form = (EsquemaDTO) this.getRequestBody();
        if (!esq_form.getId().equals(this.getId())) {
            this.getResponse().setStatus(400);
            return this.respondAsJson("ids don't match");
        }
        Esquema esq = this.model.esquemas.formToEntity(esq_form);

        Esquema esq_new =
        this.model.esquemas.update(esq, getId());

        esq_form = this.model.esquemas.entityToForm(esq_new);
        return this.respondAsJson(esq_form);
    }

    /**
     *
     */
    @Override
    protected String delete() throws Exception
    {
        Esquema esq = this.model.esquemas.find(this.getId());
        this.model.esquemas.remove(esq);
        return this.respondAsJson(null);
    }
}
