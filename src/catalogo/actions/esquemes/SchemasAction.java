package catalogo.actions.esquemes;

import java.util.ArrayList;
import java.util.List;

import catalogo.actions.ResourcesRestfulAction;
import catalogo.dtos.EsquemaDTO;
import catalogo.modelo.Esquema;

public class SchemasAction extends ResourcesRestfulAction
{
    private static final long serialVersionUID = -8635730566279987130L;

    public SchemasAction() {
        super();
        this.setRequestBody(new EsquemaDTO());
    }

    /**
     *
     */
    @Override
    protected String index() throws Exception
    {
        List<Esquema> schemas = this.model.esquemas.getAll();

        List<EsquemaDTO> schemasForms = new ArrayList<EsquemaDTO>();
    	for (Esquema esq : schemas) {
    		schemasForms.add(this.model.esquemas.entityToForm(esq));
    	}

    	return this.respondAsJson(schemasForms);
    }

    /**
     *
     */
    @Override
    protected String create() throws Exception
    {
    	EsquemaDTO esq_form = (EsquemaDTO) this.getRequestBody();
        if (!esq_form.getId().equals("0")) {
        	this.getResponse().setStatus(400);
        	return this.respondAsJson("invalid id");
        }
        Esquema esq = this.model.esquemas.formToEntity(esq_form);

        Esquema esq_new =
        this.model.esquemas.create(esq);

        esq_form = this.model.esquemas.entityToForm(esq_new);
        return this.respondAsJson(esq_form);
    }
}
