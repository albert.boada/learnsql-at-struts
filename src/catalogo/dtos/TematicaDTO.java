package catalogo.dtos;

import java.util.ArrayList;

public class TematicaDTO {
	protected String id;
    protected String nombre;
    protected String descripcion;
    protected ArrayList<Integer> categoriasIds = new ArrayList<Integer>();
    
	public String getId() {
		return this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public ArrayList<Integer> getCategoriasIds() {
		return categoriasIds;
	}
	public void setCategoriasIds(ArrayList<Integer> categoriasIds) {
		this.categoriasIds = categoriasIds;
	}
}
