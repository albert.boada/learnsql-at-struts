package catalogo.dtos;

public class CategoriaDTO {
    protected int id;
	protected String id_editable;
    protected String descripcio;
    protected boolean inits;
    protected boolean postinits;
    protected boolean limpieza;
    protected boolean estado;
    protected boolean solucion;
    protected boolean initsJP;
    protected boolean entradaJP;
    protected boolean limpiezaJP;
    protected boolean comprobaciones;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getId_editable() {
		return id_editable;
	}
	public void setId_editable(String id_editable) {
		this.id_editable = id_editable;
	}
	public String getDescripcio() {
		return descripcio;
	}
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
	public boolean isInits() {
		return inits;
	}
	public void setInits(boolean inits) {
		this.inits = inits;
	}
	public boolean isPostinits() {
		return postinits;
	}
	public void setPostinits(boolean postinits) {
		this.postinits = postinits;
	}
	public boolean isLimpieza() {
		return limpieza;
	}
	public void setLimpieza(boolean limpieza) {
		this.limpieza = limpieza;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public boolean isSolucion() {
		return solucion;
	}
	public void setSolucion(boolean solucion) {
		this.solucion = solucion;
	}
	public boolean isInitsJP() {
		return initsJP;
	}
	public void setInitsJP(boolean initsJP) {
		this.initsJP = initsJP;
	}
	public boolean isEntradaJP() {
		return entradaJP;
	}
	public void setEntradaJP(boolean entradaJP) {
		this.entradaJP = entradaJP;
	}
	public boolean isLimpiezaJP() {
		return limpiezaJP;
	}
	public void setLimpiezaJP(boolean limpiezaJP) {
		this.limpiezaJP = limpiezaJP;
	}
	public boolean isComprobaciones() {
		return comprobaciones;
	}
	public void setComprobaciones(boolean comprobaciones) {
		this.comprobaciones = comprobaciones;
	}
    
    
}
