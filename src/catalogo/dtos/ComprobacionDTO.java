package catalogo.dtos;

public class ComprobacionDTO {
    private String id;
    private String nombre;
    private String descripcion;
    private String msgError;
    private String msgErrorEsp;
    private String msgErrorIng;
    private String entrada;
    private String salida;
    private String salidaAlumno;
    private Boolean binaria;
    private Boolean consumeix;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	public String getMsgErrorEsp() {
		return msgErrorEsp;
	}
	public void setMsgErrorEsp(String msgErrorEsp) {
		this.msgErrorEsp = msgErrorEsp;
	}
	public String getMsgErrorIng() {
		return msgErrorIng;
	}
	public void setMsgErrorIng(String msgErrorIng) {
		this.msgErrorIng = msgErrorIng;
	}
	public String getEntrada() {
		return entrada;
	}
	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}
	public String getSalida() {
		return salida;
	}
	public void setSalida(String salida) {
		this.salida = salida;
	}
	public String getSalidaAlumno() {
		return salidaAlumno;
	}
	public void setSalidaAlumno(String salidaAlumno) {
		this.salidaAlumno = salidaAlumno;
	}
	public Boolean getBinaria() {
		return binaria;
	}
	public void setBinaria(Boolean binaria) {
		this.binaria = binaria;
	}
	public Boolean getConsumeix() {
		return consumeix;
	}
	public void setConsumeix(Boolean consumeix) {
		this.consumeix = consumeix;
	}
}
