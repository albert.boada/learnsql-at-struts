package catalogo.dtos;

import java.util.ArrayList;

/**
 *
 */
public class CuestionDTO
{

/*******************************************************************************
 * Properties
 */

    protected int idCuestion;
    public int getId() { return this.idCuestion; }
    public void setId(int id) { this.idCuestion = id; }

    protected String titulo;
    public String getTitulo() { return titulo; }
	public void setTitulo(String titulo) { this.titulo = titulo; }

	// Phrasing
    protected String enunciado;
    protected String enunciadoEsp;
    protected String enunciadoIng;
    protected String ficheroAdjunto;
    protected String extensionAdjunto;

    // Classification
    protected String         autor;
    protected float          dificultad;
    protected int            categoriaId;
    protected String         esquemaId;
    protected ArrayList<String> tematicasIds = new ArrayList<String>();
    protected Boolean        binaria;
    protected Boolean        gabia;

    // SQL Statements
    protected String inits;
    protected String solucion;
    protected String solucionAlumno;
    protected String limpieza;
    protected String postInits;
    protected int    postInitsJP;

    // Solution
    protected int     tiposolucionId;
    protected String  url;
    protected String  usuario;
    protected String  clave;
    protected Boolean ssl;
    protected String ficheroSolucion;
    protected String extensionSolucion;

    protected ArrayList<JuegoPruebasDTO> juegosPruebas = new ArrayList<JuegoPruebasDTO>();

    // State
    protected boolean disponible;
    protected boolean bloqueada;

    protected String estado;



	public String getEnunciado() {
		return enunciado;
	}
	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}
	public String getEnunciadoEsp() {
		return enunciadoEsp;
	}
	public void setEnunciadoEsp(String enunciadoEsp) {
		this.enunciadoEsp = enunciadoEsp;
	}
	public String getEnunciadoIng() {
		return enunciadoIng;
	}
	public void setEnunciadoIng(String enunciadoIng) {
		this.enunciadoIng = enunciadoIng;
	}
	public String getFicheroAdjunto() {
		return ficheroAdjunto;
	}
	public void setFicheroAdjunto(String ficheroAdjunto) {
		this.ficheroAdjunto = ficheroAdjunto;
	}
	public String getExtensionAdjunto() {
		return extensionAdjunto;
	}
	public void setExtensionAdjunto(String extensionAdjunto) {
		this.extensionAdjunto = extensionAdjunto;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public float getDificultad() {
		return dificultad;
	}
	public void setDificultad(float dificultad) {
		this.dificultad = dificultad;
	}
	public int getCategoriaId() {
		return categoriaId;
	}
	public void setCategoriaId(int categoria_id) {
		this.categoriaId = categoria_id;
	}
	public String getEsquemaId() {
		return esquemaId;
	}
	public void setEsquemaId(String esquema_id) {
		this.esquemaId = esquema_id;
	}
	public ArrayList<String> getTematicasIds() {
		return tematicasIds;
	}
	public void setTematicasIds(ArrayList<String> tematicas_ids) {
		this.tematicasIds = tematicas_ids;
	}
	public Boolean getBinaria() {
		return binaria;
	}
	public void setBinaria(Boolean binaria) {
		this.binaria = binaria;
	}
	public Boolean getGabia() {
		return gabia;
	}
	public void setGabia(Boolean gabia) {
		this.gabia = gabia;
	}
	public String getInits() {
		return inits;
	}
	public void setInits(String inits) {
		this.inits = inits;
	}
	public String getSolucion() {
		return solucion;
	}
	public void setSolucion(String solucion) {
		this.solucion = solucion;
	}
	public String getSolucionAlumno() {
		return solucionAlumno;
	}
	public void setSolucionAlumno(String solucionAlumno) {
		this.solucionAlumno = solucionAlumno;
	}
	public String getLimpieza() {
		return limpieza;
	}
	public void setLimpieza(String limpieza) {
		this.limpieza = limpieza;
	}
	public String getPostInits() {
		return postInits;
	}
	public void setPostInits(String postInits) {
		this.postInits = postInits;
	}
	public int getPostInitsJP() {
		return postInitsJP;
	}
	public void setPostInitsJP(int postInitsJP) {
		this.postInitsJP = postInitsJP;
	}
	public int getTiposolucionId() {
		return tiposolucionId;
	}
	public void setTiposolucionId(int tiposolucion_id) {
		this.tiposolucionId = tiposolucion_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Boolean getSsl() {
		return ssl;
	}
	public void setSsl(Boolean ssl) {
		this.ssl = ssl;
	}
	public String getFicheroSolucion() {
		return ficheroSolucion;
	}
	public void setFicheroSolucion(String ficheroSolucion) {
		this.ficheroSolucion = ficheroSolucion;
	}
	public String getExtensionSolucion() {
		return extensionSolucion;
	}
	public void setExtensionSolucion(String extensionSolucion) {
		this.extensionSolucion = extensionSolucion;
	}
	public ArrayList<JuegoPruebasDTO> getJuegosPruebas() {
		return juegosPruebas;
	}
	public void setJuegosPruebas(ArrayList<JuegoPruebasDTO> juegosPruebas) {
		this.juegosPruebas = juegosPruebas;
	}
	public boolean isDisponible() {
		return disponible;
	}
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	public boolean isBloqueada() {
		return bloqueada;
	}
	public void setBloqueada(boolean bloqueada) {
		this.bloqueada = bloqueada;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String toString() {
		return this.getAutor()+" "+this.getId()+" "+this.getTitulo();
	}
}