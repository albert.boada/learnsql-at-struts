package catalogo.dtos;

import java.util.ArrayList;

public class JuegoPruebasDTO {
    private String id;
    private String nombre;
    private float peso;
    private String descripcion;
    private String msgError;
    private String msgErrorEsp;
    private String msgErrorIng;
    private String inits;
    private String entrada;
    private String salida;
    private String salidaAlumno;
    private String limpieza;
    private Boolean binaria;
    private Boolean mensaje;
    private Boolean consumeix;
    private ArrayList<ComprobacionDTO> comprobaciones = new ArrayList<ComprobacionDTO>();

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getPeso() {
		return peso;
	}
	public void setPeso(float peso) {
		this.peso = peso;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	public String getMsgErrorEsp() {
		return msgErrorEsp;
	}
	public void setMsgErrorEsp(String msgErrorEsp) {
		this.msgErrorEsp = msgErrorEsp;
	}
	public String getMsgErrorIng() {
		return msgErrorIng;
	}
	public void setMsgErrorIng(String msgErrorIng) {
		this.msgErrorIng = msgErrorIng;
	}
	public String getInits() {
		return inits;
	}
	public void setInits(String inits) {
		this.inits = inits;
	}
	public String getEntrada() {
		return entrada;
	}
	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}
	public String getSalida() {
		return salida;
	}
	public void setSalida(String salida) {
		this.salida = salida;
	}
	public String getSalidaAlumno() {
		return salidaAlumno;
	}
	public void setSalidaAlumno(String salidaAlumno) {
		this.salidaAlumno = salidaAlumno;
	}
	public String getLimpieza() {
		return limpieza;
	}
	public void setLimpieza(String limpieza) {
		this.limpieza = limpieza;
	}
	public Boolean getBinaria() {
		return binaria;
	}
	public void setBinaria(Boolean binaria) {
		this.binaria = binaria;
	}
	public Boolean getMensaje() {
		return mensaje;
	}
	public void setMensaje(Boolean mensaje) {
		this.mensaje = mensaje;
	}
	public Boolean getConsumeix() {
		return consumeix;
	}
	public void setConsumeix(Boolean consumeix) {
		this.consumeix = consumeix;
	}
	public ArrayList<ComprobacionDTO> getComprobaciones() {
		return comprobaciones;
	}
	public void setComprobaciones(ArrayList<ComprobacionDTO> comprobaciones) {
		this.comprobaciones = comprobaciones;
	}
}
