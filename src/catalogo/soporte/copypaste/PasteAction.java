//////////////////////////////////////////////////////
//  CLASSE: PasteAction.java                        //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.copypaste;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.text.JTextComponent;

/**
 *
 * Action class to provide an action to Paste into a component.
 *
 * @author steve.webb
 */
class PasteAction extends AbstractAction
{
    /**
     * The component the action is associated with.
     */
    JTextComponent comp;
    
    /**
     * Default constructor.
     * @param comp The component the action is associated with.
     */
    public PasteAction(JTextComponent comp)
    {
        super("Pegar" /*,icon*/);
        this.comp = comp;
    }
    
    /**
     * Action has been performed on the component.
     * @param e ignored
     */
    public void actionPerformed(ActionEvent e)
    {
        comp.paste();
    }
    
    /**
     * Checks if the action can be performed.
     * @return True if the action is allowed
     */
    public boolean isEnabled()
    {
        if (comp.isEditable() && comp.isEnabled())
        {
            Transferable contents = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(this);
            return contents.isDataFlavorSupported(DataFlavor.stringFlavor);
        }
        else
            return false;
    }
}//fin clase PasteAction

