//////////////////////////////////////////////////////
//  CLASSE: SelectAllAction.java                    //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.copypaste;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 *
 * Action class to provide an action to selectAll on a component.
 *
 * @author steve.webb
 */
class SelectAllAction extends AbstractAction
{
    /**
     * Icon to displayed against this action.
     */
    //static final private ImageIcon icon =
    //        new ImageIcon(ClassLoader.getSystemResource("toolbarButtonGraphics/general/Import16.gif"));
    
    /**
     * The component the action is associated with.
     */
    protected JTextComponent comp;
    
    /**
     * Default constructor.
     * @param comp The component the action is associated with.
     */
    public SelectAllAction(JTextComponent comp)
    {
        super("Seleccionar Todo" /*,icon*/);
        this.comp = comp;
    }
    
    /**
     * Action has been performed on the component.
     * @param e ignored
     */
    public void actionPerformed(ActionEvent e)
    {
        comp.selectAll();
        /* Need to also selectAll() via a later because in the case of FormattedText fields
         * the field is re-drawn if the request is made durring a focusGained event.
         * This is a pain but there doesn't appear to be any need solution to this and it is
         * a known swing bug but it isn't going to be fixed anytime soon. */
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                comp.selectAll();
            }
        });
        
    }
    
    /**
     * Checks if the action can be performed.
     * @return True if the action is allowed
     */
    public boolean isEnabled()
    {
        return comp.isEnabled()
        && comp.getText().length()>0;
    }
}//fin clase SelectAllAction