//////////////////////////////////////////////////////
//  CLASSE: ContextMouseAdapter.java                //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.copypaste;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 *
 * Action class to provide an action to selectAll on a component.
 *
 * @author steve.webb
 */
public class ContextMouseAdapter extends MouseAdapter
{
    public ContextMouseAdapter(){
    }
    
    public void mouseClicked(MouseEvent evt)
    {
        evt.getComponent().requestFocus();
        dealWithMousePress(evt);
    }
    
    protected void dealWithMousePress(MouseEvent evt)
    {
        // Only interested in the right button
        if(SwingUtilities.isRightMouseButton(evt))
        {
            //if(MenuSelectionManager.defaultManager().getSelectedPath().length>0)
            //return;
            
            JPopupMenu menu = new JPopupMenu();
            menu.add(new CutAction((JTextComponent) evt.getComponent()));
            menu.add(new CopyAction((JTextComponent) evt.getComponent()));
            menu.add(new PasteAction((JTextComponent) evt.getComponent()));
            menu.add(new DeleteAction((JTextComponent) evt.getComponent()));
            menu.addSeparator();
            menu.add(new SelectAllAction((JTextComponent) evt.getComponent()));
            
            // Display the menu
            Point pt = SwingUtilities.convertPoint(evt.getComponent(), evt.getPoint(), evt.getComponent());
            menu.show(evt.getComponent(), pt.x, pt.y);
        }
    }
    
} // Fin clase ContextMouseAdapter
