//////////////////////////////////////////////////////
//  CLASSE: CopyAction.java                         //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.copypaste;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.text.JTextComponent;

/**
 *
 * Action class to provide an action to Copy from a component.
 *
 * @author steve.webb
 */
class CopyAction extends AbstractAction
{
    /**
     * Icon to displayed against this action.
     */
    //static final private ImageIcon icon =
    //        new ImageIcon(ClassLoader.getSystemResource("toolbarButtonGraphics/general/Copy16.gif"));
    
    /**
     * The component the action is associated with.
     */
    JTextComponent comp;
    
    /**
     * Default constructor.
     * @param comp The component the action is associated with.
     */
    public CopyAction(JTextComponent comp)
    {
        super("Copiar" /*,icon*/);
        this.comp = comp;
    }
    
    /**
     * Action has been performed on the component.
     * @param e ignored
     */
    public void actionPerformed(ActionEvent e)
    {
        comp.copy();
    }
    
    /**
     * Checks if the action can be performed.
     * @return True if the action is allowed
     */
    public boolean isEnabled()
    {
        return comp.isEnabled()
        && comp.getSelectedText()!=null;
    }
} //Fin clase CopyAction