//////////////////////////////////////////////////////
//  CLASSE: TreeTableEjecucionCellRenderer.java     //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.renderers;

import catalogo.modelo.Comprobacion;
import catalogo.modelo.Cuestion;
import catalogo.modelo.JuegoPruebas;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.ImageIcon;

public class TreeTableEjecucionCellRenderer extends DefaultTreeCellRenderer {

    private Icon iconoCuestion, iconoJP, iconoCompr;

    public TreeTableEjecucionCellRenderer() {
        iconoCuestion = loadImage("imagenes/bullet_red.png");
        iconoJP = loadImage("imagenes/bullet_black.png");
        iconoCompr = loadImage("imagenes/bullet_blue.png");
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        if (value instanceof JuegoPruebas) setIcon(iconoJP);
        else if (value instanceof Comprobacion) setIcon(iconoCompr);
        else if (value instanceof Cuestion) setIcon(iconoCuestion);
        return this;
    }
    
    private ImageIcon loadImage(String imageName) {
        ImageIcon icon  = null;
        try {
            ClassLoader classloader = getClass().getClassLoader();
            java.net.URL url = classloader.getResource(imageName);
            if ( url != null ) {
                icon = new ImageIcon(url);
            }
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
        return icon;
   }
      
} // Fin clase TreeTableEjecucionCellRenderer
