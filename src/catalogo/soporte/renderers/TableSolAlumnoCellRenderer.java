//////////////////////////////////////////////////////
//  CLASSE: TableSolAlumnoCellRenderer.java         //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.renderers;

import catalogo.modelo.Comprobacion;
import catalogo.modelo.Cuestion;
import catalogo.modelo.JuegoPruebas;
import java.awt.Component;
import javax.swing.*;
import javax.swing.Icon;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.JTree;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;
import javax.swing.ImageIcon;

//public class TableSolAlumnoCellRenderer extends DefaultTreeCellRenderer {
//public class TableSolAlumnoCellRenderer extends JTextArea extends DefaultTreeCellRenderer {
//public class TableSolAlumnoCellRenderer extends JTextArea implements DefaultTreeCellRenderer {
//public class TableSolAlumnoCellRenderer extends JTextArea implements TreeCellRenderer {
public class TableSolAlumnoCellRenderer extends JTextArea implements TableCellRenderer {

public static final long serialVersionUID = 24362462L;

    private Icon iconoCuestion, iconoJP, iconoCompr;
    private JTextArea etiqueta;

    public TableSolAlumnoCellRenderer() {
        iconoCuestion = loadImage("imagenes/bullet_red.png");
        iconoJP = loadImage("imagenes/bullet_black.png");
        iconoCompr = loadImage("imagenes/bullet_blue.png");

        etiqueta = new JTextArea();

//        setWrapStyleWord(true);
//        setLineWrap(true);
//        setOpaque ( true ) ;
    }

//    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
//            boolean leaf, int row, boolean hasFocus) {
//
//        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
//        if (value instanceof JuegoPruebas) setIcon(iconoJP);
////        if (value instanceof JuegoPruebas) add(iconoJP);
//        else if (value instanceof Comprobacion) setIcon(iconoCompr);
//        else if (value instanceof Cuestion) setIcon(iconoCuestion);
         
//         if (isSelected)
//             etiqueta.setBackground (Color.CYAN);
//         else
//             etiqueta.setBackground (Color.YELLOW);

//        if (value instanceof String)
//         {
//                 etiqueta.setOpaque(true);
//                 etiqueta.setText((String)value);
//                 setText((String)value);
//         }

//         if (value instanceof Integer)
//         {
//             int valor = ((Integer)value).intValue();
//             if (valor > 60)
//                etiqueta.setIcon (iconos[2]);
//             else if (valor > 30)
//                 etiqueta.setIcon (iconos[1]);
//             else
//                 etiqueta.setIcon (iconos[0]);
//             etiqueta.setToolTipText (Integer.toString (valor));
//         }

//         return this;
//     }

     public Component getTableCellRendererComponent ( JTable table, Object value,
       boolean isSelected, boolean hasFocus, int row, int column ) {

         setText (( value == null ) ? "" : value.toString ()) ;
         return this;
     }

    private ImageIcon loadImage(String imageName) {
        ImageIcon icon  = null;
        try {
            ClassLoader classloader = getClass().getClassLoader();
            java.net.URL url = classloader.getResource(imageName);
            if ( url != null ) {
                icon = new ImageIcon(url);
            }
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
        return icon;
   }

    

} // Fin clase TableSolAlumnoCellRenderer
