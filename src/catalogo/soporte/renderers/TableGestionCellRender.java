//////////////////////////////////////////////////////
//  CLASSE: TableGestionCellRender.java             //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.renderers;

import java.awt.Component;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;


public class TableGestionCellRender implements TableCellRenderer{

    public TableGestionCellRender() {

    }

    public Component getTableCellRendererComponent ( JTable table, Object value,
       boolean isSelected, boolean hasFocus, int row, int column ) {

        JLabel etiqueta=new JLabel();
        if (column==1) {
//          etiqueta.setOpaque(true);
//          etiqueta.setBackground(new Color(237,234,220));
//          Integer a=new Integer(row);
          etiqueta.setText(value.toString());
          etiqueta.setHorizontalAlignment(SwingConstants.CENTER);
        }
//        else
//        {
//         etiqueta.setOpaque(false);
//
//         etiqueta.setHorizontalAlignment(SwingConstants.CENTER);
//        }
        return etiqueta;
     }

}
