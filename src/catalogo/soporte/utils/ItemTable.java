//////////////////////////////////////////////////////
//  CLASSE: ItemTable.java                          //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.utils;

public class ItemTable {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private boolean exec;
    private int dif;

    public boolean getExec() {
        return exec;
    }

    public void setExec(boolean exec) {
        this.exec = exec;
    }

    public int getDif() {
        return dif;
    }

    public void setDif(int dif) {
        this.dif = dif;
    }



}// Fin de clase ItemTable