//////////////////////////////////////////////////////
//  CLASSE: TextParser.java                         //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.utils;

public class TextParser {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/
    
    private final static String SW_RESULTADO        = "<swres>";
    private final static String SW_CODIGO           = "<swcodi>";
    private final static String SW_NULL             = "<swnull>";
    private final static String SW_SEP_FILA         = "<swsepfila>";
    private final static String SW_SEP_CAMPO        = "<swsepcamp>"; 
    
    private final static byte UTF_COMAS_DOBLES_1    = -108;
    private final static byte UTF_COMAS_DOBLES_2    = -109;
    private final static byte UTF_COMAS_SIMPLES_1   = -110;
    private final static byte UTF_COMAS_SIMPLES_2   = -111;
    private final static byte LATIN9_COMAS_DOBLES   = 34;
    private final static byte LATIN9_COMAS_SIMPLES  = 39;

    // Elimina el marcaje de las salidas obtenidas de la BD de cuestiones
    
    public static String parseSalida(String s) {
        if (s.contains(SW_CODIGO)) return "Códigos: " + s.replace(SW_CODIGO, "").replace(SW_SEP_CAMPO, ", ");
        else if (s.compareTo(SW_RESULTADO) == 0) return "Salida vacía";
        else return s.replace(SW_RESULTADO, "").replace(SW_SEP_FILA, "\n").replace(SW_SEP_CAMPO, ", ").replace(SW_NULL, "NULL");
    }

    public static int getContador(String s){
        int contador = 1;
        while (s.indexOf(SW_SEP_FILA) > -1) {
              s = s.substring(s.indexOf(SW_SEP_FILA)+SW_SEP_FILA.length(),s.length());
              contador++;
        }
        return contador;
    }
    
    // Convierte símbolos que admite el formato UTF8 (sólo comillas simples y dobles)
    // a símbolos admitidos en formato LATIN9
    
    public static String parseUTF8(String s) {
        byte[] org = s.getBytes();
        byte[] dst = new byte[org.length];
        int i = 0;
        for (byte b : org)
        {
          if (b == UTF_COMAS_SIMPLES_1 || b == UTF_COMAS_SIMPLES_2) dst[i] = LATIN9_COMAS_SIMPLES;
          else if (b == UTF_COMAS_DOBLES_1 || b == UTF_COMAS_DOBLES_2) dst[i] = LATIN9_COMAS_DOBLES;
          else dst[i] = b;
          i++;
        }
        return new String(dst);
    }
}// Fin clase TextParser
