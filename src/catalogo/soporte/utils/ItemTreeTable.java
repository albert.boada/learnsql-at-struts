//////////////////////////////////////////////////////
//  CLASSE: ItemTreeTable.java                      //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.utils;

public class ItemTreeTable {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/
    
    public final static int ESTADO_SIN_RESOLVER = 1;
    public final static int ESTADO_RESUELTO     = 2;
    public final static int ESTADO_ERROR        = 3;
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    
    private int estado;
    private String comparacion="--";
    private String nota="--";
    
    public int getEstado() {
        return estado;
    }
    
    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getComparacion() {
        return comparacion;
    }

    public String getNota() {
        return nota;
    }

    public void setComparacion(String c) {
        this.comparacion = c;
    }

    public void setNota(String n) {
        this.nota = n;
    }

}// Fin de clase ItemTreeTable
