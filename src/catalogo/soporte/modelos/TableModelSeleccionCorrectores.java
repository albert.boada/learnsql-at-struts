//////////////////////////////////////////////////////
//  CLASSE: TableModelSeleccionCorrectores.java     //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.Categoria;
import catalogo.modelo.Corrector;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelSeleccionCorrectores extends AbstractTableModel {
    private static final String[] NOMBRES_COLUMNAS = {"Selecció", "URL", "SGBD", "Incidencies", "Descripció"};
    private List<Corrector> seleccion, correctores;
    private Categoria categoria;

    public TableModelSeleccionCorrectores(List<Corrector> lista){
        super();
        correctores = lista;
        seleccion = new ArrayList<Corrector>();
    }

    public int getRowCount(){
        return correctores.size();
    }
    
    public int getColumnCount(){
        return NOMBRES_COLUMNAS.length;
    }
    
    public String getColumnName(int columnIndex){
        return NOMBRES_COLUMNAS[columnIndex];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex){
        Corrector cor = correctores.get(rowIndex);
        switch (columnIndex){
            case 0:
                if (seleccion != null && seleccion.contains(cor)) return true;
                else return false;
            case 1:
                return cor.getURL();
            case 2:
                if (cor.getSGBD() == null) return "desconocido";
                else return cor.getSGBD().toUpperCase();
            case 3:
                if (cor.getEstado() == Corrector.INACCESIBLE) return "no accesible";
                else if (cor.getEstado() == Corrector.DESCONOCIDO) return "accesibilidad desconocida";
                else if (!cor.admiteCategoria(categoria)) return "no corrige '" + categoria.getDescripcio() + "'";
                else return "ninguna";
            case 4:
                return correctores.get(rowIndex).getDescripcion();
            default:
                return null;
        }
    }

    public Class getColumnClass(int columnIndex){
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int rowIndex, int colIndex){
        if (colIndex == 0 && correctores.get(rowIndex).getEstado() == Corrector.ACCESIBLE &&
                correctores.get(rowIndex).admiteCategoria(categoria)) return true;
        else return false;
    }
    
    public void setValueAt(Object value, int rowIndex, int colIndex){
        if (colIndex == 0 && seleccion != null){
            Corrector cor = correctores.get(rowIndex);
            if (seleccion.contains(cor)) seleccion.remove(cor);
            else seleccion.add(cor);
            fireTableCellUpdated(rowIndex, colIndex);
        }
    }
    
    // Otros métodos **********************************************************
    
    public Corrector getValueAt(int rowIndex){
        return correctores.get(rowIndex);
    }
    
    public int getIndex(Corrector cor){
        return correctores.indexOf(cor);
    }
    
    public void setSeleccion(List<Corrector> lista, Categoria cat){
        categoria = cat;
        seleccion.clear();
        for (Corrector cor : lista) {
            if (cor.getEstado() == Corrector.ACCESIBLE && cor.admiteCategoria(cat)) seleccion.add(cor);
        }
        fireTableRowsUpdated(0, correctores.size());
    }
    
    public List<Corrector> getSeleccion(){
        return seleccion;
    }
    
    public void remove(Corrector cor){
        seleccion.remove(cor);
    }
    
} // Fin clase TableModelSeleccionCorrectores


