//////////////////////////////////////////////////////
//  CLASSE: TableModelComprobaciones.java           //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.Comprobacion;
import java.util.*;
import javax.swing.table.AbstractTableModel;

public class TableModelComprobaciones extends AbstractTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"Comprovació", "Entrada", "Sortida"};

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private List<Comprobacion> datos;
    private List<Comprobacion> entrada, salida;

    public TableModelComprobaciones(List<Comprobacion> l) {
        super();
        datos = l;
        entrada = new ArrayListModel<Comprobacion>();
        salida = new ArrayListModel<Comprobacion>();
    }

    public int getRowCount() {
        return datos.size();
    }

    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }

    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("tcomp0");
        NOMBRES_COLUMNAS[1] = recursos.getString("tcomp1");
        NOMBRES_COLUMNAS[2] = recursos.getString("tcomp2");
        fireTableStructureChanged();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return datos.get(rowIndex).getNombre();
            case 1:
                if (entrada != null && entrada.contains(datos.get(rowIndex))) return true;
                else return false;
            case 2:
                if (salida != null && salida.contains(datos.get(rowIndex))) return true;
                else return false;
            default:
                return null;
        }
    }

    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (colIndex == 0) return false;
        else return true;
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        if (colIndex == 1 && entrada != null) {
            Comprobacion com = (Comprobacion) datos.get(rowIndex);
            if (entrada.contains(com)) entrada.remove(com);
            else entrada.add(com);
            fireTableCellUpdated(rowIndex, colIndex);
        }
        else if (colIndex == 2 && salida != null) {
            Comprobacion com = (Comprobacion) datos.get(rowIndex);
            if (salida.contains(com)) salida.remove(com);
            else salida.add(com);
            fireTableCellUpdated(rowIndex, colIndex);
        }
    }

    /*********************************************************/
    /*      Otros Métodos                                    */
    /*********************************************************/

    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }

    public int getIndex(Object o) {
        return datos.indexOf(o);
    }

//    public void setSeleccion(List<JuegoPruebas> lista) {
//        seleccion = lista;
//        fireTableDataChanged();
//        //fireTableRowsUpdated(0, datos.size());
//    }
//        return resultado;
//    }

    public void setModel(List<Comprobacion> lista) {
        datos = lista;
        fireTableDataChanged();
        //fireTableRowsUpdated(0, datos.size());
    }


    public List<Comprobacion> getEntrada() {
        return entrada;
    }

    public List<Comprobacion> getSalida() {
        return salida;
    }

} // Fin clase TableModelComprobaciones
