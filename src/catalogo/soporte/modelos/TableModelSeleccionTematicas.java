//////////////////////////////////////////////////////
//  CLASSE: TableModelSeleccionTematicas.java       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.Tematica;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelSeleccionTematicas extends AbstractTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"Selecció", "Nom", "Descripció"};

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private List<Tematica> datos, seleccion;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public TableModelSeleccionTematicas(List<Tematica> l) {
        super();
        datos = l;
        seleccion = null;
    }

    public int getRowCount() {
        return datos.size();
    }
    
    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }
    
    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                if (seleccion != null && seleccion.contains(datos.get(rowIndex))) return true;
                else return false;
            case 1:
                return datos.get(rowIndex).getNombre();
            case 2:
                return datos.get(rowIndex).getDescripcion();
            default:
                return null;
        }
    }

    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (colIndex == 0) return true;
        else return false;
    }
    
    public void setValueAt(Object value, int rowIndex, int colIndex) {
        if (colIndex == 0 && seleccion != null) {
            Tematica t = (Tematica) datos.get(rowIndex);
            if (seleccion.contains(t)) seleccion.remove(t);
            else seleccion.add(t);
            fireTableCellUpdated(rowIndex, colIndex);
        }
    }
    
    // Otros métodos **********************************************************
    
    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }
    
    public int getIndex(Object o) {
        return datos.indexOf(o);
    }
    
    public void setSeleccion(List<Tematica> lista) {
        seleccion = lista;
        fireTableDataChanged();
        //fireTableRowsUpdated(0, datos.size());
    }
    
    public List<Tematica> getSeleccion() {
        return seleccion;
    }

} // Fin clase TableModelSeleccionTematicas
