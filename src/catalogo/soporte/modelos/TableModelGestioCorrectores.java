//////////////////////////////////////////////////////
//  CLASSE: TableModelGestioCorrectores.java        //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.Corrector;
import catalogo.modelo.Cuestion;
import java.util.*;
import javax.swing.table.AbstractTableModel;

public class TableModelGestioCorrectores extends AbstractTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"Selecció", "Id", "Títol", "Esquema", "Categoria", "Exec","Igual"};

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private ArrayListModel<Cuestion> datos, seleccion;
    private Corrector corrector;
    private Boolean ejecutado;

    public TableModelGestioCorrectores() {
        super();
        datos = new ArrayListModel<Cuestion>();
        seleccion = new ArrayListModel<Cuestion>();
        ejecutado = false;
    }

    public int getRowCount() {
        return datos.size();
    }

    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }

    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("tgcor0");
        NOMBRES_COLUMNAS[1] = recursos.getString("tgcor1");
        NOMBRES_COLUMNAS[2] = recursos.getString("tgcor2");
        NOMBRES_COLUMNAS[3] = recursos.getString("tgcor3");
        NOMBRES_COLUMNAS[4] = recursos.getString("tgcor4");
        NOMBRES_COLUMNAS[5] = recursos.getString("tgcor5");
        NOMBRES_COLUMNAS[6] = recursos.getString("tgcor6");
        fireTableStructureChanged();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Cuestion c = (Cuestion) datos.get(rowIndex);
        switch (columnIndex) {
            case 0: if (seleccion != null && seleccion.contains(datos.get(rowIndex))) return true;
                    else return false;
            case 1: return c.getId();
            case 2: return c.getTitulo();
            case 3: return c.getEsquema().getDescripcion();
            case 4: return c.getCategoria().getDescripcio();
            case 5:
                if(ejecutado){
                    if (seleccion == null || !seleccion.contains(datos.get(rowIndex))) return "--";
                    else if (c.getExec()) return "OK";
                    else return "ERROR";
                }
                else{
                    if (seleccion == null || !seleccion.contains(datos.get(rowIndex))) return "--";
                    else return "X";
                }
            case 6:
                if(ejecutado){
                    if (seleccion == null || !seleccion.contains(datos.get(rowIndex))) return "--";
                    else if(c.getCorrectores().size() == 1) return "NA";
                    else if (c.getDif()==0) return "OK";
                    else if (c.getDif()== -120) return "NO";
                    else return "ERROR";
                }
                else{
                    if (seleccion == null || !seleccion.contains(datos.get(rowIndex))) return "--";
                    else if(c.getCorrectores().size() == 1) return "NA";
                    else return "X";
                }
                
            default: return null;
        }
    }

    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (colIndex == 0 && corrector.getEstado() == Corrector.ACCESIBLE &&
                corrector.admiteCategoria(datos.get(colIndex).getCategoria())) return true;
        else return false;
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        if (colIndex == 0 && seleccion != null) {
            Cuestion  c = (Cuestion) datos.get(rowIndex);
            if (seleccion.contains(c)) seleccion.remove(c);
            else seleccion.add(c);
            fireTableCellUpdated(rowIndex, colIndex);
        }
    }

    // Otros métodos **********************************************************

    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }

    public int getIndex(Object o) {
        return datos.indexOf(o);
    }

    public void setModel(ArrayListModel<Cuestion> todas, ArrayListModel<Cuestion> lista, Corrector c) {
        datos = todas;
        seleccion = lista;
        corrector = c;
        fireTableDataChanged();
    }

    public void setEjecutado(Boolean b){
        ejecutado = b;
    }

    public void setTodos(Boolean marcado){
        if(marcado) seleccion = (ArrayListModel)datos.clone();
        else seleccion.clear();
        fireTableDataChanged();
    }

    public List<Cuestion> getSeleccion(){
        return seleccion;
    }

    public List<Cuestion> getDatos(){
        return datos;
    }

} // Fin clase TableModelGestioCorrectores
