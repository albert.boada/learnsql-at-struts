//////////////////////////////////////////////////////
//  CLASSE: ListSelectionModelNone.java             //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import javax.swing.DefaultListSelectionModel;

public class ListSelectionModelNone extends DefaultListSelectionModel
{
    @Override
    public void setSelectionInterval(int index0, int index1) {}
    
} // Fin clase ListSelectionModelNone
