//////////////////////////////////////////////////////
//  CLASSE: TableModelTematicas.java                //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.Tematica;
import java.util.*;
import javax.swing.table.AbstractTableModel;

public class TableModelTematicas extends AbstractTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"Nom", "Descripció"};
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private List datos;

    public TableModelTematicas(List l) {
        super();
        datos = l;
    }

    public int getRowCount() {
        return datos.size();
    }
    
    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }
    
    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("ttem0");
        NOMBRES_COLUMNAS[1] = recursos.getString("ttem1");
        fireTableStructureChanged();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
        Tematica t = (Tematica) datos.get(rowIndex);
        switch (columnIndex)
        {
            case 0: return t.getNombre();
            case 1: return t.getDescripcion();
            default: return null;
        }
    }


    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }


    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    // Otros métodos **********************************************************
    
    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }
    
    public int getIndex(Object o) {
        return datos.indexOf(o);
    }

} // Fin clase TableModelTematicas
