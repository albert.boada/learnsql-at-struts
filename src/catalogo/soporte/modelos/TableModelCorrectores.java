//////////////////////////////////////////////////////
//  CLASSE: TableModelCorrectores.java              //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.Corrector;
import java.util.*;
import javax.swing.table.AbstractTableModel;

public class TableModelCorrectores extends AbstractTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"URL", "Comunicació", "SGBD", "Categories", "Descripció"};
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private List datos;

    public TableModelCorrectores(List l) {
        super();
        datos = l;
    }

    public int getRowCount() {
        return datos.size();
    }
    
    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }
    
    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("tcor0");
        NOMBRES_COLUMNAS[1] = recursos.getString("tcor1");
        NOMBRES_COLUMNAS[2] = recursos.getString("tcor2");
        NOMBRES_COLUMNAS[3] = recursos.getString("tcor3");
        NOMBRES_COLUMNAS[4] = recursos.getString("tcor4");
        fireTableStructureChanged();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
        Corrector c = (Corrector) datos.get(rowIndex);
        switch (columnIndex)
        {
            case 0: return c.getURL();
            case 1:
                if (c.getEstado() == Corrector.ACCESIBLE) return "accesible";
                else if (c.getEstado() == Corrector.INACCESIBLE) return "no accesible";
                else if (c.getEstado() == Corrector.ERROR) return c.getError();
                else return "desconocido";
            case 2: 
                if (c.getSGBD() == null) return "desconocido";
                else return c.getSGBD().toUpperCase();
            case 3: return c.getCategoriasString();
            case 4: return c.getDescripcion();
            default: return null;
        }
    }

    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    // Otros métodos **********************************************************
    
    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }
    
    public int getIndex(Object o) {
        return datos.indexOf(o);
    }

} // Fin clase TableModelCorrectores
