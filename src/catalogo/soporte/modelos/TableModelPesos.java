//////////////////////////////////////////////////////
//  CLASSE: TableModelPesos.java                    //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.JuegoPruebas;
import java.util.*;
import javax.swing.table.AbstractTableModel;

public class TableModelPesos extends AbstractTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"Joc de proves", "Pes"};
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private List datos;
    private Boolean modificado;
    private float peso;

    public TableModelPesos() {
        super();
        datos = null;
        modificado = false;
        peso=0;
    }

    public int getRowCount() {
        if (datos == null) return 0;
        else return datos.size();
    }
    
    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }
    
    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("tpes0");
        NOMBRES_COLUMNAS[1] = recursos.getString("tpes1");
        fireTableStructureChanged();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        JuegoPruebas jp = (JuegoPruebas) datos.get(rowIndex);
        switch (columnIndex) {
            case 0: return jp.toString();
            case 1: return jp.getPeso();
            default: return null;
        }
    }

//    public void setValueAt (Object value, int rowIndex, int colIndex) {
//        if (colIndex == 1 && rowIndex != -1){
//            JuegoPruebas jp = (JuegoPruebas) datos.get(rowIndex);
//            peso = (Float)value;
////            if(jp.getPeso() != peso){
//                System.out.println("pesito: "+peso);
//                jp.setPeso(peso);
//                modificado =true;
//                fireTableCellUpdated(rowIndex, colIndex);
////            }
//        }
//    }

    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int row, int col)  {
//        if(col==1){
//            return true;
//        }
//        else return false;
        return false;
    }

    public Boolean getModificado(){
        return modificado;
    }

    public float getPeso(){
        return peso;
    }

    public void setModificado(Boolean m){
        modificado = m;
    }

    // Otros métodos **********************************************************
    
    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }
    
    public int getIndex(Object o) {
        return datos.indexOf(o);
    }
    
    public void setData(List l) {
        datos = l;
        fireTableDataChanged();
    }

} // Fin clase TableModelPesos


