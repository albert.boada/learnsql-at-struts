//////////////////////////////////////////////////////
//  CLASSE: TableModelCategorias.java                //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.Categoria;
import java.util.*;
import javax.swing.table.AbstractTableModel;

public class TableModelCategorias extends AbstractTableModel {
    
    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/
    
    private static final String[] NOMBRES_COLUMNAS = {"Identificador", "Descripció"};

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private List datos;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public TableModelCategorias(List l) {
        super();
        datos = l;
    }

    public int getRowCount() {
        return datos.size();
    }

    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }

    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("tcat0");
        NOMBRES_COLUMNAS[1] = recursos.getString("tcat1");
        fireTableStructureChanged();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Categoria c = (Categoria) datos.get(rowIndex);
        switch (columnIndex)
        {
            case 0: return Integer.toString(c.getId());
            case 1: return c.getDescripcio();
            default: return null;
        }
    }

    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    // Otros métodos **********************************************************

    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }

    public int getIndex(Object o) {
        return datos.indexOf(o);
    }

} // Fin clase TableModelCategorias
