//////////////////////////////////////////////////////
//  CLASSE: TableModelSeleccionCategorias.java      //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.modelo.Categoria;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelSeleccionCategorias extends AbstractTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"Selecció", "Identificador", "Descripció"};
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private List<Categoria> datos, seleccion;

    public TableModelSeleccionCategorias(List<Categoria> l) {
        super();
        datos = l;
        seleccion = null;
    }

    public int getRowCount() {
        return datos.size();
    }

    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }

    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                if (seleccion != null && seleccion.contains(datos.get(rowIndex))) return true;
                else return false;
            case 1:
                return datos.get(rowIndex).getId();
            case 2:
                return datos.get(rowIndex).getDescripcio();
            default:
                return null;
        }
    }

    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (colIndex == 0) return true;
        else return false;
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        if (colIndex == 0 && seleccion != null) {
            Categoria c = (Categoria) datos.get(rowIndex);
            if (seleccion.contains(c)) seleccion.remove(c);
            else seleccion.add(c);
            fireTableCellUpdated(rowIndex, colIndex);
        }
    }

    // Otros métodos **********************************************************

    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }

    public int getIndex(Object o) {
        return datos.indexOf(o);
    }

    public void setSeleccion(List<Categoria> lista) {
        seleccion = lista;
        fireTableDataChanged();
        //fireTableRowsUpdated(0, datos.size());
    }

    public List<Categoria> getSeleccion() {
        return seleccion;
    }

} // Fin clase TableModelSeleccionCategorias
