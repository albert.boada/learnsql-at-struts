//////////////////////////////////////////////////////
//  CLASSE: ArrayListComboBoxModel.java             //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;


import javax.swing.ComboBoxModel;

/**
 * This class extends ArrayListModel to support a collection as a model for a JComboBox
 * @author Phil Herold
 */
public class ArrayListComboBoxModel<E> extends ArrayListModel<E> implements ComboBoxModel
{
    private Object selectedItem;
    
    /**
     * Implements the method in the ComboBoxModel interface
     * @return Object
     * @see javax.swing.ComboBoxModel#getSelectedItem()
     */
    public Object getSelectedItem() {
        return selectedItem;
    }
    
    /**
     * Implements the method in the ComboBoxModel interface
     * @param item Object
     * @see javax.swing.ComboBoxModel#setSelectedItem(java.lang.Object)
     */
    public void setSelectedItem(Object item) {
        selectedItem = item;
        fireIntervalUpdated(-1, -1);
    }

} // Fin clase ArrayListComboBoxModel

