//////////////////////////////////////////////////////
//  CLASSE: TreeTableModelSolAlumno.java            //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.soporte.utils.ItemTreeTable;
import catalogo.modelo.JuegoPruebas;
import catalogo.modelo.Comprobacion;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.TreeNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

public class TreeTableModelSolAlumno extends DefaultTreeTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"JP/Compr", "Resposta LEARN-SQL",
        "Resposta Alumne","Comparació", "NOTA"};

    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }

    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("tsol0");
        NOMBRES_COLUMNAS[1] = recursos.getString("tsol1");
        NOMBRES_COLUMNAS[2] = recursos.getString("tsol2");
        NOMBRES_COLUMNAS[3] = recursos.getString("tsol3");
        NOMBRES_COLUMNAS[4] = recursos.getString("tsol4");
        this.fireTreeStructureChanged(root, NOMBRES_COLUMNAS, null, NOMBRES_COLUMNAS);
    }

    public boolean isCellEditable(Object node, int col) {
        return false;
    }

//    public Class getColumnClass(int columnIndex) {
//        // Devuelve la clase que hay en cada columna.
//        switch (columnIndex)
//        {
//
//            case 1:
//                // La columna dos contine la edad de la persona, que es un
//                // Integer (no vale int, debe ser una clase)
//                return JTextArea.class;
//            default:
//                // Devuelve una clase Object por defecto.
//                return Object.class;
//        }
//    }


    public Object getValueAt(Object node, int columnIndex) {
//        System.out.println(node.getClass().getSimpleName());
        if (node instanceof JuegoPruebas) {
            JuegoPruebas itt = (JuegoPruebas) node;
            switch (columnIndex) {
                case 0: return itt.toString();
                case 1:
                    if(itt.getSalida().isEmpty()) return "--";
                    else return itt.getSalida();
                case 2:
                    if(itt.getSalidaAlumno().isEmpty()) return "--";
                    else return itt.getSalidaAlumno();
                case 3:
//                    if (itt.getEstado() == ItemTreeTable.ESTADO_RESUELTO) return "OK";
//                    else return "NO";
                    return itt.getComparacion();
                case 4: return itt.getNota();
                default: return null;
            }
        }
        else if (node instanceof Comprobacion) {
            Comprobacion itt = (Comprobacion) node;
            switch (columnIndex) {
                case 0: return itt.toString();
                case 1:
                    if(itt.getSalida().isEmpty()) return "--";
                    else return itt.getSalida();
                case 2:
                    if(itt.getSalidaAlumno().isEmpty()) return "--";
                    else return itt.getSalidaAlumno();
                case 3: 
//                    if (itt.getEstado() == ItemTreeTable.ESTADO_RESUELTO) return "OK";
//                    else return "NO";
                    return itt.getComparacion();
                case 4: return "";//itt.getNota();
                default: return null;
            }
        }
        else return null;
    }


//    public void setValueAt (Object dato, int fila, int columna) {
//         // Obtenemos la persona de la fila indicada
//        Persona aux = (Persona)datos.get (fila);
//        switch (columna) {
//           // Nos pasan el nombre.
//           case 0:
//               aux.nombre = (String)dato;
//               break;
//           // Nos pasan el apellido.
//           case 1:
//               aux.apellido = (String)dato;
//               break;
//           // Nos pasan la edad.
//           case 2:
//               aux.edad = ((Integer)dato).intValue();
//               break;
//        }
//
//        // Aquí hay que avisar a los sucriptores del cambio.
//        // Ver unpoco más abajo cómo.
//    }


} // Fin clase TreeTableModelSolAlumno
