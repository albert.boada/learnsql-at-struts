//////////////////////////////////////////////////////
//  CLASSE: TreeTableModelEjecucion.java            //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;

import catalogo.soporte.utils.ItemTreeTable;
import javax.swing.tree.TreeNode;
import java.util.*;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

public class TreeTableModelEjecucion extends DefaultTreeTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"Joc de provess / Comprovació", "Estat"};
    
    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }
    
    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("texec0");
        NOMBRES_COLUMNAS[1] = recursos.getString("texec1");
        this.fireTreeStructureChanged(root, NOMBRES_COLUMNAS, null, NOMBRES_COLUMNAS);
    }
    
    public boolean isCellEditable(Object node, int col) {
        return false;
    }
    
    public Object getValueAt(Object node, int columnIndex) {
        if (node instanceof ItemTreeTable) {
            ItemTreeTable itt = (ItemTreeTable) node;
            switch (columnIndex) {
                case 0: return itt.toString();
                case 1:
                    //if (jp.getSalida().length() == 0) return "SIN RESOLVER";
                    //else return "RESUELTO";
                    if (itt.getEstado() == ItemTreeTable.ESTADO_SIN_RESOLVER) return "--";
                    else if (itt.getEstado() == ItemTreeTable.ESTADO_RESUELTO) return "Resuelto";
                    else return "ERROR";
                default: return null;
            }
        }
        else return null;
    }

    
} // Fin clase TreeTableModelEjecucion

