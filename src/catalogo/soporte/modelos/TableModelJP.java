//////////////////////////////////////////////////////
//  CLASSE: TableModelJP.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.soporte.modelos;
import catalogo.modelo.JuegoPruebas;
import catalogo.modelo.Comprobacion;
import java.util.*;
import javax.swing.table.AbstractTableModel;

public class TableModelJP extends AbstractTableModel {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    private static final String[] NOMBRES_COLUMNAS = {"Joc de proves", "Inicialitzacions", "Entrada", "Resultat", "Neteja"};
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private List<JuegoPruebas> datos;
    private List<JuegoPruebas> init, entrada, resultado, limpieza;
    private List<Comprobacion> entcomp, salcomp;
    private TableModelComprobaciones t;

    public TableModelJP(List<JuegoPruebas> l) {
        super();
        datos = l;
        init = new ArrayListModel<JuegoPruebas>();
        init.add(l.get(0));
        entrada = new ArrayListModel<JuegoPruebas>();
        entrada.add(l.get(0));
        resultado = new ArrayListModel<JuegoPruebas>();
        resultado.add(l.get(0));
        limpieza = new ArrayListModel<JuegoPruebas>();
        limpieza.add(l.get(0));
        entcomp = new ArrayListModel<Comprobacion>(); 
        salcomp = new ArrayListModel<Comprobacion>(); 
    }

    public int getRowCount() {
        return datos.size();
    }

    public int getColumnCount() {
        return NOMBRES_COLUMNAS.length;
    }

    public String getColumnName(int columnIndex) {
        return NOMBRES_COLUMNAS[columnIndex];
    }

    public void setColumnName(ResourceBundle recursos) {
        NOMBRES_COLUMNAS[0] = recursos.getString("tmjp0");
        NOMBRES_COLUMNAS[1] = recursos.getString("tmjp1");
        NOMBRES_COLUMNAS[2] = recursos.getString("tmjp2");
        NOMBRES_COLUMNAS[3] = recursos.getString("tmjp3");
        NOMBRES_COLUMNAS[4] = recursos.getString("tmjp4");
        fireTableStructureChanged();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return datos.get(rowIndex).getNombre();
            case 1:
                if (init != null && init.contains(datos.get(rowIndex))) return true;
                else return false;
            case 2:
                if (entrada != null && entrada.contains(datos.get(rowIndex))) return true;
                else return false;
            case 3:
                if (resultado != null && resultado.contains(datos.get(rowIndex))) return true;
                else return false;
            case 4:
                if (limpieza != null && limpieza.contains(datos.get(rowIndex))) return true;
                else return false;
            default:
                return null;
        }
    }

    public Class getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (colIndex == 0) return false;
        else return true;
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        if (colIndex == 1 && init != null) {
            JuegoPruebas jp = (JuegoPruebas) datos.get(rowIndex);
            if (init.contains(jp)) init.remove(jp);
            else init.add(jp);
            fireTableCellUpdated(rowIndex, colIndex);
        }
        else if (colIndex == 2 && entrada != null) {
            JuegoPruebas jp = (JuegoPruebas) datos.get(rowIndex);
            if (entrada.contains(jp)) entrada.remove(jp);
            else entrada.add(jp);
            fireTableCellUpdated(rowIndex, colIndex);
        }
        else if (colIndex == 3 && resultado != null) {
            JuegoPruebas jp = (JuegoPruebas) datos.get(rowIndex);
            if (resultado.contains(jp)) resultado.remove(jp);
            else resultado.add(jp);
            fireTableCellUpdated(rowIndex, colIndex);
        }
        else if (colIndex == 4 && limpieza != null) {
            JuegoPruebas jp = (JuegoPruebas) datos.get(rowIndex);
            if (limpieza.contains(jp)) limpieza.remove(jp);
            else limpieza.add(jp);
            fireTableCellUpdated(rowIndex, colIndex);
        }
    }

    // Otros métodos **********************************************************

    public Object getValueAt(int rowIndex) {
        return datos.get(rowIndex);
    }

    public int getIndex(Object o) {
        return datos.indexOf(o);
    }

//    public void setSeleccion(List<JuegoPruebas> lista) {
//        seleccion = lista;
//        fireTableDataChanged();
//        //fireTableRowsUpdated(0, datos.size());
//    }
//        return resultado;
//    }

    public List<JuegoPruebas> getLimpieza() {
        return limpieza;
    }

    public List<JuegoPruebas> getInits() {
        return init;
    }

    public List<JuegoPruebas> getEntrada() {
        return entrada;
    }

    public List<JuegoPruebas> getResultado() {
        return resultado;
    }

    public void setEntradaComp(List<Comprobacion> l) {
        entcomp = l;
    }

    public void setSalidaComp(List<Comprobacion> l) {
        salcomp = l;
    }

} // Fin clase TableModelJP
