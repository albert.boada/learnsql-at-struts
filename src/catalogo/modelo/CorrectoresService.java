//////////////////////////////////////////////////////
//  CLASSE: DiccCorrectores.java                    //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import java.util.List;

import catalogo.comunicacion.CorrectorCliente;
import catalogo.gestiondatos.CtrlCorrectores;
import catalogo.soporte.modelos.ArrayListComboBoxModel;
import catalogo.soporte.modelos.ArrayListModel;

public class CorrectoresService extends BaseService
{
    private CtrlCorrectores ctrlCorrectores;
    
    private CategoriasService categoriasService;
    
    private CorrectorCliente cliente;

    public CorrectoresService(CtrlCorrectores dao, CategoriasService dicc, CorrectorCliente cliente) throws Exception {
        this.ctrlCorrectores      = dao;
        this.categoriasService = dicc;
        this.cliente           = cliente;
    }

    /**
     * @throws Exception 
     *
     */
    public List<Corrector> getAll() throws Exception
    {
        return this.ctrlCorrectores.select();
    }

    public Corrector get(String url)
    {
    	//@TODO make this method useful again

        //for (Corrector cor : correctores) if (cor.getURL().equals(url)) return cor;
        return null;
    }

    /**
     *
     */
    public boolean create(String url, String descripcion) throws Exception
    {
        Corrector cor = new Corrector(url, descripcion);

        this.validate(cor, CorrectoresService.CREATE);
        if (!this.validation.isEmpty()) { return false; }

        try {
            this.ctrlCorrectores.insert(url, descripcion);
        }
        catch (Exception e) {
            this.validation.put("corrector._global", e.getMessage());
            return false;
        }

        return true;
    }

    /**
     *
     */
    public boolean update(Corrector cor, String url, String descripcion) throws Exception
    {
        Corrector _cor = new Corrector(url, descripcion);

        this.validate(_cor, CorrectoresService.CREATE);
        if (!this.validation.isEmpty()) { return false; }

        try {
            this.ctrlCorrectores.update(cor.getURL(), url, descripcion);
        }
        catch (Exception e) {
            this.validation.put("corrector._global", e.getMessage());
            return false;
        }

        //@TODO move somewhere else! (frontend)
        	//boolean urlChanged = !url.equals(cor.getURL());
        	//if (urlChanged) cor.setEstado(Corrector.DESCONOCIDO);

        return true;
    }

    /**
     *
     */
    public void validate(Corrector cor, String mode)
    {
        this.resetValidations();

        if (cor == null) {
            //@TODO throw Exception for developer might better here!
            this.validation.put("corrector", "isnull");
            return;
        }
    }

    public void remove(Corrector cor) throws Exception
    {
        ctrlCorrectores.delete(cor.getURL());
    }

    public ArrayListModel<Cuestion> getCuestiones(ArrayListModel<Cuestion> todas, Corrector actual)
    {
        ArrayListModel<Cuestion> cuestiones = new ArrayListModel<Cuestion>();
        for(Cuestion c : todas){
            ArrayListModel<Corrector> listacorrectores = c.getCorrectores();
            if(listacorrectores.contains(actual)){
                cuestiones.add(c);
            }
        }
        return cuestiones;

    }

    /* frontend??!
    public void setPropiedades(boolean todos) {
        String sgbd = null;
        int codigo =0;
        int[] ids = null;
        for (Corrector cor : correctores) {
            if (todos || cor.getEstado() == Corrector.DESCONOCIDO) {
                cor.setSGBD(null);
                cor.clearCategorias();
                sgbd = cliente.getDBMS(cor.getURL());
                if (sgbd == null) {
                    cor.setEstado(Corrector.INACCESIBLE);
                }
                else {
                    cor.setSGBD(sgbd);
                    codigo = cliente.checkRefereeStatus(cor.getURL());
                    if(codigo!=0){
                        cor.setEstado(Corrector.ERROR);
                        cor.setError(codigo+": "+cliente.getMensaje());
                    }
                    else{
                        ids = cliente.getQuestionTypes(cor.getURL());
                        if (ids == null) cor.setEstado(Corrector.INACCESIBLE);
                        else {
                            for (int id : ids) cor.addCategoria(categoriasService.get(id));
                            cor.setEstado(Corrector.ACCESIBLE);
                        }
                    }
                }
            }
        }
    }
    */

}
