//////////////////////////////////////////////////////
//  CLASSE: Resultado.java                          //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import java.util.List;


public class Resultado {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    public int codigo;
    public String texto;
    public int ordenJP;
    public int ordenCompr;
    public String solucion;
    public int contador;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public Resultado(int codigo, String texto, int ordenJP, int ordenCompr, String solucion, int contador) {
        this.codigo = codigo;
        this.texto = texto;
        this.ordenJP = ordenJP;
        this.ordenCompr = ordenCompr;
        this.solucion = solucion;
        this.contador = contador;
    }


}
