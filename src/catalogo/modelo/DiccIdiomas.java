//////////////////////////////////////////////////////
//  CLASSE: DiccIdiomas.java                        //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import catalogo.soporte.modelos.ArrayListComboBoxModel;

public class DiccIdiomas {

    /*********************************************************/
    /*      Constantes                                       */
    /*********************************************************/

    public final static int ID_CATALA  = 1;
    public final static int ID_ESPAÑOL = 2;
    public final static int ID_ENGLISH = 3;

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private ArrayListComboBoxModel<Idioma> idiomas;

    public DiccIdiomas() {
        idiomas = new ArrayListComboBoxModel<Idioma>();
        idiomas.add(new Idioma(1,"Català"));
        idiomas.add(new Idioma(2,"Español"));
        idiomas.add(new Idioma(3,"English"));
    }

    public ArrayListComboBoxModel<Idioma> getAll() {
        return idiomas;
    }

    public Idioma get(int id) {
        for (Idioma sol : idiomas) if (sol.getId() == id) return sol;
        return null;
    }

} // Fin clase DiccIdiomas
