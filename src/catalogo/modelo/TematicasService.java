//////////////////////////////////////////////////////
//  CLASSE: DiccTematicas.java                      //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import catalogo.actions.RestfulValidationException;
import catalogo.dtos.ComprobacionDTO;
import catalogo.dtos.CuestionDTO;
import catalogo.dtos.JuegoPruebasDTO;
import catalogo.dtos.TematicaDTO;
import catalogo.gestiondatos.CtrlCategorias;
import catalogo.gestiondatos.CtrlTematicas;
import catalogo.soporte.modelos.ArrayListComboBoxModel;

import java.util.List;

public class TematicasService extends BaseService
{
	private CtrlTematicas ctrlTematicas;
    private CtrlCategorias ctrlCategorias;

    public TematicasService(CtrlTematicas dao, CtrlCategorias categoria_dao) throws Exception
    {
        this.ctrlTematicas = dao;
        this.ctrlCategorias = categoria_dao;
    }

    /**
     * @throws Exception 
     *
     */
    public List<Tematica> getAll() throws Exception
    {
        return this.ctrlTematicas.select();
    }

    public Tematica find(String nombre) throws Exception
    {
    	return this.ctrlTematicas.get(nombre);
    }

    /**
     *
     */
    public Tematica create(Tematica tem) throws Exception {
        this.validate(tem, null, TematicasService.CREATE);
        if (!this.validation.isEmpty()) { throw new RestfulValidationException(this.validation); }

        try {
            this.ctrlTematicas.insert(tem.getNombre(), tem.getDescripcion(), tem.getCategorias());

            return this.find(tem.getNombre());
        }
        catch (Exception e) {
        	throw e;
            //this.validation.put("tematica._global", e.getMessage());
            //return null;
        }
    }

    /**
     *
     */
    public Tematica update(Tematica tem, String id) throws Exception
    {
        this.validate(tem, id, TematicasService.UPDATE);
        if (!this.validation.isEmpty()) { throw new RestfulValidationException(this.validation); }

        try {
            this.ctrlTematicas.update(id, tem.getNombre(), tem.getDescripcion(), tem.getCategorias());
        
            return this.find(tem.getNombre());
        }
        catch (Exception e) {
            throw e;
        	//this.validation.put("tematica._global", e.getMessage());
            //return null;
        }
    }

    /**
     *
     */
    public void validate(Tematica tem, String id, String mode) throws Exception
    {   
        this.resetValidations();

        if (tem == null) {
            throw new Exception("thematic is null on validate");
        }

        if (tem.getNombre() == null || tem.getNombre().trim().isEmpty()) {
            this.setValidation("EMPTY", "", "NAME", null);
        }
        else {
        	Tematica existing_tem = this.ctrlTematicas.get(tem.getNombre());
        	if (existing_tem != null && (mode == TematicasService.CREATE || (mode == TematicasService.UPDATE && !existing_tem.getNombre().equals(id)))) {
        		this.setValidation("EXISTING", "", "ID", null);        		
        	}
        	else if (tem.getDescripcion() == null || tem.getDescripcion().trim().isEmpty()) {
        		this.setValidation("EMPTY", "", "DESCRIPTION", null);
        	}
        }
    }

    public void remove(Tematica tem) throws Exception
    {
        ctrlTematicas.delete(tem.getNombre());
    }
    
    public TematicaDTO entityToForm(Tematica t) {
		TematicaDTO form = new TematicaDTO();
		
		form.setId(t.getNombre());
		form.setNombre(t.getNombre());
		form.setDescripcion(t.getDescripcion());
		for (Categoria cat : t.getCategorias()) {
			form.getCategoriasIds().add(cat.getId());
		}

		return form;
	}
    

	public Tematica formToEntity(TematicaDTO form) throws Exception {
		Tematica t = new Tematica();
		t.setNombre(form.getNombre());
		t.setDescripcion(form.getDescripcion());
		t.setCategorias(this.ctrlCategorias.getByIds(form.getCategoriasIds()));
		
		return t;
	}
}
