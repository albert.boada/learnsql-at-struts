//////////////////////////////////////////////////////
//  CLASSE: Comprobacion.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import catalogo.global.CtrlModelo;
import catalogo.soporte.utils.ItemTreeTable;
import catalogo.soporte.utils.TextParser;
import java.util.Enumeration;
import javax.swing.tree.TreeNode;

public class Comprobacion extends ItemTreeTable implements TreeNode {//, Comparable<Comprobacion> {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private JuegoPruebas juegoPruebas;

    private String id;
    private String nombre;
    private String descripcion;
    private String msgError;
    private String msgErrorEsp;
    private String msgErrorIng;
    private String entrada;
    private String salida;
    private String salidaAlumno;
    private Boolean binaria;
    private Boolean consumeix;

    private Idioma idioma;

    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/

    public Comprobacion() {
        inicializa();
    }

    public Comprobacion(String nombre) {
        inicializa();
        this.id     = nombre;
        this.nombre = nombre;
    }

    public Comprobacion(Comprobacion c) {
        inicializa();
        this.id     = c.nombre;
        this.nombre = c.nombre;
        this.descripcion = c.descripcion;
        this.msgError = c.msgError;
        this.msgErrorEsp = c.msgErrorEsp;
        this.msgErrorIng = c.msgErrorIng;
        this.entrada = c.entrada;
        this.salida = c.salida;
        this.salidaAlumno = c.salidaAlumno;
        this.binaria = c.binaria;
        this.consumeix = c.consumeix;
        this.idioma = c.idioma;
    }

    /*********************************************************/
    /*      Métodos Get/Set                                  */
    /*********************************************************/

    public JuegoPruebas getJuegoPruebas() {
        return juegoPruebas;
    }

    public int getOrden() {
        if (juegoPruebas == null) return -1;
        else return juegoPruebas.getOrdenCompr(this);
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMsgError() {
        return msgError;
    }

    public String getMsgErrorEsp() {
        return msgErrorEsp;
    }

    public String getMsgErrorIng() {
        return msgErrorIng;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getEntrada() {
        return entrada;
    }

    public String getSalida() {
        return salida;
    }

    public String getSalidaAlumno() {
        return salidaAlumno;
    }

    public Boolean getBinaria() {
        return binaria;
    }

    public Boolean getConsumeix() {
        return consumeix;
    }

    public Idioma getIdioma() {
        return idioma;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public void setMsgErrorEsp(String msgError) {
        this.msgErrorEsp = msgError;
    }

    public void setMsgErrorIng(String msgError) {
        this.msgErrorIng = msgError;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public void setSalida(String salida) {
        this.salida = TextParser.parseSalida(salida);
    }

    public void setSalidaAlumno(String salidaAlumno) {
        this.salidaAlumno = TextParser.parseSalida(salidaAlumno);
    }

    public void setBinaria(Boolean binaria) {
        this.binaria = binaria;
    }

    public void setConsumeix(Boolean consumeix) {
        this.consumeix = consumeix;
    }

    public void setJuegoPruebas(JuegoPruebas jp) {
        this.juegoPruebas = jp;
    }

    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }

    // Métodos para gestionar comprobaciones **********************************

    public void clearSalida() {
        salida = "";
        setEstado(ItemTreeTable.ESTADO_SIN_RESOLVER);
    }

    // Implementación interfaz TreeNode ***************************************

    public TreeNode getChildAt(int childIndex) {
        return null;
    }

    public int getChildCount() {
        return 0;
    }

    public TreeNode getParent() {
        return juegoPruebas;
    }

    public int getIndex(TreeNode node) {
        return -1;
    }

    public boolean getAllowsChildren() {
        return false;
    }

    public boolean isLeaf() {
        return true;
    }

    public Enumeration children() {
        return null;
    }

    // Otros ******************************************************************

    private void inicializa() {
        juegoPruebas = null;
        nombre = "";
        descripcion = "";
        msgError = "";
        msgErrorEsp = "";
        msgErrorIng = "";
        entrada = "";
        salida = "";
        salidaAlumno="";
        binaria = false;
        consumeix = false;
        // idioma = CtrlModelo.idiomas.getAll().get(0); //@albert this crashes... is it even needed??,.

        setEstado(ItemTreeTable.ESTADO_SIN_RESOLVER);

    }

    @Override
    public String toString() {
        if (nombre.isEmpty()) return "Nueva Comprobación (#" + getOrden() + ")";
        else return nombre+"-"+juegoPruebas.getId();
    }

} // Fin clase Comprobacion


