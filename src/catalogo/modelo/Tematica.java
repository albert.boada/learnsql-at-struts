//////////////////////////////////////////////////////
//  CLASSE: Tematica.java                           //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import catalogo.soporte.modelos.ArrayListModel;
import java.util.List;

public class Tematica /*implements Comparable<Tematica>*/ {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private String nombre;
    private String descripcion;
    private ArrayListModel<Categoria> categorias;

    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/
    
    // necessitem constructor buit per mappeig de parametres struts
    public Tematica() 
    { 	
    	this.categorias = new ArrayListModel<Categoria>();
    }
    
    public Tematica(String nombre, String descripcion, List<Categoria> listacategorias) {
        setNombre(nombre);
        setDescripcion(descripcion);
        if(listacategorias!=null)
        {
        	this.categorias = new ArrayListModel<Categoria>();
        	setCategorias(listacategorias);
        }
    }
    
    /*********************************************************/
    /*      Métodos Get/Set                                  */
    /*********************************************************/

    public String getNombre() {
        return nombre;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public ArrayListModel<Categoria> getCategorias() {
        return categorias;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCategorias(List<Categoria> lista) {
        if (!categorias.isEmpty()) categorias.clear();
        categorias.addAll(lista);
    }
    
    // Otros ******************************************************************
    
    @Override
    public String toString() {
        return nombre;
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Tematica)) return false;
        Tematica t = (Tematica) o;
        return nombre.toLowerCase().equals(t.getNombre().toLowerCase());
    }
    
    public int compareTo(Tematica t) {
        return nombre.toLowerCase().compareTo(t.getNombre().toLowerCase());
    }
    
} // Fin clase Tematica
