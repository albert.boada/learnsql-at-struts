//////////////////////////////////////////////////////
//  CLASSE: Esquema.java                            //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

public class Esquema implements Comparable<Esquema> {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    
    private String nombre;
    private String descripcion;
    
    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/
    
    public Esquema()
    {
    	
    }
    
    public Esquema(String nombre, String descripcion) {
        setNombre(nombre);
        setDescripcion(descripcion);
    }
    
    /*********************************************************/
    /*      Métodos Get/Set                                  */
    /*********************************************************/
    
    public String getNombre() {
        return nombre;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setDescripcion(String descripcion)  {
        this.descripcion = descripcion;
    }
    
    // Otros ******************************************************************
    
    @Override
    public String toString() {
        return getNombre();
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Esquema)) return false;
        Esquema e = (Esquema) o;
        return nombre.toLowerCase().equals(e.getNombre().toLowerCase());
    }
    
    public int compareTo(Esquema e) {
        return nombre.toLowerCase().compareTo(e.getNombre().toLowerCase());
    }
    
} // Fin clase Esquema
