//////////////////////////////////////////////////////
//  CLASSE: Corrector.java                          //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import java.util.ArrayList;
import java.util.List;

public class Corrector implements Comparable<Corrector> {

    public final static int DESCONOCIDO = 1;
    public final static int INACCESIBLE = 2;
    public final static int ACCESIBLE   = 3;
    public final static int ERROR       = 4;
    
    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    
    private String url, descripcion, sgbd;
    private int estado;
    private String error;
    private ArrayList<Categoria> categorias;
    
    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/
    
    public Corrector(String url, String descripcion) {
        this.url = url;
        this.descripcion = descripcion;
        this.sgbd = null;
        this.estado = DESCONOCIDO;
        this.error = "";
        categorias = new ArrayList<Categoria>();
    }
    
    /*********************************************************/
    /*      Métodos Get/Set                                  */
    /*********************************************************/
    
    public String getURL() {
        return url;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public String getError() {
        return error;
    }
    
    public int getEstado() {
        return estado;
    }
    
    public String getSGBD() {
        return sgbd;
    }
    
    public List<Categoria> getCategorias() {
        return categorias;
    }
    
    public String getCategoriasString() {
        if (categorias.isEmpty()) return "ninguna";
        String res = "";
        for (Categoria cat : categorias) res += ", " + cat.getDescripcio();
        return res.substring(2);
    }
    
    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    public void setURL(String url) {
        this.url = url;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setError(String error) {
        this.error = error;
    }
    
    public void setSGBD(String sgbd) {
        this.sgbd = sgbd;
    }
    
    public void addCategoria(Categoria cat) {
        categorias.add(cat);
    }
    
    public void clearCategorias() {
        categorias.clear();
    }
    
    // Otros ******************************************************************
    
    public boolean admiteCategoria(Categoria cat) {
        return categorias.contains(cat);
    }
    
    @Override
    public String toString() {
        return getURL();
    }
    
    public int compareTo(Corrector c) {
        return url.compareTo(c.getURL());
    }
    
} // Fin clase Corrector


