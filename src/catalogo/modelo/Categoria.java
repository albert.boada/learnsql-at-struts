//////////////////////////////////////////////////////
//  CLASSE: Categoria.java                          //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;


public class Categoria /* implements Comparable<Categoria> */ {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    
    private int id;
    private String descripcio;
    private Boolean inits;
    private Boolean postinits;
    private Boolean limpieza;
    private Boolean estado;
    private Boolean solucion;
    private Boolean initsJP;
    private Boolean entradaJP;
    private Boolean limpiezaJP;
    private Boolean comprobaciones;
    
    /*********************************************************/
    /*      Métodos constructores                            */
    /*********************************************************/
    
    public Categoria() { }
    
    public Categoria(int id, String descripcio, Boolean inits, Boolean postinits, Boolean limpieza, Boolean estado,
            Boolean solucion, Boolean initsJP, Boolean entradaJP, Boolean limpiezaJP, Boolean comprobaciones){
        setId(id);
        setDescripcio(descripcio);
        setInits(inits);
        setPostinits(postinits);
        setLimpieza(limpieza);
        setEstado(estado);
        setSolucion(solucion);
        setInitsJP(initsJP);
        setEntradaJP(entradaJP);
        setLimpiezaJP(limpiezaJP);
        setComprobaciones(comprobaciones);

    }
    
    /*********************************************************/
    /*      Métodos Get/Set                                  */
    /*********************************************************/
    
    public int getId() {
        return id;
    }
    
    public String getDescripcio() {
        return descripcio;
    }

    public Boolean getInits() {
        return inits;
    }

    public Boolean getPostinits() {
        return postinits;
    }

    public Boolean getLimpieza() {
        return limpieza;
    }

    public Boolean getEstado() {
        return estado;
    }

    public Boolean getSolucion() {
        return solucion;
    }

    public Boolean getInitsJP() {
        return initsJP;
    }

    public Boolean getEntradaJP() {
        return entradaJP;
    }

    public Boolean getLimpiezaJP() {
        return limpiezaJP;
    }

    public Boolean getComprobaciones() {
        return comprobaciones;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescripcio(String descripcion) {
        this.descripcio = descripcion;
    }

    public void setInits(Boolean b) {
        this.inits = b;
    }

    public void setPostinits(Boolean b) {
        this.postinits = b;
    }

    public void setLimpieza(Boolean b) {
        this.limpieza = b;
    }

    public void setEstado(Boolean b) {
        this.estado = b;
    }

    public void setSolucion(Boolean b) {
        this.solucion = b;
    }

    public void setInitsJP(Boolean b) {
        this.initsJP = b;
    }

    public void setEntradaJP(Boolean b) {
        this.entradaJP = b;
    }

    public void setLimpiezaJP(Boolean b) {
        this.limpiezaJP = b;
    }

    public void setComprobaciones(Boolean b) {
        this.comprobaciones = b;
    }

    // Otros ******************************************************************
    
    @Override
    public String toString() {
        return id+" | "+descripcio+" | "+inits+" | "+postinits+" | "+limpieza+" | "+estado+" | "+solucion+" | "+initsJP+" | "+entradaJP+" | "+limpiezaJP+" | "+comprobaciones;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Categoria)) return false;
        Categoria c = (Categoria) o;
        return new Integer(this.id).equals(new Integer(c.getId()));
    }
    
    public int compareTo(Categoria c) {
        return new Integer(this.id).compareTo(new Integer(c.getId()));
    }
    
} // Fin clase Categoria

