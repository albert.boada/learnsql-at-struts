//////////////////////////////////////////////////////
//  CLASSE: Idioma.java                             //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

public class Idioma {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/

    private int id;
    private String nombre;

    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/

    public Idioma(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    /*********************************************************/
    /*      Métodos Get/Set                                  */
    /*********************************************************/

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    // Otros ******************************************************************

    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Idioma)) return false;
        Idioma idioma = (Idioma) o;
        return new Integer(this.id).equals(new Integer(idioma.getId()));
    }


} // Fin clase Idioma
