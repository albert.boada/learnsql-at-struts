package catalogo.modelo;

import java.sql.Connection;

import catalogo.comunicacion.CorrectorCliente;
import catalogo.gestiondatos.CtrlCategorias;
import catalogo.gestiondatos.CtrlComprobacion;
import catalogo.gestiondatos.CtrlCorrectores;
import catalogo.gestiondatos.CtrlCuestion;
import catalogo.gestiondatos.CtrlEsquemas;
import catalogo.gestiondatos.CtrlJuegoPruebas;
import catalogo.gestiondatos.CtrlTematicas;
import catalogo.gestiondatos.CtrlTiposSolucion;
import catalogo.gestiondatos.SaveOnCommit;
import catalogo.gestiondatos.WrapperConexion;

public class DataModel
{
    public CuestionesService cuestiones;
    public CategoriasService categorias;
	public EsquemasService esquemas;
	public TematicasService tematicas;
	public CorrectoresService correctores;
	public TipossolucionService tiposSolucion;
	public DiccIdiomas idiomas;
	public SaveOnCommit saveOnCommit;

    /**
     * @throws Exception
     *
     */
    public DataModel(WrapperConexion wrapperConexion) throws Exception
    {
    	Connection con    = wrapperConexion.getConexion();
        String con_host   = wrapperConexion.getHost();
        String con_dbname = wrapperConexion.getNombre();

    	CorrectorCliente cliente = new CorrectorCliente(con_host, con_dbname);

        CtrlCategorias categoria_dao        = new CtrlCategorias(con);
        CtrlEsquemas esquema_dao            = new CtrlEsquemas(con);
        CtrlTematicas tematica_dao          = new CtrlTematicas(con);
        CtrlCorrectores corrector_dao        = new CtrlCorrectores(con);
        CtrlJuegoPruebas jp_dao            = new CtrlJuegoPruebas(con);
        CtrlComprobacion comprobacion_dao  = new CtrlComprobacion(con);
        CtrlTiposSolucion tiposolucion_dao = new CtrlTiposSolucion();
        CtrlCuestion cuestion_dao          = new CtrlCuestion(con, categoria_dao, tematica_dao, esquema_dao, tiposolucion_dao, corrector_dao);


        this.categorias    = new CategoriasService(categoria_dao);
        this.esquemas      = new EsquemasService(esquema_dao);
        this.tematicas     = new TematicasService(tematica_dao, categoria_dao);
        this.correctores   = new CorrectoresService(corrector_dao, this.categorias, cliente);
        this.tiposSolucion = new TipossolucionService(tiposolucion_dao);
        this.cuestiones    = new CuestionesService(cuestion_dao, categoria_dao, esquema_dao, tematica_dao, tiposolucion_dao, jp_dao, comprobacion_dao, cliente, wrapperConexion.getUsuario());
    }

}
