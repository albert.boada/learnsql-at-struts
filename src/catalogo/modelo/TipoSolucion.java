//////////////////////////////////////////////////////
//  CLASSE: TipoSolucion.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

public class TipoSolucion {

    /*********************************************************/
    /*      Atributos                                        */
    /*********************************************************/
    
    private int id;
    private String nombre;
    
    /*********************************************************/
    /*      Método constructor                               */
    /*********************************************************/
    
    public TipoSolucion()
    {
    	
    }
    
    public TipoSolucion(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    
    /*********************************************************/
    /*      Métodos Get/Set                                  */
    /*********************************************************/
    
    public int getId() {
        return id;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setId(int id) {
    	this.id=id;
    }
    
    public void setNombre(String nombre) {
    	this.nombre=nombre;
    }
    
    // Otros ******************************************************************
    
    @Override
    public String toString() {
        return nombre;
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof TipoSolucion)) return false;
        TipoSolucion ts = (TipoSolucion) o;
        return new Integer(this.id).equals(new Integer(ts.getId()));
    }
    
    
} // Fin clase TipoSolucion
