package catalogo.modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseService
{
    /**
     *
     */
    protected List<Object> validation;
    //public void setValidation(Map<String,String> validation) { this.validation = validation; }
    public void setValidation(String error, String path, String field, Map<String, String> errorParams) { this.validation.add(error); this.validation.add(path); this.validation.add(field); this.validation.add(errorParams); }
    public List<Object> getValidation() { return validation; }


    /**
     *
     */
    public static final String CREATE = "create";
    public static final String UPDATE = "update";

    /**
     *
     */
    public BaseService()
    {
        this.resetValidations();
    }

    /**
     *
     */
    public void resetValidations()
    {
        this.validation = new ArrayList<Object>();
    }
}
