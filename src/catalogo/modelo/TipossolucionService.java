//////////////////////////////////////////////////////
//  CLASSE: DiccTiposSolucion.java                  //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import java.util.List;

import catalogo.gestiondatos.CtrlTiposSolucion;

public class TipossolucionService
{
    public final static int ID_TEXTO   = 1;
    public final static int ID_FICHERO = 2;
    public final static int ID_URL     = 3;

    private CtrlTiposSolucion ctrlTiposSolucion;

    public TipossolucionService(CtrlTiposSolucion dao)
    {
        this.ctrlTiposSolucion = dao;
    }

    /**
     *
     */
    public List<TipoSolucion> getAll()
    {
        return this.ctrlTiposSolucion.select();
    }

    public TipoSolucion get(int id)
    {
    	//@TODO make this method useful again

        //for (TipoSolucion sol : tiposSolucion) if (sol.getId() == id) return sol;
        return null;
    }
}
