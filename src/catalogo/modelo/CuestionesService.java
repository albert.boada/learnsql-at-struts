//////////////////////////////////////////////////////
//  CLASSE: DiccCatologo.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import catalogo.actions.RestfulValidationException;
import catalogo.comunicacion.CorrectorCliente;
import catalogo.dtos.ComprobacionDTO;
import catalogo.dtos.CuestionDTO;
import catalogo.dtos.JuegoPruebasDTO;
import catalogo.gestiondatos.CtrlCategorias;
import catalogo.gestiondatos.CtrlComprobacion;
import catalogo.gestiondatos.CtrlCuestion;
import catalogo.gestiondatos.CtrlEsquemas;
import catalogo.gestiondatos.CtrlJuegoPruebas;
import catalogo.gestiondatos.CtrlTematicas;
import catalogo.gestiondatos.CtrlTiposSolucion;
import catalogo.gestiondatos.SaveOnCommit;

public class CuestionesService extends BaseService
{
	private CtrlCuestion ctrlCuestion;

	private CtrlCategorias ctrlCategorias;
	private CtrlEsquemas ctrlEsquemas;
	private CtrlTematicas ctrlTematicas;
	private CtrlTiposSolucion ctrlTiposSolucion;

	private CtrlJuegoPruebas ctrlJuegoPruebas;

	private CtrlComprobacion ctrlComprobacion;

	private CorrectorCliente cliente;

	private String autor;

	/**
	 *
	 */
	public CuestionesService(CtrlCuestion ctrlCuestion,
							 CtrlCategorias ctrlCategorias,
							 CtrlEsquemas ctrlEsquemas,
							 CtrlTematicas ctrlTematicas,
							 CtrlTiposSolucion ctrlTiposSolucion,
							 CtrlJuegoPruebas ctrlJuegoPruebas, /*@TODO try to eliminar aquesta dependència!! */
							 CtrlComprobacion ctrlComprobacion,
							 CorrectorCliente cliente,
							 String autor
	) {
		this.ctrlCuestion     = ctrlCuestion;
		this.ctrlCategorias    = ctrlCategorias;
		this.ctrlEsquemas      = ctrlEsquemas;
		this.ctrlTematicas     = ctrlTematicas;
		this.ctrlTiposSolucion = ctrlTiposSolucion;
		this.ctrlJuegoPruebas = ctrlJuegoPruebas;
		this.ctrlComprobacion = ctrlComprobacion;
		this.autor           = autor;
		this.cliente         = cliente;
	}

	/**
     *
     */
	public Cuestion find(int id) throws Exception
	{
		List<Cuestion> l = this.search(id, 0, null, null, -1, -1, null, null, null);
		return l.get(0);
	}


	/**
	 *
	 */
	public List<Cuestion> getAll() throws Exception
	{
		return this.search(0, 0, null, null, -1, -1, null, null, null);
	}

	/**
	 *
	 */
	public List<Cuestion> search(int id,
					   int estado,
					   String titulo,
					   String autor,
					   float mindificultad,
					   float maxdificultad,
					   List<Categoria> categorias,
					   List<Esquema> esquemas,
					   List<Tematica> tematicas
	) throws Exception
	{
		if (mindificultad == 0 && maxdificultad == 1) {
			mindificultad = -1;
			maxdificultad = -1;
		}

		return this.ctrlCuestion.select(id, estado, titulo, autor, mindificultad, maxdificultad, categorias,esquemas, tematicas);
	}

	/*
	public void clearSalidas(Corrector cor) {
		for (Cuestion c : this.cuestiones) {
			if (c.getCorrectores().contains(cor)) c.clearSalidas();
		}
	}
	*/

	/*
	public void actualizar(Cuestion c){
		try{
			cuestionDao.update(c);
		}catch(Exception e){
			System.out.println("error al update "+e.getMessage());
		}
	}
	*/

	public void actualizarCorrectores(Cuestion c)
	{
		try {
			ctrlCuestion.insertCorrectores(c.getCorrectores(), c.getId());
			ctrlCuestion.updateSalidas(c);
			ctrlCuestion.commit();
		}
		catch (Exception e) {
			System.out.println("error al actualizar correctores "+e.getMessage());
		}
	}

	/*
	public ArrayListModel<Cuestion> getTodasCuestiones() throws Exception
	{
		ArrayListModel<Cuestion> todascuestiones = new ArrayListModel<Cuestion>();
		for (Cuestion c : cuestionDao.select(0, 0, null, null, -1, -1, null, null, null)) todascuestiones.add(c);
		return todascuestiones;
	}
	*/

	/**
	 *
	 */
	public Cuestion create(Cuestion c) throws Exception
	{
		this.validate(c, CuestionesService.CREATE);
		if (!this.validation.isEmpty()) {
			throw new RestfulValidationException(this.validation);
		}

		SaveOnCommit soc = new SaveOnCommit(this.ctrlCuestion, this.ctrlJuegoPruebas, this.ctrlComprobacion);
		soc.inicializa(c);
		soc.setCuestionModif();
		// soc.setSalidasModif();
		// c.setDisponible(false);
		this.__registraCambiosJps(c, new Cuestion(), soc);
		soc.commit();

		return this.find(c.getId());
	}

	/**
	 *
	 */
	public Cuestion update(Cuestion c) throws Exception
	{
		this.validate(c, CuestionesService.UPDATE);
		if (!this.validation.isEmpty()) { 
			throw new RestfulValidationException(this.validation); 
		}

		SaveOnCommit soc = new SaveOnCommit(this.ctrlCuestion, this.ctrlJuegoPruebas, this.ctrlComprobacion);
		soc.inicializa(c);
		soc.setCuestionModif();
		Cuestion c_old = this.find(c.getId());

		c.setDisponible(c_old.getDisponible());

		if (!CuestionesService.__equals(c.getInits(), c_old.getInits())
			|| !CuestionesService.__equals(c.getSolucion(), c_old.getSolucion())
			|| !CuestionesService.__equals(c.getLimpieza(), c_old.getLimpieza())
			|| !CuestionesService.__equals(c.getPostInits(), c_old.getPostInits())
		) {
			soc.setSalidasModif();
		}

		this.__registraCambiosJps(c, c_old, soc);

		soc.commit();

		return this.find(c.getId());
	}

	private static boolean __equals(String str1, String str2) {
		return str1 == null ? str2 == null : str1.equals(str2);
	}

	private void __registraCambiosJps(Cuestion c, Cuestion c_old, SaveOnCommit soc) {
		Boolean found;
		Boolean found2;
		for (JuegoPruebas jp : c.getJuegosPruebas()) {
			found = false;
			for (JuegoPruebas jp_old : c_old.getJuegosPruebas()) {
				if (jp.getId() != null && jp.getId().equals(jp_old.getId())) {
					found = true;

					// Check comprobaciones as well
					for (Comprobacion compr : jp.getComprobaciones()) {
						found2 = false;
						for (Comprobacion compr_old : jp_old.getComprobaciones()) {
							if (compr.getId() != null && compr.getId().equals(compr_old.getId())) {
								found2 = true;

								// Check if Salida could change because modified sensitive fields
								if (!soc.isSalidasModif()) {
									if (!CuestionesService.__equals(compr.getEntrada(), compr_old.getEntrada())) {
										soc.setSalidasModif();
									}
								}
							}
						}
						if (found2) {
							soc.set(compr, SaveOnCommit.ESTADO_MODIFICADO);
						}
						else {
							soc.set(compr, SaveOnCommit.ESTADO_NUEVO);
							soc.setSalidasModif(); // Salida will change because of new Compr additions
						}
					}

					// Check if Salida could change because modified sensitive fields
					if (!soc.isSalidasModif()) {
						if (!CuestionesService.__equals(jp.getInits(), jp_old.getInits())
							|| !CuestionesService.__equals(jp.getEntrada(), jp_old.getEntrada())
							|| !CuestionesService.__equals(jp.getLimpieza(), jp_old.getLimpieza())
						) {
							soc.setSalidasModif();
						}
					}
				}
			}
			if (found) {
				soc.set(jp, SaveOnCommit.ESTADO_MODIFICADO);
			}
			else {
				soc.set(jp, SaveOnCommit.ESTADO_NUEVO);
				soc.setSalidasModif(); // Salida will change because of new JP additions
			}
		}
		for (JuegoPruebas jp_old : c_old.getJuegosPruebas()) {
			found = false;
			for (JuegoPruebas jp : c.getJuegosPruebas()) {
				if (jp.getId() != null && jp_old.getId().equals(jp.getId())) {
					found = true;

					for (Comprobacion compr_old : jp_old.getComprobaciones()) {
						found2 = false;
						for (Comprobacion compr : jp.getComprobaciones()) {
							if (compr.getId() != null && compr_old.getId().equals(compr.getId())) {
								found2 = true;
							}
						}
						if (!found2) {
							soc.set(compr_old, SaveOnCommit.ESTADO_ELIMINADO);
							soc.setSalidasModif(); // Salida will change because of deleted Comprs
						}
					}
				}
			}
			if (!found) {
				soc.set(jp_old, SaveOnCommit.ESTADO_ELIMINADO);
				soc.setSalidasModif(); // Salida will change because of deleted JPs
			}
		}
	}

	/**
	 *
	 */
	public void validate(Cuestion c, String mode) throws Exception
	{
		this.resetValidations();

		if (c == null) {
			throw new Exception("question is null on validate");
		}

		int postjp = c.getMaxOrden();

        if (c.getTitulo() == null || c.getTitulo().trim().isEmpty()) {
        	this.setValidation("EMPTY", "", "TITLE", null);
        }
        else if (c.getEnunciado() == null || c.getEnunciado().trim().isEmpty()) {
        	this.setValidation("EMPTY", "", "WORDING", null);
        }
        else if (c.getAutor() == null || c.getAutor().trim().isEmpty()) {
        	this.setValidation("EMPTY", "", "AUTHOR", null);
        }
        else if (c.getCategoria() == null || c.getCategoria().getId() == 0) {
        	this.setValidation("EMPTY", "", "CATEGORY", null);
        }
        else if (c.getEsquema() == null) {
        	this.setValidation("EMPTY", "", "SCHEMA", null);
        }
        else if (c.getPostInitsJP() > postjp) {
        	HashMap<String, String> ruleValues = new HashMap<String, String>();
        	ruleValues.put("tss", Integer.toString(postjp));
        	this.setValidation("POSTINITSJP", "", "POSTINITSJP", ruleValues);
        }
        else if (c.getTipoSolucion() == null) {
        	this.setValidation("EMPTY", "", "SOLUTIONTYPE", null);
        }
        else if (c.getTipoSolucion().getId() == TipossolucionService.ID_TEXTO && c.getSolucion().isEmpty()) {
        	this.setValidation("EMPTY", "", "SOLUTION", null);
        }
        else if (c.getTipoSolucion().getId() == TipossolucionService.ID_FICHERO && c.getBytesSolucion() == null) {
        	this.setValidation("EMPTY", "", "SOLUTION", null);
        }
    	else if (c.getTipoSolucion().getId() == TipossolucionService.ID_URL) {
    		if (c.getUrl().isEmpty()) {
        		this.setValidation("EMPTY", "", "URL", null);
    		}
    		else if (c.getUsuario().isEmpty()) {
        		this.setValidation("EMPTY", "", "USER", null);
    		}
    		else if (c.getClave().isEmpty()) {
        		this.setValidation("EMPTY", "", "PASSWORD", null);
    		}
    	}
    	else {
    		this.validateJPs(c);
    	}
	}

	public void validateJPs(Cuestion c) {
		if (!c.getJuegosPruebas().isEmpty()) {
			// float total = .0f;
			int i = 0;
            for (JuegoPruebas jp : c.getJuegosPruebas()) {
            	this.validateJP(jp, i);
            	i++;
                // total += jp.getPeso();
            }
            /*
            if (this.validation.isEmpty()) {
            	total *= 100;
            	total = (float) Math.round(total) / 100;
            	if (total != 1.0f) {
            		this.setValidation("TESTSETSWEIGHTS", "", null, null);
            	}
            }
            */
		}
	}

	public void validateJP(JuegoPruebas jp, int index) {
		String path = "juegosPruebas."+index;
        if (jp.getNombre() == null || jp.getNombre().trim().isEmpty()) {
        	this.setValidation("EMPTY", path, "NAME", null);
        }
        else if (jp.getDescripcion() == null || jp.getDescripcion().trim().isEmpty()) {
        	this.setValidation("EMPTY", path, "DESCRIPTION", null);
        }
        else if (jp.getMsgError() == null || jp.getMsgError().trim().isEmpty()) {
        	this.setValidation("EMPTY", path, "ERROR_MSG", null);
        }
        else if (jp.getPeso() < 0 || jp.getPeso() > 1) {
        	this.setValidation("NOTVALID", path, "WEIGHT", null);
        }
        else {
        	this.validateComprs(jp, index);
        }
	}

	public void validateComprs(JuegoPruebas jp, int jpIndex) {
		if (!jp.getComprobaciones().isEmpty()) {
			int i = 0;
            for (Comprobacion compr : jp.getComprobaciones()) {
            	this.validateCompr(compr, jpIndex, i);
            	i++;
            }
		}
	}

	public void validateCompr(Comprobacion compr, int jpIndex, int comprIndex) {
		String path = "juegosPruebas."+jpIndex+".comprobaciones."+comprIndex;
		if (compr.getNombre() == null || compr.getNombre().trim().isEmpty()) {
        	this.setValidation("EMPTY", path, "NAME", null);
		}
		else if (compr.getDescripcion() == null || compr.getDescripcion().trim().isEmpty()) {
			this.setValidation("EMPTY", path, "DESCRIPTION", null);
		}
		else if (compr.getMsgError() == null || compr.getMsgError().trim().isEmpty()) {
			this.setValidation("EMPTY", path, "ERROR_MSG", null);
		}
		else if (compr.getEntrada() == null || compr.getEntrada().trim().isEmpty()) {
        	this.setValidation("EMPTY", path, "INPUT", null);
		}
	}

	public Cuestion copy(Cuestion c) throws Exception
	{
		Cuestion copia = new Cuestion(c);
		copia.setAutor(autor);
		copia.setTitulo("Copia de " + c.getTitulo());
		int id = ctrlCuestion.insert(copia);
		for (JuegoPruebas jp : copia.getJuegosPruebas()) {
			ctrlJuegoPruebas.insert(jp, id);
			for (Comprobacion compr : jp.getComprobaciones()) ctrlComprobacion.insert(compr, id);
		}
		ctrlCuestion.insertCorrectores(copia.getCorrectores(), id);
		ctrlCuestion.commit();
		copia.setId(id);
		copia.clearSalidas();
		return copia;
	}

	/*
	public void replace(Cuestion original, Cuestion nueva) {
		int pos = cuestiones.getIndex(original);
		if (pos != -1) cuestiones.set(pos, nueva);
	}
	*/

	public boolean remove(Cuestion c) throws Exception
	{
		ctrlCuestion.delete(c);
		ctrlCuestion.commit();
		//int index = cuestiones.getIndex(c);
		//cuestiones.remove(c);
		//return getNearest(index);

		return true;
	}
	
	public void lock(Cuestion c) throws Exception {
		SaveOnCommit soc = new SaveOnCommit(this.ctrlCuestion, this.ctrlJuegoPruebas, this.ctrlComprobacion);
		soc.inicializa(c);
		soc.setCuestionModif();
		soc.setBlock(!c.getBloqueada());
		c.setBloqueada(!c.getBloqueada());
	}

	public Resultado generaSalidas(Cuestion c)
	{
		System.out.println("Id: "+c.getId());
		String texto = "";
		boolean exec = false;
		int dif = -1;
		int codigo;
		int idCuestion = c.getId();
		int idCategoria = c.getCategoria().getId();
		int idTipoSol = c.getTipoSolucion().getId();
		List<Corrector> correctores = c.getCorrectores();
		System.out.println("Estado: " + cliente.checkRefereeStatus(correctores.get(0).getURL()) + "\nMissatge: " + cliente.getMensaje());
		codigo = cliente.generateOutputs(correctores.get(0).getURL(), idCuestion, idCategoria, idTipoSol);
		if (codigo == 0) {
			exec =true;
			int num = c.getNumPesoCero();
			if (num > 1) texto = "Salidas generadas correctamente (recuerde que existen " + num + " juegos de pruebas con peso 0)\n";
			else if (num == 1) texto = "Salidas generadas correctamente (recuerde que existe 1 juego de pruebas con peso 0)\n";
			else texto = "Salidas generadas correctamente\n";
			for (Corrector cor : c.getCorrectores().subList(1, c.getCorrectores().size())) {
				System.out.println("corrector: "+cor);
				texto += cor.getURL() + ": ";
				codigo = cliente.compareOutputs(cor.getURL(), idCuestion, idCategoria, idTipoSol);
				dif =codigo;
				if (codigo == CorrectorCliente.SALIDAS_DIFERENTES) {
					texto += "Distintas salidas generadas.\n1. Salida existente:\n" + cliente.getSalidaExistente() +
							"\n2. Salida generada:\n" + cliente.getSalidaGenerada() + "\n";
					break;
				} else if (codigo != 0) {
					texto += cliente.getMensaje() + "\n";
					break;
				} else {
					texto += "Idénticas salidas generadas.\n";
				}
			}
		}
		else texto += cliente.getMensaje() + "\n";
		c.setExec(exec);
		c.setDif(dif);
		//Revisar el 0 del new resultado!!!
		return new Resultado(codigo, texto, cliente.getOrdenJuegoPruebas(), cliente.getOrdenComprobacion(),null,0);
	}

	public List<Resultado> generaComparacion(Cuestion c)throws Exception {
		String texto = "";
		int codigo;
		int idCuestion = c.getId();
		int idCategoria = c.getCategoria().getId();
		int idTipoSol = c.getTipoSolucion().getId();
		List<Corrector> correctores = c.getCorrectores();
		List<Resultado> res = new ArrayList<Resultado>();
		String[] solucionalumno = {c.getSolucionAlumno()};
		try{
			codigo = cliente.beginRefereeingProcess(correctores.get(0).getURL(), idCuestion, idCategoria, idTipoSol, solucionalumno);
			if (codigo == 0) {
				res = cliente.checkRefereeingProcessStatus(correctores.get(0).getURL(), idCuestion, idCategoria, idTipoSol);
				while(res.get(0).codigo == 100){
					esperar(2);
					res = cliente.checkRefereeingProcessStatus(correctores.get(0).getURL(), idCuestion, idCategoria, idTipoSol);
				}
			}
			else{
				res.add(new Resultado(codigo, cliente.getMensaje(),0,0,"salida",1));
			}
			return res;

		}catch(Exception e){
			throw e;
		}
	}
	
	/*public CuestionDTO entityToSimpleDTO(Cuestion c) {
		CuestionDTO dto = new CuestionDTO();
		dto.setId(c.getId());
		dto.setTitulo(c.getTitulo());
		dto.setAutor(c.getAutor());
		dto.setDificultad(c.getDificultad());
		Categoria cat = c.getCategoria();
		if (cat != null) {
			dto.setCategoriaId(cat.getId());
		}
		Esquema sch = c.getEsquema();
		if (sch != null) {
			dto.setEsquemaId(sch.getNombre());
		}
		for (Tematica t : c.getTematicas()) {
			dto.getTematicasIds().add(t.getNombre());
		}
		dto.setDisponible(c.getDisponible());
		dto.setBloqueada(c.getBloqueada());
		dto.setEstado(c.getEstado());
		
		return dto;
	}*/

	public CuestionDTO entityToDTO(Cuestion c) {
		CuestionDTO form = new CuestionDTO();
		form.setId(c.getId());
		form.setTitulo(c.getTitulo());
		form.setEnunciado(c.getEnunciado());
		form.setEnunciadoEsp(c.getEnunciadoEsp());
		form.setEnunciadoIng(c.getEnunciadoIng());
		if (c.getExtensionAdjunto().equals("txt") || c.getExtensionAdjunto().equals("sql")) {
			form.setFicheroAdjunto(new String(c.getFicheroAdjunto()));
		}
		form.setExtensionAdjunto(c.getExtensionAdjunto());
		form.setAutor(c.getAutor());
		form.setDificultad(c.getDificultad());
		Categoria cat = c.getCategoria();
		if (cat != null) {
			form.setCategoriaId(cat.getId());
		}
		Esquema sch = c.getEsquema();
		if (sch != null) {
			form.setEsquemaId(sch.getNombre());
		}
		for (Tematica t : c.getTematicas()) {
			form.getTematicasIds().add(t.getNombre());
		}
		form.setBinaria(c.getBinaria());
		form.setGabia(c.getGabia());
		form.setInits(c.getInits());
		form.setSolucion(c.getSolucion());
		form.setSolucionAlumno(c.getSolucionAlumno());
		form.setLimpieza(c.getLimpieza());
		form.setPostInits(c.getPostInits());
		form.setPostInitsJP(c.getPostInitsJP());
		TipoSolucion ts = c.getTipoSolucion();
		if (ts != null) {
			form.setTiposolucionId(ts.getId());
		}
		form.setUrl(c.getUrl());
		form.setUsuario(c.getUsuario());
		form.setClave(c.getClave());
		form.setSsl(c.getSsl());
		if (c.getExtensionSolucion().equals("txt") || c.getExtensionSolucion().equals("sql")) {
			form.setFicheroSolucion(new String(c.getFicheroSolucion()));
		}
		form.setExtensionSolucion(c.getExtensionSolucion());
		Integer i = 0;
		Integer j;
		for (JuegoPruebas jp : c.getJuegosPruebas()) {
			JuegoPruebasDTO jpform = new JuegoPruebasDTO();
			jpform.setId(jp.getNombre());
			jpform.setNombre(jp.getNombre());
			jpform.setPeso(jp.getPeso());
			jpform.setDescripcion(jp.getDescripcion());
			jpform.setMsgError(jp.getMsgError());
			jpform.setMsgErrorEsp(jp.getMsgErrorEsp());
			jpform.setMsgErrorIng(jp.getMsgErrorIng());
			jpform.setInits(jp.getInits());
			jpform.setEntrada(jp.getEntrada());
			jpform.setSalida(jp.getSalida());
			jpform.setSalidaAlumno(jp.getSalidaAlumno());
			jpform.setLimpieza(jp.getLimpieza());
			jpform.setBinaria(jp.getBinaria());
			jpform.setMensaje(jp.getMensaje());
			jpform.setConsumeix(jp.getConsumeix());
			j = 0;
			for (Comprobacion cmp : jp.getComprobaciones()) {
				ComprobacionDTO cmpform = new ComprobacionDTO();
				cmpform.setId(cmp.getNombre());
				cmpform.setNombre(cmp.getNombre());
				cmpform.setDescripcion(cmp.getDescripcion());
				cmpform.setMsgError(cmp.getMsgError());
				cmpform.setMsgErrorEsp(cmp.getMsgErrorEsp());
				cmpform.setMsgErrorIng(cmp.getMsgErrorIng());
				cmpform.setEntrada(cmp.getEntrada());
				cmpform.setSalida(cmp.getSalida());
				cmpform.setSalidaAlumno(cmp.getSalidaAlumno());
				cmpform.setBinaria(cmp.getBinaria());
				cmpform.setConsumeix(cmp.getConsumeix());
				jpform.getComprobaciones().add(cmpform);
				j++;
			}
			form.getJuegosPruebas().add(jpform);
			i++;
		}
		form.setDisponible(c.getDisponible());
		form.setBloqueada(c.getBloqueada());
		form.setEstado(c.getEstado());

		return form;
	}

	public Cuestion DTOToEntity(CuestionDTO form) throws Exception {
		Cuestion c = new Cuestion();
		c.setId(form.getId());
		c.setTitulo(form.getTitulo());
		c.setEnunciado(form.getEnunciado());
		c.setEnunciadoEsp(form.getEnunciadoEsp());
		c.setEnunciadoIng(form.getEnunciadoIng());
		if (form.getFicheroAdjunto() != null) {
			c.setFicheroAdjunto(form.getFicheroAdjunto().getBytes(), form.getExtensionAdjunto());
		}
		c.setAutor(form.getAutor());
		c.setDificultad(form.getDificultad());
		c.setCategoria(this.ctrlCategorias.get(form.getCategoriaId()));
		c.setEsquema(this.ctrlEsquemas.get(form.getEsquemaId()));
		c.setTematicas(this.ctrlTematicas.getByNombres(form.getTematicasIds()));
		c.setBinaria(form.getBinaria());
		c.setGabia(form.getGabia());
		c.setInits(form.getInits());
		c.setSolucion(form.getSolucion());
		c.setSolucionAlumno(form.getSolucionAlumno());
		c.setLimpieza(form.getLimpieza());
		c.setPostInits(form.getPostInits());
		c.setPostInitsJP(form.getPostInitsJP());
		c.setTipoSolucion(this.ctrlTiposSolucion.get(form.getTiposolucionId()));
		c.setUrl(form.getUrl());
		c.setUsuario(form.getUsuario());
		c.setClave(form.getClave());
		c.setSsl(form.getSsl());
		if (form.getFicheroSolucion() != null) {
			c.setFicheroAdjunto(form.getFicheroSolucion().getBytes(), form.getExtensionSolucion());
		}
		form.setExtensionSolucion(c.getExtensionSolucion());
		JuegoPruebas jp = null;
		Comprobacion cmp = null;
		for (JuegoPruebasDTO jpform: form.getJuegosPruebas()) {
			//if (jpform.getId() == null) { // nou jp
			//	jp = new JuegoPruebas(jpform.getNombre());
			//} else {
				jp = new JuegoPruebas(jpform.getId());
			//}
			jp.setCuestion(c);
			jp.setNombre(jpform.getNombre());
			jp.setPeso(jpform.getPeso());
			jp.setDescripcion(jpform.getDescripcion());
			jp.setMsgError(jpform.getMsgError());
			jp.setMsgErrorEsp(jpform.getMsgErrorEsp());
			jp.setMsgErrorIng(jpform.getMsgErrorIng());
			jp.setInits(jpform.getInits());
			jp.setEntrada(jpform.getEntrada());
			// jp.setSalida(jpform.getSalida()); // useless, never trust frontend
			// jp.setSalidaAlumno(jpform.getSalidaAlumno()); // useless, never trust frontend
			jp.setLimpieza(jpform.getLimpieza());
			jp.setBinaria(jpform.getBinaria());
			jp.setMensaje(jpform.getMensaje());
			jp.setConsumeix(jpform.getConsumeix());
			for (ComprobacionDTO cmpform: jpform.getComprobaciones()) {
				//if (cmpform.getId() == null) { // nova compr
				//	cmp = new Comprobacion(cmpform.getNombre());
				//} else {
					cmp = new Comprobacion(cmpform.getId());
				//}
				cmp.setJuegoPruebas(jp);
				cmp.setNombre(cmpform.getNombre());
				cmp.setDescripcion(cmpform.getDescripcion());
				cmp.setMsgError(cmpform.getMsgError());
				cmp.setMsgErrorEsp(cmpform.getMsgErrorEsp());
				cmp.setMsgErrorIng(cmpform.getMsgErrorIng());
				cmp.setEntrada(cmpform.getEntrada());
				// cmp.setSalida(cmpform.getSalida()); // useless, never trust frontend
				// cmp.setSalidaAlumno(cmpform.getSalidaAlumno()); // useless, never trust frontend
				cmp.setBinaria(cmpform.getBinaria());
				cmp.setConsumeix(cmpform.getConsumeix());
				jp.getComprobaciones().add(cmp);
			}
			c.getJuegosPruebas().add(jp);
		}
		// c.setDisponible(form.isDisponible()); // useless, never trust frontend
		c.setBloqueada(form.isBloqueada());
		c.setEstado(form.getEstado());

		return c;
	}


	// Métodos auxiliares *****************************************************

	/*
	private Cuestion getNearest(int index) {
		int total = cuestiones.getSize();
		if (total == 0) return new Cuestion(autor);
		else if (index <= 0) return cuestiones.get(0);
		else if (index < total) return cuestiones.get(index);
		else return cuestiones.get(total - 1);
	}
	*/


	private void esperar (int segundos){
		try {
			Thread.sleep (segundos*1000);
		}catch (Exception e) {
			System.out.println("Espera error: " +e);
		}

	}
}
