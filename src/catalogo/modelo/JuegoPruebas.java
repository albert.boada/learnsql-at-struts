//////////////////////////////////////////////////////
//  CLASSE: JuegoPruebas.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import catalogo.global.CtrlModelo;
import catalogo.soporte.modelos.ArrayListComboBoxModel;
import catalogo.soporte.utils.ItemTreeTable;
import catalogo.soporte.utils.TextParser;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;

public class JuegoPruebas extends ItemTreeTable implements TreeNode {// ,Comparable<JuegoPruebas> {

	/*********************************************************/
	/* Atributos */
	/*********************************************************/

	private Cuestion cuestion;
	private float peso;

	private String id;
	private String nombre;
	private String descripcion;
	private String msgError;
	private String msgErrorEsp;
	private String msgErrorIng;
	private String inits;
	private String entrada;
	private String salida;
	private String salidaAlumno;
	private String limpieza;
	private Boolean binaria;
	private Boolean mensaje;
	private Boolean consumeix;

	private Idioma idioma;

	private ArrayListComboBoxModel<Comprobacion> comprobaciones;

	/*********************************************************/
	/* Métodos constructores */
	/*********************************************************/

	public JuegoPruebas() {
		inicializa();
	}

	public JuegoPruebas(String nombre) {
		inicializa();
		this.id     = nombre;
		this.nombre = nombre;
	}

	public JuegoPruebas(JuegoPruebas jp) {
		inicializa();
		this.id     = jp.nombre;
		this.nombre = jp.nombre;
		this.peso = jp.peso;
		this.descripcion = jp.descripcion;
		this.msgError = jp.msgError;
		this.msgErrorEsp = jp.msgErrorEsp;
		this.msgErrorIng = jp.msgErrorIng;
		this.inits = jp.inits;
		this.entrada = jp.entrada;
		this.salida = jp.salida;
		this.salidaAlumno = jp.salidaAlumno;
		this.limpieza = jp.limpieza;
		this.binaria = jp.binaria;
		this.mensaje = jp.mensaje;
		this.consumeix = jp.consumeix;
		this.idioma = jp.idioma;
		Comprobacion comprNew;
		for (Comprobacion compr : jp.comprobaciones) {
			comprNew = new Comprobacion(compr);
			this.addComprobacion(comprNew);
		}
	}

	/*********************************************************/
	/* Métodos Get/Set */
	/*********************************************************/

	public Cuestion getCuestion() {
		return cuestion;
	}

	public int getOrden() {
        if (cuestion ==  null) return -1;
        else return cuestion.getOrdenJP(this);
	}

	public float getPeso() {
		return peso;
	}

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getMsgError() {
		return msgError;
	}

	public String getMsgErrorEsp() {
		return msgErrorEsp;
	}

	public String getMsgErrorIng() {
		return msgErrorIng;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getInits() {
		return inits;
	}

	public String getEntrada() {
		return entrada;
	}

	public String getSalida() {
		return salida;
	}

	public String getSalidaAlumno() {
		return salidaAlumno;
	}

	public String getLimpieza() {
		return limpieza;
	}

	public Boolean getBinaria() {
		return binaria;
	}

	public Boolean getMensaje() {
		return mensaje;
	}

	public Boolean getConsumeix() {
		return consumeix;
	}

	public ArrayListComboBoxModel<Comprobacion> getComprobaciones() {
		return comprobaciones;
	}

	public Idioma getIdioma() {
		return idioma;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	public void setMsgErrorEsp(String msgError) {
		this.msgErrorEsp = msgError;
	}

	public void setMsgErrorIng(String msgError) {
		this.msgErrorIng = msgError;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setInits(String inits) {
		this.inits = inits;
	}

	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}

	public void setLimpieza(String limpieza) {
		this.limpieza = limpieza;
	}

	public void setSalida(String salida) {
		this.salida = TextParser.parseSalida(salida);
	}

	public void setSalidaAlumno(String salidaAlumno) {
		this.salidaAlumno = TextParser.parseSalida(salidaAlumno);
	}

	public void setBinaria(Boolean binaria) {
		this.binaria = binaria;
	}

	public void setMensaje(Boolean mensaje) {
		this.mensaje = mensaje;
	}

	public void setConsumeix(Boolean consumeix) {
		this.consumeix = consumeix;
	}

	public void setCuestion(Cuestion c) {
		this.cuestion = c;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}

	// Métodos para gestionar juegos de pruebas *******************************

	public void addComprobacion(Comprobacion compr) {
		comprobaciones.add(compr);
		compr.setJuegoPruebas(this);
	}

	public void clearSalidas() {
		salida = "";
		setEstado(ItemTreeTable.ESTADO_SIN_RESOLVER);
        for (Comprobacion compr : comprobaciones) compr.clearSalida();
	}

	public void swapComprobaciones(Comprobacion compr1, Comprobacion compr2) {
		int pos1 = comprobaciones.getIndex(compr1);
		int pos2 = comprobaciones.getIndex(compr2);
		comprobaciones.set(pos1, compr2);
		comprobaciones.set(pos2, compr1);
	}

	public void removeComprobacion(Comprobacion compr) {
		comprobaciones.remove(compr);
	}

	// Implementación interfaz TreeNode ***************************************

	public TreeNode getChildAt(int childIndex) {
		return (TreeNode) comprobaciones.get(childIndex);
	}

	public int getChildCount() {
		return comprobaciones.size();
	}

	public TreeNode getParent() {
		return cuestion;
	}

	public int getIndex(TreeNode node) {
		for (int i = 0; i < comprobaciones.size(); i++) {
            if (comprobaciones.get(i) == node) return i;
		}
		return -1;
	}

	public boolean getAllowsChildren() {
		return true;
	}

	public boolean isLeaf() {
		return comprobaciones.isEmpty();
	}

	public Enumeration children() {
		return new Vector(comprobaciones).elements();
	}

	// Otros ******************************************************************

	private void inicializa() {
		cuestion = null;
		comprobaciones = new ArrayListComboBoxModel<Comprobacion>();
		peso = .0f;
		nombre = "";
		descripcion = "";
		msgError = "";
		msgErrorEsp = "";
		msgErrorIng = "";
		inits = "";
		entrada = "";
		limpieza = "";
		salida = "";
		salidaAlumno = "";
		binaria = false;
		mensaje = false;
		consumeix = false;

		// idioma = CtrlModelo.idiomas.getAll().get(0); de què serveix?

		setEstado(ItemTreeTable.ESTADO_SIN_RESOLVER);
		// c.addJuegoPruebas(this);
	}

	@Override
	public String toString() {
        if (nombre.isEmpty()) return "Nuevo JP (#" + getOrden() + ")";
        else return nombre+"-"+cuestion;
	}

	public int getOrdenCompr(Comprobacion compr) {
        if (!comprobaciones.contains(compr)) return -1;
        else return comprobaciones.getIndex(compr) + 1;
	}

} // Fin clase JuegoPruebas

