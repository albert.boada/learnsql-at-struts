//////////////////////////////////////////////////////
//  CLASSE: DiccEsquemas.java                       //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import java.util.ArrayList;
import java.util.List;

import catalogo.actions.RestfulValidationException;
import catalogo.dtos.EsquemaDTO;
import catalogo.gestiondatos.CtrlEsquemas;
import catalogo.soporte.modelos.ArrayListComboBoxModel;

public class EsquemasService extends BaseService
{
    private CtrlEsquemas ctrlEsquemas;

    public EsquemasService(CtrlEsquemas dao) throws Exception
    {
        this.ctrlEsquemas = dao;
    }

    /**
     * @throws Exception
     *
     */
    public List<Esquema> getAll() throws Exception
    {
        return this.ctrlEsquemas.select();
    }

    public Esquema find(String nombre) throws Exception
    {
    	return this.ctrlEsquemas.get(nombre);
    }

    /**
     *
     */
    public Esquema create(Esquema esq) throws Exception
    {
        this.validate(esq, null, EsquemasService.CREATE);
        if (!this.validation.isEmpty()) { throw new RestfulValidationException(this.validation); }

        try {
            this.ctrlEsquemas.insert(esq.getNombre(), esq.getDescripcion());
            
            return this.find(esq.getNombre());
        }
        catch (Exception e) {
            throw e;
        	//this.validation.put("esquema._global", e.getMessage());
            //return null;
        }
    }

    /**
     *
     */
    public Esquema update(Esquema esq, String id) throws Exception
    {
        this.validate(esq, id, EsquemasService.UPDATE);
        if (!this.validation.isEmpty()) { throw new RestfulValidationException(this.validation); }

        try {
            this.ctrlEsquemas.update(id, esq.getNombre(), esq.getDescripcion());
            
            return this.find(esq.getNombre());
        }
        catch (Exception e) {
            throw e;
        	//this.validation.put("esquema._global", e.getMessage());
            //return null;
        }
    }

    /**
     *
     */
    public void validate(Esquema esq, String id, String mode) throws Exception
    {
        this.resetValidations();

        if (esq == null) {
            throw new Exception("schema is null on validate");
        }

        if (esq.getNombre() == null || esq.getNombre().trim().isEmpty()) {
            this.setValidation("EMPTY", "", "NAME", null);
        }
        else {
        	Esquema existing_esq = this.ctrlEsquemas.get(esq.getNombre());
        	if (existing_esq != null && (mode == EsquemasService.CREATE || (mode == EsquemasService.UPDATE && !existing_esq.getNombre().equals(id)))) {
        		this.setValidation("EXISTING", "", "ID", null);        		
        	}
        	else if (esq.getDescripcion() == null || esq.getDescripcion().trim().isEmpty()) {
        		this.setValidation("EMPTY", "", "DESCRIPTION", null);
        	}
        }
    }

    public void remove(Esquema esq) throws Exception
    {
        ctrlEsquemas.delete(esq.getNombre());
    }

    public EsquemaDTO entityToForm(Esquema esq) {
        EsquemaDTO form = new EsquemaDTO();

        form.setId(esq.getNombre());
        form.setNombre(esq.getNombre());
        form.setDescripcion(esq.getDescripcion());

        return form;
    }


    public Esquema formToEntity(EsquemaDTO form) throws Exception {
        Esquema esq = new Esquema();
        esq.setNombre(form.getNombre());
        esq.setDescripcion(form.getDescripcion());

        return esq;
    }
}
