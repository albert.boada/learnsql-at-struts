//////////////////////////////////////////////////////
//  CLASSE: Cuestion.java                           //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//          versio 3: Albert Boada Flaquer          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.tree.TreeNode;

import catalogo.soporte.modelos.ArrayListComboBoxModel;
import catalogo.soporte.modelos.ArrayListModel;
import catalogo.soporte.utils.ItemTable;

public class Cuestion extends ItemTable implements TreeNode/*, Comparable<Cuestion>*/
{

/*******************************************************************************
 * Properties
 */

    private int idCuestion;
    private String titulo;

    // Phrasing
    private String enunciado;
    private String enunciadoEsp;
    private String enunciadoIng;
    // private byte[] ficheroAdjunto;
    private byte[] ficheroAdjunto;
    private String extensionAdjunto;

    // Classification
    private String autor;
    private float dificultad;
    private Categoria categoria;
    private Esquema esquema;
    private ArrayListModel<Tematica> tematicas;
    private Boolean binaria;
    private Boolean gabia;

    private Idioma idioma;

    // SQL Statements
    private String inits;
    private String postInits;
    private int postInitsJP;
    private String solucion;
    private String solucionAlumno;
    private String limpieza;

    // Solution
    private TipoSolucion tipoSol;
    private String url;
    private String usuario;
    private String clave;
    private Boolean ssl;
    private byte[] ficheroSolucion;
    private String extensionSolucion;

    private ArrayListComboBoxModel<JuegoPruebas> juegosPruebas;

    private ArrayListModel<Corrector> correctores;


    // State
    private boolean disponible;
    private boolean bloqueada;

    private String estado;

/*******************************************************************************
 * Constructors
 */

	public Cuestion()
	{
		inicializa();
	}

    public Cuestion(String autor) {
        inicializa();
        this.autor = autor;
    }

    public Cuestion(int idCuestion) {
        inicializa(); //al loro, no vull tota la merda que em treu dels diccionaris...
        this.idCuestion = idCuestion;
    }

    public Cuestion(Cuestion c, int id) {
        this(c);
        this.idCuestion = id;
    }

    public Cuestion(Cuestion c) {
        inicializa();

        this.titulo = c.titulo;
        this.dificultad = c.dificultad;
        this.disponible = c.disponible;
        this.bloqueada = c.bloqueada;
        this.enunciado = c.enunciado;
        this.enunciadoEsp = c.enunciadoEsp;
        this.enunciadoIng = c.enunciadoIng;
        this.autor = c.autor;
        this.inits = c.inits;
        this.solucion = c.solucion;
        this.solucionAlumno = c.solucionAlumno;
        this.limpieza = c.limpieza;
        this.postInits = c.postInits;
        this.postInitsJP = c.postInitsJP;

        this.binaria = c.binaria;
        this.gabia = c.gabia;

        if (c.ficheroAdjunto != null) {
            this.ficheroAdjunto = new byte[c.ficheroAdjunto.length];
            for (int i = 0; i < c.ficheroAdjunto.length; i++) this.ficheroAdjunto[i] = c.ficheroAdjunto[i];
            this.extensionAdjunto = c.extensionAdjunto;
        }

        if (c.ficheroSolucion != null) {
            this.ficheroSolucion = new byte[c.ficheroSolucion.length];
            for (int i = 0; i < c.ficheroSolucion.length; i++) this.ficheroSolucion[i] = c.ficheroSolucion[i];
        	this.extensionSolucion = c.extensionSolucion;
        }

        this.url = c.url;
        this.usuario = c.usuario;
        this.clave = c.clave;
        this.ssl = c.ssl;

        this.estado = c.estado;

        this.categoria = c.categoria;
        this.tipoSol = c.tipoSol;
        this.idioma = c.idioma;
        this.esquema = c.esquema;

        this.tematicas.addAll(c.tematicas);
        this.correctores.addAll(c.correctores);
        JuegoPruebas jpNew;
        for (JuegoPruebas jp : c.juegosPruebas) {
            jpNew = new JuegoPruebas(jp);
            this.addJuegoPruebas(jpNew);
        }
    }

/*******************************************************************************
 * Getters
 */

    public int getId() {
        return idCuestion;
    }

    public float getDificultad() {
        return dificultad;
    }

    public boolean getBloqueada() {
        return bloqueada;
    }

    public boolean getDisponible() {
        return disponible;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public String getEnunciadoEsp() {
        return enunciadoEsp;
    }

    public String getEnunciadoIng() {
        return enunciadoIng;
    }

    public Boolean getBinaria() {
        return binaria;
    }

    public Boolean getGabia() {
        return gabia;
    }

    public String getAutor() {
        return autor;
    }

    public String getInits() {
        return inits;
    }

    public String getSolucion() {
        return solucion;
    }

    public String getSolucionAlumno() {
        return solucionAlumno;
    }

    public String getLimpieza() {
        return limpieza;
    }

    public String getPostInits() {
        return postInits;
    }

    public int getPostInitsJP() {
        return postInitsJP;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public TipoSolucion getTipoSolucion() {
        return tipoSol;
    }

    public Idioma getIdioma() {
        return idioma;
    }

    public String getUrl() {
        return url;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }

    public Boolean getSsl() {
        return ssl;
    }

    public String getEstado() {
        return estado;
    }

    public Esquema getEsquema() {
        return esquema;
    }

    public ArrayListModel<Tematica> getTematicas() {
        return tematicas;
    }

    public ArrayListModel<Corrector> getCorrectores() {
        return correctores;
    }

    public ArrayListComboBoxModel<JuegoPruebas> getJuegosPruebas() {
        // ¿devolver copia?
        return juegosPruebas;
    }

    // public byte[] getBytesFichero() {
    public byte[] getBytesFichero() {
        return ficheroAdjunto;
    }

    // public byte[] getBytesSolucion() {
    public byte[] getBytesSolucion() {
        return ficheroSolucion;
    }

    public String getExtensionFichero() {
        return extensionAdjunto;
    }

    public String getExtensionSolucion() {
        return extensionSolucion;
    }

/*******************************************************************************
 * Setters
 */

    public void setId(int id) {
        this.idCuestion = id;
    }

    public void setDificultad(float dificultad) {
        this.dificultad = dificultad;
    }

    public void setBloqueada(boolean bloqueada) {
        this.bloqueada = bloqueada;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public void setEnunciadoEsp(String enunciado) {
        this.enunciadoEsp = enunciado;
    }

    public void setEnunciadoIng(String enunciado) {
        this.enunciadoIng = enunciado;
    }

    public void setBinaria(Boolean binaria) {
        this.binaria = binaria;
    }

    public void setGabia(Boolean gabia) {
        this.gabia = gabia;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setInits(String inits) {
        this.inits = inits;
    }

    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }

    public void setSolucionAlumno(String solucion) {
        this.solucionAlumno = solucion;
    }

    public void setLimpieza(String limpieza) {
        this.limpieza = limpieza;
    }

    public void setPostInits(String postInits) {
        this.postInits = postInits;
    }

    public void setPostInitsJP(int postInitsJP) {
        this.postInitsJP = postInitsJP;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public void setTipoSolucion(TipoSolucion tipoSol) {
        this.tipoSol = tipoSol;
    }

    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setSsl(Boolean ssl) {
        this.ssl = ssl;
    }

    public void setEstado(String est) {
        this.estado = est;
    }

    public void setEsquema(Esquema esquema) {
        this.esquema = esquema;
    }

    public void setTematicas(List<Tematica> lista) {
        if (!tematicas.isEmpty()) tematicas.clear();
        tematicas.addAll(lista);
    }

    public void setCorrectores(List<Corrector> lista) throws Exception {
        if (!correctores.isEmpty()) correctores.clear();
        correctores.addAll(lista);
    }

    public void setFicheroAdjunto(byte[] fichero, String extension) {
        if (fichero == null) {
            this.ficheroAdjunto = null;
            this.extensionAdjunto = "";
        } else {
            this.ficheroAdjunto = fichero;
            this.extensionAdjunto = extension;
        }
    }

    public void setFicheroSolucion(byte[] fichero, String extension) {
        this.ficheroSolucion = fichero;
        this.extensionSolucion = extension;
    }

    // Métodos para gestionar cuestiones **************************************

    public void addTematica(Tematica t) {
        tematicas.add(t);
    }

    public void addCorrector(Corrector c) {
        correctores.add(c);
    }

    public void addJuegoPruebas(JuegoPruebas jp) {
        juegosPruebas.add(jp);
        jp.setCuestion(this);
    }

    public void removeTematicas() {
        tematicas.removeAll(tematicas);
    }

    public void removeCorrector(Corrector c) {
        correctores.remove(c);
    }

    public void removeJuegoPruebas(JuegoPruebas jp) {
        juegosPruebas.remove(jp);
    }

    public void clearSalidas() {
        disponible = false;
        for (JuegoPruebas jp : juegosPruebas) jp.clearSalidas();
    }

    public void swapJuegosPruebas(JuegoPruebas jp1, JuegoPruebas jp2) {
        int pos1 = juegosPruebas.getIndex(jp1);
        int pos2 = juegosPruebas.getIndex(jp2);
        juegosPruebas.set(pos1, jp2);
        juegosPruebas.set(pos2, jp1);
    }

    public boolean existeFicheroAdjunto() {
        return (ficheroAdjunto != null);
    }

    public boolean existeFicheroSolucion() {
        return (ficheroSolucion != null);
    }

    public int getNumPesoCero() {
        int num = 0;
        for (JuegoPruebas jp : juegosPruebas) {
            if (jp.getPeso() == .0f) num++;
        }
        return num;
    }

    public int getMaxOrden() {
        return juegosPruebas.getSize();
    }

/*******************************************************************************
 * TreeNode implementation
 */

    public TreeNode getChildAt(int childIndex) {
        return juegosPruebas.get(childIndex);
    }

    public int getChildCount() {
        return juegosPruebas.size();
    }

    public TreeNode getParent() {
        return null;
    }

    public int getIndex(TreeNode node) {
        for (int i=0; i<juegosPruebas.size(); i++) {
            if (juegosPruebas.get(i) == node) return i;
        }
        return -1;
    }

    public boolean getAllowsChildren() {
        return true;
    }

    public boolean isLeaf() {
        return juegosPruebas.isEmpty();
    }

    public Enumeration children() {
        return new Vector(juegosPruebas).elements();
    }

/*******************************************************************************
 * Others
 */

    private void inicializa() {
        tematicas = new ArrayListModel<Tematica>();
        correctores = new ArrayListModel<Corrector>();
        juegosPruebas = new ArrayListComboBoxModel<JuegoPruebas>();

        idCuestion = 0;
        dificultad = .5f;
        disponible = false;
        bloqueada = false;

        binaria = false;
        gabia = false;

        //tipoSol = CtrlModelo.tiposSolucion.getAll().get(0);

        //idioma = CtrlModelo.idiomas.getAll().get(0);
//        if (CtrlModelo.esquemas.isEmpty()) esquema = null;
//        else esquema = CtrlModelo.esquemas.getAll().get(0);

		tipoSol=new TipoSolucion();
		esquema=new Esquema();
		categoria=new Categoria();

//        if (CtrlModelo.categorias.isEmpty()) categoria = null;
//        else categoria = CtrlModelo.categorias.getAll().get(0);

        ficheroAdjunto = null;
        ficheroSolucion = null;

        titulo = "";
        enunciado = "";
        enunciadoEsp = "";
        enunciadoIng = "";
        autor = "";

        inits = "";
        solucion = "";
        solucionAlumno = "";
        limpieza = "";
        postInits = "";
        postInitsJP = 0;
        extensionAdjunto = "";
        extensionSolucion = "";

        url = "";
        usuario = "";
        clave = "";
        ssl = false;

        estado = "";
    }

    @Override
    public String toString() {
        if (idCuestion == 0) return "Nueva cuestión";
        else return titulo;
    }

    public int compareTo(Cuestion c) {
        return titulo.compareTo(c.getTitulo());
    }

    public int getOrdenJP(JuegoPruebas jp) {
        if (!juegosPruebas.contains(jp)) return -1;
        else return juegosPruebas.getIndex(jp) + 1;
    }

	public byte[] getFicheroAdjunto() {
		return ficheroAdjunto;
	}

	public void setFicheroAdjunto(byte[] ficheroAdjunto) {
		this.ficheroAdjunto = ficheroAdjunto;
	}

	public String getExtensionAdjunto() {
		return extensionAdjunto;
	}

	public void setExtensionAdjunto(String extensionAdjunto) {
		this.extensionAdjunto = extensionAdjunto;
	}

	public byte[] getFicheroSolucion() {
		return ficheroSolucion;
	}

	public void setFicheroSolucion(byte[] ficheroSolucion) {
		this.ficheroSolucion = ficheroSolucion;
	}

	public void setExtensionSolucion(String extensionSolucion) {
		this.extensionSolucion = extensionSolucion;
	}
}