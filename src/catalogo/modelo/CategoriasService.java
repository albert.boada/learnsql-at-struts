//////////////////////////////////////////////////////
//  CLASSE: DiccCategorias.java                     //
//  AUTORS: versio 1: Adrián Toporcer Korec         //
//          versio 2: Marc Fernandez Pujol          //
//////////////////////////////////////////////////////

package catalogo.modelo;

import catalogo.actions.RestfulValidationException;
import catalogo.dtos.CategoriaDTO;
import catalogo.gestiondatos.CtrlCategorias;
import catalogo.soporte.modelos.ArrayListComboBoxModel;

import java.util.HashMap;
import java.util.List;

public class CategoriasService extends BaseService
{
    private CtrlCategorias ctrlCategorias;

    public CategoriasService(CtrlCategorias dao) throws Exception
    {
        this.ctrlCategorias = dao;
    }

    /**
     *
     */
    public List<Categoria> getAll() throws Exception
    {
        return this.ctrlCategorias.select();
    }

    public Categoria find(int id) throws Exception
    {
        return this.ctrlCategorias.get(id);
    }

    /**
     *
     */
    public Categoria create(Categoria cat) throws Exception {
        this.validate(cat, -1, CategoriasService.CREATE);
        if (!this.validation.isEmpty()) {
        	throw new RestfulValidationException(this.validation); 
        }

        this.ctrlCategorias.insert(
            cat.getId(),
            cat.getDescripcio(),
            cat.getInits(),
            cat.getPostinits(),
            cat.getLimpieza(),
            cat.getEstado(),
            cat.getSolucion(),
            cat.getInitsJP(),
            cat.getEntradaJP(),
            cat.getLimpiezaJP(),
            cat.getComprobaciones()
        );

        return this.find(cat.getId());
    }

    /**
     *
     */
    public Categoria update(Categoria cat, int id) throws Exception {
        this.validate(cat, id, CategoriasService.UPDATE);
        if (!this.validation.isEmpty()) {
        	throw new RestfulValidationException(this.validation); 
        }
        
        ctrlCategorias.update(
            id,
            cat.getId(),
            cat.getDescripcio(),
            cat.getInits(),
            cat.getPostinits(),
            cat.getLimpieza(),
            cat.getEstado(),
            cat.getSolucion(),
            cat.getInitsJP(),
            cat.getEntradaJP(),
            cat.getLimpiezaJP(),
            cat.getComprobaciones()
        );
        
        return this.find(cat.getId());
    }

    /**
     *
     */
    public void validate(Categoria cat, int id, String mode) throws Exception
    {
        this.resetValidations();

        if (cat == null) {
            throw new Exception("category is null on validate");
        }

        if (cat.getId() < 1) {
            this.setValidation("NOTVALIDMUSTINTEGER", "", "ID", null);
        }
        else {
        	Categoria existing_cat = this.ctrlCategorias.get(cat.getId());
        	if (existing_cat != null && (mode == CategoriasService.CREATE || (mode == CategoriasService.UPDATE && existing_cat.getId() != id))) {
        		this.setValidation("EXISTING", "", "ID", null);        		
        	}
        	else if (cat.getDescripcio() == null || cat.getDescripcio().trim().isEmpty()) {
        		this.setValidation("EMPTY", "", "DESCRIPTION", null);
        	}
        }
    }

    public void remove(Categoria cat) throws Exception
    {
        ctrlCategorias.delete(cat.getId());
    }

    public CategoriaDTO entityToForm(Categoria cat) {
        CategoriaDTO form = new CategoriaDTO();
        form.setId(cat.getId());
        form.setId_editable(Integer.toString(cat.getId()));
        form.setDescripcio(cat.getDescripcio());
        form.setInits(cat.getInits());
        form.setPostinits(cat.getPostinits());
        form.setLimpieza(cat.getLimpieza());
        form.setEstado(cat.getEstado());
        form.setSolucion(cat.getSolucion());
        form.setInitsJP(cat.getInitsJP());
        form.setEntradaJP(cat.getEntradaJP());
        form.setLimpiezaJP(cat.getLimpiezaJP());
        form.setComprobaciones(cat.getComprobaciones());
        return form;
    }

    public Categoria formToEntity(CategoriaDTO form) throws Exception {
        Categoria cat = new Categoria();
        if (!form.getId_editable().isEmpty()) {
        	try {
        		cat.setId(Integer.parseInt(form.getId_editable()));        	
        	} catch (Exception e) {}
        }
        cat.setDescripcio(form.getDescripcio());
        cat.setInits(form.isInits());
        cat.setPostinits(form.isPostinits());
        cat.setLimpieza(form.isLimpieza());
        cat.setEstado(form.isEstado());
        cat.setSolucion(form.isSolucion());
        cat.setInitsJP(form.isInitsJP());
        cat.setEntradaJP(form.isEntradaJP());
        cat.setLimpiezaJP(form.isLimpiezaJP());
        cat.setComprobaciones(form.isComprobaciones());
        return cat;
    }
}
